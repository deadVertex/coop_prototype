#version 330

in vec3 normal;

uniform vec4 colour;

out vec4[3] output;

void main()
{
  output[0] = colour;
  output[1] = vec4( normal, 1 );
}
