#version 330

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

uniform mat4 model;
uniform mat4 viewProjection;

out vec3 normal;

void main()
{
  normal = vec3( transpose( inverse( model ) ) * vec4( vertexNormal, 0.0 ) );
  gl_Position = viewProjection * model * vec4( vertexPosition, 1.0 );
}
