#version 330

in vec2 textureCoordinates;

uniform sampler2D albedoTexture;
uniform sampler2D normalTexture;
uniform sampler2D depthTexture;

uniform mat4 invViewProjection;

out vec4 output;

vec3 CalculateWorldPosition( vec2 position, float depth )
{
  vec3 worldPosition = vec3( position, depth ) * 2.0 - 1.0;
  vec4 temp = invViewProjection * vec4( worldPosition, 1.0 );
  worldPosition = temp.xyz / temp.w;
  return worldPosition;
}

#define M_PI 3.1415926535897932384626433832795

vec3 LambertianBrdf( vec3 colourDiffuse )
{
  return colourDiffuse / M_PI;
}

void main()
{
  vec3 albedo = texture( albedoTexture, textureCoordinates ).rgb;
  vec3 normal = texture( normalTexture, textureCoordinates ).xyz;
  float depth = texture( depthTexture, textureCoordinates ).r;

  vec3 lightDirection = normalize( vec3(0.5, -1, 0.25) );
  vec3 lightColour = vec3(0.8, 0.75, 0.7 ) * 5.0;
  float nDotL = max( 0.0, dot( normal, -lightDirection ) );
  output = vec4( LambertianBrdf( albedo ) * lightColour * nDotL, 1.0 );

  vec3 skyDirection = vec3( 0, 1, 0 );
  vec3 skyColour = vec3( 0.2, 0.25, 0.4 ) * 0.5;
  float skyNDotL = dot( normal, skyDirection ) * 0.5 + 0.5;
  output += vec4( LambertianBrdf( albedo ) * skyColour * skyNDotL, 1.0 );
  output.a = texture(albedoTexture, textureCoordinates ).a;
}
