#version 330

uniform sampler2D hdrTexture;

uniform vec2 screenSize;

in vec2 textureCoordinates;

out vec4 output;

float FilmicToneMap( float x )
{
  x = max( 0.0, x - 0.004 );
  return ( x * ( 6.2 * x + 0.5 ) ) / ( x * ( 6.2 * x + 1.7 ) + 0.06 );
}

float ReinhardToneMap( float x )
{
  return x / ( 1 + x );
}

vec3 ToneMap( vec3 colour )
{
  return vec3( FilmicToneMap( colour.r ), FilmicToneMap( colour.g ),
               FilmicToneMap( colour.b ) );
}

void main()
{
  vec3 hdrColour = texture( hdrTexture, textureCoordinates ).rgb;
  vec3 ldrColour = ToneMap( hdrColour );
  output = vec4( ldrColour, 1.0 );
}
