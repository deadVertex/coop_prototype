#pragma once

#include <cstdint>

#include "common/utils.h"
#include "common/event.h"

#define MAX_ENTITY_GARBAGE_QUEUE_LENGTH 4096

typedef uint32_t EntityId;

#define DeclareComponent() EntityId owner

// NOTE: MAX_ENTITIES must be less than ENTITY_ID_MASK - 1
#define MAX_ENTITIES 1000000
#define ENTITY_ID_MASK 0x00FFFFFF
#define ENTITY_GENERATION_MASK 0xFF000000
#define MAX_ENTITY_GENERATION 0xFF
#define MAX_ENTITY_ID ENTITY_ID_MASK

#define MAX_COMPONENT_TYPES 128
#define NULL_ENTITY 0

struct EntityComponentMapping
{
  uint32_t typeId;
  void *component;
  EntityComponentMapping *next;
};

struct EntityComponentSystem
{
  uint32_t numEntities;
  uint32_t *entityIds;
  EntityComponentMapping **mappings;
  uint32_t capacity, size, mappingsPoolSize;
  EntityComponentMapping *pool;
  EntityComponentMapping *freeListHead;

  ContiguousObjectPool componentPools[MAX_COMPONENT_TYPES];

  EntityId entityGarbageQueue[MAX_ENTITY_GARBAGE_QUEUE_LENGTH];
  uint32_t entityGarbageQueueLength;

  EntityId entityIdFreeList[MAX_ENTITIES];
  uint32_t entityIdFreeListHead, entityIdFreeListTail;
};
