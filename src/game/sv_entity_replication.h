#pragma once

#include "common/math.h"

enum
{
  net_CreateEntityCommandTypeId = 1,
  net_DestroyEntityCommandTypeId = 2,
  net_UpdateEntityCommandTypeId = 3,
};

enum
{
  net_EntityFlagsNone = 0,
  net_EntityFlagsDontUpdate = BIT( 0 ),
};

struct entrep_ServerEntity
{
  net_EntityId netEntityId;
  uint32_t netEntityTypeId;
  float basePriority;
  uint8_t flags;
};

enum
{
  CLIENT_ENTITY_STATE_TO_CREATE = 0,
  CLIENT_ENTITY_STATE_CREATED = 1,
  CLIENT_ENTITY_STATE_TO_DESTROY = 2,
  CLIENT_ENTITY_STATE_DESTROYED = 3,
};
struct entrep_ClientEntity
{
  net_EntityId netEntityId;
  uint32_t netEntityTypeId;
  uint8_t state;
  uint8_t flags;
  float priority; // Higher is more important
  float positionPriority;
  float basePriority;
  float updatePriority;
};

struct entrep_Client
{
  uint32_t id;
  uint32_t numEntities;
  ContiguousObjectPool entityPool;
  vec2 origin;
};

struct entrep_Server
{
  ContiguousObjectPool entityPool;
  ContiguousObjectPool clientPool;
  LinearQueue destroyQueue;
  LinearQueue createQueue;
  CircularBuffer freeList;
  MemoryPool clientEntitiesMemoryPool;
};

struct entrep_QueueEntry
{
  float priority;
  net_EntityId netEntityId;
  uint8_t state;
};
