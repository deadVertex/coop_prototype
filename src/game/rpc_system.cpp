/*
TODO LIST
  * Don't assert that queue is not full, check that that it has enough space before adding it
*/
struct RpcSystem
{
  ByteQueue outgoingParameters;
  ByteQueue incomingParameters;
  ByteQueue outgoingResults;
  ByteQueue incomingResults;

  uint8_t outgoingParametersArray[ 512 ];
  uint8_t outgoingResultsArray[ 128 ];
  uint8_t incomingParametersArray[ 512 ];
  uint8_t incomingResultsArray[ 128 ];
};

internal void net_InitializeRpcSystem( RpcSystem *rpcSystem )
{
  ByteQueueInitializeArray( &rpcSystem->outgoingParameters, rpcSystem->outgoingParametersArray );
  ByteQueueInitializeArray( &rpcSystem->incomingParameters, rpcSystem->incomingParametersArray );
  ByteQueueInitializeArray( &rpcSystem->outgoingResults, rpcSystem->outgoingResultsArray );
  ByteQueueInitializeArray( &rpcSystem->incomingResults, rpcSystem->incomingResultsArray );
}

#define net_SendRpc( SYSTEM, DATA, TYPEID ) net_SendRpc_( SYSTEM, DATA, sizeof( TYPEID##Parameters ), TYPEID )
#define net_ReceiveRpc( SYSTEM, DATA, TYPEID ) net_ReceiveRpc_( SYSTEM, DATA, sizeof( TYPEID##Parameters ), TYPEID )

#define net_SendRpcResult( SYSTEM, DATA, TYPEID ) net_SendRpcResult_( SYSTEM, DATA, sizeof( TYPEID##Result ), TYPEID )
#define net_ReceiveRpcResult( SYSTEM, DATA, TYPEID ) net_ReceiveRpcResult_( SYSTEM, DATA, sizeof( TYPEID##Result ), TYPEID )

struct RpcQueueHeader
{
  uint32_t typeId;
  uint32_t size;
};

internal void net_SendRpc_( RpcSystem *rpcSystem, void *data, uint32_t dataSize, uint32_t typeId )
{
  RpcQueueHeader header;
  header.typeId = typeId;
  header.size = dataSize;
  if ( !ByteQueueWrite( &rpcSystem->outgoingParameters, &header, sizeof( header ) ) )
  {
    ASSERT( !"RPC outgoing parameter queue is full" );
  }

  if ( !ByteQueueWrite( &rpcSystem->outgoingParameters, data, dataSize ) )
  {
    ASSERT( !"RPC outgoing parameter queue is full" );
  }
}

internal void net_ReceiveRpc_( RpcSystem *rpcSystem, void *data, uint32_t size, uint32_t typeId )
{
  RpcQueueHeader header;
  header.typeId = typeId;
  header.size = size;
  if ( !ByteQueueWrite( &rpcSystem->incomingParameters, &header, sizeof( header ) ) )
  {
    ASSERT( !"RPC incoming parameter queue is full" );
  }

  if ( !ByteQueueWrite( &rpcSystem->incomingParameters, data, size ) )
  {
    ASSERT( !"RPC incoming parameter queue is full" );
  }
}

internal void net_SendRpcResult_( RpcSystem *rpcSystem, void *data, uint32_t size, uint32_t typeId )
{
  RpcQueueHeader header;
  header.typeId = typeId;
  header.size = size;
  if ( !ByteQueueWrite( &rpcSystem->outgoingResults, &header, sizeof( header ) ) )
  {
    ASSERT( !"RPC outgoing results queue is full" );
  }

  if ( !ByteQueueWrite( &rpcSystem->outgoingResults, data, size ) )
  {
    ASSERT( !"RPC outgoing results queue is full" );
  }
}

internal void net_ReceiveRpcResult_( RpcSystem *rpcSystem, void *data, uint32_t size, uint32_t typeId )
{
  RpcQueueHeader header;
  header.typeId = typeId;
  header.size = size;
  if ( !ByteQueueWrite( &rpcSystem->incomingResults, &header, sizeof( header ) ) )
  {
    ASSERT( !"RPC incoming results queue is full" );
  }

  if ( !ByteQueueWrite( &rpcSystem->incomingResults, data, size ) )
  {
    ASSERT( !"RPC incoming results queue is full" );
  }
}

internal void com_HandleRpcParameters( RpcSystem *rpcSystem, Bitstream *bitstream )
{
  uint32_t count = 0;
  if ( bitstream_ReadBits( bitstream, &count, NET_GAME_RPC_COUNT_NUM_BITS ) )
  {
    for ( uint32_t i = 0; i < count; ++i )
    {
      uint32_t typeId = 0;
      if ( bitstream_ReadBits( bitstream, &typeId, NET_GAME_RPC_TYPE_NUM_BITS ) )
      {
        bool breakOutOfLoop = false;
        net_GameRpcChangeHeroParameters changeHeroParameters;
        switch ( typeId )
        {
          case net_GameRpcChangeHero:
            if ( net_GameRpcChangeHeroParametersDeserialize( bitstream, &changeHeroParameters ) )
            {
              net_ReceiveRpc( rpcSystem, &changeHeroParameters, net_GameRpcChangeHero );
            }
            break;
          default:
            LOG_WARNING( "Unknown RPC type id %d", typeId ); // TODO: This will leave the packet in an unparsable state
            breakOutOfLoop = true;
            break;
        }

        if ( breakOutOfLoop )
        {
          break;
        }
      }
    }
  }
  else
  {
    LOG_WARNING( "INVALID PACKET DATA" );
  }
}

internal void com_HandleRpcResults( RpcSystem *rpcSystem, Bitstream *bitstream )
{
  uint32_t count = 0;
  if ( bitstream_ReadBits( bitstream, &count, NET_GAME_RPC_COUNT_NUM_BITS ) )
  {
    for ( uint32_t i = 0; i < count; ++i )
    {
      uint32_t typeId = 0;
      if ( bitstream_ReadBits( bitstream, &typeId, NET_GAME_RPC_TYPE_NUM_BITS ) )
      {
        bool breakOutOfLoop = false;
        net_GameRpcChangeHeroResult changeHeroResult;
        switch ( typeId )
        {
          case net_GameRpcChangeHero:
            if ( net_GameRpcChangeHeroResultDeserialize( bitstream, &changeHeroResult ) )
            {
              net_ReceiveRpcResult( rpcSystem, &changeHeroResult, net_GameRpcChangeHero );
            }
            break;
          default:
            LOG_WARNING( "Unknown RPC type id %d", typeId ); // TODO: This will leave the packet in an unparsable state
            breakOutOfLoop = true;
            break;
        }

        if ( breakOutOfLoop )
        {
          break;
        }
      }
    }
  }
  else
  {
    LOG_WARNING( "INVALID PACKET DATA" );
  }
}

bool com_WriteRpcParametersPacketChunk( Bitstream *packetBitstream, RpcSystem *rpcSystem )
{
  // Early exit
  if ( rpcSystem->outgoingParameters.length == 0 )
  {
    return false;
  }

  bool success = false;
  Bitstream bitstream;
  uint8_t buffer[256];
  uint32_t maxChunkSize = MinVal( bitstream_GetNumBytesRemaining( packetBitstream ), sizeof( buffer ) );
  bitstream_InitForWriting( &bitstream, buffer, maxChunkSize );
  if ( bitstream_WriteInt( &bitstream, net_RpcParametersChunkId, NET_GAME_PACKET_CHUNK_ID_NUM_BITS ) )
  {
    int chunkLengthLocation = bitstream_ReserveBits( &bitstream, NET_GAME_PACKET_CHUNK_LENGTH_NUM_BITS );
    if ( chunkLengthLocation >= 0 )
    {
      auto start = bitstream.size;
      int rpcCountLocation = bitstream_ReserveBits( &bitstream, NET_GAME_RPC_COUNT_NUM_BITS );
      if ( rpcCountLocation >= 0 )
      {
        uint32_t count = 0;
        bool stop = false;
        while ( !stop )
        {
          stop = true;
          RpcQueueHeader header = {};
          if ( ByteQueueRead( &rpcSystem->outgoingParameters, &header, sizeof( header ) ) )
          {
            ASSERT( header.typeId != net_GameRpcInvalid );
            uint8_t entryBuffer[ NET_GAME_RPC_DATA_LENGTH + sizeof( header.typeId ) ];
            Bitstream entryBitstream;
            bitstream_InitForWriting( &entryBitstream, entryBuffer, sizeof( entryBuffer ), bitstream_FlagAssertOnFail );
            bitstream_WriteBits( &entryBitstream, &header.typeId, NET_GAME_RPC_TYPE_NUM_BITS );
            uint8_t buffer[ NET_GAME_RPC_DATA_LENGTH ];
            if ( ByteQueueRead( &rpcSystem->outgoingParameters, buffer, header.size ) )
            {
              switch ( header.typeId )
              {
                case net_GameRpcInvalid:
                  ASSERT( !"Invalid game RPC typeId" );
                  break;
                case net_GameRpcChangeHero:
                  net_GameRpcChangeHeroParametersSerialize( &entryBitstream, ( net_GameRpcChangeHeroParameters * )buffer );
                  break;
                default:
                  ASSERT( !"Unknown game RPC" );
                  break;
              }

              if ( bitstream_WriteBits( &bitstream, entryBitstream.data, entryBitstream.size ) )
              {
                count++;
                stop = false;
                success = true;
              }
            }
          }
        }
        if ( !bitstream_WriteBitsAtLocation( &bitstream, &count, NET_GAME_RPC_COUNT_NUM_BITS, rpcCountLocation ) )
        {
          ASSERT( !"Write should not fail since bits have already been successfully reserved" );
        }
      }
      uint32_t length = bitstream.size - start;
      if ( !bitstream_WriteBitsAtLocation( &bitstream, &length, NET_GAME_PACKET_CHUNK_LENGTH_NUM_BITS, chunkLengthLocation ) )
      {
        ASSERT( !"Write should not fail since bits have already been successfully reserved" );
      }
    }
  }

  // Only write the chunk if we have successfully writen at least one RPC parameter entry
  if ( success )
  {
    // Write entire chunk to bitstream
    if ( bitstream_WriteBits( packetBitstream, buffer, bitstream_GetLengthInBits( &bitstream ) ) )
    {
      return true;
    }
  }
  return false;
}

bool com_WriteRpcResultsPacketChunk( Bitstream *packetBitstream, RpcSystem *rpcSystem )
{
  // Early exit
  if ( rpcSystem->outgoingResults.length == 0 )
  {
    return false;
  }

  bool success = false;
  Bitstream bitstream;
  uint8_t buffer[128];
  uint32_t maxChunkSize = MinVal( bitstream_GetNumBytesRemaining( packetBitstream ), sizeof( buffer ) );
  bitstream_InitForWriting( &bitstream, buffer, maxChunkSize );
  if ( bitstream_WriteInt( &bitstream, net_RpcResultsChunkId, NET_GAME_PACKET_CHUNK_ID_NUM_BITS ) )
  {
    int chunkLengthLocation = bitstream_ReserveBits( &bitstream, NET_GAME_PACKET_CHUNK_LENGTH_NUM_BITS );
    if ( chunkLengthLocation >= 0 )
    {
      auto start = bitstream.size;
      int rpcCountLocation = bitstream_ReserveBits( &bitstream, NET_GAME_RPC_COUNT_NUM_BITS );
      if ( rpcCountLocation >= 0 )
      {
        uint32_t count = 0;
        bool stop = false;
        while ( !stop )
        {
          stop = true;
          RpcQueueHeader header = {};
          if ( ByteQueueRead( &rpcSystem->outgoingResults, &header, sizeof( header ) ) )
          {
            ASSERT( header.typeId != net_GameRpcInvalid );
            uint8_t entryBuffer[ NET_GAME_RPC_DATA_LENGTH + sizeof( header.typeId ) ];
            Bitstream entryBitstream;
            bitstream_InitForWriting( &entryBitstream, entryBuffer, sizeof( entryBuffer ), bitstream_FlagAssertOnFail );
            bitstream_WriteBits( &entryBitstream, &header.typeId, NET_GAME_RPC_TYPE_NUM_BITS );
            uint8_t buffer[ NET_GAME_RPC_DATA_LENGTH ];
            if ( ByteQueueRead( &rpcSystem->outgoingResults, buffer, header.size ) )
            {
              switch ( header.typeId )
              {
                case net_GameRpcInvalid:
                  ASSERT( !"Invalid game RPC typeId" );
                  break;
                case net_GameRpcChangeHero:
                  net_GameRpcChangeHeroResultSerialize( &entryBitstream, ( net_GameRpcChangeHeroResult * )buffer );
                  break;
                default:
                  ASSERT( !"Unknown game RPC" );
                  break;
              }

              if ( bitstream_WriteBits( &bitstream, entryBitstream.data, entryBitstream.size ) )
              {
                count++;
                stop = false;
                success = true;
              }
            }
          }
        }
        if ( !bitstream_WriteBitsAtLocation( &bitstream, &count, NET_GAME_RPC_COUNT_NUM_BITS, rpcCountLocation ) )
        {
          ASSERT( !"Write should not fail since bits have already been successfully reserved" );
        }
      }
      uint32_t length = bitstream.size - start;
      if ( !bitstream_WriteBitsAtLocation( &bitstream, &length, NET_GAME_PACKET_CHUNK_LENGTH_NUM_BITS, chunkLengthLocation ) )
      {
        ASSERT( !"Write should not fail since bits have already been successfully reserved" );
      }
    }
  }

  // Only write the chunk if we have successfully writen at least one RPC parameter entry
  if ( success )
  {
    // Write entire chunk to bitstream
    if ( bitstream_WriteBits( packetBitstream, buffer, bitstream_GetLengthInBits( &bitstream ) ) )
    {
      return true;
    }
  }
  return false;
}
