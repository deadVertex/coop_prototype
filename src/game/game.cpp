#include "game.h"

#include "common/input.h"
#include "common/cmd.h"
#include "common/easing.h"
#include "common/bitstream.h"
#include "common/debug.h"

#include "game/game_net.h"
#include "game/entity.h"
#include "game/cl_entity_replication.h"

#include "renderer/opengl_renderer.cpp"
#include "common/cmd.cpp"
#include "game/cl_entity_replication.cpp"
#include "game/entity.cpp"

#include "game/game_common.cpp"
#include "game/server.cpp"

#include "platform/physics.h"

#define CONSOLE_HEIGHT 500
#define CONSOLE_OUT_BUF_LEN 4096
#define CONSOLE_IN_BUF_LEN 120
struct Console
{
  char outputBuffer[CONSOLE_OUT_BUF_LEN]; // TODO: Make circular buffer.
  uint32_t outputBufferLength;
  char inputBuffer[CONSOLE_IN_BUF_LEN];
  uint32_t inputBufferLength;
  uint32_t cursor;
  float cursorChangeTime;
};

internal void con_Puts( Console *console, const char *str )
{
  while ( *str && console->outputBufferLength < CONSOLE_OUT_BUF_LEN )
  {
    console->outputBuffer[console->outputBufferLength++] = *str;
    str++;
  }
}

struct Camera
{
  float width;
  float height;
  float fov;
  float nearClip;
  float farClip;
  vec3 orientation;
  vec3 position;
  vec3 offset;
  vec3 velocity;
};

struct Ray
{
  vec3 start;
  vec3 dir;
};

enum
{
  CL_GAME_STATE_UNINTIALIZED = 0,
  CL_GAME_STATE_MAIN_MENU,
  CL_GAME_STATE_CONNECTING,
  CL_GAME_STATE_LOADING,
  CL_GAME_STATE_PICKING_HERO,
  CL_GAME_STATE_CONNECTED,
};

const char *clGameStateNames[] = {
  "CL_GAME_STATE_UNINTIALIZED",
  "CL_GAME_STATE_MAIN_MENU",
  "CL_GAME_STATE_CONNECTING",
  "CL_GAME_STATE_LOADING",
  "CL_GAME_STATE_PICKING_HERO",
  "CL_GAME_STATE_CONNECTED"
};

#define CL_MAX_PREDICTIONS 64
struct net_EntityPlayerPrediction
{
  net_Input input;
  AgentState state;
  uint32_t timestamp;
};

struct net_EntityPlayerPredictionBuffer
{
  net_EntityPlayerPrediction predictions[CL_MAX_PREDICTIONS];
  uint32_t head;
  uint32_t tail;
};

enum
{
  DRAW_PHYSICS_MODE_NONE = 0,
  DRAW_PHYSICS_MODE_CLIENT = 1,
  DRAW_PHYSICS_MODE_SERVER = 2,
};

struct HeroPickingState
{
  int selection;
  int mouseOver;
  bool highlightConfirm;
};

struct cl_GameState
{
  uint32_t state;
  renderer_State renderer;
  MemoryArena transArena;
  MemoryArena permArena;
  vec3 playerPosition;
  vec3 playerVelocity;
  InputSystem inputSystem;
  Font font;
  Font consoleFont;
  Font hudFont;
  uint32_t testSprite;
  Console console;
  EasingFloat consoleEasing;
  bool isConsoleActive;
  debug_State debugState;
  char mapName[MAP_NAME_LENGTH];

  OpenGLStaticMesh testMesh;
  OpenGLStaticMesh testLevel;
  OpenGLStaticMesh cube;

  EntityComponentSystem entitySystem;
  EntityId localPlayerEntity;
  net_EntitySnapshotSystem entitySnapshotSystem;
  net_EntityPlayerPredictionBuffer predictionBuffer;

  uint32_t predictionTick; // TODO: Sync this with server tick when joining.
  uint32_t currentGameTick;
  uint32_t sequenceNumber; // TODO: Need to reset this at some point

  bool mapLoaded;
  uint32_t sphereShape;
  float clientSideCurrentTime;

  GamePhysics *physics;
  uint8_t drawPhysicsMode;

  Camera camera;
  Camera debugCamera;
  bool isDebugCameraActive;

  HeroPickingState heroPickingState;

  RpcSystem rpcSystem;
};

uint32_t log_activeChannels = LOG_CHANNEL_DEBUG | LOG_CHANNEL_WARNING |
                              LOG_CHANNEL_ERROR | LOG_CHANNEL_INFO;

internal void DrawConsole( renderer_State *renderer, Console *console,
                           uint32_t windowWidth, uint32_t windowHeight,
                           float currentTime, Font *font, float consoleY )
{
  mat4 projection = Orthographic( 0, windowWidth, 0, windowHeight );
  float size = 1.0f;
  float y = consoleY - CONSOLE_HEIGHT;
  float height = font->ascent + font->descent + font->lineGap;
  renderer_DrawRect( renderer, Vec2( 0, y ), windowWidth, CONSOLE_HEIGHT,
                     Vec4( 0, 0, 0, 0.5 ), &projection );
  renderer_DrawString( renderer, Vec2( 10, y ), console->outputBuffer, font,
                       Vec4( 1 ), &projection );

  float inputY = y + CONSOLE_HEIGHT - height - 10;
  const char *prompt = "> ";
  renderer_DrawString( renderer, Vec2( 10, inputY ), prompt, font, Vec4( 1 ), &projection );

  int showCursor = 1;
  if ( console->cursorChangeTime > 1.0f )
  {
    showCursor = floor( currentTime );
    showCursor %= 2;
  }

  float promptOffset = CalculateTextLength( font, prompt ) + 10.0f;
  if ( showCursor )
  {
    float offset = 0.0f;
    if ( console->cursor > 0 )
    {
      offset =
        CalculateTextLength( font, console->inputBuffer, console->cursor );
    }

    renderer_DrawRect( renderer, Vec2( promptOffset + offset, inputY ), 2.0f,
                       height, Vec4( 1 ), &projection );
  }
  renderer_DrawString( renderer, Vec2( promptOffset, inputY ), console->inputBuffer, font, Vec4( 1 ), &projection );
}

internal mat4 BuildViewProjectionMatrix( Camera camera )
{
  //mat4 perspective = Perspective( Radians( camera.fov ), camera.width / camera.height, camera.nearClip, camera.farClip);
  mat4 perspective = Orthographic( 0, camera.width, camera.height, 0, -1000.0f, 1000.0f );
  mat4 viewMatrix = Identity();
  viewMatrix *= Translate( Vec3( camera.width, camera.height, 0 ) * 0.5f );
  viewMatrix *= Scale( Vec3( 45.0f, 45.0f, 10.0f ) );
  //viewMatrix *= Translate( -camera.offset ); // TODO: Make this a parameter
  viewMatrix *= RotateX( camera.orientation.x );
  viewMatrix *= RotateY( camera.orientation.y );
  viewMatrix *= Translate( -camera.position );

  mat4 result = perspective * viewMatrix;
  return result;
}

internal Ray GenerateRay( float x, float y, Camera camera )
{
  y = camera.height - y;

  float aspect = camera.width / camera.height;
  auto rad = Radians( camera.fov );
  auto vLen = Tan( rad / 2.0f ) * camera.nearClip;
  auto hLen = vLen * aspect;

  mat4 rot;
  rot = RotateX( camera.orientation.x );
  rot *= RotateY( camera.orientation.y );
  rot = Transpose( rot );
  vec4 offset = rot * Vec4( camera.offset, 0 );

  auto right = Normalize( rot.col[ 0 ] ).xyz;
  auto up = Normalize( rot.col[ 1 ] ).xyz;
  auto forward = -Normalize( rot.col[ 2 ] ).xyz;

  auto v = up * vLen;
  auto h = right * hLen;

  x -= camera.width * 0.5f;
  y -= camera.height * 0.5f;

  x /= camera.width * 0.5f;
  y /= camera.height * 0.5f;

  auto end = camera.position + offset.xyz + forward * camera.nearClip + h * x + v * y;

  Ray result;
  result.start = camera.position + offset.xyz;
  result.dir = Normalize( end - result.start );

  return result;
}

struct HeroPortrait
{
  const char *name;
  vec4 colour;
};

internal HeroPortrait GetHeroPortrait( uint8_t heroId, cl_GameState *gameState )
{
  HeroPortrait result = {};
  switch ( heroId )
  {
    case HeroTypeStrongMan:
      result.name = "STRONG MAN";
      result.colour = Vec4( 0.2, 0.4, 0.7, 1.0 );
      break;
    case HeroTypeBarmaid:
      result.name = "BAR MAID";
      result.colour = Vec4( 0.9, 0.3, 0.1, 1.0 );
      break;
    case HeroTypeCook:
      result.name = "COOK";
      result.colour = Vec4( 0.5, 0.8, 0.1, 1.0 );
      break;
    default:
      ASSERT( !"Invalid hero type" );
  }

  return result;
}

internal void cl_DrawHeroPickingState( cl_GameState *gameState, mat4 *projection, float windowWidth, float windowHeight )
{
  auto state = &gameState->heroPickingState;
  auto renderer = &gameState->renderer;

  vec2 p = Vec2( 600, 400 );
  for ( int i = 0; i < MaxHeroTypes; ++i )
  {
    if ( state->selection == i )
    {
      renderer_DrawRect( renderer, p - Vec2( 10 ), 120, 150, Vec4( 0.8, 0.8, 0.9, 1 ), projection );
    }
    else if ( state->mouseOver == i )
    {
      renderer_DrawRect( renderer, p - Vec2( 10 ), 120, 150, Vec4( 0.3, 0.3, 0.3, 1 ), projection );
    }
    HeroPortrait portrait = GetHeroPortrait( i, gameState );
    renderer_DrawRect( renderer, p, 100, 100, portrait.colour, projection );
    renderer_DrawString( renderer, p + Vec2( 50, 110 ), portrait.name, &gameState->hudFont, Vec4( 1 ), projection, TEXT_ALIGN_CENTER );

    p.x += 200;
  }

  if ( state->selection != -1 )
  {
    if ( state->highlightConfirm )
    {
      renderer_DrawRect( renderer, Vec2( windowWidth * 0.5f, windowHeight - 400 ) - Vec2( 100, 20 ), 200, 40, Vec4( 0.3, 0.3, 0.3, 1 ), projection );
    }
    renderer_DrawString( renderer, Vec2( windowWidth * 0.5f, windowHeight - 400 ), "CONFIRM", &gameState->hudFont, Vec4( 1 ), projection, TEXT_ALIGN_CENTER );
  }
}

internal void cl_UpdateHeroPickingState( cl_GameState *gameState, float windowWidth, float windowHeight )
{
  auto state = &gameState->heroPickingState;
  auto inputSystem = &gameState->inputSystem;

  auto mousePos = Vec2( inputSystem->mouseX, inputSystem->mouseY );
  state->mouseOver = -1;
  for ( int i = 0; i < MaxHeroTypes; ++i )
  {
    rect2 boundary = Rect2( Vec2( 600, 400 ) + Vec2( 200 * i, 0 ) - Vec2( 10 ), Vec2( 120, 150 ) );
    if ( ContainsPoint( boundary, mousePos ) )
    {
      state->mouseOver = i;
      if ( WasKeyPressed( inputSystem, K_MOUSE_BUTTON_LEFT ) )
      {
        state->selection = i;
      }
    }
  }

  if ( state->selection != -1 )
  {
    rect2 boundary = Rect2( Vec2( windowWidth * 0.5f, windowHeight - 400 ) - Vec2( 100, 20 ), Vec2( 200, 40 ) );
    state->highlightConfirm = false;
    if ( ContainsPoint( boundary, mousePos ) )
    {
      state->highlightConfirm = true;
      if ( WasKeyPressed( inputSystem, K_MOUSE_BUTTON_LEFT ) )
      {
        // TODO: Send hero selection packet to server
        net_GameRpcChangeHeroParameters params = {};
        params.hero = state->selection;
        net_SendRpc( &gameState->rpcSystem, &params, net_GameRpcChangeHero );
      }
    }
  }
}

enum
{
  ProgressBarFlag_DrawValue = BIT( 0 ),
  ProgressBarFlag_InterpolateColourWithValue = BIT( 1 ),
  ProgressBarFlag_NoBorder = BIT( 2 ),
  ProgressBarFlag_NoTextShadow = BIT( 3 ),
};
struct ProgressBarParameters
{
  vec4 borderColour;
  vec4 backgroundColour;
  vec4 textColour;
  vec4 textShadowColour;

  union
  {
    vec4 foregroundColour;
    struct
    {
      vec4 foregroundMinColour;
      vec4 foregroundMaxColour;
    };
  };

  vec2 dimensions;
  vec2 position;
  Font *font;
  mat4 *projection;
  int max;
  int value;

  uint8_t flags;
};

// TODO: Border width parameter
internal void DrawProgressBar( renderer_State *renderer, ProgressBarParameters *p )
{
  if ( ( p->flags & ProgressBarFlag_NoBorder ) == 0 )
  {
    renderer_DrawRect( renderer, p->position - Vec2( 2 ), p->dimensions.x + 4, p->dimensions.y + 4, p->borderColour, p->projection );
  }
  renderer_DrawRect( renderer, p->position, p->dimensions.x, p->dimensions.y, p->backgroundColour, p->projection );

  float t = 0.0f;
  if ( p->max > 0 )
  {
    t = (float)p->value / (float)p->max;
  }
  vec4 colour;
  if ( p->flags & ProgressBarFlag_InterpolateColourWithValue )
  {
    colour = Lerp( p->foregroundMinColour, p->foregroundMaxColour, t );
  }
  else
  {
    colour = p->foregroundColour;
  }
  renderer_DrawRect( renderer, p->position, p->dimensions.x * t, p->dimensions.y, colour, p->projection );

  if ( p->flags & ProgressBarFlag_DrawValue )
  {
    char string[ 40 ];
    sprintf( string, "%d/%d", p->value, p->max );
    if ( ( p->flags & ProgressBarFlag_NoTextShadow ) == 0 )
    {
      renderer_DrawString( renderer, p->position + Vec2( p->dimensions.x, p->dimensions.y * 0.5 ) * 0.5 + Vec2( 1 ), string, p->font, p->textShadowColour, p->projection, TEXT_ALIGN_CENTER );
    }
    renderer_DrawString( renderer, p->position + Vec2( p->dimensions.x, p->dimensions.y * 0.5 ) * 0.5, string, p->font, p->textColour, p->projection, TEXT_ALIGN_CENTER );
  }
}

extern "C" GAME_RENDER( GameRender )
{
  ASSERT( memory->persistentStorageSize > sizeof( cl_GameState ) );
  cl_GameState *gameState = (cl_GameState *)memory->persistentStorageBase;
  MemoryArenaInitialize( &gameState->transArena, memory->transientStorageSize,
                         (uint8_t *)memory->transientStorageBase );
  if ( gameState->state == CL_GAME_STATE_UNINTIALIZED )
  {
    return;
  }

  if ( gameState->state == CL_GAME_STATE_MAIN_MENU )
  {
    mat4 projection = Orthographic( 0, windowWidth, 0, windowHeight );
    renderer_StartFrame( &gameState->renderer, 0, 0, 0 );
    renderer_EndFrame( &gameState->renderer );
    renderer_DrawString( &gameState->renderer, Vec2( windowWidth * 0.5f, 100 ),
                         "MAIN MENU", &gameState->font, Vec4( 1 ), &projection,
                         TEXT_ALIGN_CENTER );
  }
  else if ( ( gameState->state == CL_GAME_STATE_CONNECTING ) ||
            ( gameState->state == CL_GAME_STATE_LOADING ) )
  {
    mat4 projection = Orthographic( 0, windowWidth, 0, windowHeight);
    renderer_StartFrame( &gameState->renderer, 0, 0, 0 );
    renderer_EndFrame( &gameState->renderer );
    renderer_DrawString(
      &gameState->renderer, Vec2( windowWidth - 80, windowHeight - 80 ),
      "Loading...", &gameState->font, Vec4( 1 ), &projection, TEXT_ALIGN_RIGHT );
    if ( gameState->state == CL_GAME_STATE_LOADING )
    {
      renderer_DrawString( &gameState->renderer, Vec2( 10, windowHeight - 200 ),
                           gameState->mapName, &gameState->font,
                           Vec4( 0.8, 0.8, 0.8, 1 ), &projection );
    }
  }
  else if ( gameState->state == CL_GAME_STATE_PICKING_HERO )
  {
    mat4 projection = Orthographic( 0, windowWidth, 0, windowHeight );
    renderer_StartFrame( &gameState->renderer, 0, 0, 0 );
    renderer_EndFrame( &gameState->renderer );
    renderer_DrawString(
      &gameState->renderer, Vec2( windowWidth * 0.5f, 80 ),
      "Pick a Hero", &gameState->font, Vec4( 1 ), &projection, TEXT_ALIGN_CENTER );

    cl_DrawHeroPickingState( gameState, &projection, windowWidth, windowHeight );
  }
  else if ( gameState->state == CL_GAME_STATE_CONNECTED )
  {
    vec3 playerPosition = {};
    auto transformComponent =
      GetEntityComponent( &gameState->entitySystem,
                          gameState->localPlayerEntity, TransformComponent );
    if ( transformComponent )
    {
      playerPosition = transformComponent->transform.position;
    }
    renderer_StartSceneFrame( &gameState->renderer, windowWidth, windowHeight );
    mat4 viewProjection;
    if ( gameState->isDebugCameraActive )
    {
      viewProjection = BuildViewProjectionMatrix( gameState->debugCamera );
    }
    else
    {
      viewProjection = BuildViewProjectionMatrix( gameState->camera );
    }
    mat4 model = Scale( Vec3( 1024 ) );

    renderer_DrawRect( &gameState->renderer, &model, &viewProjection,
                       Vec4( 0.1, 0.12, 0.08, 1.0 ) * 2.0f );

    model = Translate( Vec3( -4, 0, 0 ) ) * Scale( Vec3( 4, 2, 2 ) );
    renderer_DrawMesh( &gameState->renderer, &model, &viewProjection, Vec4( 0.2, 0.2, 0.2, 1.0 ), gameState->cube );

    model = Translate( Vec3( 10, 2, 1 ) ) * RotateX( -PI * 0.5f );
    renderer_DrawMesh( &gameState->renderer, &model, &viewProjection, Vec4( 0.2, 0.5, 0.8, 1.0 ), gameState->testMesh );

    //model = Scale( Vec3( 10, 4, 10 ) ) * RotateX( -PI * 0.5f );
    //renderer_DrawMesh( &gameState->renderer, &model, &viewProjection, Vec4( 0.5, 0.5, 0.5, 1.0 ), gameState->testLevel );

    ForeachComponent( agentComponent, &gameState->entitySystem, cl_AgentComponent )
    {
      auto transformComponent = GetEntityComponent(
          &gameState->entitySystem, agentComponent->owner, TransformComponent );
      ASSERT( transformComponent );
      auto p = tr_GetPosition( transformComponent->transform );

      auto heroComponent = GetEntityComponent( &gameState->entitySystem, agentComponent->owner, HeroComponent );
      vec4 colour;
      if ( heroComponent )
      {
        if ( heroComponent->type == HeroTypeStrongMan )
        {
          colour = Vec4( 0.2, 0.4, 0.7, 1.0 );
        }
        else if ( heroComponent->type == HeroTypeBarmaid )
        {
          colour = Vec4( 0.9, 0.3, 0.1, 1.0 );
        }
        else
        {
          colour = Vec4( 0.5, 0.8, 0.1, 1.0 );
        }
      }
      else
      {
        colour = Vec4( 1, 1, 0, 1 );
      }

      if ( agentComponent->owner == gameState->localPlayerEntity )
      {
        mat4 model = Identity();
        model *= Translate( p + Vec3( 0, 1.8 * 0.25, 0 ) );
        model *= Scale( Vec2( 1, 1.8 ) * 0.5 );
        renderer_DrawRect( &gameState->renderer, &model, &viewProjection, colour );

        model = Translate( p + Vec3( 0, 1, 0 ) ) *
                     Rotate( agentComponent->angle ) *
                     Translate( Vec2( 1, 0 ) ) * Scale( Vec2( 0.5 ) );
        //vec2 offset = Vec2( 0.1, -0.1 );
        //vec2 offset = {};
        //offset = Rotate( offset, agentComponent->angle );
        //renderer_DrawRect( &gameState->renderer, p + Vec2( 0, 1 ) + offset, 1,
                           //1, Vec4( 0.8, 0.8, 0.8, 1 ),
                           //agentComponent->angle );
        //renderer_DrawRect( &gameState->renderer, &model, Vec4( 0.8, 0.8, 0.8, 1 ) );
      }
      else
      {
        mat4 model = Translate( p + Vec3( 0, 1.8 * 0.25, 0 ) );
        model *= Scale( Vec2( 1, 1.8 ) * 0.5 );
        renderer_DrawRect( &gameState->renderer, &model, &viewProjection, colour );
      }
    }

    RendererLightingPass( &gameState->renderer, viewProjection, windowWidth, windowHeight );

    auto lineBuffer = &gameState->renderer.physicsLineBuffer;

    uint32_t numVertices = 0;

    // Cast LineVertex to phys_LineVertex as they have the same data layout
    if ( gameState->drawPhysicsMode == DRAW_PHYSICS_MODE_CLIENT )
    {
      numVertices = clientPhysics->getVertexData(
        clientPhysics->physicsSystem, ( phys_LineVertex* )lineBuffer->vertices,
        lineBuffer->mesh.maxVertices );
    }
    else if ( gameState->drawPhysicsMode == DRAW_PHYSICS_MODE_SERVER )
    {
      numVertices = serverPhysics->getVertexData(
        serverPhysics->physicsSystem, ( phys_LineVertex* )lineBuffer->vertices,
        lineBuffer->mesh.maxVertices );
    }

    lineBuffer->mesh.numVertices = numVertices;
    OpenGLUpdateDynamicMesh( lineBuffer->mesh );
    debug_Draw( &gameState->debugState, &gameState->renderer, &viewProjection );

    PerformPostProcessing( &gameState->renderer, Vec2( windowWidth, windowHeight ) );
    renderer_EndFrame( &gameState->renderer );


    mat4 ortho = Orthographic( 0, windowWidth, 0, windowHeight );
    renderer_DrawRect( &gameState->renderer, Vec2( windowWidth, windowHeight ) * 0.5f,
                       2, 2, Vec4( 1, 0, 0, 1 ), &ortho );


    uint32_t allyIndex = 0;
    ForeachComponent( heroComponent, &gameState->entitySystem, HeroComponent )
    {
      HeroPortrait portrait = GetHeroPortrait( heroComponent->type, gameState );

      auto healthComponent = GetEntityComponent( &gameState->entitySystem, heroComponent->owner, HealthComponent );
      if ( healthComponent )
      {
        ProgressBarParameters params;
        params.flags = ProgressBarFlag_DrawValue | ProgressBarFlag_InterpolateColourWithValue | ProgressBarFlag_NoBorder | ProgressBarFlag_NoTextShadow;
        params.borderColour = Vec4( 0, 0, 0, 1 );
        params.backgroundColour = Vec4( 0.1, 0.1, 0.1, 1 );
        params.projection = &ortho;
        params.max = healthComponent->maximum;
        params.value = healthComponent->amount;
        params.foregroundMinColour = Vec4( 0.8, 0, 0, 1 );
        params.foregroundMaxColour = Vec4( 0.2, 0.9, 0, 1 );
        params.font = &gameState->hudFont;
        params.textColour = Vec4( 0, 0, 0, 1 );
        if ( heroComponent->owner == gameState->localPlayerEntity )
        {
          params.dimensions = Vec2( 250, 40 );
          params.position = Vec2( 100, windowHeight - 80 );
          DrawProgressBar( &gameState->renderer, &params );

          renderer_DrawRect( &gameState->renderer, Vec2( 8, windowHeight - 120 ), 84, 84, Vec4( 0, 0, 0, 1 ), &ortho );
          renderer_DrawRect( &gameState->renderer, Vec2( 10, windowHeight - 118 ), 80, 80, portrait.colour, &ortho );
        }
        else
        {
          params.dimensions = Vec2( 250, 30 );
          params.position = Vec2( 100, 50 + 100 * allyIndex);
          DrawProgressBar( &gameState->renderer, &params );

          renderer_DrawRect( &gameState->renderer, Vec2( 8, 8 + 100 * allyIndex ), 84, 84, Vec4( 0, 0, 0, 1 ), &ortho );
          renderer_DrawRect( &gameState->renderer, Vec2( 10, 10 + 100 * allyIndex ), 80, 80, portrait.colour, &ortho );

          allyIndex++;
        }
      }
    }

    ForeachComponent( healthComponent, &gameState->entitySystem, HealthComponent )
    {
      auto transformComponent = GetEntityComponent(
          &gameState->entitySystem, healthComponent->owner, TransformComponent );
      ASSERT( transformComponent );

      auto heroComponent = GetEntityComponent( &gameState->entitySystem, healthComponent->owner, HeroComponent );

      auto p = tr_GetPosition( transformComponent->transform );
      p.y -= 0.5f;

      vec4 v = viewProjection * Vec4( p, 1 );
      v.x /= v.w;
      v.y /= v.w;
      auto screenP = Vec2( v.x, v.y );
      screenP.x = ( screenP.x + 1.0f ) * windowWidth * 0.5f;
      screenP.y = ( screenP.y + 1.0f ) * windowHeight * 0.5f;
      screenP.y = windowHeight - screenP.y;

      ProgressBarParameters params;
      params.flags = ProgressBarFlag_NoBorder | ProgressBarFlag_InterpolateColourWithValue;
      params.dimensions = Vec2( 100, 20 );
      params.position = screenP - Vec2( params.dimensions.x * 0.5, 0 );
      params.backgroundColour = Vec4( 0.1, 0.1, 0.1, 1 );
      params.projection = &ortho;
      params.max = healthComponent->maximum;
      params.value = healthComponent->amount;
      params.foregroundMinColour = Vec4( 0.8, 0, 0, 1 );
      params.foregroundMaxColour = Vec4( 0.2, 0.9, 0, 1 );
      DrawProgressBar( &gameState->renderer, &params );
    }
  }

  if ( gameState->debugState.textLength > 0 )
  {
    mat4 ortho = Orthographic( 0, windowWidth, 0, windowHeight );
    renderer_DrawString( &gameState->renderer, Vec2( windowWidth - 10, 12 ),
      gameState->debugState.text, &gameState->consoleFont,
      Vec4( 0.1, 0.1, 0.1, 1 ), &ortho, TEXT_ALIGN_RIGHT );
    renderer_DrawString( &gameState->renderer, Vec2( windowWidth - 12, 10 ),
      gameState->debugState.text, &gameState->consoleFont,
      Vec4( 1 ), &ortho, TEXT_ALIGN_RIGHT );
  }

  DrawConsole( &gameState->renderer, &gameState->console, windowWidth,
               windowHeight, gameState->clientSideCurrentTime,
               &gameState->consoleFont, gameState->consoleEasing.value );

  UNUSED( dt );
}

internal void SetupKeyBindings( InputSystem *inputSystem )
{
  // Binding keys
  inputSystem->keyBindings[ K_W ] = INPUT_UP | INPUT_DEBUG_CAMERA_FORWARDS;
  inputSystem->keyBindings[ K_S ] = INPUT_DOWN | INPUT_DEBUG_CAMERA_BACKWARDS;
  inputSystem->keyBindings[ K_A ] = INPUT_LEFT | INPUT_DEBUG_CAMERA_LEFT;
  inputSystem->keyBindings[ K_D ] = INPUT_RIGHT | INPUT_DEBUG_CAMERA_RIGHT;
  inputSystem->keyBindings[K_E] = INPUT_INTERACT;
  inputSystem->keyBindings[K_UP] = INPUT_UP;
  inputSystem->keyBindings[K_DOWN] = INPUT_DOWN;
  inputSystem->keyBindings[K_LEFT] = INPUT_LEFT;
  inputSystem->keyBindings[K_RIGHT] = INPUT_RIGHT;
  inputSystem->keyBindings[K_MOUSE_BUTTON_LEFT] = INPUT_PRIMARY_ATTACK;

  inputSystem->keyBindings[K_GRAVE_ACCENT] = INPUT_TOGGLE_CONSOLE;
}

internal char ConvertKeyToCharacter( uint8_t key, bool isShiftActive )
{
  if ( key == K_SPACE )
  {
    return ' ';
  }

  if ( key >= K_A && key <= K_Z )
  {
    if ( isShiftActive )
    {
      return (char)key;
    }
    else
    {
      return (char)key + ( 'a' - 'A' );
    }
  }
  if ( isShiftActive )
  {
    switch ( key )
    {
    case K_APOSTROPHE:
      return '\"';
    case K_COMMA:
      return '<';
    case K_MINUS:
      return '_';
    case K_PERIOD:
      return '>';
    case K_SLASH:
      return '?';
    case K_0:
      return ')';
    case K_1:
      return '!';
    case K_2:
      return '@';
    case K_3:
      return '#';
    case K_4:
      return '$';
    case K_5:
      return '%';
    case K_6:
      return '^';
    case K_7:
      return '&';
    case K_8:
      return '*';
    case K_9:
      return '(';
    case K_SEMI_COLON:
      return ':';
    case K_EQUAL:
      return '+';
    case K_LEFT_BRACKET:
      return '{';
    case K_BACKSLASH:
      return '|';
    case K_RIGHT_BRACKET:
      return '}';
    default:
      break;
    }
  }
  else
  {
    switch ( key )
    {
    case K_APOSTROPHE:
      return '\'';
    case K_COMMA:
      return ',';
    case K_MINUS:
      return '-';
    case K_PERIOD:
      return '.';
    case K_SLASH:
      return '/';
    case K_0:
      return '0';
    case K_1:
      return '1';
    case K_2:
      return '2';
    case K_3:
      return '3';
    case K_4:
      return '4';
    case K_5:
      return '5';
    case K_6:
      return '6';
    case K_7:
      return '7';
    case K_8:
      return '8';
    case K_9:
      return '9';
    case K_SEMI_COLON:
      return ';';
    case K_EQUAL:
      return '=';
    case K_LEFT_BRACKET:
      return '[';
    case K_BACKSLASH:
      return '\\';
    case K_RIGHT_BRACKET:
      return ']';
    default:
      break;
    }
  }
  // TODO: Support other keys

  return 0;
}

internal void UpdateConsole( Console *console, InputSystem *inputSystem,
                             CommandSystem *commandSystem, float dt )
{
  console->cursorChangeTime += dt;

  bool shiftActive = IsKeyDown( inputSystem, K_LEFT_SHIFT ) ||
                     IsKeyDown( inputSystem, K_RIGHT_SHIFT );
  for ( uint32_t key = K_SPACE; key <= K_GRAVE_ACCENT; ++key )
  {
    if ( WasKeyPressed( inputSystem, key ) )
    {
      char character = ConvertKeyToCharacter( key, shiftActive );
      if ( character )
      {
        if ( console->inputBufferLength < CONSOLE_IN_BUF_LEN )
        {
          strcpy( console->inputBuffer + console->cursor + 1,
                  console->inputBuffer + console->cursor );
          console->inputBuffer[console->cursor++] = character;
          console->inputBufferLength++;
          console->cursorChangeTime = 0.0f;
        }
      }
    }
  }

  if ( WasKeyPressed( inputSystem, K_ENTER ) )
  {
    cmd_Exec( commandSystem, console->inputBuffer );
    con_Puts( console, console->inputBuffer );
    con_Puts( console, "\n");
    memset( console->inputBuffer, 0, console->inputBufferLength );
    console->inputBufferLength = 0;
    console->cursor = 0;
    console->cursorChangeTime = 0.0f;
  }

  if ( WasKeyPressed( inputSystem, K_LEFT ) )
  {
    if ( console->cursor > 0 )
    {
      console->cursor--;
    }
    console->cursorChangeTime = 0.0f;
  }
  else if ( WasKeyPressed( inputSystem, K_RIGHT ) )
  {
    if ( console->cursor < console->inputBufferLength )
    {
      console->cursor++;
    }
    console->cursorChangeTime = 0.0f;
  }

  if ( WasKeyPressed( inputSystem, K_HOME ) )
  {
    console->cursor = 0;
    console->cursorChangeTime = 0.0f;
  }
  else if ( WasKeyPressed( inputSystem, K_END ) )
  {
    console->cursor = console->inputBufferLength;
    console->cursorChangeTime = 0.0f;
  }

  if ( WasKeyPressed( inputSystem, K_DELETE ) )
  {
    if ( console->cursor < console->inputBufferLength )
    {
      strcpy( console->inputBuffer + console->cursor,
              console->inputBuffer + console->cursor + 1 );
      console->inputBufferLength--;
      console->cursorChangeTime = 0.0f;
    }
  }
  else if ( WasKeyPressed( inputSystem, K_BACKSPACE ) )
  {
    if ( console->cursor > 0 )
    {
      strcpy( console->inputBuffer + console->cursor - 1,
              console->inputBuffer + console->cursor );
      console->inputBufferLength--;
      console->cursor--;
      console->cursorChangeTime = 0.0f;
    }
  }
}

internal int cl_HandleCommand( cl_GameState *gameState, GameMemory *memory,
                               ExecuteCommandEvent *event,
                               CommandSystem *commandSystem )
{
  switch( event->command )
  {
    case EchoCommand:
      if ( event->numArgs != 1 )
      {
        con_Puts( &gameState->console, "Expected argument to echo\n" );
        return CMD_FAILURE;
      }
      con_Puts( &gameState->console, event->args[0] );
      con_Puts( &gameState->console, "\n" );
      return CMD_SUCCESS;
    case ConnectCommand:
      if ( event->numArgs == 1 )
      {
        gameState->state = CL_GAME_STATE_CONNECTING;
        return CMD_SUCCESS;
      }
      else
      {
        return CMD_FAILURE;
      }
    case DisconnectCommand:
      gameState->state = CL_GAME_STATE_MAIN_MENU;
      cmd_Exec( commandSystem, "killclient" );
      cmd_Exec( commandSystem, "killserver" );
      return CMD_SUCCESS;
    case DrawPhysicsCommand:
    if ( event->numArgs != 1 )
    {
      con_Puts( &gameState->console, "Expected draw physics mode" );
      return CMD_FAILURE;
    }
    else
    {
      int mode = atoi( event->args[ 0 ] );
      if ( mode < 0 || mode > DRAW_PHYSICS_MODE_SERVER )
      {
        con_Puts( &gameState->console, "Invalid draw physics mode, must be in range from 0 to 2" );
        return CMD_FAILURE;
      }
      gameState->drawPhysicsMode = mode;
      return CMD_SUCCESS;
    }
    case DebugCameraCommand:
    gameState->isDebugCameraActive = !gameState->isDebugCameraActive;
    if ( gameState->isDebugCameraActive )
    {
      gameState->debugCamera = gameState->camera;
      gameState->debugCamera.offset = Vec3( 0 );
      gameState->debugCamera.orientation = Vec3( 0 );
      gameState->isDebugCameraActive = true;
      memory->debugShowCursor( false );
    }
    else
    {
      memory->debugShowCursor( true );
    }
    return CMD_SUCCESS;
    default:
      return CMD_UNKNOWN_COMMAND;
  }
}

internal void cl_OpenConsole( cl_GameState *gameState )
{
  gameState->consoleEasing.start = 0;
  gameState->consoleEasing.delta = CONSOLE_HEIGHT;
  gameState->consoleEasing.duration = 0.2f;
  gameState->consoleEasing.t = 0.0f;
  gameState->consoleEasing.function = EASE_QUAD_IN_OUT;
}

internal void cl_CloseConsole( cl_GameState *gameState )
{
  gameState->consoleEasing.start = CONSOLE_HEIGHT;
  gameState->consoleEasing.delta = -CONSOLE_HEIGHT;
  gameState->consoleEasing.duration = 0.2f;
  gameState->consoleEasing.t = 0.0f;
  gameState->consoleEasing.function = EASE_QUAD_IN_OUT;
}

internal vec3 CalculateDebugCameraAcceleration( InputSystem *inputSystem )
{
  vec3 result = {};
  if ( IsInputActive( inputSystem, INPUT_DEBUG_CAMERA_FORWARDS ) )
  {
    result.z -= 1.0f;
  }
  if ( IsInputActive( inputSystem, INPUT_DEBUG_CAMERA_BACKWARDS ) )
  {
    result.z += 1.0f;
  }

  if ( IsInputActive( inputSystem, INPUT_DEBUG_CAMERA_LEFT ) )
  {
    result.x -= 1.0f;
  }
  if ( IsInputActive( inputSystem, INPUT_DEBUG_CAMERA_RIGHT ) )
  {
    result.x += 1.0f;
  }
  if ( Length( result ) > 1.0f )
  {
    result = Normalize( result );
  }
  return result;
}

internal void UpdateDebugCamera( Camera *camera, InputSystem *inputSystem, float dt )
{

  auto localAcceleration =
    CalculateDebugCameraAcceleration( inputSystem );

  float sensitivity = 0.03f;
  float dx = ( float )inputSystem->mouseX - inputSystem->prevMouseX;
  float dy = ( float )inputSystem->mouseY - inputSystem->prevMouseY;
  vec3 cameraRotation = Vec3( dy, dx, 0 );
  cameraRotation *= sensitivity;
  camera->orientation += cameraRotation * dt;
  camera->orientation.x =
    Clamp( camera->orientation.x, -PI * 0.5f, PI * 0.5f );
  camera->orientation.z = 0.0f;

  debug_Printf( "CAMERA ORIENTATION: %g %g %g\n",
                camera->orientation.x, camera->orientation.y, camera->orientation.z );

  mat4 rotation;
  rotation = RotateX( camera->orientation.x );
  rotation *= RotateY( camera->orientation.y );
  rotation = Transpose( rotation );

  auto forward = Normalize( rotation.col[ 2 ] ).xyz;
  auto right = Normalize( rotation.col[ 0 ] ).xyz;

  vec3 acceleration = {};
  acceleration += forward * localAcceleration.z;
  acceleration += right * localAcceleration.x;

  // TODO: Formalize constants
  float friction = 15.0f;
  float speed = 65.0f;
  acceleration *= speed;
  acceleration -= camera->velocity * friction;

  camera->velocity+= acceleration * dt;
  camera->position += camera->velocity * dt;
}

internal void cl_AddPrediction( cl_GameState *gameState, AgentState state,
                                net_Input input )
{
  auto predictionBuffer = &gameState->predictionBuffer;
  uint32_t newTail = ( predictionBuffer->tail + 1 ) % CL_MAX_PREDICTIONS;
  if ( newTail == predictionBuffer->head )
  {
    LOG_WARNING( "May overwrite predictions" );
  }

  auto prediction = predictionBuffer->predictions + predictionBuffer->tail;
  prediction->state = state;
  prediction->timestamp = gameState->predictionTick;
  prediction->input = input;
  predictionBuffer->tail = newTail;
}

internal void cl_AddPlayer( cl_GameState *gameState,
                            net_PlayerEntityCreateData data,
                            net_EntityId netEntityId )
{
  auto physics = gameState->physics;

  auto entityId = CreateEntity( &gameState->entitySystem );
  auto agentComponent =
    AddEntityComponent( &gameState->entitySystem, cl_AgentComponent, entityId );
  ASSERT( agentComponent );
  agentComponent->character =
    physics->createCharacter( physics->physicsSystem, data.position );

  auto transformComponent = AddEntityComponent( &gameState->entitySystem,
                                                TransformComponent, entityId );
  ASSERT( transformComponent );
  transformComponent->transform = tr_Translate( data.position );

  auto heroComponent = AddEntityComponent( &gameState->entitySystem, HeroComponent, entityId );
  ASSERT( heroComponent );
  heroComponent->type = data.heroType;

  auto weaponControllerComponent = AddEntityComponent( &gameState->entitySystem, WeaponControllerComponent, entityId );
  ASSERT( weaponControllerComponent );
  weaponControllerComponent->timeBetweenAttacks = 0.3f;

  auto healthComponent = AddEntityComponent( &gameState->entitySystem, HealthComponent, entityId );
  ASSERT( healthComponent );
  healthComponent->team = PlayerTeam;

  switch ( data.heroType )
  {
    case HeroTypeStrongMan:
      healthComponent->maximum = 220;
      healthComponent->amount = 220;
      break;
    case HeroTypeBarmaid:
      healthComponent->maximum = 180;
      healthComponent->amount = 180;
      break;
    case HeroTypeCook:
      healthComponent->maximum = 180;
      healthComponent->amount = 180;
      break;
    default:
      ASSERT( !"Invalid hero type" );
      break;
  }

  if ( data.isOwner )
  {
    gameState->localPlayerEntity = entityId;
  }
  else
  {
    // TODO: Add render component
  }

  NetComponent *netComponent =
    AddEntityComponent( &gameState->entitySystem, NetComponent, entityId );
  netComponent->id = netEntityId;
  netComponent->typeId = net_PlayerEntityTypeId;;
}

internal void cl_AddBasicEnemy( cl_GameState *gameState, net_BasicEnemyEntityCreateData data, net_EntityId netEntityId )
{
  auto physics = gameState->physics;

  auto entityId = CreateEntity( &gameState->entitySystem );
  auto agentComponent =
    AddEntityComponent( &gameState->entitySystem, cl_AgentComponent, entityId );
  ASSERT( agentComponent );
  agentComponent->character =
    physics->createCharacter( physics->physicsSystem, data.position );

  auto transformComponent = AddEntityComponent( &gameState->entitySystem,
                                                TransformComponent, entityId );
  ASSERT( transformComponent );
  transformComponent->transform = tr_Translate( data.position );

  auto healthComponent = AddEntityComponent( &gameState->entitySystem, HealthComponent, entityId );
  ASSERT( healthComponent );
  healthComponent->amount = 100;
  healthComponent->maximum = 100;
  healthComponent->team = WorldTeam;

  // TODO: Add render component

  NetComponent *netComponent =
    AddEntityComponent( &gameState->entitySystem, NetComponent, entityId );
  netComponent->id = netEntityId;
  netComponent->typeId = net_BasicEnemyEntityTypeId;
}

internal void cl_ParseEntityOpCreate( cl_GameState *gameState,
                                      Bitstream *bitstream )
{
  net_EntityId netEntityId = 0;
  if ( bitstream_ReadBits( bitstream, &netEntityId, NET_ENTITY_ID_NUM_BITS ) )
  {
    uint32_t entityTypeId = 0;
    if ( bitstream_ReadBits( bitstream, &entityTypeId,
                             NET_ENTITY_TYPEID_NUM_BITS ) )
    {
      switch ( entityTypeId )
      {
        // TODO: Deserialize data and add it to queue for later processing
      case net_PlayerEntityTypeId:
      {
        net_PlayerEntityCreateData data;
        if ( !net_PlayerEntityCreateDataDeserialize( bitstream, &data ) )
        {
          ASSERT( 0 );
        }
        if ( !data.isOwner )
        {
          // We don't need to interpolate updates for the local player entity
          // we perform prediction/extrapolation for it only.
          cl_CreateEntitySnapshotBuffer( &gameState->entitySnapshotSystem,
                                         netEntityId,
                                         sizeof( net_PlayerEntityUpdateData ) );
        }

        // TODO: Buffer creation so that it can be part of clientside interp
        cl_AddPlayer( gameState, data, netEntityId );
        break;
      }
      case net_BasicEnemyEntityTypeId:
      {
                                       net_BasicEnemyEntityCreateData data;
                                       if ( !net_BasicEnemyEntityCreateDataDeserialize( bitstream, &data ) )
                                       {
                                         ASSERT( 0 );
                                       }
                                       cl_CreateEntitySnapshotBuffer( &gameState->entitySnapshotSystem, netEntityId, sizeof( net_BasicEnemyEntityUpdateData ) );
                                       cl_AddBasicEnemy( gameState, data, netEntityId );
        break;
      }
      default:
        ASSERT( !"Unsupported entityTypeId" );
        break;
      }
    }
  }
}

internal AgentState cl_UpdateAgentComponent( cl_AgentComponent *agentComponent,
                                             cl_GameState *gameState,
                                             net_Input input, float dt )
{
  auto transformComponent = GetEntityComponent(
      &gameState->entitySystem, agentComponent->owner, TransformComponent );
  ASSERT( transformComponent );

  AgentState state;
  state.position = tr_GetPosition( transformComponent->transform );
  state.velocity = agentComponent->velocity;
  auto result =
    com_CalculateCharacterPosition( input, state, agentComponent->character,
                                    gameState->physics, PLAYER_SPEED, dt );

  transformComponent->transform.position = result.position;
  agentComponent->velocity = result.velocity;
  agentComponent->angle = input.angle;

  debug_PushBox( result.position, Vec3( 1 ), Vec4( 1, 1, 0, 1 ) );
  debug_Printf( "Angle: %g\n", input.angle );

  {
    vec3 v = Vec3( 0, 0, 3 );
    mat4 rotation = RotateY( input.angle );
    vec4 v4 = rotation * Vec4( v.x, v.y, v.z, 0 );
    v = Vec3( v4.x, v4.y, v4.z );
    debug_PushLine( result.position, result.position + v, Vec4( 1, 0, 0, 1 ) );
  }

  return result;
}

internal void cl_SetAgentState( EntityComponentSystem *entitySystem,
                                GamePhysics *physics, AgentState state,
                                EntityId entity )
{
  auto agentComponent =
    GetEntityComponent( entitySystem, entity, cl_AgentComponent );
  auto transformComponent =
    GetEntityComponent( entitySystem, entity, TransformComponent );
  ASSERT( agentComponent );
  ASSERT( transformComponent );

  agentComponent->velocity = state.velocity;
  transformComponent->transform.position = state.position;
  physics->setCharacterPosition( physics->physicsSystem,
                                 agentComponent->character, state.position );
}

internal void cl_VerifyAndCorrectPrediction(
  cl_GameState *gameState,
  net_PlayerEntityUpdateData data, uint32_t timestamp, float dt,
  uint32_t sequenceNumber )
{
  auto predictionBuffer = &gameState->predictionBuffer;
  uint32_t i = predictionBuffer->head;
  bool found = false;
  while ( i != predictionBuffer->tail )
  {
    auto prediction = predictionBuffer->predictions + i;
    if ( prediction->input.sequenceNumber == sequenceNumber )
    {
      found = true;
      // TODO: Formalize this constant
      if ( Length( prediction->state.position - data.position ) > 0.0000001f )
      {
        LOG_DEBUG( "Incorrect prediction! sequence number=%d", sequenceNumber );
        auto state = prediction->state;
        LOG_DEBUG( "Client Position: %g %g %g, Server Position: %g %g %g",
                   state.position.x, state.position.y, state.position.z,
                   data.position.x, data.position.y, data.position.z );
        LOG_DEBUG( "Client Velocity: %g %g %g, Server Velocity: %g %g %g",
                   state.velocity.x, state.velocity.y, state.velocity.z,
                   data.velocity.x, data.velocity.y, data.velocity.z );
        state.position = data.position;
        state.velocity = data.velocity;
        cl_SetAgentState( &gameState->entitySystem, gameState->physics, state,
                          gameState->localPlayerEntity );
        auto agentComponent =
          GetEntityComponent( &gameState->entitySystem,
                              gameState->localPlayerEntity, cl_AgentComponent );
        ASSERT( agentComponent );
        uint32_t temp = i;
        while ( i != predictionBuffer->tail )
        {
          prediction = predictionBuffer->predictions + i;
          if ( prediction->input.sequenceNumber > sequenceNumber )
          {
            prediction->state = cl_UpdateAgentComponent(
              agentComponent, gameState, prediction->input, dt );
          }
          i = ( i + 1 ) % CL_MAX_PREDICTIONS;
        }
        i = temp;
      }
      //cl_PrintDiscardedSequenceNumbers( predictionBuffer, i );
      predictionBuffer->head = i;
      break;
    }
    i = ( i + 1 ) % CL_MAX_PREDICTIONS;
  }

  auto healthComponent = GetEntityComponent( &gameState->entitySystem, gameState->localPlayerEntity, HealthComponent );
  ASSERT( healthComponent );
  healthComponent->amount = data.health;

  if ( !found )
  {
    LOG_ERROR( "Failed to find prediction for sequence number %d",
            sequenceNumber );
  }
}

internal void cl_ParseEntityOpUpdate( cl_GameState *gameState,
                                      Bitstream *bitstream, uint32_t timestamp,
                                      float dt, uint32_t sequenceNumber )
{
  // TODO: Refactor common code
  net_EntityId netEntityId = 0;
  if ( bitstream_ReadBits( bitstream, &netEntityId, NET_ENTITY_ID_NUM_BITS ) )
  {
    uint32_t entityTypeId = 0;
    if ( bitstream_ReadBits( bitstream, &entityTypeId,
                             NET_ENTITY_TYPEID_NUM_BITS ) )
    {
      switch ( entityTypeId )
      {
      case net_PlayerEntityTypeId:
      {
        net_PlayerEntityUpdateData data;
        if ( !net_PlayerEntityUpdateDataDeserialize( bitstream, &data ) )
        {
          ASSERT( 0 );
        }

        auto netCom =
          GetNetComponentWithNetId( &gameState->entitySystem, netEntityId );
        if ( netCom )
        {
          ASSERT( netCom->typeId == entityTypeId );
          if ( netCom->owner != gameState->localPlayerEntity )
          {
            cl_StoreEntitySnapshot( &gameState->entitySnapshotSystem,
                                    netEntityId, timestamp, &data,
                                    sizeof( data ) );
          }
          else
          {
            cl_VerifyAndCorrectPrediction( gameState, data, timestamp, dt,
                                           sequenceNumber );
          }
        }
        break;
      }
      case net_BasicEnemyEntityTypeId:
      {
                                       net_BasicEnemyEntityUpdateData data;
                                       if ( !net_BasicEnemyEntityUpdateDataDeserialize( bitstream, &data ) )
                                       {
                                         ASSERT( 0 );
                                       }
                                       cl_StoreEntitySnapshot( &gameState->entitySnapshotSystem, netEntityId, timestamp, &data, sizeof( data ) );
                                       break;
      }
      default:
        ASSERT( !"Unsupported entityTypeId" );
        break;
      }
    }
  }
}
internal void cl_ParseEntityOpDestroy( cl_GameState *gameState,
                                       Bitstream *bitstream )
{
  net_EntityId netEntityId = 0;
  if ( bitstream_ReadBits( bitstream, &netEntityId, NET_ENTITY_ID_NUM_BITS ) )
  {
    ForeachComponent( com, &gameState->entitySystem, NetComponent )
    {
      if ( com->id == netEntityId )
      {
        // NOTE: ignore snapshot buffer for local player entity
        RemoveEntity( &gameState->entitySystem, com->owner );
        break;
      }
    }
  }
}

internal void cl_ParseEntityData( cl_GameState *gameState, Bitstream *bitstream,
                                  uint32_t timestamp, float dt,
                                  uint32_t sequenceNumber )
{
  uint32_t count = 0;
  uint32_t numEntitiesCreated = 0;
  uint32_t numEntitiesDestroyed = 0;
  uint32_t numEntitiesUpdated = 0;
  if ( bitstream_ReadBits( bitstream, &count, NET_ENTITY_COUNT_NUM_BITS ) )
  {
    for ( uint32_t i = 0; i < count; ++i )
    {
      uint32_t op = 0;
      if ( bitstream_ReadBits( bitstream, &op, NET_ENTITY_OP_NUM_BITS ) )
      {
        switch ( op )
        {
          case net_EntityOpCreate:
            cl_ParseEntityOpCreate( gameState, bitstream );
            numEntitiesCreated++;
            break;
          case net_EntityOpDestroy:
            cl_ParseEntityOpDestroy( gameState, bitstream );
            numEntitiesDestroyed++;
            break;
          case net_EntityOpUpdate:
            cl_ParseEntityOpUpdate( gameState, bitstream, timestamp, dt, sequenceNumber );
            numEntitiesUpdated++;
            break;
          default:
            ASSERT( !"Unsupported entity op" );
            break;
        }
      }
    }
  }
  LOG_DEBUG( "Num entities created: %d", numEntitiesCreated );
  LOG_DEBUG( "Num entities destroyed: %d", numEntitiesDestroyed );
  LOG_DEBUG( "Num entities updated: %d", numEntitiesUpdated );
  //gameState->netStats.packetStats[0].numEntitiesCreated = numEntitiesCreated;
  //gameState->netStats.packetStats[0].numEntitiesDestroyed = numEntitiesDestroyed;
  //gameState->netStats.packetStats[0].numEntitiesUpdated = numEntitiesUpdated;
}

internal void cl_HandlePacketReceived( cl_GameState *gameState, void *eventData, float dt)
{
  ReceivePacketEvent *event = (ReceivePacketEvent *)eventData;
  Bitstream bitstream = {};
  bitstream_InitForReading( &bitstream, event->packet.data, event->packet.len );

  uint8_t packetType = 0;
  if ( bitstream_ReadBits( &bitstream, &packetType, NET_PACKET_TYPE_NUM_BITS ) )
  {
    if ( packetType == net_GamePacketType )
    {
      // TODO: Read as much data as we can into a struct, and apply it one go
      // so that we don't risk updating something to an invalid value
      uint32_t numTicks = 0;
      if ( bitstream_ReadBits( &bitstream, &numTicks,
                               NET_GAME_TICK_COUNT_NUM_BITS ) )
      {
        if ( gameState->state == CL_GAME_STATE_LOADING )
        {
          gameState->currentGameTick = numTicks;
          gameState->state = CL_GAME_STATE_PICKING_HERO;
        }

        if ( ( gameState->state == CL_GAME_STATE_CONNECTED ) || ( gameState->state == CL_GAME_STATE_PICKING_HERO ) )
        {
          uint32_t sequenceNumber = 0;
          if ( bitstream_ReadBits( &bitstream, &sequenceNumber, NET_GAME_INPUT_SEQ_NUM_NUM_BITS ) )
          {
            uint32_t chunkCount = 0;
            if ( bitstream_ReadBits( &bitstream, &chunkCount, NET_GAME_PACKET_CHUNK_COUNT_NUM_BITS ) )
            {
              for ( uint32_t i = 0; i < chunkCount; ++i )
              {
                uint8_t chunkId = 0;
                if ( bitstream_ReadBits( &bitstream, &chunkId, NET_GAME_PACKET_CHUNK_ID_NUM_BITS ) )
                {
                  uint32_t chunkLength = 0;
                  if ( bitstream_ReadBits( &bitstream, &chunkLength, NET_GAME_PACKET_CHUNK_LENGTH_NUM_BITS ) )
                  {
                    uint32_t start = bitstream.readCursor;
                    if ( chunkId == net_RpcParametersChunkId )
                    {
                      com_HandleRpcParameters( &gameState->rpcSystem, &bitstream );
                    }
                    else if ( chunkId == net_RpcResultsChunkId )
                    {
                      com_HandleRpcResults( &gameState->rpcSystem, &bitstream );
                    }
                    else if ( chunkId == net_EntityChunkId )
                    {
                      cl_ParseEntityData( gameState, &bitstream, numTicks, dt,
                                          sequenceNumber );
                    }
                    else
                    {
                      LOG_WARNING( "Unrecognized chunk id %d received from server", chunkId );
                    }

                    // Move read cursor to start of next chunk even if an error was encountered while parsing the chunk
                    uint32_t nextChunkStart = start + chunkLength;
                    if ( nextChunkStart < bitstream.size )
                    {
                      bitstream.readCursor = nextChunkStart;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    else if ( packetType == net_ConnectionPacketType )
    {
      uint8_t connectionOp = 0;
      if ( bitstream_ReadBits( &bitstream, &connectionOp, NET_CONNECTION_OP_NUM_BITS ) )
      {
        if ( connectionOp == net_ConnectionOpLoadMap )
        {
          if ( gameState->state == CL_GAME_STATE_CONNECTING )
          {
            net_ConnectionOpLoadMapData data;
            if ( net_ConnectionOpLoadMapDataDeserialize( &bitstream, &data ) )
            {
              strncpy( gameState->mapName, data.mapName, data.mapNameLength );
              gameState->state = CL_GAME_STATE_LOADING;
            }
          }
        }
        else
        {
          LOG_WARNING( "Unhandled connection op %d", connectionOp );
        }
      }
    }
    else
    {
      LOG_WARNING( "Received unknown packet type %d", packetType );
    }
  }
}

internal void cl_HandleNetworkStatsEvent( cl_GameState *gameState, void *eventData, CommandSystem *commandSystem )
{
  NetworkStatsEvent *event = ( NetworkStatsEvent * )eventData;
  if ( gameState->state == CL_GAME_STATE_CONNECTING )
  {
    // If no packet received after 1 seconds when connecting then connection attempt has failed
    if ( event->noPacketTime > 1.0f )
    {
      cmd_Exec( commandSystem, "killclient" );
      gameState->state = CL_GAME_STATE_MAIN_MENU;
    }
  }
  debug_Printf( "PING: %d\n", event->delay );
}

internal void cl_RegisterEntityComponents( EntityComponentSystem *entitySystem,
                                           MemoryArena *arena )
{
  RegisterEntityComponent( entitySystem, arena, MAX_TRANSFORM_COMPONENTS,
                           TransformComponent );
  RegisterEntityComponent( entitySystem, arena, SV_MAX_AGENT_COMPONENTS,
                           cl_AgentComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_NET_COMPONENTS,
                           NetComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_HEALTH_COMPONENTS, HealthComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_HERO_COMPONENTS, HeroComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_WEAPON_CONTROLLER_COMPONENTS, WeaponControllerComponent );
}

internal void cl_Initialize( cl_GameState *gameState, uint32_t windowWidth,
                             uint32_t windowHeight, GameMemory *memory,
                             CommandSystem *commandSystem )
{
  MemoryArenaInitialize(
      &gameState->permArena,
      memory->persistentStorageSize - sizeof( cl_GameState ),
      (uint8_t *)memory->persistentStorageBase + sizeof( cl_GameState ) );
  renderer_Init( &gameState->renderer, windowWidth, windowHeight, &gameState->transArena );
  debug_Init( &gameState->debugState );
  gameState->font =
    LoadFont( "../content/fonts/Hack-Regular.ttf", &gameState->transArena,
        memory, 1024, 1024, 48 );
  gameState->consoleFont =
    LoadFont( "../content/fonts/Hack-Regular.ttf", &gameState->transArena,
        memory, 512, 512, 18 );
  gameState->hudFont = LoadFont( "../content/fonts/Vera.ttf", &gameState->transArena, memory, 1024, 1024, 24 );
  gameState->testSprite =
    LoadSprite( "../content/sprites/test_sprite.png", memory );
  gameState->testMesh = LoadMesh( "../content/meshes/suzanne.dae", memory, &gameState->transArena );
  gameState->testLevel = LoadMesh( "../content/meshes/test_level.dae", memory, &gameState->transArena );
  gameState->cube = LoadMesh( "../content/meshes/cube.dae", memory, &gameState->transArena );
  uint32_t program = LoadShader( "../content/shaders/directional_light.vert", "../content/shaders/directional_light.frag", &gameState->transArena, memory );
  gameState->renderer.directionalLightShader = CreateDirectionalLightShader( program );
  program = LoadShader( "../content/shaders/directional_light.vert", "../content/shaders/post_processing.frag", &gameState->transArena, memory );
  gameState->renderer.postProcessingShader = CreatePostProcessingShader( program );
  program = LoadShader( "../content/shaders/colour_deferred.vert", "../content/shaders/colour_deferred.frag", &gameState->transArena, memory );
  gameState->renderer.colourDeferredShader = CreateColourDeferredShader( program );

  SetupKeyBindings( &gameState->inputSystem );
  cmd_Register(commandSystem, "echo", EchoCommand );
  cmd_Register(commandSystem, "disconnect", DisconnectCommand);
  cmd_Register( commandSystem, "draw_physics", DrawPhysicsCommand );
  cmd_Register( commandSystem, "toggle_debug_camera", DebugCameraCommand );

  CreateEntityComponentSystem( &gameState->entitySystem, &gameState->permArena,
                               4096, 0xFFFF );
  cl_RegisterEntityComponents( &gameState->entitySystem, &gameState->permArena );
  cl_InitializeEntitySnapshotSystem( &gameState->entitySnapshotSystem,
                                     &gameState->permArena );

  gameState->heroPickingState.mouseOver = -1;
  gameState->heroPickingState.selection = -1;

  net_InitializeRpcSystem( &gameState->rpcSystem );

  gameState->state = CL_GAME_STATE_MAIN_MENU;

  cmd_Exec( commandSystem, "host" );
  cmd_Exec( commandSystem, "connect localhost" );
}

internal bool cl_WriteInputPacketChunk( Bitstream *packetBitstream, cl_GameState *gameState )
{
  if ( gameState->state == CL_GAME_STATE_CONNECTED )
  {
    if ( gameState->localPlayerEntity )
    {
      cl_AgentComponent *agentComponent =
        GetEntityComponent( &gameState->entitySystem,
                            gameState->localPlayerEntity, cl_AgentComponent );
      if ( agentComponent )
      {
        bool success = false;
        Bitstream bitstream;
        uint8_t buffer[ 128 ];
        uint32_t maxChunkSize = MinVal( bitstream_GetNumBytesRemaining( packetBitstream ), sizeof( buffer ) );
        bitstream_InitForWriting( &bitstream, buffer, maxChunkSize );
        if ( bitstream_WriteInt( &bitstream, net_InputChunkId, NET_GAME_PACKET_CHUNK_ID_NUM_BITS ) )
        {
          int chunkLengthLocation = bitstream_ReserveBits( &bitstream, NET_GAME_PACKET_CHUNK_LENGTH_NUM_BITS );
          if ( chunkLengthLocation >= 0 )
          {
            uint32_t start = bitstream.size;
            if ( net_InputSerialize( &bitstream, &agentComponent->input ) )
            {
              success = true;
            }

            uint32_t length = bitstream.size - start;
            if ( !bitstream_WriteBitsAtLocation( &bitstream, &length, NET_GAME_PACKET_CHUNK_LENGTH_NUM_BITS, chunkLengthLocation ) )
            {
              ASSERT( !"Write should not fail since bits have already been successfully reserved" );
            }
          }
        }

        if ( success )
        {
          // Write entire chunk to bitstream
          if ( bitstream_WriteBits( packetBitstream, buffer, bitstream_GetLengthInBits( &bitstream ) ) )
          {
            return true;
          }
        }
      }
    }
  }
  return false;
}

internal void cl_WritePacket( cl_GameState *gameState, Packet *outgoingPacket )
{
  if ( gameState->state == CL_GAME_STATE_LOADING )
  {
    // Nothing to load yet
    bool success = false;
    if ( outgoingPacket )
    {
      Bitstream bitstream = {};
      bitstream_InitForWriting( &bitstream, outgoingPacket->data,
                                outgoingPacket->capacity );
      if ( bitstream_WriteInt( &bitstream, net_ConnectionPacketType,
                          NET_PACKET_TYPE_NUM_BITS ) )
      {
        if ( bitstream_WriteInt( &bitstream, net_ConnectionOpLoadMapResponse,
                      NET_CONNECTION_OP_NUM_BITS ) )
        {
          outgoingPacket->len = bitstream_GetLengthInBytes( &bitstream );
          success = true;
        }
      }
      if ( !success )
      {
        LOG_ERROR( "Failed to serialize packet data" );
      }
    }
  }
  else if ( ( gameState->state == CL_GAME_STATE_CONNECTED ) || ( gameState->state == CL_GAME_STATE_PICKING_HERO ) )
  {
    Bitstream bitstream;
    bitstream_InitForWriting( &bitstream, outgoingPacket->data,
                              outgoingPacket->capacity );
    if ( bitstream_WriteInt( &bitstream, net_GamePacketType, NET_PACKET_TYPE_NUM_BITS ) )
    {
      uint32_t chunkCount = 0;
      int chunkCountLocation = bitstream_ReserveBits( &bitstream, NET_GAME_PACKET_CHUNK_COUNT_NUM_BITS );
      if ( chunkCountLocation >= 0 )
      {
        if ( com_WriteRpcParametersPacketChunk( &bitstream, &gameState->rpcSystem ) )
        {
          chunkCount++;
        }
        if ( com_WriteRpcResultsPacketChunk( &bitstream, &gameState->rpcSystem ) )
        {
          chunkCount++;
        }
        if ( cl_WriteInputPacketChunk( &bitstream, gameState ) )
        {
          chunkCount++;
        }
        bitstream_WriteBitsAtLocation( &bitstream, &chunkCount, NET_GAME_PACKET_CHUNK_COUNT_NUM_BITS, chunkCountLocation );
        outgoingPacket->len = bitstream_GetLengthInBytes( &bitstream );
      }
    }
  }
}

internal void
cl_ApplyPlayerEntityUpdateData( EntityComponentSystem *entitySystem, GamePhysics *physics,
                                EntityId entityId,
                                net_PlayerEntityUpdateData data )
{
  auto agentComponent =
    GetEntityComponent( entitySystem, entityId, cl_AgentComponent );
  ASSERT( agentComponent );

  auto transformComponent =
    GetEntityComponent( entitySystem, entityId, TransformComponent );
  ASSERT( transformComponent );

  auto healthComponent = GetEntityComponent( entitySystem, entityId, HealthComponent );
  ASSERT( healthComponent );

  agentComponent->velocity = data.velocity;
  transformComponent->transform.position = data.position;
  healthComponent->amount = data.health;

  physics->setCharacterPosition( physics->physicsSystem, agentComponent->character, data.position );
}

internal void cl_ApplyBasicEnemyEntityUpdateData( EntityComponentSystem *entitySystem, GamePhysics *physics, EntityId entityId, net_BasicEnemyEntityUpdateData data, uint8_t fields = net_BasicEnemyEntityUpdateData_all )
{
  auto agentComponent =
    GetEntityComponent( entitySystem, entityId, cl_AgentComponent );
  ASSERT( agentComponent );

  auto transformComponent =
    GetEntityComponent( entitySystem, entityId, TransformComponent );
  ASSERT( transformComponent );

  auto healthComponent = GetEntityComponent( entitySystem, entityId, HealthComponent );
  ASSERT( healthComponent );

  if ( fields & net_BasicEnemyEntityUpdateData_velocity )
  {
    agentComponent->velocity.x = data.velocity.x;
    agentComponent->velocity.z = data.velocity.y;
  }

  if ( fields & net_BasicEnemyEntityUpdateData_position )
  {
    transformComponent->transform.position.x = data.position.x;
    transformComponent->transform.position.z = data.position.y;
    physics->setCharacterPosition( physics->physicsSystem, agentComponent->character, transformComponent->transform.position );
  }

  if ( fields & net_BasicEnemyEntityUpdateData_health )
  {
    healthComponent->amount = data.health;
  }
}

// TODO: Use command queues rather than a function for this to invert the dependency
internal void cl_InterpolateNetEntity( net_EntitySnapshot *lower,
                                       net_EntitySnapshot *upper, float t,
                                       NetComponent *netComponent,
                                       cl_GameState *gameState )
{
  switch ( netComponent->typeId )
  {
  case net_PlayerEntityTypeId:
  {
    auto lowerData = (net_PlayerEntityUpdateData *)lower->data;
    auto upperData = (net_PlayerEntityUpdateData *)upper->data;
    net_PlayerEntityUpdateData result;
    result = net_PlayerEntityUpdateDataInterpolate( *lowerData, *upperData, t );

    cl_ApplyPlayerEntityUpdateData( &gameState->entitySystem, gameState->physics,
                                    netComponent->owner, result );
    break;
  }
  case net_BasicEnemyEntityTypeId:
  {
    auto lowerData = (net_BasicEnemyEntityUpdateData *)lower->data;
    auto upperData = (net_BasicEnemyEntityUpdateData *)upper->data;
    net_BasicEnemyEntityUpdateData result;
    result = net_BasicEnemyEntityUpdateDataInterpolate( *lowerData, *upperData, t );

    cl_ApplyBasicEnemyEntityUpdateData( &gameState->entitySystem, gameState->physics,
                                    netComponent->owner, result );
    break;
  }
  default:
    ASSERT( !"Unknown entity type id" );
    break;
  }
}

// TODO: Limit extrapolation as ping increases to stop things snapping back
internal void cl_ExtrapolateNetEntity( net_EntitySnapshot *lower,
                                       net_EntitySnapshot *upper,
                                       NetComponent *netComponent,
                                       cl_GameState *gameState )
{
  uint32_t range = upper->timestamp - lower->timestamp;
  float d = gameState->currentGameTick - upper->timestamp;
  switch ( netComponent->typeId )
  {
  case net_PlayerEntityTypeId:
  {
                               /// This is stupid, we have the last known velocity, just use it!
    auto lowerData = (net_PlayerEntityUpdateData *)lower->data;
    auto upperData = (net_PlayerEntityUpdateData *)upper->data;
    net_PlayerEntityUpdateData delta = *upperData;
    delta.position = upperData->position - lowerData->position;
    delta.position *= 1.0f / (float)range;
    delta.position *= d;
    delta.position += upperData->position;
    cl_ApplyPlayerEntityUpdateData( &gameState->entitySystem, gameState->physics,
                                    netComponent->owner, delta );
    break;
  }
  case net_BasicEnemyEntityTypeId:
  {
                                   auto data = ( net_BasicEnemyEntityUpdateData * )upper->data;
                                   cl_ApplyBasicEnemyEntityUpdateData( &gameState->entitySystem, gameState->physics, netComponent->owner, *data, net_BasicEnemyEntityUpdateData_velocity );
                                   break;
  }
  default:
    ASSERT( !"Unknown entity type id" );
    break;
  }
}

internal void cl_UpdateNetEntities( cl_GameState *gameState )
{
  ForeachComponent( netComponent, &gameState->entitySystem, NetComponent )
  {
    //if ( netComponent->owner == gameState->localPlayerEntity )
    //{
      //continue; // Skip local player entity, we use prediction for it.
    //}
    auto entitySnapshots = GetEntitySnapshots( &gameState->entitySnapshotSystem,
                                               netComponent->id );
    if ( entitySnapshots )
    {
      auto lower =
        GetLowerSnapshot( entitySnapshots, gameState->currentGameTick );
      auto upper =
        GetUpperSnapshot( entitySnapshots, gameState->currentGameTick );
      if ( upper && lower )
      {
        uint32_t range = upper->timestamp - lower->timestamp;
        float t = (float)( gameState->currentGameTick - lower->timestamp ) /
                  (float)range;

        cl_InterpolateNetEntity( lower, upper, t, netComponent, gameState );
      }
      else if ( lower )
      {
        // printf( "Only one snapshot for net entity %d, need to extrapolate",
        // netComponent->id );
        LOG_DEBUG( "currentGameTick = %d lower->timestamp = %d",
                   gameState->currentGameTick, lower->timestamp );
        if ( gameState->currentGameTick - lower->timestamp < 15 )
        {
          upper = lower;
          lower = GetLowerSnapshot( entitySnapshots, upper->timestamp );
          if ( lower )
          {
            cl_ExtrapolateNetEntity( lower, upper, netComponent, gameState );
          }
          else
          {
            LOG_DEBUG( "Cannot extrapolate, not enough snapshots" );
          }
        }
        else
        {
          LOG_DEBUG( "Cannot extrapolate, timestamp difference is too large" );
        }
      }
      else
      {
        LOG_DEBUG( "No snapshots available for net entity %d",
                   netComponent->id );
      }
    }
  }
}

internal void cl_RpcOnChangeHero( cl_GameState *gameState, net_GameRpcChangeHeroResult result )
{
  if ( gameState->state == CL_GAME_STATE_PICKING_HERO )
  {
    if ( result.code == 0 )
    {
      gameState->state = CL_GAME_STATE_CONNECTED;
    }
    else
    {
      LOG_ERROR( "Change Hero RPC failed!" );
    }
  }
}

internal void cl_ProcessIncomingRpcs( cl_GameState *gameState )
{
  auto rpcSystem = &gameState->rpcSystem;

  bool stop = false;
  while ( !stop )
  {
    stop = true;
    RpcQueueHeader header = {};
    if ( ByteQueueRead( &rpcSystem->incomingResults, &header, sizeof( header ) ) )
    {
      uint8_t parametersBuffer[ NET_GAME_RPC_DATA_LENGTH ];
      if ( ByteQueueRead( &rpcSystem->incomingResults, parametersBuffer, header.size ) )
      {
        switch ( header.typeId )
        {
          case net_GameRpcInvalid:
            ASSERT( !"Invalid game RPC typeId" );
            break;
          case net_GameRpcChangeHero:
            cl_RpcOnChangeHero( gameState, *( net_GameRpcChangeHeroResult* )parametersBuffer );
            break;
          default:
            ASSERT( !"Unknown game RPC" );
            break;
        }
      }
    }
  }
}
extern "C" GAME_CLIENT_UPDATE( cl_GameUpdate )
{
  ASSERT( memory->persistentStorageSize > sizeof( cl_GameState ) );
  cl_GameState *gameState = (cl_GameState *)memory->persistentStorageBase;
  MemoryArenaInitialize( &gameState->transArena, memory->transientStorageSize,
                         (uint8_t *)memory->transientStorageBase );

  if ( gameState->state == CL_GAME_STATE_UNINTIALIZED )
  {
    gameState->sphereShape =
      physics->createSphereShape( physics->physicsSystem, 1.5f );

    gameState->camera.farClip = 100.0f;
    gameState->camera.nearClip = 0.1f;
    gameState->camera.height = windowHeight;
    gameState->camera.width = windowWidth;
    gameState->camera.fov = 80.0f;
    gameState->camera.offset = Vec3( 0, 0, 15 );
    gameState->camera.orientation = Vec3( Radians( 30.0f ), 0, 0 );

    cl_Initialize( gameState, windowWidth, windowHeight, memory,
                   commandSystem );
  }
  gameState->physics = physics;

  globalDebugState = &gameState->debugState;

  //float start = 0.0f;
  //float end = 100.0f;
  //for ( float z = start; z < end; z += 1.0f )
  //{
  //  debug_PushLine( Vec3( start, 0, z ), Vec3( end, 0, z ),
  //                  Vec4( 0.6, 0.8, 0.1, 1 ) );
  //}
  //for ( float x = start; x < end; x += 1.0f )
  //{
  //  debug_PushLine( Vec3( x, 0, start ), Vec3( x, 0, end ),
  //                  Vec4( 0.6, 0.8, 0.1, 1 ) );
  //}
  debug_PushLine( Vec3( 0 ), Vec3( 1, 0, 0 ), Vec4( 1, 0, 0, 1 ) );
  debug_PushLine( Vec3( 0 ), Vec3( 0, 1, 0 ), Vec4( 0, 1, 0, 1 ) );
  debug_PushLine( Vec3( 0 ), Vec3( 0, 0, 1 ), Vec4( 0, 0, 1, 1 ) );


  InputSystem *inputSystem = &gameState->inputSystem;

  for ( uint32_t i = 0; i < MAX_KEYS; ++i )
  {
    inputSystem->prevKeyStates[i] = inputSystem->currentKeyStates[i];
  }
  inputSystem->prevKeyInput = inputSystem->currentKeyInput;
  inputSystem->prevMouseX = inputSystem->mouseX;
  inputSystem->prevMouseY = inputSystem->mouseY;

  Packet *outgoingPacket = NULL;
  EventPeakResult result = evt_Peak( eventQueue, NULL );
  while ( result.header )
  {
    switch ( result.header->typeId )
    {
    case KeyPressEventTypeId:
    {
      KeyPressEvent *event = (KeyPressEvent *)result.data;
      inputSystem->currentKeyStates[event->key] = 1;
      uint32_t input = inputSystem->keyBindings[event->key];
      inputSystem->currentKeyInput |= input;
      break;
    }
    case KeyReleaseEventTypeId:
    {
      KeyReleaseEvent *event = (KeyReleaseEvent *)result.data;
      inputSystem->currentKeyStates[event->key] = 0;
      uint32_t input = inputSystem->keyBindings[event->key];
      inputSystem->currentKeyInput &= ~input;
      break;
    }
    case MouseMotionEventTypeId:
    {
      MouseMotionEvent *event = (MouseMotionEvent *)result.data;
      inputSystem->mouseX = event->x;
      inputSystem->mouseY = event->y;
      break;
    }
    case ExecuteCommandEventTypeId:
    {
      ExecuteCommandEvent *event = (ExecuteCommandEvent *)result.data;
      cl_HandleCommand( gameState, memory, event, commandSystem );
      break;
    }
    case PlatformStatsEventTypeId:
    {
      PlatformStatsEvent *event = (PlatformStatsEvent *)result.data;
      debug_Printf_( &gameState->debugState, "FPS: %g(%g)\n",
                    1.0f / event->frameTime, event->frameTime );
      debug_Printf_( &gameState->debugState, "UPS: %g(%g)\n",
                    1.0f / event->updateTime, event->updateTime );
      debug_Printf_( &gameState->debugState, "HOST: %s\n",
                    event->isListenServer ? "TRUE" : "FALSE" );
      break;
    }
    case PacketGeneratedEventTypeId:
    {
      PacketGeneratedEvent *event = (PacketGeneratedEvent *)result.data;
      outgoingPacket = &event->packet;
      break;
    }
    case ReceivePacketEventTypeId:
      cl_HandlePacketReceived( gameState, result.data, dt );
      break;
    case NetworkStatsEventTypeId:
      cl_HandleNetworkStatsEvent( gameState, result.data, commandSystem );
      break;
    default:
      break;
    }
    result = evt_Peak( eventQueue, result.header );
  }

  debug_Printf_( &gameState->debugState, "STATE: %s\n",
                clGameStateNames[gameState->state] );
  debug_Printf_( &gameState->debugState, "TICK: %d\n",
                gameState->currentGameTick );

  if ( gameState->state == CL_GAME_STATE_MAIN_MENU )
  {
    if ( gameState->isConsoleActive )
    {
      UpdateConsole( &gameState->console, inputSystem, commandSystem, dt );
    }
  }
  else if ( gameState->state == CL_GAME_STATE_CONNECTING )
  {
    if ( gameState->isConsoleActive )
    {
      gameState->isConsoleActive = false;
      cl_CloseConsole( gameState );
    }

    bool success = false;
    if ( outgoingPacket )
    {
      Bitstream bitstream = {};
      bitstream_InitForWriting( &bitstream, outgoingPacket->data,
                                outgoingPacket->capacity );
      if ( bitstream_WriteInt( &bitstream, net_ConnectionPacketType,
                          NET_PACKET_TYPE_NUM_BITS ) )
      {
        if ( bitstream_WriteInt( &bitstream, net_ConnectionOpClientInfo,
                      NET_CONNECTION_OP_NUM_BITS ) )
        {
          net_ConnectionOpClientInfoData data = {};
          strncpy( data.name, "deadVertex", ARRAY_COUNT( data.name ) );
          data.nameLength = strlen( data.name );
          if ( net_ConnectionOpClientInfoDataSerialize( &bitstream, &data ) )
          {
            outgoingPacket->len = bitstream_GetLengthInBytes( &bitstream );
            success = true;
          }
        }
      }
      if ( !success )
      {
        LOG_ERROR( "Failed to serialize packet data" );
      }
    }
  }
  else if ( gameState->state == CL_GAME_STATE_PICKING_HERO )
  {
    if ( gameState->isConsoleActive )
    {
      UpdateConsole( &gameState->console, inputSystem, commandSystem, dt );
    }
    cl_UpdateHeroPickingState( gameState, windowWidth, windowHeight );
  }
  else if ( gameState->state == CL_GAME_STATE_CONNECTED )
  {
    net_Input input = {};
    input.sequenceNumber = gameState->sequenceNumber++;

    auto agentComponent =
      GetEntityComponent( &gameState->entitySystem,
                          gameState->localPlayerEntity, cl_AgentComponent );
    if ( gameState->isConsoleActive )
    {
      UpdateConsole( &gameState->console, inputSystem, commandSystem, dt );
    }
    else if ( gameState->isDebugCameraActive )
    {
      UpdateDebugCamera( &gameState->debugCamera, &gameState->inputSystem, dt );
    }
    else
    {
      if ( IsInputActive( inputSystem, INPUT_UP ) )
      {
        input.actions |= INPUT_ACTION_MOVE_UP;
      }
      if ( IsInputActive( inputSystem, INPUT_DOWN ) )
      {
        input.actions |= INPUT_ACTION_MOVE_DOWN;
      }
      if ( IsInputActive( inputSystem, INPUT_LEFT ) )
      {
        input.actions |= INPUT_ACTION_MOVE_LEFT;
      }
      if ( IsInputActive( inputSystem, INPUT_RIGHT ) )
      {
        input.actions |= INPUT_ACTION_MOVE_RIGHT;
      }
      if ( IsInputActive( inputSystem, INPUT_PRIMARY_ATTACK ) )
      {
        input.actions |= INPUT_ACTION_PRIMARY_ATTACK;
      }

      if ( agentComponent )
      {
        auto transformComponent = GetEntityComponent(
          &gameState->entitySystem, gameState->localPlayerEntity,
          TransformComponent );
        ASSERT( transformComponent );

        gameState->camera.position = tr_GetPosition( transformComponent->transform );

        vec2 screenCenter = Vec2( windowWidth, windowHeight ) * 0.5f;
        vec2 mousePosition =
          Vec2( inputSystem->mouseX, windowHeight - inputSystem->mouseY );

        Ray ray = GenerateRay( inputSystem->mouseX, inputSystem->mouseY, gameState->camera );
        //debug_PushLine( ray.start, ray.start + ray.dir * 25.0f, Vec4( 1, 0, 0, 1 ) );
        float t = Dot( ray.dir, Vec3( 0, 1, 0 ) );
        if ( t < 0.0000001f )
        {
          float distance = Dot( -ray.start, Vec3( 0, 1, 0 ) ) / t;
          vec3 hitPoint = ray.start + ray.dir * distance;
          //debug_PushPoint( hitPoint, Vec4( 0, 1, 1, 1 ) );

          vec3 direction = hitPoint - tr_GetPosition( transformComponent->transform );
          input.angle = ATan(direction.x, direction.z );
        }

        auto weaponControllerComponent = GetEntityComponent( &gameState->entitySystem, gameState->localPlayerEntity, WeaponControllerComponent );
        ASSERT( weaponControllerComponent );
        // Attack
        if ( input.actions & INPUT_ACTION_PRIMARY_ATTACK )
        {
          if ( weaponControllerComponent->lastAttackTime + weaponControllerComponent->timeBetweenAttacks < gameState->clientSideCurrentTime )
          {
            auto p = tr_GetPosition( transformComponent->transform );
            com_AttackResult attackResult = com_ProcessAttack( input, p, physics, gameState->sphereShape );

            debug_PushSphere( attackResult.start, 1.5f, Vec4( 1, 0, 0, 1 ), 5.0f );
            debug_PushLine( attackResult.start, attackResult.end, Vec4( 1, 1, 0, 1 ), 5.0f );
            debug_PushSphere( attackResult.end, 1.5f, Vec4( 1, 0, 0, 1 ), 5.0f );

            if ( attackResult.count > 0 )
            {
              for ( int i = 0; i < attackResult.count; ++i )
              {
                auto result = attackResult.entries + i;
                physics->applyImpulse( physics->physicsSystem,
                    result->physicsHandle, 120.0f * attackResult.direction,
                    result->hitPoint );
              }
            }

            weaponControllerComponent->lastAttackTime = gameState->clientSideCurrentTime;
          }
        }
      }
    }

    physics->update( physics->physicsSystem, dt );

    if ( agentComponent )
    {
      agentComponent->input = input;
      auto state =
        cl_UpdateAgentComponent( agentComponent, gameState, input, dt );
      cl_AddPrediction( gameState, state, input );
    }
    cl_UpdateNetEntities( gameState );
  }
  else if ( gameState->state == CL_GAME_STATE_LOADING )
  {
    if ( !gameState->mapLoaded )
    {
      {
        uint32_t box =
          physics->createBoxShape( physics->physicsSystem, Vec3( 1 ) );
        PhysRigidBodyParameters params = {};
        params.position = Vec3( 0, 10, 0 );
        params.mass = 20.0f;
        params.shapeHandle = box;
        params.type = RIGID_BODY_DYNAMIC;
        params.collidesWith = COLLISION_TYPE_DEFAULT | COLLISION_TYPE_STATIC |
                              COLLISION_TYPE_CHARACTER;

        physics->createRigidBody( physics->physicsSystem, params );
      }

      {
        uint32_t box =
          physics->createBoxShape( physics->physicsSystem, Vec3( 5, 2, 10 ) );
        PhysRigidBodyParameters params = {};
        params.position = Vec3( 15, 1, 5 );
        params.shapeHandle = box;
        params.type = RIGID_BODY_STATIC;
        params.collidesWith = COLLISION_TYPE_ALL;

        physics->createRigidBody( physics->physicsSystem, params );
      }

      {
        uint32_t plane = physics->createBoxShape( physics->physicsSystem,
                                                  Vec3( 500, 500, 0.1 ) );
        PhysRigidBodyParameters params = {};
        params.position = Vec3( 0, -0.1, 0 );
        params.shapeHandle = plane;
        params.type = RIGID_BODY_STATIC;
        params.collidesWith = COLLISION_TYPE_ALL;

        physics->createRigidBody( physics->physicsSystem, params );
      }

      gameState->mapLoaded = true;
    }
  }

  if ( gameState->state != CL_GAME_STATE_CONNECTING )
  {
    if ( WasInputActivated( inputSystem, INPUT_TOGGLE_CONSOLE ) )
    {
      gameState->isConsoleActive = !gameState->isConsoleActive;
      if ( gameState->isConsoleActive )
      {
        cl_OpenConsole( gameState );
      }
      else
      {
        cl_CloseConsole( gameState );
      }
    }
  }

  cl_ProcessIncomingRpcs( gameState );

  cl_WritePacket( gameState, outgoingPacket );

  UpdateEasing( &gameState->consoleEasing, dt );

  debug_Update( &gameState->debugState, &gameState->renderer );

  void *tempBuffer = MemoryArenaAllocate( &gameState->transArena, 4096 );
  SimpleEventQueue componentGarbageQueue = evt_CreateQueue( tempBuffer, 4096 );
  GarbageCollectEntities( &gameState->entitySystem, &componentGarbageQueue,
                          &gameState->transArena );
  EventPeakResult peakResult = evt_Peak( &componentGarbageQueue, NULL );
  while ( peakResult.header )
  {
    switch ( peakResult.header->typeId )
    {
    case DestroyEntityComponentEventTypeId:
    {
      DestroyEntityComponentEvent *event =
        (DestroyEntityComponentEvent *)peakResult.data;
      if ( event->componentTypeId == NetComponentTypeId )
      {
        NetComponent *netComponent = (NetComponent*)event->componentData;
        cl_DestroyEntitySnapshotBuffer( &gameState->entitySnapshotSystem,
                                        netComponent->id );
      }
      else if ( event->componentTypeId == cl_AgentComponentTypeId )
      {
        cl_AgentComponent *agentComponent =
          (cl_AgentComponent *)event->componentData;
        physics->destroyObject( physics->physicsSystem,
                                agentComponent->character );
      }
      break;
    }
    default:
      break;
    }
    peakResult = evt_Peak( &componentGarbageQueue, peakResult.header );
  }

  gameState->currentGameTick++;
  gameState->clientSideCurrentTime += dt;
}

extern "C" GAME_START_UPDATE( GameStartUpdate )
{
  if ( globalDebugState )
  {
    debug_Cleanup( globalDebugState, dt );
  }
}