#pragma once

#define SV_GAME_CLIENT_NAME_LENGTH 30
#define MAP_NAME_LENGTH 60

#define NET_EVENT_COUNT_NUM_BITS 6 // Maximum of 64 events per packet
#define NET_EVENT_TYPE_NUM_BITS 12

#define NET_ENTITY_TYPEID_NUM_BITS 12
#define NET_MAX_ENTITY_TYPEID BIT( NET_ENTITY_TYPEID_NUM_BITS )
#define NET_ENTITY_OP_NUM_BITS 2
enum
{
  net_EntityOpCreate = 1,
  net_EntityOpUpdate = 2,
  net_EntityOpDestroy = 3,
};
enum
{
  net_ConnectionPacketType = 1,
  net_GamePacketType = 2,
  net_GameSyncPacketType = 3,
};

#define NET_PACKET_TYPE_NUM_BITS 3

enum
{
  net_EventChunkId = 1,
  net_EntityChunkId = 2,
  net_InputChunkId = 3,
  net_RpcParametersChunkId = 4,
  net_RpcResultsChunkId = 5,
};
#define NET_GAME_PACKET_CHUNK_ID_NUM_BITS 3
#define NET_GAME_PACKET_CHUNK_COUNT_NUM_BITS 3
#define NET_GAME_PACKET_CHUNK_LENGTH_NUM_BITS 11 // NOTE: Chunk length is stored in bits

enum
{
  net_ConnectionOpClientInfo = 0,
  net_ConnectionOpLoadMap = 1,
  net_ConnectionOpLoadMapResponse = 2,
  net_ConnectionOpRequestConnection = 3,
};

#define NET_CONNECTION_OP_NUM_BITS 3

struct net_ConnectionOpClientInfoData
{
  uint8_t nameLength;
  char name[SV_GAME_CLIENT_NAME_LENGTH];
};

enum
{
  net_PlayerEntityTypeId = 1,
  net_BasicEnemyEntityTypeId = 2,
};

struct net_PlayerEntityCreateData
{
  vec3 position;
  uint8_t heroType;
  bool isOwner;
};

struct net_PlayerEntityUpdateData
{
  vec3 position;
  vec3 velocity;
  uint16_t health;
};

struct net_BasicEnemyEntityCreateData
{
  vec3 position;
};

enum
{
  net_BasicEnemyEntityUpdateData_position = BIT( 1 ),
  net_BasicEnemyEntityUpdateData_velocity = BIT( 2 ),
  net_BasicEnemyEntityUpdateData_health = BIT( 3 ),
  net_BasicEnemyEntityUpdateData_all = BIT( 4 ) - 1,
};

struct net_BasicEnemyEntityUpdateData
{
  vec2 position;
  vec2 velocity;
  uint16_t health;
};

#define NET_INPUT_ACTION_NUM_BITS 10
#define NET_GAME_TICK_COUNT_NUM_BITS 29 // This could be reduced
#define NET_GAME_INPUT_SEQ_NUM_NUM_BITS 29

struct net_Input
{
  uint32_t timestamp;
  uint32_t sequenceNumber;
  float angle;
  uint16_t actions;
};

enum
{
  HeroTypeStrongMan = 0,
  HeroTypeBarmaid   = 1,
  HeroTypeCook      = 2,
  MaxHeroTypes,
};

#define NET_HERO_TYPE_NUM_BITS 3

enum
{
  net_GameRpcInvalid            = 0,
  net_GameRpcChangeHero        = 1,
};

#define NET_GAME_RPC_TYPE_NUM_BITS 8
#define NET_GAME_RPC_COUNT_NUM_BITS 5 // Maximum of 32 RPCs per tick
#define NET_GAME_RPC_DATA_LENGTH 64

struct net_GameRpcChangeHeroParameters
{
  uint8_t hero;
};

inline bool net_GameRpcChangeHeroParametersSerialize( Bitstream *bitstream, const net_GameRpcChangeHeroParameters *data )
{
  if ( bitstream_WriteBits( bitstream, &data->hero, NET_HERO_TYPE_NUM_BITS ) )
  {
    return true;
  }
  return false;
}

inline bool net_GameRpcChangeHeroParametersDeserialize( Bitstream *bitstream, net_GameRpcChangeHeroParameters *data )
{
  ZeroPointerToStruct( data );
  if ( bitstream_ReadBits( bitstream, &data->hero, NET_HERO_TYPE_NUM_BITS ) )
  {
    return true;
  }
  return false;
}

struct net_GameRpcChangeHeroResult
{
  uint8_t code;
};

#define NET_GAME_RPC_CHANGE_HERO_RESULT_CODE_NUM_BITS 1

inline bool net_GameRpcChangeHeroResultSerialize( Bitstream *bitstream, const net_GameRpcChangeHeroResult *data )
{
  if ( bitstream_WriteBits( bitstream, &data->code, NET_GAME_RPC_CHANGE_HERO_RESULT_CODE_NUM_BITS ) )
  {
    return true;
  }
  return false;
}

inline bool net_GameRpcChangeHeroResultDeserialize( Bitstream *bitstream, net_GameRpcChangeHeroResult *data )
{
  ZeroPointerToStruct( data );
  if ( bitstream_ReadBits( bitstream, &data->code, NET_GAME_RPC_CHANGE_HERO_RESULT_CODE_NUM_BITS ) )
  {
    return true;
  }
  return false;
}

inline bool net_ConnectionOpClientInfoDataSerialize(
  Bitstream *bitstream, const net_ConnectionOpClientInfoData *data )
{
  ASSERT( data->nameLength < ARRAY_COUNT( data->name ) );
  if ( bitstream_WriteBytes( bitstream, &data->nameLength, 1 ) )
  {
    if ( bitstream_WriteBytes( bitstream, &data->name, data->nameLength ) )
    {
      return true;
    }
  }
  return false;
}

inline bool net_ConnectionOpClientInfoDataDeserialize(
  Bitstream *bitstream, net_ConnectionOpClientInfoData *data )
{
  ZeroPointerToStruct( data );
  if ( bitstream_ReadBytes( bitstream, &data->nameLength, 1 ) )
  {
    // Extra byte for the NUL
    if ( data->nameLength < ARRAY_COUNT( data->name ) )
    {
      if ( bitstream_ReadBytes( bitstream, &data->name, data->nameLength ) )
      {
        return true;
      }
    }
  }
  return false;
}

struct net_ConnectionOpLoadMapData
{
  uint32_t clientId;
  uint8_t mapNameLength;
  char mapName[MAP_NAME_LENGTH];
};

inline bool
net_ConnectionOpLoadMapDataSerialize( Bitstream *bitstream,
                                      const net_ConnectionOpLoadMapData *data )
{
  ASSERT( data->mapNameLength < MAP_NAME_LENGTH );
  if ( bitstream_WriteBytes( bitstream, &data->clientId,
                             sizeof( data->clientId ) ) )
  {
    if ( bitstream_WriteBytes( bitstream, &data->mapNameLength,
                               sizeof( data->mapNameLength ) ) )
    {
      if ( bitstream_WriteBytes( bitstream, &data->mapName,
                                 data->mapNameLength ) )
      {
        return true;
      }
    }
  }
  return false;
}

inline bool
net_ConnectionOpLoadMapDataDeserialize( Bitstream *bitstream,
                                        net_ConnectionOpLoadMapData *data )
{
  ZeroPointerToStruct( data );
  if ( bitstream_ReadBytes( bitstream, &data->clientId,
                            sizeof( data->clientId ) ) )
  {
    if ( bitstream_ReadBytes( bitstream, &data->mapNameLength,
                              sizeof( data->mapNameLength ) ) )
    {
      if ( data->mapNameLength < ARRAY_COUNT( data->mapName ) )
      {
        if ( bitstream_ReadBytes( bitstream, &data->mapName,
                                  data->mapNameLength ) )
        {
          return true;
        }
      }
    }
  }
  return false;
}

inline bool
net_PlayerEntityCreateDataSerialize( Bitstream *bitstream,
                                     const net_PlayerEntityCreateData *data )
{
  if ( bitstream_WriteBytes( bitstream, &data->position, sizeof( data->position) ) )
  {
    if ( bitstream_WriteBits( bitstream, &data->isOwner, 1 ) )
    {
      if ( bitstream_WriteBits( bitstream, &data->heroType, NET_HERO_TYPE_NUM_BITS ) )
      {
        return true;
      }
    }
  }
  return false;
}

inline bool
net_PlayerEntityCreateDataDeserialize( Bitstream *bitstream,
                                       net_PlayerEntityCreateData *data )
{
  ClearToZero( data, sizeof( *data ) );
  if ( bitstream_ReadBytes( bitstream, &data->position,
                            sizeof( data->position ) ) )
  {
    if ( bitstream_ReadBits( bitstream, &data->isOwner, 1 ) )
    {
      if ( bitstream_ReadBits( bitstream, &data->heroType, NET_HERO_TYPE_NUM_BITS ) )
      {
        return true;
      }
    }
  }
  return false;
}

inline bool
net_PlayerEntityUpdateDataSerialize( Bitstream *bitstream,
                                     const net_PlayerEntityUpdateData *data )
{
  if ( bitstream_WriteBytes( bitstream, &data->position,
                             sizeof( data->position ) ) )
  {
    if ( bitstream_WriteBytes( bitstream, &data->velocity,
                               sizeof( data->velocity ) ) )
    {
      if ( bitstream_WriteBytes( bitstream, &data->health, sizeof( data->health ) ) )
      {
        return true;
      }
    }
  }
  return false;
}

inline bool
net_PlayerEntityUpdateDataDeserialize( Bitstream *bitstream,
                                       net_PlayerEntityUpdateData *data )
{
  ClearToZero( data, sizeof( *data ) );
  if ( bitstream_ReadBytes( bitstream, &data->position,
                            sizeof( data->position ) ) )
  {
    if ( bitstream_ReadBytes( bitstream, &data->velocity,
                              sizeof( data->velocity ) ) )
    {
      if ( bitstream_ReadBytes( bitstream, &data->health, sizeof( data->health ) ) )
      {
        return true;
      }
    }
  }
  return false;
}

inline net_PlayerEntityUpdateData
net_PlayerEntityUpdateDataInterpolate( net_PlayerEntityUpdateData a,
                                       net_PlayerEntityUpdateData b,
                                       float t )
{
  net_PlayerEntityUpdateData result = b;
  result.position = Lerp( a.position, b.position, t );
  result.velocity = Lerp( a.velocity, b.velocity, t );
  result.health = Lerp( a.health, b.health, t );
  return result;
}

inline bool net_InputSerialize( Bitstream *bitstream, const net_Input *data )
{
  if ( bitstream_WriteBits( bitstream, &data->sequenceNumber,
                            NET_GAME_INPUT_SEQ_NUM_NUM_BITS ) )
  {
    if ( bitstream_WriteBytes( bitstream, &data->timestamp, 4 ) )
    {
      if ( bitstream_WriteBytes( bitstream, &data->angle,
                                 sizeof( data->angle ) ) )
      {
        if ( bitstream_WriteBits( bitstream, &data->actions,
                                  NET_INPUT_ACTION_NUM_BITS ) )
        {
          return true;
        }
      }
    }
  }
  return false;
}

inline bool net_InputDeserialize( Bitstream *bitstream, net_Input *data )
{
  ClearToZero( data, sizeof( *data ) );
  if ( bitstream_ReadBits( bitstream, &data->sequenceNumber,
                           NET_GAME_INPUT_SEQ_NUM_NUM_BITS ) )
  {
    if ( bitstream_ReadBytes( bitstream, &data->timestamp, 4 ) )
    {
      if ( bitstream_ReadBytes( bitstream, &data->angle,
                                sizeof( data->angle ) ) )
      {
        if ( bitstream_ReadBits( bitstream, &data->actions,
                                 NET_INPUT_ACTION_NUM_BITS ) )
        {
          return false;
        }
      }
    }
  }
  return false;
}

inline bool
net_BasicEnemyEntityCreateDataSerialize( Bitstream *bitstream,
                                     const net_BasicEnemyEntityCreateData *data )
{
  if ( bitstream_WriteBytes( bitstream, &data->position, sizeof( data->position) ) )
  {
    return true;
  }
  return false;
}

inline bool
net_BasicEnemyEntityCreateDataDeserialize( Bitstream *bitstream,
                                       net_BasicEnemyEntityCreateData *data )
{
  ClearToZero( data, sizeof( *data ) );
  if ( bitstream_ReadBytes( bitstream, &data->position,
                            sizeof( data->position ) ) )
  {
    return true;
  }
  return false;
}

inline bool
net_BasicEnemyEntityUpdateDataSerialize( Bitstream *bitstream,
                                     const net_BasicEnemyEntityUpdateData *data )
{
  if ( bitstream_WriteBytes( bitstream, &data->position,
                             sizeof( data->position ) ) )
  {
    if ( bitstream_WriteBytes( bitstream, &data->velocity,
                               sizeof( data->velocity ) ) )
    {
      if ( bitstream_WriteBytes( bitstream, &data->health, sizeof( data->health ) ) )
      {
        return true;
      }
    }
  }
  return false;
}

inline bool
net_BasicEnemyEntityUpdateDataDeserialize( Bitstream *bitstream,
                                       net_BasicEnemyEntityUpdateData *data )
{
  ClearToZero( data, sizeof( *data ) );
  if ( bitstream_ReadBytes( bitstream, &data->position,
                            sizeof( data->position ) ) )
  {
    if ( bitstream_ReadBytes( bitstream, &data->velocity,
                              sizeof( data->velocity ) ) )
    {
      if ( bitstream_ReadBytes( bitstream, &data->health, sizeof( data->health ) ) )
      {
        return true;
      }
    }
  }
  return false;
}

inline net_BasicEnemyEntityUpdateData
net_BasicEnemyEntityUpdateDataInterpolate( net_BasicEnemyEntityUpdateData a,
                                       net_BasicEnemyEntityUpdateData b,
                                       float t )
{
  net_BasicEnemyEntityUpdateData result = b;
  result.position = Lerp( a.position, b.position, t );
  result.velocity = Lerp( a.velocity, b.velocity, t );
  result.health = (uint16_t)Lerp( (float)a.health, (float)b.health, t );
  return result;
}