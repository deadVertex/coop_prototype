#pragma once

#define NET_MAX_ENTITY_SNAPSHOTS 24
#define NET_ENTITY_SNAPSHOT_BUFFER_DATA_POOL_SIZE 800 // TODO: Remove this limit

struct net_EntitySnapshot
{
  uint32_t timestamp;
  void *data;
};

struct net_EntitySnapshotBuffer
{
  net_EntityId netEntityId;
  net_EntitySnapshot snapshots[NET_MAX_ENTITY_SNAPSHOTS];
  uint32_t head;
  uint32_t tail;
  MemoryPool dataPool;
};

struct net_EntitySnapshotSystem
{
  net_EntitySnapshotBuffer snapshotBuffers[NET_MAX_ENTITIES];
  uint32_t count;
  MemoryPool dataPool;
};
