#pragma once
#include "common/utils.h"
#include "common/event.h"
#include "common/input.h"
#include "common/logging.h"
#include "common/cmd.h"
#include "common/net.h"
#include "common/math.h"
#include "common/bitstream.h"

#include "game/entity.h"
#include "game/game_net.h"

#include "platform/physics.h"

struct GameMemory;
struct GamePhysics;

#define GAME_RENDER( NAME )                                                    \
  void NAME( float dt, GameMemory *memory, GamePhysics *clientPhysics,         \
             GamePhysics *serverPhysics, uint32_t windowWidth,                 \
             uint32_t windowHeight )

typedef GAME_RENDER( GameRenderFunction );

#define GAME_CLIENT_UPDATE( NAME )                                             \
  void NAME( float dt, GameMemory *memory, SimpleEventQueue *eventQueue,       \
             CommandSystem *commandSystem, GamePhysics *physics,               \
             uint32_t windowWidth, uint32_t windowHeight )

typedef GAME_CLIENT_UPDATE( cl_GameUpdateFunction );

#define GAME_SERVER_UPDATE( NAME )                                             \
  void NAME( float dt, GameMemory *memory, SimpleEventQueue *eventQueue,       \
             CommandSystem *commandSystem, GamePhysics *physics)

typedef GAME_SERVER_UPDATE( sv_GameUpdateFunction );

#define GAME_START_UPDATE( NAME ) void NAME( float dt )

typedef GAME_START_UPDATE( GameStartUpdateFunction );

struct ReadFileResult
{
  void *memory;
  uint32_t size;
};

#define DEBUG_READ_ENTIRE_FILE( NAME ) ReadFileResult NAME( const char *path )
typedef DEBUG_READ_ENTIRE_FILE( DebugReadEntireFileFunction );

#define DEBUG_FREE_FILE_MEMORY( NAME ) void NAME( void *memory )
typedef DEBUG_FREE_FILE_MEMORY( DebugFreeFileMemoryFunction );

#define DEBUG_WRITE_FILE( NAME )                                               \
  bool NAME( const char *path, const void *data, uint32_t numBytes )
typedef DEBUG_WRITE_FILE( DebugWriteFileFunction );

#define DEBUG_GET_CURRENT_TIME( NAME ) double NAME()
typedef DEBUG_GET_CURRENT_TIME( DebugGetCurrentTimeFunction );

#define DEBUG_SHOW_CURSOR( NAME ) void NAME( bool isCursorVisible )
typedef DEBUG_SHOW_CURSOR( DebugShowCursorFunction );

struct GameMemory
{
  bool isInitialized;
  void *persistentStorageBase, *transientStorageBase;
  size_t persistentStorageSize, transientStorageSize;

  DebugReadEntireFileFunction *debugReadEntireFile;
  DebugFreeFileMemoryFunction *debugFreeFileMemory;
  DebugWriteFileFunction *debugWriteFile;
  DebugGetCurrentTimeFunction *debugGetCurrentTime;
  DebugShowCursorFunction *debugShowCursor;

  bool wasCodeReloaded;
};

enum
{
  NullEventTypeId                                                     = 0x0,

  // System events
  ReceivePacketEventTypeId                                            = 0x1000,
  ReceiveTcpPacketEventTypeId                                         = 0x1001,
  NetworkStatsEventTypeId                                             = 0x1002,
  PlatformStatsEventTypeId                                            = 0x1003,
  KeyPressEventTypeId                                                 = 0x1004,
  KeyReleaseEventTypeId                                               = 0x1005,
  MouseMotionEventTypeId                                              = 0x1006,
  ScrollEventTypeId                                                   = 0x1007,
  WindowResizeEventTypeId                                             = 0x1008,
  ClientConnectedEventTypeId                                          = 0x1009,
  PacketGeneratedEventTypeId                                          = 0x100A,
  ExecuteCommandEventTypeId                                           = 0x100B,


  // TODO: These functionalities should has specialized event systems
  DestroyEntityComponentEventTypeId                                   = 0x9000,


  // Game events
  KillEntityEventTypeId                                               = 0xA000,
};

struct KeyPressEvent
{
  uint8_t key;
};

struct KeyReleaseEvent
{
  uint8_t key;
};

struct MouseMotionEvent
{
  int x;
  int y;
};

struct ScrollEvent
{
  float amount;
};

struct WindowResizeEvent
{
  uint32_t w;
  uint32_t h;
};

struct PlatformStatsEvent
{
  float frameTime;
  float updateTime;
  bool isListenServer;
};

struct ClientConnectedEvent
{
  uint32_t clientId;
};

struct ReceivePacketEvent
{
  Packet packet;
};

struct PacketGeneratedEvent
{
  Packet packet;
};

// Client specific
struct NetworkStatsEvent
{
  uint32_t delay;
  uint32_t receiveQueueLength;
  uint32_t outgoingQueueLength;
  float noPacketTime; // Number of seconds passed without a packet being received
};

struct DestroyEntityComponentEvent
{
  uint32_t componentTypeId;
  void *componentData;
};

struct KillEntityEvent
{
  EntityId owner;
  uint8_t team;
};

enum
{
  INPUT_UP = BIT( 0 ),
  INPUT_DOWN = BIT( 1 ),
  INPUT_LEFT = BIT( 2 ),
  INPUT_RIGHT = BIT( 3 ),
  INPUT_INTERACT = BIT( 4 ),
  INPUT_PRIMARY_ATTACK = BIT( 5 ),

  INPUT_DEBUG_CAMERA_FORWARDS = BIT( 27 ),
  INPUT_DEBUG_CAMERA_BACKWARDS = BIT( 28 ),
  INPUT_DEBUG_CAMERA_LEFT = BIT( 29 ),
  INPUT_DEBUG_CAMERA_RIGHT = BIT( 30 ),
  INPUT_TOGGLE_CONSOLE = BIT(31),
};

enum
{
  INPUT_ACTION_MOVE_UP = BIT( 0 ),
  INPUT_ACTION_MOVE_DOWN = BIT( 1 ),
  INPUT_ACTION_MOVE_LEFT = BIT( 2 ),
  INPUT_ACTION_MOVE_RIGHT = BIT( 3 ),
  INPUT_ACTION_PRIMARY_ATTACK = BIT( 4 ),
};

enum
{
  UnknownCommand    = 0x0,
  EchoCommand       = 0x1,
  MapCommand        = 0x2,
  ExitCommand       = 0x3,
  ConnectCommand    = 0x4,
  HostCommand       = 0x5,
  KillServerCommand = 0x6,
  KillClientCommand = 0x7,
  DisconnectCommand = 0x8,
  NetLatencyCommand = 0x9,
  DrawPhysicsCommand = 0xA,
  DebugCameraCommand = 0XB,
};

typedef uint16_t net_EntityId;
#define NULL_NET_ENTITY_ID 0
#define NET_ENTITY_ID_NUM_BITS 14
#define NET_ENTITY_MAX_ID BIT( NET_ENTITY_ID_NUM_BITS )
#define NET_ENTITY_COUNT_NUM_BITS ( NET_ENTITY_ID_NUM_BITS - 1 )
#define NET_MAX_ENTITIES ( BIT( NET_ENTITY_COUNT_NUM_BITS ) - 1 )

enum
{
  NetComponentTypeId,
  TransformComponentTypeId,
  sv_AgentComponentTypeId,
  cl_AgentComponentTypeId,
  HealthComponentTypeId,
  BrainComponentTypeId,
  HeroComponentTypeId,
  WeaponControllerComponentTypeId,
};

struct NetComponent
{
  DeclareComponent();
  net_EntityId id;
  uint32_t typeId;
};

struct Transform
{
  vec3 position;
};

inline Transform tr_Translate( vec3 position )
{
  Transform result = {};
  result.position = position;
  return result;
}

inline vec3 tr_GetPosition( Transform transform )
{
  vec3 result = transform.position;
  return result;
}

#define MAX_TRANSFORM_COMPONENTS 4096
#define MAX_NET_COMPONENTS NET_MAX_ENTITIES
#define SV_MAX_AGENT_COMPONENTS 64
#define MAX_HEALTH_COMPONENTS 4096
#define MAX_BRAIN_COMPONENTS 2048
#define MAX_HERO_COMPONENTS 4
#define MAX_WEAPON_CONTROLLER_COMPONENTS 4096

struct TransformComponent
{
  DeclareComponent();
  Transform transform;
};

struct sv_AgentComponent
{
  DeclareComponent();
  vec3 velocity;
  uint32_t clientId;
  PhysicsHandle character;
};

struct cl_AgentComponent
{
  DeclareComponent();
  vec3 velocity;
  float angle;
  net_Input input;
  PhysicsHandle character;
};

struct AgentState
{
  vec3 position;
  vec3 velocity;
};

enum
{
  PlayerTeam = 0,
  WorldTeam = 1,
};

struct HealthComponent
{
  DeclareComponent();
  uint16_t amount;
  uint16_t maximum;
  uint8_t team;
};

struct BrainComponent
{
  DeclareComponent();
  float nextWanderTime;
};

struct HeroComponent
{
  DeclareComponent();
  uint8_t type;
};

struct WeaponControllerComponent
{
  float lastAttackTime;
  float timeBetweenAttacks;
  int damage;
};
