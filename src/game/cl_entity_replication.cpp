// TODO: Turn constants into parameters
internal void cl_InitializeEntitySnapshotSystem( net_EntitySnapshotSystem *system, MemoryArena *arena )
{
  ZeroPointerToStruct( system );
  system->dataPool = CreateMemoryPool(
    arena, NET_ENTITY_SNAPSHOT_BUFFER_DATA_POOL_SIZE, NET_MAX_ENTITIES );
}

internal void cl_DestroyEntitySnapshotBuffer( net_EntitySnapshotSystem *system,
                                              net_EntityId netEntityId )
{
  if ( system->count > 0 )
  {
    for ( uint32_t i = 0; i < NET_MAX_ENTITIES; ++i )
    {
      auto snapshotBuffer = system->snapshotBuffers + i;
      if ( snapshotBuffer->netEntityId == netEntityId )
      {
        MemoryPoolFree( &system->dataPool, snapshotBuffer->dataPool.base );
        snapshotBuffer->netEntityId = NULL_ENTITY;
        system->count--;
        break;
      }
    }
  }
}

internal void cl_CreateEntitySnapshotBuffer( net_EntitySnapshotSystem *system,
                                             net_EntityId netEntityId,
                                             uint32_t dataSize )
{
  if ( system->count < NET_MAX_ENTITIES )
  {
    // TODO: Better allocation stratergy
    for ( uint32_t i = 0; i < NET_MAX_ENTITIES; ++i )
    {
      auto snapshotBuffer = system->snapshotBuffers + i;
      if ( snapshotBuffer->netEntityId == NULL_NET_ENTITY_ID )
      {
        snapshotBuffer->netEntityId = netEntityId;
        auto buffer = (uint8_t*)MemoryPoolAllocate( &system->dataPool );
        snapshotBuffer->dataPool =
          CreateMemoryPool( buffer, NET_ENTITY_SNAPSHOT_BUFFER_DATA_POOL_SIZE,
                            dataSize, NET_MAX_ENTITY_SNAPSHOTS );
        for ( uint32_t i = 0; i < NET_MAX_ENTITY_SNAPSHOTS; ++i )
        {
          auto snapshot = snapshotBuffer->snapshots + i;
          snapshot->data = MemoryPoolAllocate( &snapshotBuffer->dataPool );
        }
        system->count++;
        break;
      }
    }
  }
  else
  {
    LOG_ERROR( "Failed to create snapshot buffer, too many net entities" );
  }
}

internal void AddEntitySnapshotToBuffer( net_EntitySnapshotBuffer *buffer,
                                         uint32_t timestamp, void *data,
                                         uint32_t size )
{
  ASSERT( buffer->dataPool.objectSize == size );
  auto snapshot = buffer->snapshots + buffer->tail;
  snapshot->timestamp = timestamp;
  memcpy( snapshot->data, data, size );
  buffer->tail = ( buffer->tail + 1 ) % NET_MAX_ENTITY_SNAPSHOTS;
  if ( buffer->tail == buffer->head )
  {
    buffer->head = ( buffer->head + 1 ) % NET_MAX_ENTITY_SNAPSHOTS;
  }
}

internal void cl_StoreEntitySnapshot( net_EntitySnapshotSystem *system,
                                      net_EntityId netEntityId,
                                      uint32_t timestamp, void *data,
                                      uint32_t size )
{
  // TODO: More efficient searching
  bool found = false;
  for ( uint32_t i = 0; i < NET_MAX_ENTITIES; ++i )
  {
    auto snapshotBuffer = system->snapshotBuffers + i;
    if ( snapshotBuffer->netEntityId == netEntityId )
    {
      AddEntitySnapshotToBuffer( snapshotBuffer, timestamp, data, size );
      found = true;
      break;
    }
  }
  if ( !found )
  {
    LOG_ERROR( "No snapshot buffer for net entity %d", netEntityId );
  }
}

// TODO: Double check this logic on paper
internal net_EntitySnapshot *GetLowerSnapshot( net_EntitySnapshotBuffer *buffer,
                                               uint32_t timestamp )
{
  net_EntitySnapshot *result = NULL;
  for ( uint32_t i = buffer->head; i != buffer->tail;
        i = ( i + 1 ) % NET_MAX_ENTITY_SNAPSHOTS )
  {
    auto snapshot = buffer->snapshots + i;
    if ( snapshot->timestamp < timestamp )
    {
      if ( !result || snapshot->timestamp > result->timestamp )
      {
        result = snapshot;
      }
    }
    else
    {
      break;
    }
  }
  //if ( result )
  //{
    //buffer->head = result - buffer->snapshots;
  //}
  return result;
}

internal net_EntitySnapshot *GetUpperSnapshot( net_EntitySnapshotBuffer *buffer,
                                               uint32_t timestamp )
{
  net_EntitySnapshot *result = NULL;
  for ( uint32_t i = buffer->head; i != buffer->tail;
        i = ( i + 1 ) % NET_MAX_ENTITY_SNAPSHOTS )
  {
    if ( buffer->snapshots[i].timestamp >= timestamp )
    {
      result = buffer->snapshots + i;
      break;
    }
  }
  return result;
}

internal net_EntitySnapshotBuffer *
GetEntitySnapshots( net_EntitySnapshotSystem *system, net_EntityId netEntityId )
{
  // TODO: More efficient searching
  for ( uint32_t i = 0; i < NET_MAX_ENTITIES; ++i )
  {
    if ( system->snapshotBuffers[i].netEntityId == netEntityId )
    {
      return system->snapshotBuffers + i;
    }
  }
  return NULL;
}
