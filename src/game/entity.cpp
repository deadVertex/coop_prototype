#include "entity.h"

#include "game.h" // TODO: Remove and don't use generic event system for component destroyed

#define MAX_ENTITY_HASH_COLLISIONS 10

// Hash function from https://gist.github.com/badboy/6267743
inline uint32_t HashShiftMult32( uint32_t key )
{
  // TODO: Find hashing function that cannot produce 0.
  //uint32_t c2 = 0x27d4eb2d; // a prime or an odd constant
  //key = ( key ^ 61 ) ^ ( key >> 16 );
  //key = key + ( key << 3 );
  //key = key ^ ( key >> 4 );
  //key = key * c2;
  //key = key ^ ( key >> 15 );
  return key;
}

internal EntityComponentMapping *
AllocateMapping( EntityComponentSystem *system, void *component,
                 uint32_t typeId )
{
  auto result = system->freeListHead;
  ASSERT( result );
  system->freeListHead = result->next;
  result->typeId = typeId;
  result->component = component;
  result->next = NULL;
  return result;
}

internal void FreeMapping( EntityComponentSystem *system,
                           EntityComponentMapping *mapping )
{
  mapping->next = system->freeListHead;
  system->freeListHead = mapping;
  mapping->component = NULL;
}

#define AttachEntityComponent( TABLE, ID, COMPONENT, TYPE )                    \
  AttachEntityComponent_( TABLE, ID, COMPONENT, TYPE##TypeId )

internal void AttachEntityComponent_( EntityComponentSystem *system,
                                     EntityId id, void *component,
                                     uint32_t typeId )
{
  uint32_t hashId = HashShiftMult32( id );
  ASSERT( hashId );
  uint32_t idx = hashId % system->capacity;
  uint32_t k = 0;
  int sign = 1;
  for ( uint32_t attempts = 0; attempts < MAX_ENTITY_HASH_COLLISIONS;
        ++attempts )
  {
    if ( system->entityIds[idx] == 0 )
    {
      system->entityIds[idx] = id;
      *(EntityId*)component = id;
      auto mapping = AllocateMapping( system, component, typeId );
      system->mappings[idx] = mapping;
      system->size++;
      return;
    }
    else if ( system->entityIds[idx] == id )
    {
      auto mapping = system->mappings[idx];
      if ( !mapping )
      {
        *(EntityId*)component = id;
        system->mappings[idx] = AllocateMapping( system, component, typeId );
        return;
      }
      while ( mapping )
      {
        if ( mapping->typeId == typeId )
        {
          ASSERT( 0 );
          // ERROR: A component of the same type is already attached.
          return;
        }
        if ( !mapping->next )
        {
          *(EntityId*)component = id;
          mapping->next = AllocateMapping( system, component, typeId );
          return;
        }
        mapping = mapping->next;
      }
    }
    if ( sign > 0 )
    {
      k++;
    }
    idx = ( hashId + sign * k * k ) % system->capacity;
    sign *= -1;
  }
  ASSERT( 0 );
  // ERROR: Failed to attach component.
}

struct RemoveEntityResult
{
  uint32_t typeId;
  void *component;
};

inline uint32_t GetEntityIdGeneration( EntityId entity )
{
  return ( entity & ENTITY_GENERATION_MASK ) >> 24;
}

inline uint32_t GetEntityIdRawId( EntityId entity )
{
  return entity & ENTITY_ID_MASK;
}

inline EntityId ConstructEntityId( uint32_t generation, uint32_t id )
{
  return ( generation << 24 ) | id;
}

internal uint32_t RemoveEntity( EntityComponentSystem *system, EntityId id,
                                RemoveEntityResult *results, uint32_t size )
{
  uint32_t hashId = HashShiftMult32( id );
  ASSERT( hashId );
  uint32_t idx = hashId % system->capacity;
  uint32_t overwriteIdx = 0, prevIdx = 0;
  bool found = false;
  int i = 0;
  int sign = 1;
  uint32_t count = 0;
  for ( uint32_t attempts = 0; attempts < MAX_ENTITY_HASH_COLLISIONS;
        ++attempts )
  {
    if ( system->entityIds[idx] == 0 )
    {
      break;
    }

    if ( system->entityIds[idx] == id )
    {
      auto mapping = system->mappings[idx];
      while ( mapping )
      {
        ASSERT( count < size );
        results[count].component = mapping->component;
        results[count].typeId = mapping->typeId;
        count++;
        auto prev = mapping;
        mapping = mapping->next;
        FreeMapping( system, prev );
      }
      system->mappings[idx] = NULL;
      overwriteIdx = idx;
      found = true;
    }
    if ( sign > 0 )
    {
      i++;
    }
    idx = ( hashId + sign * i * i ) % system->capacity;
    sign *= -1;
  }
  if ( found )
  {
    if ( overwriteIdx != prevIdx )
    {
      system->entityIds[overwriteIdx] = system->entityIds[prevIdx];
      system->entityIds[prevIdx] = 0;
      system->mappings[overwriteIdx] = system->mappings[prevIdx];
    }
    else
    {
      system->entityIds[overwriteIdx] = 0;
    }
    system->size--;
  }
  uint32_t generation = ( GetEntityIdGeneration( id ) + 1 ) % 0xFF;
  uint32_t rawId = GetEntityIdRawId( id );
  id = ConstructEntityId( generation, rawId );
  system->entityIdFreeListTail = ( system->entityIdFreeListTail + 1 ) %
                                 ARRAY_COUNT( system->entityIdFreeList );
  ASSERT( system->entityIdFreeListTail != system->entityIdFreeListHead );
  // TODO: Fix crash here
  system->entityIdFreeList[system->entityIdFreeListTail] = id;
  system->numEntities--;
  return count;
}

#define GetEntityComponent( TABLE, ID, TYPE )                                  \
  ( TYPE * ) GetEntityComponent_( TABLE, ID, TYPE##TypeId )

internal void *GetEntityComponent_( EntityComponentSystem *system,
                                    EntityId id, uint32_t typeId )
{
  if ( !id )
  {
    return NULL;
  }
  uint32_t hashId = HashShiftMult32( id );
  ASSERT( hashId );
  uint32_t idx = hashId % system->capacity;
  uint32_t k = 0;
  int sign = 1;
  for ( uint32_t attempts = 0; attempts < MAX_ENTITY_HASH_COLLISIONS;
        ++attempts )
  {
    if ( system->entityIds[idx] == id )
    {
      auto mapping = system->mappings[idx];
      while ( mapping )
      {
        if ( mapping->typeId == typeId )
        {
          return mapping->component;
        }
        mapping = mapping->next;
      }
      // ERROR: Could not find component of that type.
      return NULL;
    }
    else if ( system->entityIds[idx] == 0 )
    {
      // ERROR: Could not find entity.
      return NULL;
    }
    if ( sign > 0 )
    {
      k++;
    }
    idx = ( hashId + sign * k * k ) % system->capacity;
    sign *= -1;
  }
  // ERROR: Too many collisions.
  return NULL;
}

#define DetachEntityComponent( TABLE, ID, TYPE )                               \
  DetachEntityComponent_( TABLE, ID, TYPE##TypeId )

internal void DetachEntityComponent_( EntityComponentSystem *system,
                                     EntityId id, uint32_t typeId )
{
  uint32_t hashId = HashShiftMult32( id );
  ASSERT( hashId );
  uint32_t idx = hashId % system->capacity;
  uint32_t overwriteIdx = 0, prevIdx = 0;
  bool found = false;
  int i = 0;
  int sign = 1;
  for ( uint32_t attempts = 0; attempts < MAX_ENTITY_HASH_COLLISIONS;
        ++attempts )
  {
    if ( system->entityIds[idx] == 0 )
    {
      break;
    }

    if ( system->entityIds[idx] == id )
    {
      auto mapping = system->mappings[idx];
      EntityComponentMapping *prev = NULL;
      while ( mapping )
      {
        if ( mapping->typeId == typeId )
        {
          if ( prev )
          {
            prev->next = mapping->next;
          }
          else
          {
            system->mappings[idx] = mapping->next;
          }
          FreeMapping( system, mapping );
          break;
        }
        prev = mapping;
        mapping = mapping->next;
      }
      if ( system->mappings[idx] )
      {
        return;
      }
      overwriteIdx = idx;
      found = true;
    }
    if ( sign > 0 )
    {
      i++;
    }
    idx = ( hashId + sign * i * i ) % system->capacity;
    sign *= -1;
  }
  if ( found )
  {
    if ( overwriteIdx != prevIdx )
    {
      system->entityIds[overwriteIdx] = system->entityIds[prevIdx];
      system->entityIds[prevIdx] = 0;
      system->mappings[overwriteIdx] = system->mappings[prevIdx];
    }
    else
    {
      system->entityIds[overwriteIdx] = 0;
    }
    system->size--;
  }
}

internal void CreateEntityComponentSystem( EntityComponentSystem *entitySystem,
                                           MemoryArena *arena,
                                           uint32_t maxEntities,
                                           uint32_t maxMappings )
{
  ZeroPointerToStruct( entitySystem );
  entitySystem->capacity = maxEntities;
  entitySystem->mappingsPoolSize = maxMappings;
  entitySystem->entityIds = AllocateArray( arena, uint32_t, maxEntities );
  entitySystem->mappings =
    AllocateArray( arena, EntityComponentMapping *, maxEntities );
  entitySystem->pool =
    AllocateArray( arena, EntityComponentMapping, maxMappings );
  for ( uint32_t i = 0; i < maxMappings - 1; ++i )
  {
    entitySystem->pool[i].next = &entitySystem->pool[i + 1];
  }
  entitySystem->pool[maxMappings - 1].next = NULL;
  entitySystem->freeListHead = entitySystem->pool;

  entitySystem->entityIdFreeListHead = 0;
  entitySystem->entityIdFreeListTail =
    ARRAY_COUNT( entitySystem->entityIdFreeList ) - 1;
  for ( uint32_t i = 0; i < MAX_ENTITIES; ++i )
  {
    entitySystem->entityIdFreeList[i] = ConstructEntityId( 0, i + 1 );
  }
}

internal void RemoveAllEntities( EntityComponentSystem *system )
{
  for ( uint32_t i = 0; i < system->capacity; ++i )
  {
    system->entityIds[i] = NULL_ENTITY;
    system->mappings[i] = NULL;
  }
  for ( uint32_t i = 0; i < system->mappingsPoolSize - 1; ++i )
  {
    system->pool[i].next = &system->pool[i+1];
  }
  system->pool[system->mappingsPoolSize - 1].next = NULL;
  system->freeListHead = system->pool;

  for ( uint32_t i = 0; i < MAX_COMPONENT_TYPES; ++i )
  {
    system->componentPools[i].size = 0;
  }
  system->entityIdFreeListHead = 0;
  system->entityIdFreeListTail = MAX_ENTITIES - 1;
  for ( uint32_t i = 0; i < MAX_ENTITIES; ++i )
  {
    system->entityIdFreeList[i] = ConstructEntityId( 0, i + 1 );
  }
}


internal void RegisterEntityComponent_( EntityComponentSystem *system,
                                        MemoryArena *arena, uint32_t capacity,
                                        uint32_t componentSize,
                                        uint32_t typeId )
{
  ASSERT( typeId < MAX_COMPONENT_TYPES );
  auto memory = MemoryArenaAllocate( arena, capacity * componentSize );
  system->componentPools[typeId] =
    CreateContiguousObjectPool( memory, capacity, componentSize );
}

internal void *CreateEntityComponent_( EntityComponentSystem *system,
                                       uint32_t typeId )
{
  ASSERT( typeId < MAX_COMPONENT_TYPES );
  auto pool = &system->componentPools[typeId];
  auto component = AllocateObject( pool );
  *(EntityId*)component = NULL_ENTITY;
  return component;
}

internal void *DestroyEntityComponent_( EntityComponentSystem *system,
                                        uint32_t typeId, void *component )
{
  ASSERT( typeId < MAX_COMPONENT_TYPES );
  auto pool = &system->componentPools[typeId];
  return FreeObject( pool, component );
}

internal void *AddEntityComponent_( EntityComponentSystem *system,
                                    uint32_t typeId, EntityId owner )
{
  auto result = CreateEntityComponent_( system, typeId );
  AttachEntityComponent_( system, owner, result, typeId );
  return result;
}

internal void RemoveEntityComponent_( EntityComponentSystem *system,
                                      uint32_t typeId,  EntityId owner )
{
  ASSERT( typeId < MAX_COMPONENT_TYPES );
  auto component = GetEntityComponent_( system, owner, typeId );
  if ( component )
  {
    DetachEntityComponent_( system, owner, typeId );
    component = DestroyEntityComponent_( system, typeId, component );
    if ( component )
    {
      owner = *(EntityId*)component;
      DetachEntityComponent_( system, owner, typeId );
      AttachEntityComponent_( system, owner, component, typeId );
    }
  }
}

inline EntityId CreateEntity( EntityComponentSystem *system )
{
  ASSERT( system->entityIdFreeListHead != system->entityIdFreeListTail );
  EntityId result = system->entityIdFreeList[system->entityIdFreeListHead];
  system->entityIdFreeListHead = ( system->entityIdFreeListHead + 1 ) %
                                 ARRAY_COUNT( system->entityIdFreeList );
  system->numEntities++;
  return result;
}

inline void RemoveEntity( EntityComponentSystem *system, EntityId entity )
{
  ASSERT( system->entityGarbageQueueLength < MAX_ENTITY_GARBAGE_QUEUE_LENGTH );
  system->entityGarbageQueue[system->entityGarbageQueueLength++] = entity;
}

internal void GarbageCollectEntities( EntityComponentSystem *entitySystem,
                                      SimpleEventQueue *eventQueue,
                                      MemoryArena *arena )
{
  for ( uint32_t i = 0; i < entitySystem->entityGarbageQueueLength; ++i )
  {
    auto entity = entitySystem->entityGarbageQueue[i];

    RemoveEntityResult buffer[128];
    auto n = RemoveEntity( entitySystem, entity, buffer,
                           ARRAY_COUNT( buffer ) );
    for ( uint32_t componentIdx = 0; componentIdx < n; ++componentIdx )
    {
      auto data = buffer + componentIdx;
      ASSERT( data->typeId < MAX_COMPONENT_TYPES );

      auto pool = &entitySystem->componentPools[data->typeId];

      DestroyEntityComponentEvent event = {};
      event.componentTypeId = data->typeId;
      event.componentData = MemoryArenaAllocate( arena, pool->objectSize );
      memcpy( event.componentData, data->component, pool->objectSize );
      evt_Push( eventQueue, &event, DestroyEntityComponentEvent );

      void *newPointer = FreeObject( pool, data->component );
      if ( newPointer )
      {
        EntityId owner = *(EntityId*)newPointer;
        DetachEntityComponent_( entitySystem, owner, data->typeId );
        AttachEntityComponent_( entitySystem, owner, newPointer, data->typeId );
      }
    }
  }
  entitySystem->entityGarbageQueueLength = 0;
}

#define NumComponents( SYSTEM, TYPE ) NumComponents_( SYSTEM, TYPE##TypeId )
inline uint32_t NumComponents_( EntityComponentSystem *system, uint32_t typeId )
{
  ASSERT( typeId < MAX_COMPONENT_TYPES );
  return system->componentPools[typeId].size;
}

#define ComponentsArray( SYSTEM, TYPE )                                        \
  ( TYPE * ) ComponentsArray_( SYSTEM, TYPE##TypeId )
inline void* ComponentsArray_( EntityComponentSystem *system, uint32_t typeId )
{
  ASSERT( typeId < MAX_COMPONENT_TYPES );
  return system->componentPools[typeId].start;
}

inline void* ComponentsBegin_( EntityComponentSystem *system, uint32_t typeId )
{
  ASSERT( typeId < MAX_COMPONENT_TYPES );
  return system->componentPools[typeId].start;
}

inline void* ComponentsEnd_( EntityComponentSystem *system, uint32_t typeId )
{
  ASSERT( typeId < MAX_COMPONENT_TYPES );
  auto pool = system->componentPools[typeId];
  return (uint8_t*)pool.start + ( pool.size * pool.objectSize );
}

#define ForeachComponent( NAME, SYSTEM, TYPE )                                 \
  for ( TYPE *NAME = (TYPE *)ComponentsBegin_( SYSTEM, TYPE##TypeId );         \
        NAME != (TYPE *)ComponentsEnd_( SYSTEM, TYPE##TypeId ); ++NAME )

#define RegisterEntityComponent( SYSTEM, ARENA, CAPACITY, TYPE )               \
  RegisterEntityComponent_( SYSTEM, ARENA, CAPACITY, sizeof( TYPE ),           \
                            TYPE##TypeId )

#define CreateEntityComponent( SYSTEM, TYPE )                                  \
  ( TYPE * ) CreateEntityComponent_( SYSTEM, TYPE##TypeId )

#define DestroyEntityComponent( SYSTEM, TYPE, COMPONENT )                      \
  ( TYPE * ) DestroyEntityComponent_( SYSTEM, TYPE##TypeId, COMPONENT )

#define AddEntityComponent( SYSTEM, TYPE, ENTITY )                             \
  ( TYPE * ) AddEntityComponent_( SYSTEM, TYPE##TypeId, ENTITY )

#define RemoveEntityComponent( SYSTEM, TYPE, ENTITY )                          \
  RemoveEntityComponent_( SYSTEM, TYPE##TypeId, ENTITY )
