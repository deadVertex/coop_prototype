
internal NetComponent *
GetNetComponentWithNetId( EntityComponentSystem *entitySystem,
                          net_EntityId netEntityId )
{
  ForeachComponent( netComponent, entitySystem, NetComponent )
  {
    if ( netComponent->id == netEntityId )
    {
      return netComponent;
    }
  }
  return NULL;
}

