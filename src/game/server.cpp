#include "game.h"

#include "sv_entity_replication.cpp"

#include "common/ai_steering.cpp"

#include "game/rpc_system.cpp"

#define PLAYER_SPEED 130.0f
#define PLAYER_FRICTION 7.0f
#define PLAYER_HEALTH 200

#define BASIC_ENEMY_SPEED 80.0f

enum
{
  SV_GAME_STATE_UNINITIALIZED = 0,
  SV_GAME_STATE_WAITING_FOR_PLAYERS,
  SV_GAME_STATE_READY,
};

enum
{
  SV_CLIENT_STATE_CONNECTING = 0,
  SV_CLIENT_STATE_LOADING,
  SV_CLIENT_STATE_RECEIVING_UPDATES,
  SV_CLIENT_STATE_PICKING_HERO,
  SV_CLIENT_STATE_READY,
};

#define SV_INPUT_BUFFER_LEN 24

struct sv_GameClient
{
  int state;
  uint32_t id;

  char name[SV_GAME_CLIENT_NAME_LENGTH+1];

  uint32_t timestamp;
  uint32_t sequenceNumber;
  uint32_t expectedSequenceNumber;
  net_Input inputBuffer[SV_INPUT_BUFFER_LEN];
  uint32_t inputBufferLength;

  EntityId entity;
  float secondsUntilRespawn;

  int8_t chosenHero;

  RpcSystem rpcSystem;
};


struct sv_GameState
{
  uint32_t state;
  MemoryArena transArena;
  MemoryArena permArena;

  sv_GameClient clients[NET_MAX_CLIENTS];
  ContiguousObjectPool clientsPool;

  char mapName[MAP_NAME_LENGTH];

  entrep_Server entityServer;
  EntityComponentSystem entitySystem;

  uint32_t currentTick;

  GamePhysics *physics;

  float currentTime;
  uint32_t sphereShape;

  RandomNumberGenerator randomNumberGenerator;

  SimpleEventQueue eventQueue;
};

internal void sv_InitializeNetComponent( NetComponent *netComponent,
                                         entrep_Server *server,
                                         uint32_t entityTypeId )
{
  netComponent->id =
    entrep_AddEntity( server, netComponent->owner, entityTypeId );
  ASSERT( netComponent->id != NULL_NET_ENTITY_ID );
  netComponent->typeId = entityTypeId;
}

internal EntityId sv_AddPlayer( sv_GameState *gameState, vec3 position, uint32_t clientId, uint8_t heroType )
{
  auto physics = gameState->physics;
  auto entity = CreateEntity( &gameState->entitySystem );

  auto transformComponent = AddEntityComponent( &gameState->entitySystem,
                                                TransformComponent, entity );
  transformComponent->transform = tr_Translate( position );

  auto agentComponent =
    AddEntityComponent( &gameState->entitySystem, sv_AgentComponent, entity );
  agentComponent->clientId = clientId;
  agentComponent->character =
    physics->createCharacter( physics->physicsSystem, position );
  ASSERT( PhysIsHandleValid( agentComponent->character ) );

  auto netComponent =
    AddEntityComponent( &gameState->entitySystem, NetComponent, entity );
  sv_InitializeNetComponent( netComponent, &gameState->entityServer,
                             net_PlayerEntityTypeId );

  auto weaponControllerComponent = AddEntityComponent( &gameState->entitySystem, WeaponControllerComponent, entity );
  ASSERT( weaponControllerComponent );
  weaponControllerComponent->timeBetweenAttacks = 0.3f;
  weaponControllerComponent->damage = 30;

  auto healthComponent = AddEntityComponent( &gameState->entitySystem, HealthComponent, entity );
  ASSERT( healthComponent );
  healthComponent->team = PlayerTeam;
  switch ( heroType )
  {
    case HeroTypeStrongMan:
      healthComponent->maximum = 220;
      healthComponent->amount = 220;
      break;
    case HeroTypeBarmaid:
      healthComponent->maximum = 180;
      healthComponent->amount = 180;
      break;
    case HeroTypeCook:
      healthComponent->maximum = 180;
      healthComponent->amount = 180;
      break;
    default:
      ASSERT( !"Invalid hero type" );
      break;
  }

  auto heroComponent = AddEntityComponent( &gameState->entitySystem, HeroComponent, entity );
  ASSERT( heroComponent );
  heroComponent->type = heroType;

  return entity;
}

internal EntityId sv_AddBot( sv_GameState *gameState, vec3 position, uint8_t heroType )
{
  EntityId result = sv_AddPlayer( gameState, position, NET_NULL_CLIENT_ID, heroType );
  return result;
}

internal EntityId sv_AddBasicEnemy( sv_GameState *gameState, GamePhysics *physics, vec3 position )
{
  auto entity = CreateEntity( &gameState->entitySystem );

  auto transformComponent = AddEntityComponent( &gameState->entitySystem,
                                                TransformComponent, entity );
  transformComponent->transform = tr_Translate( position );

  auto agentComponent =
    AddEntityComponent( &gameState->entitySystem, sv_AgentComponent, entity );
  agentComponent->character =
    physics->createCharacter( physics->physicsSystem, position );
  ASSERT( PhysIsHandleValid( agentComponent->character ) );

  auto netComponent =
    AddEntityComponent( &gameState->entitySystem, NetComponent, entity );
  sv_InitializeNetComponent( netComponent, &gameState->entityServer,
                             net_BasicEnemyEntityTypeId );

  auto weaponControllerComponent = AddEntityComponent( &gameState->entitySystem, WeaponControllerComponent, entity );
  ASSERT( weaponControllerComponent );
  weaponControllerComponent->timeBetweenAttacks = 1.2f;
  weaponControllerComponent->damage = 15;

  auto healthComponent = AddEntityComponent( &gameState->entitySystem, HealthComponent, entity );
  ASSERT( healthComponent );
  healthComponent->amount = 100;
  healthComponent->maximum = 100;
  healthComponent->team = WorldTeam;

  auto brainComponent =
    AddEntityComponent( &gameState->entitySystem, BrainComponent, entity );
  ASSERT( brainComponent );

  return entity;
}

internal bool sv_SerializeEntityCreateData( EntityComponentSystem *entitySystem,
                                            EntityId entityId,
                                            net_EntityId netEntityId,
                                            uint32_t netEntityTypeId,
                                            Bitstream *bitstream,
                                            uint32_t clientId )
{
  if ( bitstream_WriteInt( bitstream, net_EntityOpCreate,
                           NET_ENTITY_OP_NUM_BITS ) )
  {
    if ( bitstream_WriteInt( bitstream, netEntityId,
                             NET_ENTITY_ID_NUM_BITS ) )
    {
      if ( bitstream_WriteInt( bitstream, netEntityTypeId,
                               NET_ENTITY_TYPEID_NUM_BITS ) )
      {
        if ( netEntityTypeId == net_PlayerEntityTypeId )
        {
          auto transformComponent =
            GetEntityComponent( entitySystem, entityId, TransformComponent );
          ASSERT( transformComponent );

          auto agentComponent =
            GetEntityComponent( entitySystem, entityId, sv_AgentComponent );
          ASSERT( agentComponent );

          auto heroComponent = GetEntityComponent( entitySystem, entityId, HeroComponent );
          ASSERT( heroComponent );

          net_PlayerEntityCreateData data;
          data.position = tr_GetPosition( transformComponent->transform );
          data.heroType = heroComponent->type;
          data.isOwner = ( agentComponent->clientId == clientId ) ? 1 : 0;
          return net_PlayerEntityCreateDataSerialize( bitstream, &data );
        }
        else if ( netEntityTypeId == net_BasicEnemyEntityTypeId )
        {
          auto transformComponent =
            GetEntityComponent( entitySystem, entityId, TransformComponent );
          ASSERT( transformComponent );


          net_BasicEnemyEntityCreateData data;
          data.position = tr_GetPosition( transformComponent->transform );
          return net_BasicEnemyEntityCreateDataSerialize( bitstream, &data );
        }
        else
        {
          ASSERT( !"Invalid netEntityTypeId!" );
        }
      }
    }
  }
  return false;
}
//
// This is the maximum amount of entity create data per entity
#define NET_ENTITY_STREAM_BUFFER_LEN 50
internal bool
sv_SerializeCreateEntityOp( net_EntityId netEntityId, uint32_t clientId,
                            EntityComponentSystem *entitySystem,
                            Bitstream *packetBitstream )
{
  uint8_t buffer[NET_ENTITY_STREAM_BUFFER_LEN];
  Bitstream bitstream;

  bitstream_InitForWriting( &bitstream, buffer,
                            NET_ENTITY_STREAM_BUFFER_LEN );
  auto netComponent =
    GetNetComponentWithNetId( entitySystem, netEntityId );
  ASSERT( netComponent );

  if ( !sv_SerializeEntityCreateData(
    entitySystem, netComponent->owner, netComponent->id,
    netComponent->typeId, &bitstream, clientId ) )
  {
    ASSERT( !"Too much data for buffer" );
  }
  if ( !bitstream_WriteBits( packetBitstream, bitstream.data,
    bitstream.size ) )
  {
    // Trace: Too much data for packet
    return false;
  }
  return true;
}

internal bool sv_SerializeEntityDestroyData( net_EntityId netEntityId,
                                             Bitstream *stream )
{
  if ( !bitstream_WriteInt( stream, net_EntityOpDestroy,
                            NET_ENTITY_OP_NUM_BITS ) )
  {
    return false;
  }
  return bitstream_WriteInt( stream, netEntityId, NET_ENTITY_ID_NUM_BITS );
}

internal bool
  sv_SerializeDestroyEntityOp( uint32_t clientId, net_EntityId netEntityId,
                               EntityComponentSystem *entitySystem,
                               Bitstream *packetBitstream )
{
  uint8_t buffer[NET_ENTITY_STREAM_BUFFER_LEN];
  Bitstream bitstream;

  uint32_t count = 0;
  bitstream_InitForWriting( &bitstream, buffer,
                            NET_ENTITY_STREAM_BUFFER_LEN );

  if ( !sv_SerializeEntityDestroyData( netEntityId, &bitstream ) )
  {
    ASSERT( !"Too much data for buffer" );
  }
  if ( !bitstream_WriteBits( packetBitstream, bitstream.data,
    bitstream.size ) )
  {
    // Trace: Too much data for packet
    return false;
  }
  return true;
}

internal bool sv_SerializeEntityUpdateData( EntityComponentSystem *entitySystem,
                                            EntityId entityId,
                                            net_EntityId netEntityId,
                                            uint32_t netEntityTypeId,
                                            Bitstream *bitstream )
{
  if ( !bitstream_WriteInt( bitstream, net_EntityOpUpdate,
                            NET_ENTITY_OP_NUM_BITS ) )
  {
    return false;
  }
  if ( !bitstream_WriteInt( bitstream, netEntityId,
                            NET_ENTITY_ID_NUM_BITS ) )
  {
    return false;
  }
  if ( !bitstream_WriteInt( bitstream, netEntityTypeId,
                            NET_ENTITY_TYPEID_NUM_BITS ) )
  {
    return false;
  }
  if ( netEntityTypeId == net_PlayerEntityTypeId )
  {
    auto transformComponent =
      GetEntityComponent( entitySystem, entityId, TransformComponent );
    ASSERT( transformComponent );

    auto agentComponent =
      GetEntityComponent( entitySystem, entityId, sv_AgentComponent );
    ASSERT( agentComponent );

    auto healthComponent = GetEntityComponent( entitySystem, entityId, HealthComponent );
    ASSERT( healthComponent );

    net_PlayerEntityUpdateData data;
    data.position = tr_GetPosition( transformComponent->transform );
    data.velocity = agentComponent->velocity;
    data.health = healthComponent->amount;
    return net_PlayerEntityUpdateDataSerialize( bitstream, &data );
  }
  else if ( netEntityTypeId == net_BasicEnemyEntityTypeId )
  {
    auto transformComponent =
      GetEntityComponent( entitySystem, entityId, TransformComponent );
    ASSERT( transformComponent );

    auto agentComponent =
      GetEntityComponent( entitySystem, entityId, sv_AgentComponent );
    ASSERT( agentComponent );

    auto healthComponent = GetEntityComponent( entitySystem, entityId, HealthComponent );
    ASSERT( healthComponent );

    net_BasicEnemyEntityUpdateData data;
    auto p = tr_GetPosition( transformComponent->transform );
    auto v = agentComponent->velocity;
    data.position = Vec2( p.x, p.z );
    data.velocity = Vec2( v.x, v.z );
    data.health = healthComponent->amount;
    return net_BasicEnemyEntityUpdateDataSerialize( bitstream, &data );
  }
  else
  {
    ASSERT( !"Invalid netEntityTypeId!" );
  }
  return true;
}

internal bool
  sv_SerializeUpdateEntityOp( entrep_Server *server, net_EntityId netEntityId, uint32_t clientId,
                              EntityComponentSystem *entitySystem,
                              Bitstream *packetBitstream )
{
    uint8_t buffer[ NET_ENTITY_STREAM_BUFFER_LEN ];
    Bitstream bitstream;

    bitstream_InitForWriting( &bitstream, buffer,
                              NET_ENTITY_STREAM_BUFFER_LEN );
    auto netComponent =
      GetNetComponentWithNetId( entitySystem, netEntityId );
    ASSERT( netComponent );

    if ( !sv_SerializeEntityUpdateData(
      entitySystem, netComponent->owner, netComponent->id,
      netComponent->typeId, &bitstream ) )
    {
      ASSERT( !"Too much data for buffer" );
    }
    if ( !bitstream_WriteBits( packetBitstream, bitstream.data,
      bitstream.size ) )
    {
      // Trace: Too much data for packet
      return false;;
    }
    entrep_ResetEntityUpdatePriority( server, clientId, &netEntityId, 1 );
    return true;
  }

internal bool sv_WriteEntityDataPacketChunk( entrep_Server *server, uint32_t clientId,
                                             EntityComponentSystem *entitySystem,
                                             Bitstream *packetBitstream )
{
  Bitstream bitstream;
  uint32_t count = 0;
  uint8_t buffer[256];
  uint32_t maxChunkSize = MinVal( bitstream_GetNumBytesRemaining( packetBitstream ), sizeof( buffer ) );
  bitstream_InitForWriting( &bitstream, buffer, maxChunkSize );
  if ( bitstream_WriteInt( &bitstream, net_EntityChunkId, NET_GAME_PACKET_CHUNK_ID_NUM_BITS ) )
  {
    int chunkLengthLocation = bitstream_ReserveBits( &bitstream, NET_GAME_PACKET_CHUNK_LENGTH_NUM_BITS );
    if ( chunkLengthLocation >= 0 )
    {
      auto start = bitstream.size;
      int countLocation =
        bitstream_ReserveBits( &bitstream, NET_ENTITY_COUNT_NUM_BITS );
      if ( countLocation >= 0 )
      {
        entrep_QueueEntry queue[ NET_MAX_ENTITIES ];
        uint32_t queueLength = entrep_GetEntityQueue( server, clientId, queue, ARRAY_COUNT( queue ) );
        bool isPacketFull = false;
        for ( uint32_t i = 0; i < queueLength; ++i )
        {
          auto entry = queue + i;

          if ( !isPacketFull )
          {
            switch ( entry->state )
            {
              case CLIENT_ENTITY_STATE_TO_CREATE:
                if ( sv_SerializeCreateEntityOp( entry->netEntityId, clientId, entitySystem, &bitstream ) )
                {
                  count++;
                }
                else
                {
                  isPacketFull = true;
                }
                break;
              case CLIENT_ENTITY_STATE_CREATED:
                if ( sv_SerializeUpdateEntityOp( server, entry->netEntityId, clientId, entitySystem, &bitstream ) )
                {
                  count++;
                }
                else
                {
                  isPacketFull = true;
                  entrep_IncreaseEntityUpdatePriority( server, clientId, &entry->netEntityId, 1 );
                }
                break;
              case CLIENT_ENTITY_STATE_TO_DESTROY:
                if ( sv_SerializeDestroyEntityOp( entry->netEntityId, clientId, entitySystem, &bitstream ) )
                {
                  count++;
                }
                else
                {
                  isPacketFull = true;
                }
                break;
              default:
                /* Not interested in other states */
                break;
            }
          }
          else
          {
            if ( entry->state == CLIENT_ENTITY_STATE_CREATED )
            {
              entrep_IncreaseEntityUpdatePriority( server, clientId, &entry->netEntityId, 1 );
            }
          }
        }

        entrep_AdvanceEntityStatesFromQueue( server, clientId, queue, count );

        if ( !bitstream_WriteBitsAtLocation(
          &bitstream, &count, NET_ENTITY_COUNT_NUM_BITS, countLocation ) )
        {
          ASSERT( !"Write should not fail since bits have already been successfully reserved" );
        }
      }

      uint32_t length = bitstream.size - start;
      if ( !bitstream_WriteBitsAtLocation( &bitstream, &length, NET_GAME_PACKET_CHUNK_LENGTH_NUM_BITS, chunkLengthLocation ) )
      {
        ASSERT( !"Write should not fail since bits have already been successfully reserved" );
      }
    }
  }

  // Only write the chunk if we have successfully writen at least one entity entry
  if ( count > 0 )
  {
    // Write entire chunk to bitstream
    if ( bitstream_WriteBits( packetBitstream, buffer, bitstream_GetLengthInBits( &bitstream ) ) )
    {
      return true;
    }
  }
  return false;
}

internal void sv_QueueInput( sv_GameState *gameState, net_Input input,
                             uint32_t clientId )
{
  for ( uint32_t i = 0; i < NET_MAX_CLIENTS; ++i )
  {
    auto client = gameState->clients + i;
    if ( client->id == clientId )
    {
      if ( client->inputBufferLength < SV_INPUT_BUFFER_LEN )
      {
        client->inputBuffer[client->inputBufferLength++] = input;
      }
      else
      {
        LOG_WARNING( "InputBuffer for client %d is full!", clientId );
      }
      break;
    }
  }
}

internal void sv_HandlePacketReceived( sv_GameState *gameState,
                                       void *eventData )
{
  ReceivePacketEvent *event = (ReceivePacketEvent *)eventData;
  Bitstream bitstream = {};
  bitstream_InitForReading( &bitstream, event->packet.data, event->packet.len );

  // Find client entry
  auto clientId = event->packet.clientId;
  sv_GameClient *client = NULL;
  for ( uint32_t i = 0; i < gameState->clientsPool.size; ++i )
  {
    auto tempClient = gameState->clients + i;
    if ( tempClient->id == clientId )
    {
      client = tempClient;
      break;
    }
  }
  if ( !client )
  {
    LOG_WARNING( "Received packet from unknown client id %d", clientId );
    return;
  }

  uint32_t packetType = 0;
  if ( bitstream_ReadBits( &bitstream, &packetType, NET_PACKET_TYPE_NUM_BITS ) )
  {
    if ( packetType == net_ConnectionPacketType )
    {
      uint8_t connectionOp = 0;
      if ( bitstream_ReadBits( &bitstream, &connectionOp, NET_CONNECTION_OP_NUM_BITS ) )
      {
        if ( connectionOp == net_ConnectionOpClientInfo )
        {
          if ( client->state == SV_CLIENT_STATE_CONNECTING )
          {
            net_ConnectionOpClientInfoData data;
            if ( net_ConnectionOpClientInfoDataDeserialize( &bitstream, &data ) )
            {
              strncpy( client->name, data.name, data.nameLength );
              LOG_INFO( "Client '%s' has connected", client->name );
              client->state = SV_CLIENT_STATE_LOADING;
              // TODO: Tell client which level to load
            }
            else
            {
              LOG_WARNING( "Invalid client info" );
            }
          }
        }
        else if ( connectionOp == net_ConnectionOpLoadMapResponse )
        {
          if ( client->state == SV_CLIENT_STATE_LOADING )
          {
            client->state = SV_CLIENT_STATE_RECEIVING_UPDATES;
            entrep_AddClient( &gameState->entityServer, clientId );
          }
        }
        else
        {
          LOG_WARNING( "Unhandled connection op %d", connectionOp );
          // TODO: Advance client state
        }
      }
    }
    else if ( packetType == net_GamePacketType )
    {
      uint32_t chunkCount = 0;
      if ( bitstream_ReadBits( &bitstream, &chunkCount, NET_GAME_PACKET_CHUNK_COUNT_NUM_BITS ) )
      {
        for ( uint32_t i = 0; i < chunkCount; ++i )
        {
          uint8_t chunkId = 0;
          if ( bitstream_ReadBits( &bitstream, &chunkId, NET_GAME_PACKET_CHUNK_ID_NUM_BITS ) )
          {
            uint32_t chunkLength = 0;
            if ( bitstream_ReadBits( &bitstream, &chunkLength, NET_GAME_PACKET_CHUNK_LENGTH_NUM_BITS ) )
            {
              uint32_t start = bitstream.readCursor;
              if ( chunkId == net_InputChunkId )
              {
                if ( gameState->state == SV_GAME_STATE_READY )
                {
                  net_Input netInput = {};
                  net_InputDeserialize( &bitstream, &netInput );
                  sv_QueueInput( gameState, netInput, clientId );
                }
                // Discard received input if game is not running
              }
              else if ( chunkId == net_RpcParametersChunkId )
              {
                com_HandleRpcParameters( &client->rpcSystem, &bitstream );
              }
              else if ( chunkId == net_RpcResultsChunkId )
              {
                com_HandleRpcResults( &client->rpcSystem, &bitstream );
              }
              else
              {
                LOG_WARNING( "Unrecognized chunk id %d received from client %d", chunkId, client->id );
              }

              // Move read cursor to start of next chunk even if an error was encountered while parsing the chunk
              uint32_t nextChunkStart = start + chunkLength;
              if ( nextChunkStart < bitstream.size )
              {
                bitstream.readCursor = nextChunkStart;
              }
            }
          }
        }
      }
    }
    else
    {
      LOG_WARNING( "Received unknown packet type %d", packetType );
    }
  }
}

internal void sv_HandleClientConnection( sv_GameState *gameState, void *eventData )
{
  ClientConnectedEvent *event = (ClientConnectedEvent *)eventData;

  auto client = (sv_GameClient *)AllocateObject( &gameState->clientsPool );
  if ( client )
  {
    client->id = event->clientId;
    client->state = SV_CLIENT_STATE_CONNECTING;
    net_InitializeRpcSystem( &client->rpcSystem );
    //evtstream_Init( &client->eventStream, &gameState->testArena );

    LOG_INFO( "Client %d has connected", event->clientId );
  }
  // TODO: Should probably notify client that we are full
}

internal sv_GameClient *sv_GetClient( sv_GameState *gameState, uint32_t clientId )
{
  for ( uint32_t i = 0; i < gameState->clientsPool.size; ++i )
  {
    auto client = gameState->clients + i;
    if ( client->id == clientId )
    {
      return client;
    }
  }
  return NULL;
}

internal void sv_RpcChangeHero( sv_GameClient *client, net_GameRpcChangeHeroParameters params )
{
  net_GameRpcChangeHeroResult result = {};
  if ( client->state == SV_CLIENT_STATE_PICKING_HERO )
  {
    client->chosenHero = params.hero;
    client->state = SV_CLIENT_STATE_READY;
  }
  result.code = 0;
  net_SendRpcResult( &client->rpcSystem, &result, net_GameRpcChangeHero );
}

internal void sv_ProcessIncomingRpcs( sv_GameState *gameState, sv_GameClient *client )
{
  auto rpcSystem = &client->rpcSystem;

  bool stop = false;

  // Process received RPC parameters
  while ( !stop )
  {
    stop = true;
    RpcQueueHeader header = {};
    if ( ByteQueueRead( &rpcSystem->incomingParameters, &header, sizeof( header ) ) )
    {
      uint8_t parametersBuffer[ NET_GAME_RPC_DATA_LENGTH ];
      if ( ByteQueueRead( &rpcSystem->incomingParameters, parametersBuffer, header.size ) )
      {
        switch ( header.typeId )
        {
          case net_GameRpcInvalid:
            ASSERT( !"Invalid game RPC typeId" );
            break;
          case net_GameRpcChangeHero:
            sv_RpcChangeHero( client, *(net_GameRpcChangeHeroParameters*)parametersBuffer );
            break;
          default:
            ASSERT( !"Unknown game RPC" );
            break;
        }
      }
    }
  }

  // Process received RPC results
  stop = false;
  while ( !stop )
  {
    stop = true;
    RpcQueueHeader header = {};
    if ( ByteQueueRead( &rpcSystem->incomingResults, &header, sizeof( header ) ) )
    {
      uint8_t resultBuffer[ NET_GAME_RPC_DATA_LENGTH ];
      if ( ByteQueueRead( &rpcSystem->incomingResults, resultBuffer, header.size ) )
      {
        switch ( header.typeId )
        {
          case net_GameRpcInvalid:
            ASSERT( !"Invalid game RPC typeId" );
            break;
          default:
            ASSERT( !"Unknown game RPC" );
            break;
        }
      }
    }
  }
}

internal void sv_WritePacketForClient( sv_GameState *gameState,
                                       sv_GameClient *client,
                                       Packet *outgoingPacket )
{
  Bitstream bitstream;
  bitstream_InitForWriting( &bitstream, outgoingPacket->data,
      outgoingPacket->capacity );
  if ( ( client->state == SV_CLIENT_STATE_READY ) || ( client->state == SV_CLIENT_STATE_PICKING_HERO ) )
  {
    if ( bitstream_WriteInt( &bitstream, net_GamePacketType,
      NET_PACKET_TYPE_NUM_BITS ) )
    {
      if ( bitstream_WriteBits( &bitstream, &gameState->currentTick,
        NET_GAME_TICK_COUNT_NUM_BITS ) )
      {
        if ( bitstream_WriteBits( &bitstream, &client->sequenceNumber,
          NET_GAME_INPUT_SEQ_NUM_NUM_BITS ) )
        {
          uint32_t chunkCount = 0;
          int chunkCountLocation = bitstream_ReserveBits( &bitstream, NET_GAME_PACKET_CHUNK_COUNT_NUM_BITS );
          if ( chunkCountLocation >= 0 )
          {
            if ( com_WriteRpcParametersPacketChunk( &bitstream, &client->rpcSystem ) )
            {
              chunkCount++;
            }
            if ( com_WriteRpcResultsPacketChunk( &bitstream, &client->rpcSystem ) )
            {
              chunkCount++;
            }
            if ( sv_WriteEntityDataPacketChunk( &gameState->entityServer, client->id,
              &gameState->entitySystem, &bitstream ) )
            {
              chunkCount++;
            }
            bitstream_WriteBitsAtLocation( &bitstream, &chunkCount, NET_GAME_PACKET_CHUNK_COUNT_NUM_BITS, chunkCountLocation );
            outgoingPacket->len = bitstream_GetLengthInBytes( &bitstream );
          }
        }
      }
    }
  }
  else if ( client->state == SV_CLIENT_STATE_RECEIVING_UPDATES )
  {
    // TODO: Send updates but for now just set state to ready
    client->state = SV_CLIENT_STATE_PICKING_HERO;
    //client->state = SV_CLIENT_STATE_READY;
    //client->entity = sv_AddPlayer( gameState, Vec3( 0, 0.5, 0 ), client->id, HeroTypeStrongMan );
  }
  else if ( client->state == SV_CLIENT_STATE_LOADING )
  {
    if ( bitstream_WriteInt( &bitstream, net_ConnectionPacketType,
                        NET_PACKET_TYPE_NUM_BITS ) )
    {
      if ( bitstream_WriteInt( &bitstream, net_ConnectionOpLoadMap,
                               NET_CONNECTION_OP_NUM_BITS ) )
      {
        net_ConnectionOpLoadMapData data = {};
        data.clientId = client->id;
        data.mapNameLength = strlen( gameState->mapName );
        strncpy( data.mapName, gameState->mapName,
                 ARRAY_COUNT( data.mapName ) );
        if ( net_ConnectionOpLoadMapDataSerialize( &bitstream, &data ) )
        {
          outgoingPacket->len = bitstream_GetLengthInBytes( &bitstream );
        }
      }
    }
  }
}

internal void sv_RegisterEntityComponents( EntityComponentSystem *entitySystem,
                                           MemoryArena *arena )
{
  RegisterEntityComponent( entitySystem, arena, MAX_TRANSFORM_COMPONENTS,
                           TransformComponent );
  RegisterEntityComponent( entitySystem, arena, SV_MAX_AGENT_COMPONENTS,
                           sv_AgentComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_NET_COMPONENTS,
                           NetComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_HEALTH_COMPONENTS,
                           HealthComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_BRAIN_COMPONENTS,
                           BrainComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_HERO_COMPONENTS, HeroComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_WEAPON_CONTROLLER_COMPONENTS, WeaponControllerComponent );
}

internal void sv_Initialize( sv_GameState *gameState, GamePhysics *physics,
                             GameMemory *memory )
{
  MemoryArenaInitialize(
      &gameState->permArena,
      memory->persistentStorageSize - sizeof( sv_GameState ),
      (uint8_t *)memory->persistentStorageBase + sizeof( sv_GameState ) );

  gameState->clientsPool =
    CreateContiguousObjectPoolArray( gameState->clients );

  auto eventBuffer = MemoryArenaAllocate( &gameState->permArena, 4096 );
  gameState->eventQueue = evt_CreateQueue( eventBuffer, 4096 );

  strncpy( gameState->mapName, "Test map 01", sizeof( gameState->mapName ) );

  // TODO: These numbers could use formalizing
  CreateEntityComponentSystem( &gameState->entitySystem, &gameState->permArena,
                               4096, 0xFFFF );

  sv_RegisterEntityComponents( &gameState->entitySystem, &gameState->permArena );

  entrep_InitializeServer( &gameState->entityServer, NET_MAX_CLIENTS,
      NET_MAX_ENTITIES, &gameState->permArena );

  sv_AddBot( gameState, Vec3( 5, 0, 2 ), HeroTypeBarmaid );
  sv_AddBot( gameState, Vec3( 5, 0, 4 ), HeroTypeCook );
  {
    uint32_t plane = physics->createBoxShape( physics->physicsSystem,
        Vec3( 500, 500, 0.1 ) );
    PhysRigidBodyParameters params = {};
    params.position = Vec3( 0, -0.1, 0 );
    params.shapeHandle = plane;
    params.type = RIGID_BODY_STATIC;
    params.collidesWith = COLLISION_TYPE_ALL;

    physics->createRigidBody( physics->physicsSystem, params );
  }

  {
    uint32_t box =
      physics->createBoxShape( physics->physicsSystem, Vec3( 5, 2, 10 ) );
    PhysRigidBodyParameters params = {};
    params.position = Vec3( 15, 1, 5 );
    params.shapeHandle = box;
    params.type = RIGID_BODY_STATIC;
    params.collidesWith = COLLISION_TYPE_ALL;

    physics->createRigidBody( physics->physicsSystem, params );
  }

  gameState->sphereShape = physics->createSphereShape( physics->physicsSystem, 1.5f );

  // NOTE: Be careful that they don't get stuck in ground, otherwise they will
  // appear to not collide with anything.
  //sv_AddBasicEnemy( gameState, physics, Vec3( 25, 0, 2 ) );
  //sv_AddBasicEnemy( gameState, physics, Vec3( 25, 0, 3 ) );
  //sv_AddBasicEnemy( gameState, physics, Vec3( 25, 0, 4 ) );
  //sv_AddBasicEnemy( gameState, physics, Vec3( 25, 0, 5 ) );
  //sv_AddBasicEnemy( gameState, physics, Vec3( 25, 0, 6 ) );
  //sv_AddBasicEnemy( gameState, physics, Vec3( 25, 0, 7 ) );
  //sv_AddBasicEnemy( gameState, physics, Vec3( 25, 0, 8 ) );

  gameState->randomNumberGenerator =
    CreateRandomNumberGenerator( 0xBADA55, 0xFFF613 );

  gameState->state = SV_GAME_STATE_READY;
}

internal AgentState
  com_CalculateCharacterPosition( net_Input input, AgentState state,
                                  PhysicsHandle character, GamePhysics *physics,
                                  float speed,
                                  float dt )
{
  AgentState result = state;

  vec3 acceleration = {};
  if ( input.actions & INPUT_ACTION_MOVE_UP )
  {
    acceleration.y += 1.0f;
  }
  if ( input.actions & INPUT_ACTION_MOVE_DOWN )
  {
    acceleration.y -= 1.0f;
  }
  if ( input.actions & INPUT_ACTION_MOVE_LEFT )
  {
    acceleration.x -= 1.0f;
  }
  if ( input.actions & INPUT_ACTION_MOVE_RIGHT )
  {
    acceleration.x += 1.0f;
  }
  if ( LengthSq( acceleration ) > 1.0f )
  {
    acceleration = Normalize( acceleration );
  }
  acceleration *= speed;

  vec3 velocity = state.velocity + acceleration * dt -
                  ( state.velocity * PLAYER_FRICTION * dt );
  vec3 position = {};

  vec3 currentPosition = state.position;
  vec3 pushBackResult;
  if ( physics->performCharacterPushBack( physics->physicsSystem, character,
                                          &pushBackResult ) )
  {
    physics->setCharacterPosition( physics->physicsSystem, character,
                                   pushBackResult );
    currentPosition = pushBackResult;
  }

  vec3 playerDelta = velocity * dt;
  vec3 normal = {};
  float hitFraction = 0.0f;

  int maxIterations = 10;
  while ( maxIterations-- > 0 )
  {
    float playerDeltaLength = Length( playerDelta );
    if ( playerDeltaLength > 0.002f ) // TODO: Formalize this constant
    {
      auto targetPosition = currentPosition + playerDelta;
      physics->canCharacterMoveTo( physics->physicsSystem, character,
                                       targetPosition, NULL, &normal,
                                       &hitFraction );
      if ( hitFraction < 1.0f ) // Something in the way
      {
        // Move as close to the surface as possible plus a fudge value.
        currentPosition += playerDelta * ( hitFraction - 0.002f );
        playerDelta = targetPosition - currentPosition;

        // Clip velocity and delta to the wall so we can slide
        playerDelta = playerDelta - Dot( playerDelta, normal ) * normal;
        velocity = velocity - Dot( velocity, normal ) * normal;
      }
      else
      {
        break; // Completed a full move
      }
    }
    else
    {
      playerDelta = Vec3(0);
      break; // Break early to save cycles.
    }
  }
  if ( maxIterations > 0 )
  {
    position = currentPosition + playerDelta;
  }
  else
  {
    // If player can't slide to valid position then just prevent the move.
    position = currentPosition;
    velocity = Vec3( 0 );
  }
  physics->setCharacterPosition( physics->physicsSystem, character, position );

  result.velocity = velocity;
  result.position = position;

  return result;
}

internal sv_AgentComponent* sv_GetAgentComponentForPhysicsHandle( EntityComponentSystem *entitySystem, PhysicsHandle handle )
{
  ForeachComponent( agentComponent, entitySystem, sv_AgentComponent )
  {
    if ( ComparePhysicsHandles( agentComponent->character, handle ) )
    {
      return agentComponent;
    }
  }
  return NULL;
}

struct com_AttackResult
{
  ShapeCastMultiResult entries[ 8 ];
  uint32_t count;
  vec3 start;
  vec3 end;
  vec3 direction;
};

internal com_AttackResult com_ProcessAttack( net_Input input, vec3 position, GamePhysics *physics, uint32_t sphereShape )
{
  com_AttackResult result = {};

  vec3 start = Vec3( 0, 0, 0.0 );
  vec3 end = Vec3( 0, 0, 3 );
  mat4 rotation = RotateY( input.angle );
  vec4 startV4 = rotation * Vec4( start.x, start.y, start.z, 0 );
  vec4 endV4 = rotation * Vec4( end.x, end.y, end.z, 0 );
  start = position + startV4.xyz;
  end = position + endV4.xyz;

  // TODO: Start offset
  result.count = physics->shapeCastMulti( physics->physicsSystem,
                                    sphereShape, start, end,
                                    result.entries, ARRAY_COUNT( result.entries ) );

  result.start = start;
  result.end = end;
  result.direction = Normalize( endV4.xyz );

  return result;
}

internal void sv_UpdatePlayer( sv_GameState *gameState,
                               sv_AgentComponent *agentComponent,
                               net_Input input, float speed, float dt )
{
  auto transformComponent = GetEntityComponent(
    &gameState->entitySystem, agentComponent->owner, TransformComponent );
  ASSERT( transformComponent );
  
  auto healthComponent = GetEntityComponent( &gameState->entitySystem, agentComponent->owner, HealthComponent );
  ASSERT( healthComponent );

  auto weaponControllerComponent = GetEntityComponent( &gameState->entitySystem, agentComponent->owner, WeaponControllerComponent );
  ASSERT( weaponControllerComponent );

  AgentState state;
  state.position = tr_GetPosition( transformComponent->transform );
  state.velocity = agentComponent->velocity;
  auto result = com_CalculateCharacterPosition(
    input, state, agentComponent->character, gameState->physics, speed, dt );

  transformComponent->transform.position = result.position;
  agentComponent->velocity = result.velocity;

  debug_PushBox( result.position, Vec3( 1 ), Vec4( 0, 0.6, 0.9, 1 ) );

  if ( input.actions & INPUT_ACTION_PRIMARY_ATTACK )
  {
    if ( weaponControllerComponent->lastAttackTime + weaponControllerComponent->timeBetweenAttacks < gameState->currentTime )
    {
      com_AttackResult attackResult = com_ProcessAttack( input, result.position, gameState->physics, gameState->sphereShape );
      if ( attackResult.count > 0 )
      {
        for ( int i = 0; i < attackResult.count; ++i )
        {
          auto result = attackResult.entries + i;
          auto agentComponent = sv_GetAgentComponentForPhysicsHandle(
            &gameState->entitySystem, result->physicsHandle );
          if ( agentComponent )
          {
            // Check that we're not hitting ourselves
            if ( agentComponent->owner != transformComponent->owner )
            {
              auto targetHealthComponent =
                GetEntityComponent( &gameState->entitySystem,
                agentComponent->owner, HealthComponent );
              if ( targetHealthComponent )
              {
                if ( targetHealthComponent->team != healthComponent->team )
                {
                 int health = targetHealthComponent->amount;
                 health -= weaponControllerComponent->damage;
                  if ( health < 0 )
                  {
                    health = 0;
                    RemoveEntity( &gameState->entitySystem,
                                  targetHealthComponent->owner );
                    KillEntityEvent event = {};
                    event.owner = targetHealthComponent->owner;
                    event.team = targetHealthComponent->team;
                    evt_Push( &gameState->eventQueue, &event, KillEntityEvent );
                  }
                  targetHealthComponent->amount = ( uint16_t )health;
                }
              }
            }
          }
        }
      }

      weaponControllerComponent->lastAttackTime = gameState->currentTime;
    }
  }
}


internal void sv_ApplyInput( sv_GameClient *client, net_Input input,
    sv_GameState *gameState, float dt )
{
  if ( client->entity != NULL_ENTITY )
  {
    auto agentComponent = GetEntityComponent(
      &gameState->entitySystem, client->entity, sv_AgentComponent );
    if ( agentComponent )
    {

      if ( input.sequenceNumber != client->expectedSequenceNumber )
      {
        LOG_WARNING( "Processing out of order player command %d expected %d",
                     input.sequenceNumber, client->expectedSequenceNumber );
      }

      sv_UpdatePlayer( gameState, agentComponent, input, PLAYER_SPEED, dt );
    }
  }
  client->sequenceNumber = input.sequenceNumber;
  client->timestamp = input.timestamp;
  client->expectedSequenceNumber = client->sequenceNumber + 1;
}

inline uint32_t AmountToConsume( uint32_t length )
{
  if ( length > 0 )
  {
    if ( length > 3 ) // At 30Hz 3 updates is ~100ms
    {
      return 2;
    }
    return 1;
  }
  return 0;
}

internal int sv_CompareInput( const void *p0, const void *p1 )
{
  net_Input *a = (net_Input*)p0;
  net_Input *b = (net_Input*)p1;

  if ( a->sequenceNumber < b->sequenceNumber )
  {
    return 1;
  }
  else if ( a->sequenceNumber > b->sequenceNumber )
  {
    return -1;
  }
  return 0;
}

internal void sv_CleanClientInputBuffer( sv_GameClient *client )
{
  if ( client->inputBufferLength > 0 )
  {
    // TODO: More efficient way of keeping this buffer sorted.
    qsort( client->inputBuffer, client->inputBufferLength,
           sizeof( net_Input ), sv_CompareInput );

    uint32_t count = 0;
    for ( int j = client->inputBufferLength - 1; j >= 0; --j )
    {
      auto entry = client->inputBuffer + j;
      if ( entry->sequenceNumber <= client->sequenceNumber )
      {
        count++;
      }
    }
    client->inputBufferLength -= count;
  }
}

internal void sv_ProcessInput( sv_GameState *gameState,
                               float dt )
{
  for ( uint32_t i = 0; i < NET_MAX_CLIENTS; ++i )
  {
    auto client = gameState->clients + i;
    if ( client->id != 0 )
    {
      sv_CleanClientInputBuffer( client );

      uint32_t count = AmountToConsume( client->inputBufferLength );
      for ( uint32_t j = 0; j < count; ++j )
      {
        // reverse the index
        uint32_t inputIdx = ( client->inputBufferLength - 1 ) - j;

        auto input = client->inputBuffer[inputIdx];
        sv_ApplyInput( client, input, gameState, dt );
      }
      client->inputBufferLength -= count;
    }
  }
}

internal net_Input ai_ConvertStateToInput(ai_KinematicState currentState)
{
  net_Input result = {};
  if ( currentState.velocity.x < 0.0f )
  {
    result.actions |= INPUT_ACTION_MOVE_LEFT;
  }
  else if ( currentState.velocity.x > 0.0f )
  {
    result.actions |= INPUT_ACTION_MOVE_RIGHT;
  }

  if ( currentState.velocity.z < 0.0f )
  {
    result.actions |= INPUT_ACTION_MOVE_UP;
  }
  else if ( currentState.velocity.z > 0.0f )
  {
    result.actions |= INPUT_ACTION_MOVE_DOWN;
  }

  result.angle = currentState.orientation;

  return result;
}

struct FindNearestEnemyPlayerResult
{
  EntityId entity;
  vec3 position;
};

// NOTE: Optional parameter ignoredHandle for aiAgent physics handle.
internal FindNearestEnemyPlayerResult
  ai_FindNearestEnemyPlayer( sv_GameState *gameState, GamePhysics *gamePhysics,
                             vec3 currentPosition, float min,
                             PhysicsHandle *ignoredHandle = NULL )
{
  FindNearestEnemyPlayerResult result = {};
  for ( uint32_t i = 0; i < gameState->clientsPool.size; ++i )
  {
    auto client = gameState->clients + i;
    if ( client->entity != NULL_ENTITY )
    {
      auto transformComponent = GetEntityComponent(
        &gameState->entitySystem, client->entity, TransformComponent );
      auto agentComponent = GetEntityComponent(
        &gameState->entitySystem, client->entity, sv_AgentComponent );
      if ( transformComponent && agentComponent )
      {
        auto targetPosition = tr_GetPosition( transformComponent->transform );

        uint32_t size = 0;
        if ( ignoredHandle )
        {
          size = 1;
        }

        auto rayCastResult = gamePhysics->rayCast(
          gamePhysics->physicsSystem, currentPosition, targetPosition,
          ignoredHandle, size, COLLISION_TYPE_CHARACTER,
          COLLISION_TYPE_ALL );
        if ( ComparePhysicsHandles( rayCastResult.physicsHandle,
                               agentComponent->character ) )
        {
          float d = Length( targetPosition - currentPosition );
          if ( d < min )
          {
            min = d;
            result.entity = client->entity;
            result.position = targetPosition;
          }
        }
      }
    }
  }
  return result;
}

internal net_Input
  ai_ProcessWanderState( sv_GameState *gameState, GamePhysics *gamePhysics,
                       BrainComponent *brainComponent,
                       TransformComponent *transformComponent,
                       sv_AgentComponent *agentComponent )
{
  net_Input newInput = {};
  ai_KinematicState agentState, targetState;
  agentState.position = tr_GetPosition( transformComponent->transform );
  auto output = ai_Wander( &agentState, &gameState->randomNumberGenerator,
                           BASIC_ENEMY_SPEED );
  newInput = ai_ConvertStateToInput( agentState );

  return newInput;
}

internal void sv_UpdateBrainComponents( sv_GameState *gameState, float dt )
{
  ForeachComponent( brainComponent, &gameState->entitySystem, BrainComponent )
  {
    auto agentComponent = GetEntityComponent(
      &gameState->entitySystem, brainComponent->owner, sv_AgentComponent );
    ASSERT( agentComponent );
    auto transformComponent = GetEntityComponent(
      &gameState->entitySystem, brainComponent->owner, TransformComponent );
    ASSERT( transformComponent );

    net_Input input = {};

    auto currentPosition = tr_GetPosition( transformComponent->transform );
    FindNearestEnemyPlayerResult result =
      ai_FindNearestEnemyPlayer( gameState, gameState->physics, currentPosition,
                                 100.0f, &agentComponent->character );
    if ( result.entity != NULL_ENTITY )
    {
      ai_KinematicState agent = {};
      agent.position = currentPosition;
      ai_KinematicState target = {};
      target.position = result.position;
      auto seekOutput = ai_Seek( &agent, &target, 1.0f );
      agent.velocity = seekOutput.velocity;
      agent.orientation = seekOutput.orientation;
      input = ai_ConvertStateToInput( agent );
    }
    input.actions |= INPUT_ACTION_PRIMARY_ATTACK;

    vec3 v = Vec3( 0, 0, 3 );
    mat4 rotation = RotateY( input.angle );
    vec4 v4 = rotation * Vec4( v.x, v.y, v.z, 0 );
    v = Vec3( v4.x, v4.y, v4.z );
    debug_PushLine( currentPosition, currentPosition + v, Vec4( 0.7, 0.5, 0.1, 1 ) );

    sv_UpdatePlayer( gameState, agentComponent, input, BASIC_ENEMY_SPEED, dt );
  }
}

internal void sv_HandleKillEntityEvent( sv_GameState *gameState, void *eventData )
{
  KillEntityEvent *event = ( KillEntityEvent * )eventData;
  if ( event->team == PlayerTeam )
  {
    for ( uint32_t i = 0; i < gameState->clientsPool.size; ++i )
    {
      auto client = gameState->clients + i;
      if ( client->entity == event->owner )
      {
        client->entity = NULL_ENTITY;
        client->secondsUntilRespawn = 3.0f;
      }
    }
  }
}

internal void sv_ProcessGameEventQueue( sv_GameState *gameState )
{
  EventPeakResult result = evt_Peak( &gameState->eventQueue, NULL );
  while ( result.header )
  {
    switch ( result.header->typeId )
    {
      case KillEntityEventTypeId:
        sv_HandleKillEntityEvent( gameState, result.data );
        break;
      default:
        break;
    }
    result = evt_Peak( &gameState->eventQueue, result.header );
  }
}

extern "C" GAME_SERVER_UPDATE( sv_GameUpdate )
{
  ASSERT( memory->persistentStorageSize > sizeof( sv_GameState ) );
  sv_GameState *gameState = (sv_GameState *)memory->persistentStorageBase;
  MemoryArenaInitialize( &gameState->transArena, memory->transientStorageSize,
                         (uint8_t *)memory->transientStorageBase );

  gameState->physics = physics;

  if ( gameState->state == SV_GAME_STATE_UNINITIALIZED )
  {
    sv_Initialize( gameState, physics, memory );
  }

  if ( memory->wasCodeReloaded )
  {
    globalDebugState = NULL;
    memory->wasCodeReloaded = false;
  }

  EventPeakResult result = evt_Peak( eventQueue, NULL );
  while ( result.header )
  {
    switch ( result.header->typeId )
    {
      case ReceivePacketEventTypeId:
        sv_HandlePacketReceived( gameState, result.data );
        break;
      case PacketGeneratedEventTypeId:
        // Handled later
        break;
      case ClientConnectedEventTypeId:
        sv_HandleClientConnection( gameState, result.data );
        break;
      default:
        break;
    }
    result = evt_Peak( eventQueue, result.header );
  }

  for ( uint32_t i = 0; i < gameState->clientsPool.size; ++i )
  {
    auto client = gameState->clients + i;
    if ( client->state == SV_CLIENT_STATE_READY )
    {
      if ( client->entity == NULL_ENTITY )
      {
        if ( client->secondsUntilRespawn <= 0 )
        {
          // TODO: Store hero type in client data
          client->entity = sv_AddPlayer( gameState, Vec3( 0 ), client->id, client->chosenHero );
          client->secondsUntilRespawn = 0.0f;
        }
        else
        {
          client->secondsUntilRespawn -= dt;
        }
      }
    }
  }

  sv_ProcessInput( gameState, dt );

  for ( uint32_t i = 0; i < gameState->clientsPool.size; ++i )
  {
    auto client = gameState->clients + i;
    sv_ProcessIncomingRpcs( gameState, client );
  }

  sv_UpdateBrainComponents( gameState, dt );

  sv_ProcessGameEventQueue( gameState );

  physics->update( physics->physicsSystem, dt );

  // TODO: Update server game state
  entrep_ServerUpdate( &gameState->entityServer );

  result = evt_Peak( eventQueue, NULL );
  while ( result.header )
  {
    if ( result.header->typeId == PacketGeneratedEventTypeId )
    {
      auto *event = (PacketGeneratedEvent *)result.data;
      auto client = sv_GetClient( gameState, event->packet.clientId );
      if ( client )
      {
        sv_WritePacketForClient( gameState, client, &event->packet );
      }
      else
      {
        LOG_ERROR( "Cannot fill packet for unknown client %d",
            event->packet.clientId );
      }
    }
    result = evt_Peak( eventQueue, result.header );
  }

  void *tempBuffer = MemoryArenaAllocate( &gameState->transArena, 4096 );
  SimpleEventQueue componentGarbageQueue = evt_CreateQueue( tempBuffer, 4096 );
  GarbageCollectEntities( &gameState->entitySystem, &componentGarbageQueue,
                          &gameState->transArena );
  EventPeakResult peakResult = evt_Peak( &componentGarbageQueue, NULL );
  while ( peakResult.header )
  {
    switch ( peakResult.header->typeId )
    {
    case DestroyEntityComponentEventTypeId:
    {
      DestroyEntityComponentEvent *event =
        (DestroyEntityComponentEvent *)peakResult.data;
      if ( event->componentTypeId == NetComponentTypeId )
      {
        NetComponent *netComponent = (NetComponent*)event->componentData;
        if ( !entrep_RemoveEntity( &gameState->entityServer, netComponent->id ) )
        {
          LOG_ERROR(
            "Failed to remove entity %d from entity replication system.",
            netComponent->owner );
        }
      }
      else if ( event->componentTypeId == sv_AgentComponentTypeId )
      {
        sv_AgentComponent *agentComponent =
          (sv_AgentComponent *)event->componentData;
        physics->destroyObject( physics->physicsSystem,
                                agentComponent->character );
      }
      break;
    }
    default:
      break;
    }
    peakResult = evt_Peak( &componentGarbageQueue, peakResult.header );
  }

  gameState->currentTick++;
  gameState->currentTime += dt;
}
