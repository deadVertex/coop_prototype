#undef internal
#undef global
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#define internal static
#define global static

#define GLEW_STATIC
#include <GL/glew.h>

#include "common/utils.h"
#include "common/math.h"
#include "common/byte_buffer.h"

#include "opengl_utils.cpp"

#if !defined( STB_IMAGE_IMPLEMENTATION )
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#endif

#if !defined( STB_RECT_PACK_IMPLEMENTATION )
#define STB_RECT_PACK_IMPLEMENTATION
#include <stb_rect_pack.h>
#endif

#if !defined( STB_TRUETYPE_IMPLEMENTATION )
#define STB_TRUETYPE_IMPLEMENTATION
#include <stb_truetype.h>
#endif

static bool CompileShader( GLuint shader, const char* src, bool isVertex )
{
  int len = strlen( src );
  glShaderSource( shader, 1, &src, &len );
  glCompileShader( shader );

  int success;
  glGetShaderiv( shader, GL_COMPILE_STATUS, &success );
  if ( !success ) // Check if the shader was compiled successfully.
  {
    int logLength;
    glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &logLength );

    char log[4096];
    ASSERT( (uint32_t)logLength < ARRAY_COUNT(log) );

    int len = 0;
    glGetShaderInfoLog( shader, logLength, &len, log );

    if ( isVertex )
    {
      printf("[VERTEX]\n%s", log);
    }
    else
    {
      printf("[FRAGMENT]\n%s", log);
    }
    return false;
  }
  return true;
}

static bool LinkShader( GLuint vertex, GLuint fragment, GLuint program )
{
  glAttachShader( program, vertex );
  glAttachShader( program, fragment );
  glLinkProgram( program );

  int success;
  glGetProgramiv( program, GL_LINK_STATUS, &success );
  if ( !success ) // Check if the shader was linked successfully.
  {
    int logLength;
    glGetShaderiv( program, GL_INFO_LOG_LENGTH, &logLength );

    char log[4096];
    int len = 0;
    glGetProgramInfoLog( program, logLength, &len, log );
    printf( "[LINKER]\n%s", log);
    return false;
  }
  return true;
}

static void DeleteShader( GLuint program )
{
  glUseProgram( 0 );
  GLsizei count = 0;
  GLuint shaders[ 2 ];
  // Need to retrieve the vertex and fragment shaders and delete them
  // explicitly, deleting the shader program only detaches the two shaders, it
  // does not free the resources for them.
  glGetAttachedShaders( program, 2, &count, shaders );
  glDeleteProgram( program );
  glDeleteShader( shaders[ 0 ] );
  glDeleteShader( shaders[ 1 ] );
}

static GLuint CreateShader( const char *vertexSource, const char *fragmentSource )
{
  GLuint vertex = glCreateShader( GL_VERTEX_SHADER );
  GLuint fragment = glCreateShader( GL_FRAGMENT_SHADER );
  GLuint program = glCreateProgram();

  bool vertexSuccess = CompileShader( vertex, vertexSource, true );
  bool fragmentSuccess = CompileShader( fragment, fragmentSource, false );

  if ( vertexSuccess && fragmentSuccess )
  {
    if ( LinkShader( vertex, fragment, program ) )
    {
      return program;
    }
  }
  DeleteShader( program );
  return 0;
}

const char *colourVertexSource =
"#version 330\n"
"layout (location=0) in vec3 vertexPosition;\n"
"uniform mat4 viewProjection = mat4(1);"
"uniform mat4 model = mat4(1);"
"void main()"
"{"
"  gl_Position = viewProjection * model * vec4( vertexPosition, 1.0 );"
"}\n";

const char *colourFragmentSource =
"#version 330\n"
"out vec4 outputColour;"
"uniform vec4 colour;"
"void main()"
"{"
"  outputColour = colour;"
"}\n";

const char *bitmapVertexSource =
"#version 330\n"
"layout (location = 0) in vec3 in_position;\n"
"layout (location = 2) in vec2 in_texCoords;\n"
"uniform mat4 viewProjection = mat4(1);"
"uniform mat4 model = mat4(1);"
"out vec2 textureCoordinates;"
"void main()"
"{"
"  textureCoordinates = in_texCoords;"
"  gl_Position = viewProjection * model * vec4( in_position, 1.0 );"
"}\n";

const char *bitmapFragmentSource =
"#version 330\n"
"in vec2 textureCoordinates;"
"out vec4 outputColour;"
"uniform sampler2D sprite;"
"uniform float opacity = 1.0;"
"uniform vec4 clipping = vec4(0,0,1,1);"
"void main()"
"{"
"  vec2 coords = clipping.xy + ( textureCoordinates * clipping.zw );"
"  outputColour = texture( sprite, textureCoordinates).rgba;"
"  outputColour.a *= opacity;"
"}\n";

const char *textVertexSource =
"#version 330\n"
"layout (location = 0) in vec4 vertex;\n"
"uniform mat4 viewProjection = mat4(1);"
"uniform mat4 model = mat4(1);"
"out vec2 textureCoordinates;"
"void main()"
"{"
"  textureCoordinates = vertex.zw;"
"  gl_Position = viewProjection * model * vec4( vertex.xy, 0.0, 1.0 );"
"}\n";

const char *textFragmentSource =
"#version 330\n"
"in vec2 textureCoordinates;"
"out vec4 outputColour;"
"uniform sampler2D glyphSheet;"
"uniform vec4 colour = vec4( 1 );"
"void main()"
"{"
"  outputColour = colour;"
"  outputColour.a = texture( glyphSheet, textureCoordinates ).a;"
//"  if ( texture( glyphSheet, textureCoordinates).a < 0.5 )"
//"  {"
//"    discard;"
//"  }"
"}\n";

const char *vertexColourVertexSource =
"#version 330\n"
"layout (location = 0) in vec3 position;\n"
"layout (location = 3) in vec4 vertexColour;\n"
"uniform mat4 viewProjection = mat4(1);\n"
"out vec4 colour;\n"
"void main()\n"
"{\n"
"  colour = vertexColour;\n"
"  gl_Position = viewProjection * vec4( position, 1.0 );\n"
"}\n";

const char *vertexColourFragmentSource =
"#version 330\n"
"out vec4 outputColour;\n"
"in vec4 colour;\n"
"void main()\n"
"{\n"
"  outputColour = colour;\n"
"}\n";

const char *directionalLightVertexSource =
"#version 330\n"
"layout (location = 0) in vec3 position;\n"
"layout (location = 1) in vec2 texCoord;\n"
"out vec2 textureCoordinates;\n"
"void main()\n"
"{\n"
"  textureCoordinates = texCoord;\n"
"  gl_Position = vec4(position, 1.0);\n"
"}\n";


struct ColourShader
{
  uint32_t program;
  int viewProjection;
  int model;
  int colour;
};

ColourShader CreateColourShader()
{
  ColourShader result = {};
  result.program = CreateShader( colourVertexSource, colourFragmentSource );
  ASSERT( result.program );

  result.viewProjection = glGetUniformLocation( result.program, "viewProjection" );
  result.model = glGetUniformLocation( result.program, "model" );
  result.colour = glGetUniformLocation( result.program, "colour" );

  return result;
}

struct SpriteShader
{
  uint32_t program;
  int viewProjection;
  int model;
  int sprite;
  int opacity;
  int clipping;
};

SpriteShader CreateSpriteShader()
{
  SpriteShader result = {};
  result.program = CreateShader( bitmapVertexSource, bitmapFragmentSource );
  ASSERT( result.program );

  result.viewProjection = glGetUniformLocation( result.program, "viewProjection" );
  result.model = glGetUniformLocation( result.program, "model" );
  result.sprite = glGetUniformLocation( result.program, "bitmap" );
  result.opacity = glGetUniformLocation( result.program, "opacity" );
  result.clipping = glGetUniformLocation( result.program, "clipping" );

  return result;
}

struct TextShader
{
  uint32_t program;
  int viewProjection;
  int model;
  int glyphSheet;
  int colour;
};

TextShader CreateTextShader()
{
  TextShader result = {};
  result.program = CreateShader( textVertexSource, textFragmentSource );
  ASSERT( result.program );

  result.viewProjection = glGetUniformLocation( result.program, "viewProjection" );
  result.model = glGetUniformLocation( result.program, "model" );
  result.glyphSheet = glGetUniformLocation( result.program, "glyphSheet" );
  result.colour = glGetUniformLocation( result.program, "colour" );
  return result;
}

struct VertexColourShader
{
  uint32_t program;
  int viewProjection;
};

VertexColourShader CreateVertexColourShader()
{
  VertexColourShader result = {};
  result.program =
    CreateShader( vertexColourVertexSource, vertexColourFragmentSource );
  ASSERT( result.program );

  result.viewProjection = glGetUniformLocation( result.program, "viewProjection" );
  return result;
}

struct DirectionalLightShader
{
  uint32_t program;
  int albedoTexture;
  int normalTexture;
  int depthTexture;
  int invViewProjection;
};

DirectionalLightShader CreateDirectionalLightShader( uint32_t program )
{
  DirectionalLightShader result = {};
  result.program = program;
  result.albedoTexture = glGetUniformLocation( result.program, "albedoTexture" );
  result.normalTexture = glGetUniformLocation( result.program, "normalTexture" );
  result.depthTexture = glGetUniformLocation( result.program, "depthTexture" );
  result.invViewProjection = glGetUniformLocation( result.program, "invViewProjection" );

  return result;
}

struct PostProcessingShader
{
  uint32_t program;
  int hdrTexture;
  int screenSize;
};

PostProcessingShader CreatePostProcessingShader( uint32_t program )
{
  PostProcessingShader result = {};
  result.program = program;
  result.hdrTexture = glGetUniformLocation( result.program, "hdrTexture" );
  result.screenSize = glGetUniformLocation( result.program, "screenSize" );

  return result;
}

struct ColourDeferredShader
{
  uint32_t program;
  int model;
  int viewProjection;
  int colour;
};

ColourDeferredShader CreateColourDeferredShader( uint32_t program )
{
  ColourDeferredShader result = {};
  result.program = program;
  result.model = glGetUniformLocation( result.program, "model" );
  result.viewProjection = glGetUniformLocation( result.program, "viewProjection" );
  result.colour = glGetUniformLocation( result.program, "colour" );
  return result;
}

uint32_t LoadShader( const char *vertexPath, const char *fragmentPath, MemoryArena *arena, GameMemory *memory )
{
  uint32_t result = 0;

  ReadFileResult vertexData = memory->debugReadEntireFile( vertexPath );
  if ( vertexData.memory )
  {
    char *vertexSource = AllocateArray( arena, char, vertexData.size + 1 );
    strncpy( vertexSource, (const char *)vertexData.memory, vertexData.size );
    vertexSource[ vertexData.size ] = '\0';
    if ( vertexSource )
    {
      ReadFileResult fragmentData = memory->debugReadEntireFile( fragmentPath );
      if ( fragmentData.memory )
      {
        char *fragmentSource = AllocateArray( arena, char, fragmentData.size + 1 );
        if ( fragmentSource )
        {
          strncpy( fragmentSource, ( const char * )fragmentData.memory, fragmentData.size );
          fragmentSource[ fragmentData.size ] = '\0';

          result = CreateShader( vertexSource, fragmentSource );
          MemoryArenaFree( arena, fragmentSource );
        }
      }
      MemoryArenaFree( arena, vertexSource );
    }
  }

  return result;
}

struct Glyph
{
  float u0, v0, u1, v1, x, y, w, h, advance, leftSideBearing;
};

#define GLYPH_COUNT 128
struct Font
{
  Glyph glyphs[GLYPH_COUNT];
  uint32_t texture;
  float ascent, descent, lineGap;
};

Font LoadFont( const char *path, MemoryArena *arena, GameMemory *memory,
               uint32_t bitmapWidth, uint32_t bitmapHeight,
               uint32_t pixelHeight )
{
  Font result = {};

  ReadFileResult fileData = memory->debugReadEntireFile( path );
  if ( fileData.memory )
  {
    stbtt_fontinfo fontInfo;
    if ( stbtt_InitFont(
           &fontInfo, (uint8_t *)fileData.memory,
           stbtt_GetFontOffsetForIndex( (uint8_t *)fileData.memory, 0 ) ) )
    {
      float scale = stbtt_ScaleForPixelHeight( &fontInfo, pixelHeight );

      int ascent, descent, lineGap;
      stbtt_GetFontVMetrics( &fontInfo, &ascent, &descent, &lineGap );
      result.ascent = ascent * scale;
      result.descent = descent * scale;
      result.lineGap = lineGap * scale;

      uint8_t *bitmap =
        (uint8_t *)MemoryArenaAllocate( arena, bitmapWidth * bitmapHeight );

      ClearToZero( bitmap, bitmapWidth * bitmapHeight );

      stbrp_rect rects[GLYPH_COUNT];
      uint8_t *bitmaps[GLYPH_COUNT];
      for ( uint32_t i = 0; i < GLYPH_COUNT; ++i )
      {
        int ix0, iy0, ix1, iy1;
        stbtt_GetCodepointBitmapBox( &fontInfo, i, scale, scale, &ix0, &iy0,
                                     &ix1, &iy1 );
        int w = ix1 - ix0;
        int h = iy1 - iy0;
        bitmaps[i] = (uint8_t *)MemoryArenaAllocate( arena, w * h );
        stbtt_MakeCodepointBitmap( &fontInfo, bitmaps[i], w, h, w, scale, scale,
                                   i );

        Glyph *glyph = result.glyphs + i;
        glyph->w = w;
        glyph->h = h;
        glyph->x = ix0;
        glyph->y = iy0;
        int advance, leftSideBearing;
        stbtt_GetCodepointHMetrics( &fontInfo, i, &advance, &leftSideBearing );
        glyph->advance = advance * scale;
        glyph->leftSideBearing = leftSideBearing * scale;

        rects[i].w = w;
        rects[i].h = h;
      }

      stbrp_context context;
      stbrp_node nodes[512];
      stbrp_init_target( &context, bitmapWidth, bitmapWidth, nodes,
                         ARRAY_COUNT( nodes ) );

      stbrp_pack_rects( &context, rects, ARRAY_COUNT( rects ) );

      for ( uint32_t i = 0; i < GLYPH_COUNT; ++i )
      {
        stbrp_rect *rect = rects + i;
        Glyph *glyph = result.glyphs + i;
        glyph->u0 = rect->x / (float)bitmapWidth;
        glyph->v0 = rect->y / (float)bitmapHeight;
        glyph->u1 = ( rect->x + rect->w ) / (float)bitmapWidth;
        glyph->v1 = ( rect->y + rect->h ) / (float)bitmapHeight;

        for ( uint32_t y = 0; y < rect->h; ++y )
        {
          uint8_t *dst = bitmap + ( ( rect->y + y ) * bitmapWidth + rect->x );
          uint8_t *src = bitmaps[i] + y * rect->w;
          memcpy( dst, src, rect->w );
        }
      }

      glGenTextures( 1, &result.texture );
      glBindTexture( GL_TEXTURE_2D, result.texture );

      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
      glTexImage2D( GL_TEXTURE_2D, 0, GL_ALPHA, bitmapWidth, bitmapHeight, 0,
                    GL_ALPHA, GL_UNSIGNED_BYTE, bitmap );
      glBindTexture( GL_TEXTURE_2D, 0 );
      // NOTE: This frees all of the other bitmaps as well.
      MemoryArenaFree( arena, bitmap );
    }

    memory->debugFreeFileMemory( fileData.memory );
  }
  return result;
}

enum
{
  VERTEX_DATA_POSITION = 1,
  VERTEX_DATA_NORMAL = 2,
  VERTEX_DATA_TEXTURE_COORDINATE = 4,
};

struct StaticSubMesh
{
  uint32_t vertexSize, numVertices;
  uint8_t vertexDataFlags;
  uint8_t *vertices;
  uint32_t *indices;
  uint32_t numIndices;
  StaticSubMesh *next;
};

StaticSubMesh *CopyMeshes( const aiScene *scene, MemoryArena *arena,
                           const aiNode *node = nullptr )
{
  if ( !node )
  {
    node = scene->mRootNode;
  }

  StaticSubMesh *prev = NULL;
  StaticSubMesh *result = NULL;
  fprintf( stdout, "DEBUG: %d submeshes for node.\n", node->mNumMeshes );
  for ( uint32_t i = 0; i < node->mNumMeshes; ++i )
  {
    fprintf( stdout, "DEBUG: Submesh index %d.\n", i );
    auto inputMesh = scene->mMeshes[i];
    StaticSubMesh *subMesh = AllocateStruct( arena, StaticSubMesh );
    subMesh->vertexSize = sizeof( vec3 );
    subMesh->vertexDataFlags = VERTEX_DATA_POSITION;

    if ( !inputMesh->HasPositions() )
    {
      printf( "Error: Submesh must have position data.\n" );
      continue;
    }
    if ( inputMesh->HasNormals() )
    {
      subMesh->vertexSize += sizeof( vec3 );
      subMesh->vertexDataFlags |= VERTEX_DATA_NORMAL;
    }
    if ( inputMesh->HasTextureCoords( 0 ) )
    {
      subMesh->vertexSize += sizeof( vec2 );
      subMesh->vertexDataFlags |= VERTEX_DATA_TEXTURE_COORDINATE;
    }
    subMesh->numVertices = inputMesh->mNumVertices;
    subMesh->vertices = (uint8_t *)MemoryArenaAllocate(
      arena, subMesh->numVertices * subMesh->vertexSize );

    ByteBuffer byteBuffer = {};
    InitializeByteBuffer( &byteBuffer, subMesh->vertices,
                          subMesh->numVertices * subMesh->vertexSize );

    printf( "Vertices: %d\n", subMesh->numVertices );
    for ( uint32_t j = 0; j < subMesh->numVertices; ++j )
    {
      auto p = &inputMesh->mVertices[j];
      vec3 position = Vec3( p->x, p->y, p->z );
      WriteData( &position, sizeof( position ), &byteBuffer );

      if ( subMesh->vertexDataFlags & VERTEX_DATA_NORMAL )
      {
        auto n = &inputMesh->mNormals[j];
        vec3 normal = Vec3( n->x, n->y, n->z );
        WriteData( &normal, sizeof( normal ), &byteBuffer );
      }

      if ( subMesh->vertexDataFlags & VERTEX_DATA_TEXTURE_COORDINATE )
      {
        auto t = &inputMesh->mTextureCoords[0][j];
        vec2 texCoord = Vec2( t->x, t->y );
        WriteData( &texCoord, sizeof( texCoord ), &byteBuffer );
      }
    }

    subMesh->numIndices = inputMesh->mNumFaces * 3;
    subMesh->indices = AllocateArray( arena, uint32_t, subMesh->numIndices );
    printf( "Indices: %u\n", (uint32_t)subMesh->numIndices );
    uint32_t index = 0;
    for ( uint32_t j = 0; j < inputMesh->mNumFaces; ++j )
    {
      auto face = &inputMesh->mFaces[j];
      subMesh->indices[index++] = face->mIndices[0];
      subMesh->indices[index++] = face->mIndices[1];
      subMesh->indices[index++] = face->mIndices[2];
    }

    subMesh->next = prev;
    prev = subMesh;
    result = subMesh;
  }

  for ( uint32_t i = 0; i < node->mNumChildren; ++i )
  {
    StaticSubMesh *subMesh = CopyMeshes( scene, arena, node->mChildren[i] );
    StaticSubMesh *last = NULL;
    if ( subMesh )
    {
      while ( subMesh->next )
      {
        subMesh = subMesh->next;
      }
      subMesh->next = prev;
      result = subMesh;
      ASSERT( result );
    }
  }
  return result;
}

// TODO: Load all submeshes.
OpenGLStaticMesh LoadMesh( const char *path, GameMemory *memory,
                           MemoryArena *arena )
{
  OpenGLStaticMesh result = {};
  ReadFileResult fileData = memory->debugReadEntireFile( path );
  if ( fileData.memory )
  {
    Assimp::Importer importer;

    const aiScene *scene = importer.ReadFileFromMemory(
      fileData.memory, fileData.size,
      aiProcess_CalcTangentSpace | aiProcess_Triangulate |
        aiProcess_JoinIdenticalVertices | aiProcess_GenSmoothNormals |
        aiProcess_SortByPType );

    if ( !scene )
    {
      fprintf( stderr, "Model import failed: %s\n", importer.GetErrorString() );
      return result;
    }


    StaticSubMesh *subMeshesHead = CopyMeshes( scene, arena );
    if ( subMeshesHead == NULL )
    {
      fprintf( stderr, "No model data found!\n" );
      return result;
    }

    auto &meshData = *subMeshesHead;

    OpenGLVertexAttribute attribs[3];
    ASSERT( meshData.vertexDataFlags & VERTEX_DATA_POSITION );
    attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
    attribs[0].numComponents = 3;
    attribs[0].componentType = GL_FLOAT;
    attribs[0].normalized = GL_FALSE;
    attribs[0].offset = 0;
    uint32_t i = 1;
    uint32_t vertexSize = 12;
    if ( meshData.vertexDataFlags & VERTEX_DATA_NORMAL )
    {
      attribs[i].index = VERTEX_ATTRIBUTE_NORMAL;
      attribs[i].numComponents = 3;
      attribs[i].componentType = GL_FLOAT;
      attribs[i].normalized = GL_FALSE;
      attribs[i].offset = sizeof( float ) * 3;
      vertexSize += 12;
      i++;
    }
    if ( meshData.vertexDataFlags & VERTEX_DATA_TEXTURE_COORDINATE )
    {
      attribs[i].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
      attribs[i].numComponents = 2;
      attribs[i].componentType = GL_FLOAT;
      attribs[i].normalized = GL_FALSE;
      attribs[i].offset = sizeof( float ) * 6;
      vertexSize += 8;
      i++;
    }

    result = OpenGLCreateStaticMesh(
      meshData.vertices, meshData.numVertices, meshData.indices,
      meshData.numIndices, meshData.vertexSize, attribs, i, GL_TRIANGLES );

    while ( subMeshesHead->next )
    {
      subMeshesHead = subMeshesHead->next;
    }
    MemoryArenaFree( arena, subMeshesHead );
    memory->debugFreeFileMemory( fileData.memory );
  }
  else
  {
    printf( "Could not find mesh file \"%s\".", path );
  }
  return result;
}

#define INVALID_OPENGL_TEXTURE 0
enum
{
  PIXEL_FORMAT_RGB8,
  PIXEL_FORMAT_RGBA8
};

enum
{
  PIXEL_COMPONENT_TYPE_UINT8
};

struct OpenGLTextureFormat
{
  int internalFormat, format, type;
};

internal bool OpenGLGetTextureFormat( int pixelFormat, int pixelComponentType,
                                      OpenGLTextureFormat *result )
{
  switch( pixelComponentType )
  {
    case PIXEL_COMPONENT_TYPE_UINT8:
      {
        result->type = GL_UNSIGNED_BYTE;
        break;
      }
    default:
      {
        return false;
        break;
      }
  }

  switch( pixelFormat )
  {
    case PIXEL_FORMAT_RGB8:
      {
        result->internalFormat = GL_SRGB8;
        result->format = GL_RGB;
        break;
      }
    case PIXEL_FORMAT_RGBA8:
      {
        result->internalFormat = GL_SRGB8_ALPHA8;
        result->format = GL_RGBA;
        break;
      }
    default:
      {
        return false;
        break;
      }
  }

  return true;
}

void OpenGLDeleteTexture( uint32_t texture )
{
  glDeleteTextures( 1, &texture );
}

uint32_t OpenGLCreateTexture( uint32_t width, uint32_t height,
    int pixelFormat, int pixelComponentType, const void *pixels )
{
  GLuint texture;
  glGenTextures( 1, &texture );
  glBindTexture( GL_TEXTURE_2D, texture );

  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

  OpenGLTextureFormat format;
  if ( OpenGLGetTextureFormat( pixelFormat, pixelComponentType, &format ) )
  {
    glTexImage2D( GL_TEXTURE_2D, 0, format.internalFormat, width, height, 0,
        format.format, format.type, pixels );

    glBindTexture( GL_TEXTURE_2D, INVALID_OPENGL_TEXTURE );
    return texture;
  }
  OpenGLDeleteTexture( texture );
  return INVALID_OPENGL_TEXTURE;
}

uint32_t LoadSprite( const char *path, GameMemory *memory )
{
  uint32_t result = 0;
  ReadFileResult fileData = memory->debugReadEntireFile( path );
  if ( fileData.memory )
  {
    int w, h, n;
    stbi_set_flip_vertically_on_load( 0 );

    auto pixels = stbi_load_from_memory( (const uint8_t *)fileData.memory,
                                         fileData.size, &w, &h, &n, 0 );
    if ( pixels )
    {
      uint32_t pixelFormat = PIXEL_FORMAT_RGB8;
      uint32_t pixelComponentType = PIXEL_COMPONENT_TYPE_UINT8;
      if ( n == 4 )
      {
        pixelFormat = PIXEL_FORMAT_RGBA8;
      }
      result =
        OpenGLCreateTexture( w, h, pixelFormat, pixelComponentType, pixels );
      stbi_image_free( pixels );
    }
    memory->debugFreeFileMemory( fileData.memory );
  }
  return result;
}

struct TextBufferVertex
{
  float x;
  float y;
  float u;
  float v;
};

#define MAX_TEXT_BUFFER_VERTICES 8192
struct TextBuffer
{
  TextBufferVertex vertices[MAX_TEXT_BUFFER_VERTICES];
  OpenGLDynamicMesh mesh;
};

void InitializeTextBuffer( TextBuffer *buffer )
{
  OpenGLVertexAttribute vertexAttribute;
  vertexAttribute.index = VERTEX_ATTRIBUTE_POSITION;
  vertexAttribute.numComponents = 4;
  vertexAttribute.componentType = GL_FLOAT;
  vertexAttribute.normalized = GL_FALSE;
  vertexAttribute.offset = 0;
  buffer->mesh = OpenGLCreateDynamicMesh(
    buffer->vertices, ARRAY_COUNT( buffer->vertices ),
    sizeof( buffer->vertices[0] ), &vertexAttribute, 1, GL_TRIANGLES );
}

internal void UpdateTextBuffer( TextBuffer *buffer )
{
  OpenGLUpdateDynamicMesh( buffer->mesh );
}

internal float CalculateTextLength( Font *font, const char *str,
                                    uint32_t length = 0 )
{
  int result = 0.0f;
  int current = 0.0f;
  uint32_t count = 0;
  while ( *str )
  {
    if ( length > 0 && count >= length )
    {
      break;
    }
    count++;

    if ( *str >= 32 )
    {
      current += (int)font->glyphs[(int)*str].advance;
    }
    else if ( *str == '\n' )
    {
      if ( current > result )
      {
        result = current;
      }
      current = 0.0f;
    }
    else if ( *str == '\t' )
    {
      current += (int)font->glyphs[32].advance * 2;
    }
    str++;
  }
  if ( current > result )
  {
    result = current;
  }
  return result;
}

float AddCharacterToBuffer( TextBuffer *buffer, Font *font, int c, float x,
                            float y )
{
  ASSERT( buffer->mesh.numVertices + 6 < MAX_TEXT_BUFFER_VERTICES );
  ASSERT( c < GLYPH_COUNT );
  Glyph glyph = font->glyphs[c];
  float fontHeight = font->ascent + font->descent + font->lineGap;
  TextBufferVertex topLeft, bottomLeft, bottomRight, topRight;
  topLeft.x = glyph.x + x;
  topLeft.y = glyph.y + y + fontHeight;
  topLeft.u = glyph.u0;
  topLeft.v = glyph.v0;

  bottomLeft.x = glyph.x + x;
  bottomLeft.y = glyph.y + glyph.h + y + fontHeight;
  bottomLeft.u = glyph.u0;
  bottomLeft.v = glyph.v1;

  bottomRight.x = glyph.x + glyph.w + x;
  bottomRight.y = glyph.y + glyph.h + y + fontHeight;
  bottomRight.u = glyph.u1;
  bottomRight.v = glyph.v1;

  topRight.x = glyph.x + glyph.w + x;
  topRight.y = glyph.y + y + fontHeight;
  topRight.u = glyph.u1;
  topRight.v = glyph.v0;

  auto vertex = buffer->vertices + buffer->mesh.numVertices;
  *vertex++ = topRight;
  *vertex++ = topLeft;
  *vertex++ = bottomLeft;

  *vertex++ = bottomLeft;
  *vertex++ = bottomRight;
  *vertex++ = topRight;
  buffer->mesh.numVertices += 6;

  return x + glyph.advance;
}

struct LineVertex
{
  vec3 position;
  vec4 colour;
};

#define MAX_LINE_VERTICES KILOBYTES(64)
struct LineBuffer
{
  LineVertex vertices[MAX_LINE_VERTICES];
  OpenGLDynamicMesh mesh;
};

void InitializeLineBuffer( LineBuffer *lineBuffer )
{
  OpenGLVertexAttribute vertexAttributes[2];
  vertexAttributes[0].index = VERTEX_ATTRIBUTE_POSITION;
  vertexAttributes[0].numComponents = 3;
  vertexAttributes[0].componentType = GL_FLOAT;
  vertexAttributes[0].normalized = GL_FALSE;
  vertexAttributes[0].offset = 0;
  vertexAttributes[1].index = VERTEX_ATTRIBUTE_COLOUR;
  vertexAttributes[1].numComponents = 4;
  vertexAttributes[1].componentType = GL_FLOAT;
  vertexAttributes[1].normalized = GL_FALSE;
  vertexAttributes[1].offset = sizeof( vec3 );
  lineBuffer->mesh = OpenGLCreateDynamicMesh(
    lineBuffer->vertices, ARRAY_COUNT( lineBuffer->vertices ),
    sizeof( lineBuffer->vertices[0] ), vertexAttributes,
    ARRAY_COUNT( vertexAttributes ), GL_LINES );
  lineBuffer->mesh.vertices = lineBuffer->vertices;
}

void RenderLineBuffer( LineBuffer *lineBuffer )
{
  OpenGLDrawDynamicMesh( lineBuffer->mesh );
}

struct GBuffer
{
  uint32_t fbo;
  uint32_t albedoTexture, depthTexture, normalTexture;
  uint32_t width, height;

  uint32_t lightFbo, lightTexture;
};

struct renderer_State
{
  ColourShader colourShader;
  SpriteShader spriteShader;
  TextShader textShader;
  VertexColourShader vertexColourShader;
  DirectionalLightShader directionalLightShader;
  PostProcessingShader postProcessingShader;
  ColourDeferredShader colourDeferredShader;

  OpenGLStaticMesh quad;
  OpenGLStaticMesh wireFrameCube;
  OpenGLStaticMesh sphere;

  TextBuffer textBuffer;
  LineBuffer lineBuffer;
  LineBuffer physicsLineBuffer;

  uint32_t windowWidth;
  uint32_t windowHeight;

  GBuffer gbuffer;
};

#define DEBUG_MAX_LINES 0x1000
#define DEBUG_MAX_BOXES 0x200
#define DEBUG_MAX_SPHERES 0x200
#define DEBUG_TEXT_BUFFER_LEN 0x800

struct debug_Line
{
  vec3 start, end;
  vec4 colour;
  float timeRemaining;
};

struct debug_Box
{
  vec3 position;
  vec3 scale;
  vec4 colour;
  float timeRemaining;
};

struct debug_Sphere
{
  vec3 position;
  float scale;
  vec4 colour;
  float timeRemaining;
};

struct debug_State
{
  debug_Line lines[DEBUG_MAX_LINES];
  ContiguousObjectPool linePool;

  debug_Box boxes[DEBUG_MAX_BOXES];
  ContiguousObjectPool boxPool;

  debug_Sphere spheres[ DEBUG_MAX_SPHERES ];
  ContiguousObjectPool spherePool;

  char text[DEBUG_TEXT_BUFFER_LEN];
  uint32_t textLength;
};

internal void debug_Init( debug_State *debugState )
{
  debugState->linePool = CreateContiguousObjectPoolArray( debugState->lines );
  debugState->boxPool = CreateContiguousObjectPoolArray( debugState->boxes );
  debugState->spherePool = CreateContiguousObjectPoolArray( debugState->spheres );
}

internal void debug_PushLine_( debug_State *debugState, vec3 start,
                              vec3 end, vec4 colour,
                              float lifeTime = 0.0f )
{
  debug_Line *line = (debug_Line *)AllocateObject( &debugState->linePool );
  if ( line )
  {
    line->start = start;
    line->end = end;
    line->colour = colour;
    line->timeRemaining = lifeTime;
  }
}

internal void debug_PushPoint_( debug_State *debugState, vec3 position,
                               vec4 colour, float lifeTime = 0.0f )
{
  debug_PushLine_( debugState, position - Vec3(0.5, 0, 0),
                  position + Vec3(0.5, 0, 0), colour, lifeTime );
  debug_PushLine_( debugState, position - Vec3(0, 0.5, 0),
                  position + Vec3(0, 0.5, 0), colour, lifeTime );
  debug_PushLine_( debugState, position - Vec3(0, 0, 0.5),
                  position + Vec3(0, 0, 0.5), colour, lifeTime );
}

internal void debug_PushBox_( debug_State *debugState, vec3 min, vec3 max,
                              vec4 colour, float lifeTime = 0.0f )
{
  debug_Box *box = (debug_Box *)AllocateObject( &debugState->boxPool );
  if ( box )
  {
    box->position = ( min + max ) * 0.5f;
    box->scale = max - min;
    box->colour = colour;
    box->timeRemaining = lifeTime;
  }
}

internal void debug_PushSphere_( debug_State *debugState, vec3 p, float radius, vec4 colour, float lifeTime = 0.0f )
{
  debug_Sphere *sphere = ( debug_Sphere * )AllocateObject( &debugState->spherePool );
  if ( sphere )
  {
    sphere->position = p;
    sphere->scale = radius;
    sphere->colour = colour;
    sphere->timeRemaining = lifeTime;
  }
}

internal void debug_Cleanup( debug_State *debugState, float dt )
{
  uint32_t i = 0;
  while ( i < debugState->linePool.size )
  {
    debug_Line *line = debugState->lines + i;
    if ( line->timeRemaining <= 0.0f )
    {
      FreeObject( &debugState->linePool, line );
      // NOTE: Run loop again with same value because new value is in its place.
    }
    else
    {
      line->timeRemaining -= dt;
      i++;
    }
  }

  i = 0;
  while ( i < debugState->boxPool.size )
  {
    debug_Box *box = debugState->boxes + i;
    if ( box->timeRemaining <= 0.0f )
    {
      FreeObject( &debugState->boxPool, box );
      // NOTE: Run loop again with same value because new value is in its place.
    }
    else
    {
      box->timeRemaining -= dt;
      i++;
    }
  }

  i = 0;
  while ( i < debugState->spherePool.size )
  {
    debug_Sphere *sphere = debugState->spheres + i;
    if ( sphere->timeRemaining <= 0.0f )
    {
      FreeObject( &debugState->spherePool, sphere );
      // NOTE: Run loop again with same value because new value is in its place.
    }
    else
    {
      sphere->timeRemaining -= dt;
      i++;
    }
  }

  debugState->textLength = 0;
}

internal void debug_Update(debug_State *debugState, renderer_State *renderer)
{
  renderer->lineBuffer.mesh.numVertices = 0;
  LineBuffer *lineBuffer = &renderer->lineBuffer;

  for ( uint32_t i = 0; i < debugState->linePool.size; ++i )
  {
    debug_Line *line = debugState->lines + i;
    if ( lineBuffer->mesh.numVertices + 2 < lineBuffer->mesh.maxVertices )
    {
      LineVertex *v1 = lineBuffer->vertices + lineBuffer->mesh.numVertices++;
      LineVertex *v2 = lineBuffer->vertices + lineBuffer->mesh.numVertices++;
      v1->position = line->start;
      v1->colour = line->colour;
      v2->position = line->end;
      v2->colour = line->colour;
    }
  }

  OpenGLUpdateDynamicMesh( lineBuffer->mesh );
}

internal void debug_Printf_( debug_State *debugState, const char *fmt, ... )
{
  va_list args;
  va_start( args, fmt );
  int size = DEBUG_TEXT_BUFFER_LEN - debugState->textLength;
  int n = vsnprintf( debugState->text + debugState->textLength, size,
                     fmt, args );
  va_end( args );
  if ( n < size )
  {
    debugState->textLength += n;
  }
}

bool InitializeGBuffer( GBuffer *gbuffer, uint32_t width, uint32_t height )
{
  gbuffer->width = width;
  gbuffer->height = height;

  glGenFramebuffers( 1, &gbuffer->fbo );
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, gbuffer->fbo );

  glGenTextures( 1, &gbuffer->albedoTexture );
  glBindTexture( GL_TEXTURE_2D, gbuffer->albedoTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA8, gbuffer->width, gbuffer->height, 0,
                GL_RGBA, GL_UNSIGNED_BYTE, NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                          GL_TEXTURE_2D, gbuffer->albedoTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

  glGenTextures( 1, &gbuffer->normalTexture );
  glBindTexture( GL_TEXTURE_2D, gbuffer->normalTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB32F, gbuffer->width, gbuffer->height, 0,
                GL_RGB, GL_FLOAT, NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
                          GL_TEXTURE_2D, gbuffer->normalTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

  glGenTextures( 1, &gbuffer->depthTexture );
  glBindTexture( GL_TEXTURE_2D, gbuffer->depthTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, gbuffer->width,
                gbuffer->height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8,
                NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                          GL_TEXTURE_2D, gbuffer->depthTexture, 0 );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_STENCIL_ATTACHMENT,
                          GL_TEXTURE_2D, gbuffer->depthTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

  uint32_t drawBuffers[2];
  drawBuffers[0] = GL_COLOR_ATTACHMENT0;
  drawBuffers[1] = GL_COLOR_ATTACHMENT1;
  glDrawBuffers( ARRAY_COUNT( drawBuffers ), drawBuffers );

  GLenum status = glCheckFramebufferStatus( GL_DRAW_FRAMEBUFFER );
  if ( status != GL_FRAMEBUFFER_COMPLETE )
  {
    printf( "Framebuffer error, status: 0x%x\n", status );
    return false;
  }
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );

  glGenFramebuffers( 1, &gbuffer->lightFbo );
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, gbuffer->lightFbo );

  glGenTextures( 1, &gbuffer->lightTexture );
  glBindTexture( GL_TEXTURE_2D, gbuffer->lightTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB16F, gbuffer->width, gbuffer->height, 0,
                GL_RGB, GL_FLOAT, NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                          GL_TEXTURE_2D, gbuffer->lightTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
  glDrawBuffer( GL_COLOR_ATTACHMENT0 );
  status = glCheckFramebufferStatus( GL_DRAW_FRAMEBUFFER );
  if ( status != GL_FRAMEBUFFER_COMPLETE )
  {
    printf( "Framebuffer error, status: 0x%x\n", status );
    return false;
  }
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );
  return true;
}

void DeinitializeGBuffer( GBuffer *gbuffer )
{
  if ( gbuffer->fbo )
  {
    glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );

    glDeleteTextures( 1, &gbuffer->albedoTexture );
    glDeleteTextures( 1, &gbuffer->depthTexture );
    glDeleteTextures( 1, &gbuffer->normalTexture );

    glDeleteFramebuffers( 1, &gbuffer->fbo );

    glDeleteTextures( 1, &gbuffer->lightTexture );
    glDeleteFramebuffers( 1, &gbuffer->lightFbo );
    gbuffer->fbo = 0;
  }
}

#ifdef _MSC_VER
void APIENTRY OpenGLReportErrorMessage( GLenum source, GLenum type, GLuint id,
                                        GLenum severity, GLsizei length,
                                        const GLchar *message,
                                        const void *userParam )
#else
void OpenGLReportErrorMessage( GLenum source, GLenum type, GLuint id,
                                        GLenum severity, GLsizei length,
                                        const GLchar *message,
                                        void *userParam )
#endif
{
  const char *typeStr = nullptr;
  switch ( type )
  {
    case GL_DEBUG_TYPE_ERROR:
      typeStr = "ERROR";
      break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
      typeStr = "DEPRECATED_BEHAVIOR";
      break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
      typeStr = "UNDEFINED_BEHAVIOR";
      break;
    case GL_DEBUG_TYPE_PORTABILITY:
      typeStr = "PORTABILITY";
      break;
    case GL_DEBUG_TYPE_PERFORMANCE:
      typeStr = "PERFORMANCE";
      break;
    case GL_DEBUG_TYPE_OTHER:
      typeStr = "OTHER";
      break;
    default:
      typeStr = "";
      break;
  }

  const char *severityStr = nullptr;
  switch ( severity )
  {
    case GL_DEBUG_SEVERITY_LOW:
      severityStr = "LOW";
      break;
    case GL_DEBUG_SEVERITY_MEDIUM:
      severityStr = "MEDIUM";
      break;
    case GL_DEBUG_SEVERITY_HIGH:
      severityStr = "HIGH";
      break;
    default:
      severityStr = "";
      return; // Don't want to spam output with unimportant messages
      break;
  }

  LOG_ERROR( "OPENGL|%s:%s:%s", typeStr, severityStr, message );

  UNUSED( source );
  UNUSED( id );
  UNUSED( length );
  UNUSED( userParam );
}

int renderer_Init( renderer_State *renderer, uint32_t windowWidth,
                   uint32_t windowHeight, MemoryArena *arena )
{
  ZeroPointerToStruct( renderer );

  GLenum err = glewInit();
  if ( err != GLEW_OK )
  {
    LOG_ERROR( "%s", glewGetErrorString( err ) );
    return -1;
  }

  renderer->colourShader = CreateColourShader();
  renderer->spriteShader = CreateSpriteShader();
  renderer->textShader = CreateTextShader();
  renderer->vertexColourShader = CreateVertexColourShader();

  renderer->quad = CreateQuadMesh();
  renderer->wireFrameCube = CreateWireframeCube();
  renderer->sphere = CreateIcosahedronMesh( 2, arena );

  InitializeTextBuffer( &renderer->textBuffer );
  InitializeLineBuffer( &renderer->lineBuffer );
  InitializeLineBuffer( &renderer->physicsLineBuffer );

  InitializeGBuffer( &renderer->gbuffer, windowWidth, windowHeight );

  glEnable( GL_BLEND );
  glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
  //glDisable( GL_DEPTH_TEST );

  renderer->windowWidth = windowWidth;
  renderer->windowHeight = windowHeight;

  glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS );
  glDebugMessageCallback( OpenGLReportErrorMessage, nullptr );
  GLuint unusedIds = 0;
  glDebugMessageControl( GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0,
                         &unusedIds, GL_TRUE );

  return 0;
}

void renderer_Deinit( renderer_State *state )
{
  DeinitializeGBuffer( &state->gbuffer );
}

void renderer_DrawRect( renderer_State *renderer, mat4 *model, mat4 *viewProjection, vec4 colour )
{
  ColourShader shader = renderer->colourShader;
  glUseProgram( shader.program );
  glUniformMatrix4fv( shader.viewProjection, 1, GL_FALSE, viewProjection->raw );
  glUniformMatrix4fv( shader.model, 1, GL_FALSE, model->raw );
  glUniform4fv( shader.colour, 1, colour.data);
  OpenGLDrawStaticMesh( renderer->quad );
}

void renderer_DrawRect( renderer_State *renderer, vec2 p, float w, float h,
                        vec4 colour, mat4 *viewProjection, float angle = 0.0f )
{
  ColourDeferredShader shader = renderer->colourDeferredShader;
  glUseProgram( shader.program );
  glUniformMatrix4fv( shader.viewProjection, 1, GL_FALSE, viewProjection->raw);
  mat4 model = Translate( p ) * Scale( Vec2( w, h ) * 0.5f ) * Rotate( angle ) *
               Translate( Vec2( 1, 1 ) );
  glUniformMatrix4fv( shader.model, 1, GL_FALSE, model.raw );
  glUniform4fv( shader.colour, 1, colour.data);
  OpenGLDrawStaticMesh( renderer->quad );
}

void renderer_DrawRect( renderer_State *renderer, rect2 rect, vec4 colour, mat4 *viewProjection )
{
  float w = rect.max.x - rect.min.x;
  float h = rect.max.y - rect.min.y;
  renderer_DrawRect( renderer, rect.min, w, h, colour, viewProjection );
}

void renderer_DrawLine()
{
}

void renderer_DrawSprite( renderer_State *renderer, vec2 p, float w, float h, mat4 *viewProjection, uint32_t sprite )
{
  SpriteShader shader = renderer->spriteShader;
  glUseProgram( shader.program );
  glUniformMatrix4fv( shader.viewProjection, 1, GL_FALSE, viewProjection->raw);
  mat4 model = Translate( p ) * Scale( Vec2( w, h ) * 0.5f ) * Translate( Vec2( 1, 1 ) );
  glUniformMatrix4fv( shader.model, 1, GL_FALSE, model.raw );
  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, sprite );
  glUniform1i( shader.sprite, 0 );
  OpenGLDrawStaticMesh( renderer->quad );
}

void renderer_DrawMesh( renderer_State *renderer, mat4 *model, mat4 *viewProjection, vec4 colour, OpenGLStaticMesh mesh )
{
  ColourDeferredShader shader = renderer->colourDeferredShader;
  glUseProgram( shader.program );
  glUniformMatrix4fv( shader.viewProjection, 1, GL_FALSE, viewProjection->raw);
  glUniformMatrix4fv( shader.model, 1, GL_FALSE, model->raw );
  glUniform4fv( shader.colour, 1, colour.data);
  OpenGLDrawStaticMesh( mesh );
}

enum
{
  TEXT_ALIGN_LEFT,
  TEXT_ALIGN_CENTER,
  TEXT_ALIGN_RIGHT,
};

void renderer_DrawString( renderer_State *renderer, vec2 p, const char *text,
                          Font *font, vec4 colour, mat4 *viewProjection,
                          uint32_t alignment = TEXT_ALIGN_LEFT )
{
  if ( font->texture == 0 )
  {
    // Invalid font
    return;
  }
  uint32_t start = renderer->textBuffer.mesh.numVertices;

  float xOffset = 0.0f;
  float yOffset = 0.0f;
  const char *cursor = text;
  while ( *cursor )
  {
    if ( *cursor > 32 )
    {
      xOffset = AddCharacterToBuffer( &renderer->textBuffer, font, *cursor,
                                      xOffset, yOffset );
    }
    else if ( *cursor == ' ' )
    {
      xOffset += font->glyphs[32].advance;
    }
    else if ( *cursor == '\n' )
    {
      yOffset += ( font->ascent + font->descent + font->lineGap );
      xOffset = 0.0f;
    }
    else if ( *cursor == '\t' )
    {
      xOffset += font->glyphs[32].advance * 2;
    }
    cursor++;
  }
  uint32_t count = renderer->textBuffer.mesh.numVertices - start;

  UpdateTextBuffer( &renderer->textBuffer );

  TextShader shader = renderer->textShader;
  glUseProgram( shader.program );
  glUniformMatrix4fv( shader.viewProjection, 1, GL_FALSE, viewProjection->raw);
  float width = CalculateTextLength( font, text );
  if ( alignment == TEXT_ALIGN_CENTER )
  {
    p.x -= width * 0.5f;
  }
  else if ( alignment == TEXT_ALIGN_RIGHT )
  {
    p.x -= width;
  }
  mat4 model = Translate( p );
  glUniformMatrix4fv( shader.model, 1, GL_FALSE, model.raw );
  glUniform4fv( shader.colour, 1, colour.data );
  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, font->texture );
  glUniform1i( shader.glyphSheet, 0 );

  OpenGLDrawDynamicMesh( renderer->textBuffer.mesh, start, count );
}


void renderer_StartFrame( renderer_State *renderer, float r, float g, float b)
{
  glBindTexture( GL_TEXTURE_2D, 0 );
  glClearColor( r, g, b, 1.0f );
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  glEnable( GL_DEPTH_TEST );
}

void renderer_StartSceneFrame( renderer_State *renderer, uint32_t windowWidth, uint32_t windowHeight )
{
  glViewport( 0, 0, windowWidth, windowHeight );
  glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  glEnable( GL_DEPTH_TEST );
  glEnable( GL_TEXTURE_2D );
  glEnable( GL_CULL_FACE );
  if ( renderer->gbuffer.fbo )
  {
    glBindFramebuffer( GL_FRAMEBUFFER, renderer->gbuffer.fbo );
    glViewport( 0, 0, windowWidth, windowHeight );
    glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glActiveTexture( GL_TEXTURE0 );
    glDisable( GL_BLEND );
  }
}

internal void debug_Draw( debug_State *debugState, renderer_State *renderer, mat4 *viewProjection )
{
  VertexColourShader shader = renderer->vertexColourShader;
  glUseProgram( shader.program );
  glUniformMatrix4fv( shader.viewProjection, 1, GL_FALSE,
                      viewProjection->raw);
  OpenGLDrawDynamicMesh( renderer->lineBuffer.mesh );

  OpenGLDrawDynamicMesh( renderer->physicsLineBuffer.mesh );

  ColourShader colourShader = renderer->colourShader;
  glUseProgram( colourShader.program );
  glUniformMatrix4fv( colourShader.viewProjection, 1, GL_FALSE,
                      viewProjection->raw);
  for ( uint32_t i = 0; i < debugState->boxPool.size; ++i )
  {
    auto box = debugState->boxes + i;
    mat4 model = Translate( box->position ) * Scale( box->scale );
    glUniformMatrix4fv( colourShader.model, 1, GL_FALSE, model.raw );
    glUniform4fv( colourShader.colour, 1, box->colour.data );
    OpenGLDrawStaticMesh( renderer->wireFrameCube );
  }

  for ( uint32_t i = 0; i < debugState->spherePool.size; ++i )
  {
    auto sphere = debugState->spheres + i;
    mat4 model = Translate( sphere->position ) * Scale( Vec3( sphere->scale ) );
    glUniformMatrix4fv( colourShader.model, 1, GL_FALSE, model.raw );
    glUniform4fv( colourShader.colour, 1, sphere->colour.data );
    OpenGLDrawStaticMesh( renderer->sphere );
  }
}

void renderer_EndFrame( renderer_State *renderer )
{
  glDisable( GL_DEPTH_TEST );

  renderer->textBuffer.mesh.numVertices = 0;
  glEnable( GL_BLEND );
  glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
}

global debug_State *globalDebugState = NULL;

inline void debug_PushLine( vec3 start, vec3 end, vec4 colour,
                            float lifeTime = 0.0f )
{
  if ( globalDebugState )
  {
    debug_PushLine_( globalDebugState, start, end, colour, lifeTime );
  }
}

inline void debug_PushPoint( vec3 position, vec4 colour, float lifeTime = 0.0f )
{
  if ( globalDebugState )
  {
    debug_PushPoint_( globalDebugState, position, colour, lifeTime );
  }
}

inline void debug_PushBoxMinMax( vec3 min, vec3 max, vec4 colour,
                                 float lifeTime = 0.0f )
{
  if ( globalDebugState )
  {
    debug_PushBox_( globalDebugState, min, max, colour, lifeTime );
  }
}

inline void debug_PushBox( vec3 position, vec3 dimensions, vec4 colour, float lifeTime = 0.0f )
{
  if ( globalDebugState )
  {
    vec3 halfDim = dimensions * 0.5f;
    vec3 min = position - halfDim;
    vec3 max = position + halfDim;
    debug_PushBox_( globalDebugState, min, max, colour, lifeTime );
  }
}

inline void debug_PushSphere( vec3 position, float radius, vec4 colour, float lifeTime = 0.0f )
{
  if ( globalDebugState )
  {
    debug_PushSphere_( globalDebugState, position, radius, colour, lifeTime );
  }
}

#ifdef _MSC_VER
#define debug_Printf( FMT, ... )                                               \
  if ( globalDebugState )                                                      \
  {                                                                            \
    debug_Printf_( globalDebugState, FMT, __VA_ARGS__ );                       \
  }
#else
#define debug_Printf( FMT, ARGS... )                                           \
  if ( globalDebugState )                                                      \
  {                                                                            \
    debug_Printf_( globalDebugState, FMT, ##ARGS );                            \
  }
#endif

void RendererLightingPass( renderer_State *state, mat4 viewProjection, uint32_t windowWidth, uint32_t windowHeight )
{
  // TODO: Cleanup
  glBindFramebuffer( GL_FRAMEBUFFER, 0 );
  glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  glActiveTexture( GL_TEXTURE0 );
  glViewport( 0, 0, windowWidth, windowHeight );
  glEnable( GL_TEXTURE_2D );
  glDisable( GL_DEPTH_TEST );
  glDisable( GL_STENCIL_TEST );
  glDisable( GL_CULL_FACE );

  glEnable( GL_BLEND );

  auto gbuffer = state->gbuffer;
  auto directional = &state->directionalLightShader;
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, gbuffer.lightFbo );
  glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
  glClear( GL_COLOR_BUFFER_BIT );
  glBlendEquation( GL_FUNC_ADD );
  glBlendFunc( GL_ONE, GL_ONE );

  mat4 invViewProjection = Inverse( viewProjection );

  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, gbuffer.albedoTexture );
  glActiveTexture( GL_TEXTURE1 );
  glBindTexture( GL_TEXTURE_2D, gbuffer.normalTexture );
  glActiveTexture( GL_TEXTURE2 );
  glBindTexture( GL_TEXTURE_2D, gbuffer.depthTexture );

  glUseProgram( directional->program );
  glUniform1i( directional->albedoTexture, 0 );
  glUniform1i( directional->normalTexture, 1 );
  glUniform1i( directional->depthTexture, 2 );
  glUniformMatrix4fv( directional->invViewProjection, 1, GL_FALSE, invViewProjection.raw );

  OpenGLDrawStaticMesh( state->quad );

  glBlendEquation( GL_FUNC_ADD );
  glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
}

void PerformPostProcessing( renderer_State *renderer, vec2 screenSize )
{
  auto postProcessingShader = &renderer->postProcessingShader;
  auto gbuffer = &renderer->gbuffer;

  glDisable( GL_BLEND );
  glBindFramebuffer( GL_FRAMEBUFFER, 0 );
  glUseProgram( postProcessingShader->program );
  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, gbuffer->lightTexture );
  glUniform1i( postProcessingShader->hdrTexture, 0 );
  glUniform2fv( postProcessingShader->screenSize, 1, screenSize.data );

  OpenGLDrawStaticMesh( renderer->quad );
}

void renderer_SceneEndFrame( renderer_State *renderer, mat4 viewProjection, uint32_t windowWidth, uint32_t windowHeight)
{
  RendererLightingPass( renderer, viewProjection, windowWidth, windowHeight );
  PerformPostProcessing( renderer, Vec2( windowWidth, windowHeight ) );
}
