struct OpenGLStaticMesh
{
  uint32_t vao, vbo, ibo, numIndices, numVertices, primitive, indexType;
};

struct OpenGLDynamicMesh
{
  uint32_t vao;
  uint32_t vbo;
  uint32_t numVertices;
  uint32_t primitive;
  uint32_t maxVertices;
  uint32_t vertexSize;
  void *vertices;
};

struct OpenGLVertexAttribute
{
  uint32_t index;
  int numComponents;
  int componentType;
  int normalized;
  int offset;
};

enum
{
  VERTEX_ATTRIBUTE_POSITION = 0,
  VERTEX_ATTRIBUTE_NORMAL = 1,
  VERTEX_ATTRIBUTE_TEXTURE_COORDINATE = 2,
  VERTEX_ATTRIBUTE_COLOUR = 3,
  MAX_VERTEX_ATTRIBUTES,
};

void OpenGLDeleteDynamicMesh( OpenGLDynamicMesh mesh )
{
  glBindVertexArray( 0 );
  glDeleteVertexArrays( 1, &mesh.vao );
  glDeleteBuffers( 1, &mesh.vbo );
}

inline const void* ConvertUint32ToPointer( uint32_t i )
{
  return (const void*)( (uint8_t*)0 + i );
}

OpenGLDynamicMesh
OpenGLCreateDynamicMesh( void *vertices, uint32_t maxVertices,
                         uint32_t vertexSize,
                         OpenGLVertexAttribute *vertexAttributes,
                         uint32_t numVertexAttributes, int primitive )
{
  OpenGLDynamicMesh mesh = {};
  mesh.primitive = primitive;
  mesh.maxVertices = maxVertices;
  mesh.vertices = vertices;
  mesh.vertexSize = vertexSize;

  glGenVertexArrays( 1, &mesh.vao );
  glBindVertexArray( mesh.vao );

  glGenBuffers( 1, &mesh.vbo );
  glBindBuffer( GL_ARRAY_BUFFER, mesh.vbo );

  glBufferData( GL_ARRAY_BUFFER, vertexSize * maxVertices, NULL,
                GL_DYNAMIC_DRAW );

  for ( uint32_t i = 0; i < numVertexAttributes; ++i )
  {
    OpenGLVertexAttribute *attrib = vertexAttributes + i;
    if ( attrib->index >= MAX_VERTEX_ATTRIBUTES )
    {
      OpenGLDeleteDynamicMesh( mesh );
      ClearToZero( &mesh, sizeof( mesh ) );
      break;
    }
    glEnableVertexAttribArray( attrib->index);

    glVertexAttribPointer( attrib->index, attrib->numComponents,
                           attrib->componentType, attrib->normalized,
                           vertexSize,
                           ConvertUint32ToPointer( attrib->offset ) );
  }

  glBindVertexArray( 0 );
  return mesh;
}

void OpenGLUpdateDynamicMesh( OpenGLDynamicMesh mesh )
{
  glBindVertexArray( mesh.vao );
  glBindBuffer( GL_ARRAY_BUFFER, mesh.vbo );
  glBufferSubData( GL_ARRAY_BUFFER, 0, mesh.vertexSize * mesh.numVertices,
                   mesh.vertices );

  glBindVertexArray( 0 );
}

void OpenGLDrawDynamicMesh( OpenGLDynamicMesh mesh )
{
  glBindVertexArray( mesh.vao );
  glDrawArrays( mesh.primitive, 0, mesh.numVertices );
  glBindVertexArray( 0 );
}

void OpenGLDrawDynamicMesh( OpenGLDynamicMesh mesh, uint32_t start,
                            uint32_t count )
{
  glBindVertexArray( mesh.vao );
  glDrawArrays( mesh.primitive, start, count );
  glBindVertexArray( 0 );
}

void OpenGLDeleteStaticMesh( OpenGLStaticMesh mesh )
{
  glBindVertexArray( 0 );
  glDeleteVertexArrays( 1, &mesh.vao );
  glDeleteBuffers( 1, &mesh.vbo );
  if ( mesh.numIndices )
  {
    glDeleteBuffers( 1, &mesh.ibo );
  }
}

OpenGLStaticMesh
OpenGLCreateStaticMesh( const void *vertices, uint32_t numVertices,
                        uint32_t *indices, uint32_t numIndices,
                        uint32_t vertexSize,
                        OpenGLVertexAttribute *vertexAttributes,
                        uint32_t numVertexAttributes, int primitive )
{
  OpenGLStaticMesh mesh = {};
  mesh.primitive = primitive;
  mesh.indexType =
    GL_UNSIGNED_INT; // TODO: Use better index type if numVertices allowes it.
  mesh.numIndices = numIndices;
  mesh.numVertices = numVertices;

  glGenVertexArrays( 1, &mesh.vao );
  glBindVertexArray( mesh.vao );

  glGenBuffers( 1, &mesh.vbo );
  if ( mesh.numIndices )
  {
    glGenBuffers( 1, &mesh.ibo );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mesh.ibo );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( uint32_t ) * numIndices,
        indices, GL_STATIC_DRAW );
  }

  glBindBuffer( GL_ARRAY_BUFFER, mesh.vbo );

  glBufferData( GL_ARRAY_BUFFER, vertexSize * numVertices, vertices,
                GL_STATIC_DRAW );

  for ( uint32_t i = 0; i < numVertexAttributes; ++i )
  {
    OpenGLVertexAttribute *attrib = vertexAttributes + i;
    if ( attrib->index >= MAX_VERTEX_ATTRIBUTES )
    {
      OpenGLDeleteStaticMesh( mesh );
      // TODO: Zero memory.
      mesh.numIndices = 0;
      break;
    }
    glEnableVertexAttribArray( attrib->index);

    glVertexAttribPointer( attrib->index, attrib->numComponents,
                           attrib->componentType, attrib->normalized,
                           vertexSize,
                           ConvertUint32ToPointer( attrib->offset ) );
  }

  glBindVertexArray( 0 );
  return mesh;
}

void OpenGLDrawStaticMesh( OpenGLStaticMesh mesh )
{
  glBindVertexArray( mesh.vao );
  if ( mesh.numIndices )
  {
    glDrawElements( mesh.primitive, mesh.numIndices, mesh.indexType, NULL );
  }
  else
  {
    glDrawArrays( mesh.primitive, 0, mesh.numVertices );
  }
}

OpenGLStaticMesh CreateWireframeCube()
{
  // clang-format off
  float vertices[] =
  {
    -0.5f, 0.5f, -0.5f,
    0.5f, 0.5f, -0.5f,
    0.5f, 0.5f, 0.5f,
    -0.5f, 0.5f, 0.5f,

    -0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, 0.5f,
    -0.5f, -0.5f, 0.5f,
  };

  uint32_t indices[] =
  {
    // Top
    0, 1,
    1, 2,
    2, 3,
    3, 0,

    // Bottom
    4, 5,
    5, 6,
    6, 7,
    7, 4,

    // Back
    0, 4,
    1, 5,

    // Front
    3, 7,
    2, 6
  };

  OpenGLVertexAttribute attrib;
  attrib.index = VERTEX_ATTRIBUTE_POSITION;
  attrib.numComponents = 3;
  attrib.componentType = GL_FLOAT;
  attrib.normalized = GL_FALSE;
  attrib.offset = 0;

  return OpenGLCreateStaticMesh(
    vertices, 8, indices, 24, sizeof( float ) * 3, &attrib, 1, GL_LINES );
}

OpenGLStaticMesh CreateAxisMesh()
{
  // clang-format off
  float vertices[] =
  {
    // X Axis
    0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
    1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

    // Y Axis
    0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

    // Z Axis
    0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
    0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f
  };
  // clang-format on

  OpenGLVertexAttribute attribs[2];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = VERTEX_ATTRIBUTE_COLOUR;
  attribs[1].numComponents = 3;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = sizeof( float ) * 3;

  return OpenGLCreateStaticMesh( vertices, 6, nullptr, 0, sizeof( float ) * 6,
                                 attribs, 2, GL_LINES );
}
OpenGLStaticMesh CreateQuadMesh()
{
  float vertices[ ] = {
    -1, 1,0,0,1,
    -1,-1,0,0,0,
     1,-1,0,1,0,
     1, 1,0,1,1,
  };

  OpenGLVertexAttribute attribs[2];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
  attribs[1].numComponents = 2;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = sizeof( float ) * 3;

  return OpenGLCreateStaticMesh( vertices, 4, nullptr, 0, sizeof( float ) * 5,
                                 attribs, 2, GL_QUADS );
}
struct TriangleIndices
{
  uint32_t i,j,k;
};

inline uint32_t AddMidPoint( uint32_t index1, uint32_t index2,
                             vec3 *vertices, uint32_t *vertexCount)
{
  vec3 v1 = vertices[index1];
  vec3 v2 = vertices[index2];

  vertices[ (*vertexCount)++ ] = Normalize( v1 + v2 );
  return (*vertexCount) - 1;
}

OpenGLStaticMesh CreateIcosahedronMesh( uint32_t tesselationLevel, MemoryArena *arena )
{
  uint32_t maxVertices = KILOBYTES( 32 );
  uint32_t maxIndices = KILOBYTES( 64 );

  vec3 *vertices = AllocateArray( arena, vec3, maxVertices );
  TriangleIndices *indices = AllocateArray( arena, TriangleIndices, maxIndices );

  uint32_t vertexCount = 0;
  uint32_t indexCount = 0;

  float t = ( 1.0f + SquareRoot( 5.0f ) ) * 0.5f;
  vertices[ vertexCount++ ] = Normalize( Vec3( -1, t, 0 ) );
  vertices[ vertexCount++ ] = Normalize( Vec3( 1, t, 0 ) );
  vertices[ vertexCount++ ] = Normalize( Vec3( -1, -t, 0 ) );
  vertices[ vertexCount++ ] = Normalize( Vec3( 1, -t, 0 ) );

  vertices[ vertexCount++ ] = Normalize( Vec3( 0, -1, t ) );
  vertices[ vertexCount++ ] = Normalize( Vec3( 0, 1, t ) );
  vertices[ vertexCount++ ] = Normalize( Vec3( 0, -1, -t ) );
  vertices[ vertexCount++ ] = Normalize( Vec3( 0, 1, -t ) );

  vertices[ vertexCount++ ] = Normalize( Vec3( t, 0, -1 ) );
  vertices[ vertexCount++ ] = Normalize( Vec3( t, 0, 1 ) );
  vertices[ vertexCount++ ] = Normalize( Vec3( -t, 0, -1 ) );
  vertices[ vertexCount++ ] = Normalize( Vec3( -t, 0, 1 ) );

  indices[ indexCount++ ] = TriangleIndices{ 0, 11, 5 };
  indices[ indexCount++ ] = TriangleIndices{ 0, 5, 1 };
  indices[ indexCount++ ] = TriangleIndices{ 0, 1, 7 };
  indices[ indexCount++ ] = TriangleIndices{ 0, 7, 10 };
  indices[ indexCount++ ] = TriangleIndices{ 0, 10, 11 };

  indices[ indexCount++ ] = TriangleIndices{ 1, 5, 9 };
  indices[ indexCount++ ] = TriangleIndices{ 5, 11, 4 };
  indices[ indexCount++ ] = TriangleIndices{ 11, 10, 2 };
  indices[ indexCount++ ] = TriangleIndices{ 10, 7, 6 };
  indices[ indexCount++ ] = TriangleIndices{ 7, 1, 8 };

  indices[ indexCount++ ] = TriangleIndices{ 3, 9, 4 };
  indices[ indexCount++ ] = TriangleIndices{ 3, 4, 2 };
  indices[ indexCount++ ] = TriangleIndices{ 3, 2, 6 };
  indices[ indexCount++ ] = TriangleIndices{ 3, 6, 8 };
  indices[ indexCount++ ] = TriangleIndices{ 3, 8, 9 };

  indices[ indexCount++ ] = TriangleIndices{ 4, 9, 5 };
  indices[ indexCount++ ] = TriangleIndices{ 2, 4, 11 };
  indices[ indexCount++ ] = TriangleIndices{ 6, 2, 10 };
  indices[ indexCount++ ] = TriangleIndices{ 8, 6, 7 };
  indices[ indexCount++ ] = TriangleIndices{ 9, 8, 1 };

  for ( uint32_t i = 0; i < tesselationLevel; ++i )
  {
    TriangleIndices *newIndices = AllocateArray( arena, TriangleIndices, maxIndices );
    uint32_t newIndexCount = 0;
    for ( size_t j = 0; j < indexCount; ++j )
    {
      uint32_t a = AddMidPoint( indices[j].i, indices[j].j, vertices, &vertexCount );
      uint32_t b = AddMidPoint( indices[j].j, indices[j].k, vertices, &vertexCount );
      uint32_t c = AddMidPoint( indices[j].k, indices[j].i, vertices, &vertexCount );

      newIndices[ newIndexCount++ ] = TriangleIndices{ indices[ j ].i, a, c };
      newIndices[ newIndexCount++ ] = TriangleIndices{ indices[ j ].j, b, a };
      newIndices[ newIndexCount++ ] = TriangleIndices{ indices[ j ].k, c, b };
      newIndices[ newIndexCount++ ] = TriangleIndices{ a, b, c };
      ASSERT( newIndexCount < maxIndices );
    }

    indices = newIndices;
    indexCount = newIndexCount;
  }

  ASSERT( vertexCount < maxVertices );
  ASSERT( indexCount < maxIndices );

  OpenGLVertexAttribute attribs[1];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;

  OpenGLStaticMesh result = OpenGLCreateStaticMesh( vertices, vertexCount, (uint32_t*)indices, indexCount * 3,
     sizeof( vec3 ), attribs, 1, GL_TRIANGLES );

  // Free all memory allocated to build mesh data
  MemoryArenaFree( arena, vertices );
  return result;
}
