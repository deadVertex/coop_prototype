#pragma once

#include "math.h"

enum
{
  EASE_QUAD_IN,
  EASE_QUAD_OUT,
  EASE_QUAD_IN_OUT,
};

struct EasingVec2
{
  vec2 start;
  vec2 delta;
  vec2 value;
  float duration;
  float t;
  int function;
};
struct EasingFloat
{
  float start;
  float delta;
  float value;
  float duration;
  float t;
  int function;
};

// NOTE: t must be normalized.
inline vec2 EaseOutQuad( float t, vec2 start, vec2 delta,
                              float duration )
{
  return -delta * t * ( t - 2.0f ) + start;
}

inline vec2 EaseInQuad( float t, vec2 start, vec2 delta,
                             float duration )
{
  return delta * t * t + start;
}

inline vec2 EaseInOutQuad( float t, vec2 start, vec2 delta,
                                float duration )
{
  return ( t / 2.0f < 1.0f ) ? EaseInQuad( t, start, delta, duration )
                             : EaseOutQuad( t, start, delta, duration );
}

// NOTE: t must be normalized.
inline float EaseOutQuad( float t, float start, float delta, float duration )
{
  return -delta * t * ( t - 2.0f ) + start;
}

inline float EaseInQuad( float t, float start, float delta, float duration )
{
  return delta * t * t + start;
}

inline float EaseInOutQuad( float t, float start, float delta, float duration )
{
  return ( t / 2.0f < 1.0f ) ? EaseInQuad( t, start, delta, duration )
                             : EaseOutQuad( t, start, delta, duration );
}

inline void UpdateEasing( EasingVec2 *tween, float dt )
{
  float t = 0.0f;
  if ( tween->duration > 0.0f )
  {
    t = tween->t / tween->duration;
    t = Clamp( t, 0.0f, 1.0f );
  }
  switch ( tween->function )
  {
  case EASE_QUAD_IN:
    tween->value = EaseInQuad( t, tween->start, tween->delta, tween->duration );
    break;
  case EASE_QUAD_OUT:
    tween->value =
      EaseOutQuad( t, tween->start, tween->delta, tween->duration );
    break;
  case EASE_QUAD_IN_OUT:
    tween->value =
      EaseInOutQuad( t, tween->start, tween->delta, tween->duration );
    break;
  default:
    break;
  }
  tween->t += dt;
}

inline void UpdateEasing( EasingFloat *tween, float dt )
{
  float t = 0.0f;
  if ( tween->duration > 0.0f )
  {
    t = tween->t / tween->duration;
    t = Clamp( t, 0.0f, 1.0f );
  }
  switch ( tween->function )
  {
  case EASE_QUAD_IN:
    tween->value = EaseInQuad( t, tween->start, tween->delta, tween->duration );
    break;
  case EASE_QUAD_OUT:
    tween->value =
      EaseOutQuad( t, tween->start, tween->delta, tween->duration );
    break;
  case EASE_QUAD_IN_OUT:
    tween->value =
      EaseInOutQuad( t, tween->start, tween->delta, tween->duration );
    break;
  default:
    break;
  }
  tween->t += dt;
}

