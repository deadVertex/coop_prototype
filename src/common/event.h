#pragma once

#include <cstring>

struct EventHeader
{
  uint32_t typeId, size;
};

struct SimpleEventQueue
{
  void *buffer;
  uint32_t size;
  uint32_t capacity;
};

inline SimpleEventQueue evt_CreateQueue( void *buffer, uint32_t capacity )
{
  SimpleEventQueue result = {};
  result.buffer = buffer;
  result.capacity = capacity;
  return result;
}

#define evt_Push( QUEUE, EVENT, TYPE )                                         \
  evt_Push_( QUEUE, EVENT, sizeof( TYPE ), TYPE##TypeId )
inline void evt_Push_( SimpleEventQueue *queue, void *data, uint32_t size,
                         uint32_t typeId )
{
  ASSERT( size > 0 );
  uint32_t finalSize = size + sizeof( EventHeader );
  ASSERT( queue->size + finalSize < queue->capacity );
  EventHeader *dst = (EventHeader *)( (uint8_t *)queue->buffer + queue->size );
  dst->typeId = typeId;
  dst->size = size;
  memcpy( dst + 1, data, size );
  queue->size += finalSize;
}

struct EventPeakResult
{
  EventHeader *header;
  void *data;
};

inline EventPeakResult
  evt_Peak( SimpleEventQueue *queue, EventHeader *prev = NULL )
{
  EventPeakResult result = {};
  uint32_t offset = 0;
  if ( prev )
  {
    offset = (uint8_t*)prev - (uint8_t*)queue->buffer;
    offset += sizeof( EventHeader ) + prev->size;
  }
  if ( offset < queue->size )
  {
    result.header = (EventHeader *)( (uint8_t *)queue->buffer + offset );
    result.data = result.header + 1;
  }
  return result;
}

inline void evt_ClearQueue( SimpleEventQueue *queue )
{
  queue->size = 0;
}
