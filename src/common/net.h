#pragma once

#define NET_MAX_CLIENTS 4
#define NET_SERVER_PACKET_SIZE 200
#define NET_CLIENT_PACKET_SIZE 100

#define NET_MAX_ACKS_PER_PACKET 8
#define NET_NULL_CLIENT_ID 0
#define NET_SERVER_CLIENT_ID 0xFFFFFFFF

struct Packet
{
  uint16_t acks[NET_MAX_ACKS_PER_PACKET];
  void *data;
  uint32_t len;
  uint32_t capacity; // not sent
  uint32_t clientId;
  uint32_t timestamp; // Unit is milliseconds
  uint32_t returnTimestamp; // ^
  uint16_t sequenceNumber;
};

