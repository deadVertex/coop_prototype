
internal char ConvertKeyToCharacter( uint8_t key, bool isShiftActive )
{
  if ( key == K_SPACE )
  {
    return ' ';
  }

  if ( key >= K_A && key <= K_Z )
  {
    if ( isShiftActive )
    {
      return (char)key;
    }
    else
    {
      return (char)key + ( 'a' - 'A' );
    }
  }
  if ( isShiftActive )
  {
    switch ( key )
    {
    case K_APOSTROPHE:
      return '\"';
    case K_COMMA:
      return '<';
    case K_MINUS:
      return '_';
    case K_PERIOD:
      return '>';
    case K_SLASH:
      return '?';
    case K_0:
      return ')';
    case K_1:
      return '!';
    case K_2:
      return '@';
    case K_3:
      return '#';
    case K_4:
      return '$';
    case K_5:
      return '%';
    case K_6:
      return '^';
    case K_7:
      return '&';
    case K_8:
      return '*';
    case K_9:
      return '(';
    case K_SEMI_COLON:
      return ':';
    case K_EQUAL:
      return '+';
    case K_LEFT_BRACKET:
      return '{';
    case K_BACKSLASH:
      return '|';
    case K_RIGHT_BRACKET:
      return '}';
    default:
      break;
    }
  }
  else
  {
    switch ( key )
    {
    case K_APOSTROPHE:
      return '\'';
    case K_COMMA:
      return ',';
    case K_MINUS:
      return '-';
    case K_PERIOD:
      return '.';
    case K_SLASH:
      return '/';
    case K_0:
      return '0';
    case K_1:
      return '1';
    case K_2:
      return '2';
    case K_3:
      return '3';
    case K_4:
      return '4';
    case K_5:
      return '5';
    case K_6:
      return '6';
    case K_7:
      return '7';
    case K_8:
      return '8';
    case K_9:
      return '9';
    case K_SEMI_COLON:
      return ';';
    case K_EQUAL:
      return '=';
    case K_LEFT_BRACKET:
      return '[';
    case K_BACKSLASH:
      return '\\';
    case K_RIGHT_BRACKET:
      return ']';
    default:
      break;
    }
  }
  // TODO: Support other keys

  return 0;
}
