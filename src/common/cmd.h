#pragma once

#define CMD_MAX_ARGS 4
#define MAX_CMD_NAME_LENGTH 20
#define MAX_CMDS 256
#define CMD_WORKING_BUFFER_SIZE KILOBYTES(2)
#define CMD_EVENT_BUFFER_LENGTH 255

#define CMD_UNKNOWN_COMMAND 0
#define CMD_FAILURE 1
#define CMD_SUCCESS 2

struct CommandEntry
{
  char name[MAX_CMD_NAME_LENGTH + 1];
  uint32_t id;
};

struct ExecuteCommandEvent
{
  const char *args[CMD_MAX_ARGS];
  uint32_t command;
  uint32_t numArgs;
};

struct SimpleEventQueue;

struct CommandSystemBuffer
{
  ExecuteCommandEvent events[CMD_EVENT_BUFFER_LENGTH];
  uint32_t eventCount;

  char workingBuffer[CMD_WORKING_BUFFER_SIZE];
  uint32_t workingBufferSize;
};

struct CommandSystem
{
  CommandEntry cmds[MAX_CMDS];
  ContiguousObjectPool cmdPool;

  CommandSystemBuffer buffers[2];
  uint32_t bufferIdx;
};
