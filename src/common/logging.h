#pragma once

#include <cstdarg>
#ifdef _MSC_VER
#include <windows.h>
#endif

extern uint32_t log_activeChannels;

enum
{
  LOG_CHANNEL_DEBUG = BIT( 0 ),
  LOG_CHANNEL_WARNING = BIT( 1 ),
  LOG_CHANNEL_ERROR = BIT( 2 ),
  LOG_CHANNEL_INFO = BIT( 3 ),
};

inline void log_Printf( uint32_t channel, const char *fmt, ... )
{
  char buffer[256];
  va_list args;
  if ( log_activeChannels & channel )
  {
    va_start( args, fmt );
    auto n = vsnprintf( buffer, sizeof( buffer ), fmt, args );
    if ( n < 0 || n == sizeof( buffer ) )
    {
      // Failed to log
    }
    else
    {
      const char *channelName;
      switch (channel)
      {
        case LOG_CHANNEL_DEBUG:
          channelName = "DEBUG";
          break;
        case LOG_CHANNEL_WARNING:
          channelName = "WARNING";
          break;
        case LOG_CHANNEL_ERROR:
          channelName = "ERROR";
          break;
        case LOG_CHANNEL_INFO:
          channelName = "INFO";
          break;
        default:
          channelName = "UNKNOWN";
          break;
      }
#ifdef _MSC_VER
      char outputBuffer[ 280 ];
      _snprintf( outputBuffer, sizeof( outputBuffer ), "%s %s\n", channelName, buffer );
      OutputDebugString( outputBuffer );
#else
      printf( "%s %s\n", channelName, buffer );
#endif
    }
  }
  va_end( args );
}

#ifdef _MSC_VER
#define LOG_MSG( CHANNEL, FMT, ... )                                   \
  log_Printf( CHANNEL, "%s(%d):%s - " FMT, __FILE__, __LINE__, __FUNCTION__,   \
              __VA_ARGS__ )
#define LOG_DEBUG( FMT, ... ) LOG_MSG( LOG_CHANNEL_DEBUG, FMT, __VA_ARGS__ )
#define LOG_WARNING( FMT, ... ) LOG_MSG( LOG_CHANNEL_WARNING, FMT, __VA_ARGS__ )
#define LOG_WARN( ... ) LOG_WARNING( __VA_ARGS__ )
#define LOG_ERROR( FMT, ... ) LOG_MSG( LOG_CHANNEL_ERROR, FMT, __VA_ARGS__ )
#define LOG_INFO( FMT, ... ) LOG_MSG( LOG_CHANNEL_INFO, FMT, __VA_ARGS__ )
#else
#define LOG_MSG( CHANNEL, FMT, ARGS... )                                       \
  log_Printf( CHANNEL, "%s(%d):%s - " FMT, __FILE__, __LINE__, __FUNCTION__,   \
              ##ARGS )

#define LOG_DEBUG( FMT, ARGS... ) LOG_MSG( LOG_CHANNEL_DEBUG, FMT, ##ARGS )
#define LOG_WARNING( FMT, ARGS... ) LOG_MSG( LOG_CHANNEL_WARNING, FMT, ##ARGS )
#define LOG_WARN( ... ) LOG_WARNING( __VA_ARGS__ )
#define LOG_ERROR( FMT, ARGS... ) LOG_MSG( LOG_CHANNEL_ERROR, FMT, ##ARGS )
#define LOG_INFO( FMT, ARGS... ) LOG_MSG( LOG_CHANNEL_INFO, FMT, ##ARGS )
#endif
