#pragma once

#include <cmath>

#define PI 3.14159265359

inline float Radians( float degrees )
{
  float result = degrees * ( PI / 180.0f );
  return result;
}

inline float SquareRoot(float x)
{
  float result = sqrtf(x);
  return result;
}

inline float Clamp(float x, float min, float max)
{
  if ( x < min )
  {
    x = min;
  }
  else if ( x > max )
  {
    x = max;
  }
  return x;
}

inline float Sign( float x )
{
  float result = 0.0f;
  if ( x > 0.0f )
  {
    result = 1.0f;
  }
  else if ( x < 0.0f )
  {
    result = -1.0f;
  }
  return result;
}

inline float Sin( float x )
{
  float result = sin( x );
  return result;
}

inline float Cos( float x )
{
  float result = cos( x );
  return result;
}

inline float Tan( float x )
{
  float result = tan( x );
  return result;
}

inline float ACos( float x )
{
  float result = acos( x );
  return result;
}

inline float ATan( float x, float y )
{
  float result = atan2( x, y );
  return result;
}

inline float Lerp( float a, float b, float t )
{
  float result = a * ( 1.0f - t ) + b  * t;
  return result;
}

union vec2
{
  struct
  {
    float x;
    float y;
  };
  struct
  {
    float u;
    float v;
  };
  float data[2];
};

inline vec2 Vec2(float x)
{
  vec2 result;
  result.x = x;
  result.y = x;
  return result;
}

inline vec2 Vec2(float x, float y)
{
  vec2 result;
  result.x = x;
  result.y = y;
  return result;
}

inline vec2 operator*(vec2 a, float b)
{
  vec2 result;
  result.x = a.x * b;
  result.y = a.y * b;
  return result;
}

inline vec2 operator*(float a, vec2 b)
{
  vec2 result;
  result.x = a * b.x;
  result.y = a * b.y;
  return result;
}

inline vec2& operator*=(vec2 &a, float b)
{
  a = a * b;
  return a;
}

inline vec2 operator+(vec2 a, vec2 b)
{
  vec2 result;
  result.x = a.x + b.x;
  result.y = a.y + b.y;
  return result;
}

inline vec2& operator+=( vec2 &a, vec2 b )
{
  a = a + b;
  return a;
}

inline vec2 operator-(vec2 a)
{
  vec2 result;
  result.x = -a.x;
  result.y = -a.y;
  return result;
}

inline vec2 operator-(vec2 a, vec2 b)
{
  vec2 result;
  result.x = a.x - b.x;
  result.y = a.y - b.y;
  return result;
}

inline vec2& operator-=(vec2 &a, vec2 b)
{
  a = a - b;
  return a;
}

inline float Dot( vec2 a, vec2 b )
{
  float result = a.x * b.x + a.y * b.y;
  return result;
}

inline float LengthSq( vec2 a )
{
  float result = Dot( a, a );
  return result;
}

inline float Length( vec2 a )
{
  float result = SquareRoot( LengthSq( a ) );
  return result;
}

inline vec2 Normalize( vec2 a )
{
  vec2 result;
  float length = Length( a );
  ASSERT( length > 0.0f );
  result.x = a.x / length;
  result.y = a.y / length;
  return result;
}

inline vec2 Lerp( vec2 a, vec2 b, float t )
{
  vec2 result;
  result = a * ( 1.0f - t ) + b * t;
  return result;
}

inline vec2 Rotate( vec2 a, float b )
{
  vec2 result;
  result.x = a.x * Cos( b ) - a.y * Sin( b );
  result.y = a.x * Sin( b ) + a.y * Cos( b );
  return result;
}

union vec3
{
  struct
  {
    float x;
    float y;
    float z;
  };
  struct
  {
    float r;
    float g;
    float b;
  };
  float data[3];
};

inline vec3 Vec3(float x)
{
  vec3 result;
  result.x = x;
  result.y = x;
  result.z = x;
  return result;
}

inline vec3 Vec3(float x, float y, float z)
{
  vec3 result;
  result.x = x;
  result.y = y;
  result.z = z;
  return result;
}

inline vec3 operator*(vec3 a, float b)
{
  vec3 result;
  result.x = a.x * b;
  result.y = a.y * b;
  result.z = a.z * b;
  return result;
}

inline vec3 operator*(float a, vec3 b)
{
  vec3 result;
  result.x = a * b.x;
  result.y = a * b.y;
  result.z = a * b.z;
  return result;
}

inline vec3& operator*=(vec3 &a, float b)
{
  a = a * b;
  return a;
}

inline vec3 operator+(vec3 a, vec3 b)
{
  vec3 result;
  result.x = a.x + b.x;
  result.y = a.y + b.y;
  result.z = a.z + b.z;
  return result;
}

inline vec3& operator+=( vec3 &a, vec3 b )
{
  a = a + b;
  return a;
}

inline vec3 operator-(vec3 a)
{
  vec3 result;
  result.x = -a.x;
  result.y = -a.y;
  result.z = -a.z;
  return result;
}

inline vec3 operator-(vec3 a, vec3 b)
{
  vec3 result;
  result.x = a.x - b.x;
  result.y = a.y - b.y;
  result.z = a.z - b.z;
  return result;
}

inline vec3& operator-=(vec3 &a, vec3 b)
{
  a = a - b;
  return a;
}

inline float Dot( vec3 a, vec3 b )
{
  float result = a.x * b.x + a.y * b.y + a.z * b.z;
  return result;
}

inline float LengthSq( vec3 a )
{
  float result = Dot( a, a );
  return result;
}

inline float Length( vec3 a )
{
  float result = SquareRoot( LengthSq( a ) );
  return result;
}

inline vec3 Normalize( vec3 a )
{
  vec3 result;
  float length = Length( a );
  ASSERT( length > 0.0f );
  result.x = a.x / length;
  result.y = a.y / length;
  result.z = a.z / length;
  return result;
}

inline vec3 Lerp( vec3 a, vec3 b, float t )
{
  vec3 result;
  result = a * ( 1.0f - t ) + b * t;
  return result;
}

union vec4
{
  struct
  {
    float x;
    float y;
    float z;
    float w;
  };
  struct
  {
    float r;
    float g;
    float b;
    float a;
  };
  struct
  {
    vec2 xy;
    vec2 zw;
  };
  struct
  {
    vec3 xyz;
  };
  float data[4];
};

inline vec4 Vec4( float x )
{
  vec4 result;
  result.x = x;
  result.y = x;
  result.z = x;
  result.w = x;
  return result;
}

inline vec4 Vec4( float x, float y, float z, float w )
{
  vec4 result;
  result.x = x;
  result.y = y;
  result.z = z;
  result.w = w;
  return result;
}

inline vec4 Vec4( vec3 v3, float w )
{
  vec4 result;
  result.x = v3.x;
  result.y = v3.y;
  result.z = v3.z;
  result.w = w;
  return result;
}

inline vec4 operator*( vec4 a, float b )
{
  vec4 result;
  result.x = a.x * b;
  result.y = a.y * b;
  result.z = a.z * b;
  result.w = a.w * b;
  return result;
}

inline vec4 operator+( vec4 a, vec4 b )
{
  vec4 result;
  result.x = a.x + b.x;
  result.y = a.y + b.y;
  result.z = a.z + b.z;
  result.w = a.w + b.w;
  return result;
}

inline vec4 operator-( vec4 a, vec4 b )
{
  vec4 result;
  result.x = a.x - b.x;
  result.y = a.y - b.y;
  result.z = a.z - b.z;
  result.w = a.w - b.w;
  return result;
}

inline vec4 operator*( vec4 a, vec4 b )
{
  vec4 result;
  result.x = a.x * b.x;
  result.y = a.y * b.y;
  result.z = a.z * b.z;
  result.w = a.w * b.w;
  return result;
}

inline float Dot( vec4 a, vec4 b )
{
  float result = a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
  return result;
}

inline vec4 Lerp( vec4 a, vec4 b, float t )
{
  vec4 result;
  result = a * ( 1.0f - t ) + b * t;
  return result;
}

inline float LengthSq( vec4 a )
{
  float result = Dot( a, a );
  return result;
}

inline float Length( vec4 a )
{
  float result = SquareRoot( LengthSq( a ) );
  return result;
}

inline vec4 Normalize( vec4 a )
{
  vec4 result;
  float length = Length( a );
  ASSERT( length > 0.0f );
  result.x = a.x / length;
  result.y = a.y / length;
  result.z = a.z / length;
  result.w = a.w / length;
  return result;
}

union mat4
{
  vec4 col[ 4 ];
  float raw[ 16 ];
};

inline mat4 Mat4( vec4 a, vec4 b, vec4 c, vec4 d )
{
  mat4 result = {};
  result.col[ 0 ] = a;
  result.col[ 1 ] = b;
  result.col[ 2 ] = c;
  result.col[ 3 ] = d;
  return result;
}

inline mat4 Identity()
{
  mat4 result = {};
  result.col[ 0 ] = Vec4( 1, 0, 0, 0 );
  result.col[ 1 ] = Vec4( 0, 1, 0, 0 );
  result.col[ 2 ] = Vec4( 0, 0, 1, 0 );
  result.col[ 3 ] = Vec4( 0, 0, 0, 1 );
  return result;
}

inline mat4 Orthographic( float left, float right, float top, float bottom, float n = -1.0f, float f = 1.0f)
{
  mat4 result = {};
  result.col[ 0 ] = Vec4( 2.0f / ( right - left ), 0, 0, 0 );
  result.col[ 1 ] = Vec4( 0, 2.0f / ( top - bottom ), 0, 0 );
  result.col[ 2 ] = Vec4( 0, 0, -2 / ( f -n ), 0.0f );
  result.col[ 3 ] = Vec4( -( right + left ) / ( right - left ), -( top + bottom ) / ( top - bottom ), -(f + n)/(f-n), 1 );
  return result;
}

inline mat4 Perspective( float fov, float aspect, float n, float f )
{
  float tanHalfFovy = Tan( fov * 0.5f );

  mat4 result = {};
  result.col[0].x = 1.0f / ( aspect * tanHalfFovy );
  result.col[1].y = 1.0f / tanHalfFovy;
  result.col[2].z = - ( f + n ) / ( f - n );
  result.col[2].w = -1;
  result.col[3].z = -( 2.0f * f * n ) / ( f - n );
  return result;
}

inline mat4 Translate( vec2 p )
{
  mat4 result = Identity();
  result.col[ 3 ].x = p.x;
  result.col[ 3 ].y = p.y;
  return result;
}

inline mat4 Translate( vec3 p )
{
  mat4 result = Identity();
  result.col[ 3 ].x = p.x;
  result.col[ 3 ].y = p.y;
  result.col[ 3 ].z = p.z;
  return result;
}

inline mat4 Scale( vec2 p )
{
  mat4 result = Identity();
  result.col[ 0 ].x = p.x;
  result.col[ 1 ].y = p.y;
  return result;
}

inline mat4 Scale( vec3 p )
{
  mat4 result = Identity();
  result.col[ 0 ].x = p.x;
  result.col[ 1 ].y = p.y;
  result.col[ 2 ].z = p.z;
  return result;
}

inline mat4 Rotate( float angle )
{
  mat4 result = Identity();
  result.col[0].x = Cos( angle );
  result.col[0].y = Sin( angle );
  result.col[1].x = -Sin( angle );
  result.col[1].y = Cos( angle );
  return result;
}

inline mat4 RotateX( float angle )
{
  mat4 result = Identity();
  result.col[1].y = Cos( angle );
  result.col[1].z = Sin( angle );
  result.col[2].y = -Sin( angle );
  result.col[2].z = Cos( angle );
  return result;
}

inline mat4 RotateY( float angle )
{
  mat4 result = Identity();
  result.col[0].x = Cos( angle );
  result.col[0].z = -Sin( angle );
  result.col[2].x = Sin( angle );
  result.col[2].z = Cos( angle );
  return result;
}

inline mat4 RotateZ( float angle )
{
  mat4 result = Identity();
  result.col[0].x = Cos( angle );
  result.col[0].y = Sin( angle );
  result.col[1].x = -Sin( angle );
  result.col[1].y = Cos( angle );
  return result;
}

inline mat4 operator*( mat4 a, mat4 b )
{
  mat4 result;
  result.col[ 0 ] = a.col[ 0 ] * b.col[ 0 ].x + a.col[ 1 ] * b.col[ 0 ].y + a.col[ 2 ] * b.col[ 0 ].z + a.col[ 3 ] * b.col[ 0 ].w;
  result.col[ 1 ] = a.col[ 0 ] * b.col[ 1 ].x + a.col[ 1 ] * b.col[ 1 ].y + a.col[ 2 ] * b.col[ 1 ].z + a.col[ 3 ] * b.col[ 1 ].w;
  result.col[ 2 ] = a.col[ 0 ] * b.col[ 2 ].x + a.col[ 1 ] * b.col[ 2 ].y + a.col[ 2 ] * b.col[ 2 ].z + a.col[ 3 ] * b.col[ 2 ].w;
  result.col[ 3 ] = a.col[ 0 ] * b.col[ 3 ].x + a.col[ 1 ] * b.col[ 3 ].y + a.col[ 2 ] * b.col[ 3 ].z + a.col[ 3 ] * b.col[ 3 ].w;
  return result;
}

inline mat4& operator*=( mat4 &a, mat4 b )
{
  a = a * b;
  return a;
}

inline mat4 Transpose( mat4 a )
{
  mat4 result;
  result.raw[ 0 ] = a.raw[ 0 ];
  result.raw[ 1 ] = a.raw[ 4 ];
  result.raw[ 2 ] = a.raw[ 8 ];
  result.raw[ 3 ] = a.raw[ 12 ];

  result.raw[ 4 ] = a.raw[ 1 ];
  result.raw[ 5 ] = a.raw[ 5 ];
  result.raw[ 6 ] = a.raw[ 9 ];
  result.raw[ 7 ] = a.raw[ 13 ];

  result.raw[ 8 ] = a.raw[ 2 ];
  result.raw[ 9 ] = a.raw[ 6 ];
  result.raw[ 10 ] = a.raw[ 10 ];
  result.raw[ 11 ] = a.raw[ 14 ];

  result.raw[ 12 ] = a.raw[ 3 ];
  result.raw[ 13 ] = a.raw[ 7 ];
  result.raw[ 14 ] = a.raw[ 11 ];
  result.raw[ 15 ] = a.raw[ 15 ];

  return result;
}

inline vec4 operator*( mat4 a, vec4 b )
{
  vec4 result = {};
  a = Transpose( a );
  result.x = Dot( b, a.col[0] );
  result.y = Dot( b, a.col[1] );
  result.z = Dot( b, a.col[2] );
  result.w = Dot( b, a.col[3] );
  return result;
}

inline mat4 operator*( mat4 a, float b )
{
  mat4 result;
  result.col[ 0 ] = a.col[ 0 ] * b;
  result.col[ 1 ] = a.col[ 1 ] * b;
  result.col[ 2 ] = a.col[ 2 ] * b;
  result.col[ 3 ] = a.col[ 3 ] * b;
  return result;
}

// Modified version of compute_inverse for mat4x4 from GLM library
inline mat4 Inverse( mat4 m )
{
		float c00 = m.col[2].data[2] * m.col[3].data[3] - m.col[3].data[2] * m.col[2].data[3];
		float c02 = m.col[1].data[2] * m.col[3].data[3] - m.col[3].data[2] * m.col[1].data[3];
		float c03 = m.col[1].data[2] * m.col[2].data[3] - m.col[2].data[2] * m.col[1].data[3];

		float c04 = m.col[2].data[1] * m.col[3].data[3] - m.col[3].data[1] * m.col[2].data[3];
		float c06 = m.col[1].data[1] * m.col[3].data[3] - m.col[3].data[1] * m.col[1].data[3];
		float c07 = m.col[1].data[1] * m.col[2].data[3] - m.col[2].data[1] * m.col[1].data[3];

		float c08 = m.col[2].data[1] * m.col[3].data[2] - m.col[3].data[1] * m.col[2].data[2];
		float c10 = m.col[1].data[1] * m.col[3].data[2] - m.col[3].data[1] * m.col[1].data[2];
		float c11 = m.col[1].data[1] * m.col[2].data[2] - m.col[2].data[1] * m.col[1].data[2];

		float c12 = m.col[2].data[0] * m.col[3].data[3] - m.col[3].data[0] * m.col[2].data[3];
		float c14 = m.col[1].data[0] * m.col[3].data[3] - m.col[3].data[0] * m.col[1].data[3];
		float c15 = m.col[1].data[0] * m.col[2].data[3] - m.col[2].data[0] * m.col[1].data[3];

		float c16 = m.col[2].data[0] * m.col[3].data[2] - m.col[3].data[0] * m.col[2].data[2];
		float c18 = m.col[1].data[0] * m.col[3].data[2] - m.col[3].data[0] * m.col[1].data[2];
		float c19 = m.col[1].data[0] * m.col[2].data[2] - m.col[2].data[0] * m.col[1].data[2];

		float c20 = m.col[2].data[0] * m.col[3].data[1] - m.col[3].data[0] * m.col[2].data[1];
		float c22 = m.col[1].data[0] * m.col[3].data[1] - m.col[3].data[0] * m.col[1].data[1];
		float c23 = m.col[1].data[0] * m.col[2].data[1] - m.col[2].data[0] * m.col[1].data[1];

		vec4 f0 = Vec4(c00, c00, c02, c03);
		vec4 f1 = Vec4(c04, c04, c06, c07);
		vec4 f2 = Vec4(c08, c08, c10, c11);
		vec4 f3 = Vec4(c12, c12, c14, c15);
		vec4 f4 = Vec4(c16, c16, c18, c19);
		vec4 f5 = Vec4(c20, c20, c22, c23);

		vec4 v0 = Vec4(m.col[1].data[0], m.col[0].data[0], m.col[0].data[0], m.col[0].data[0]);
		vec4 v1 = Vec4(m.col[1].data[1], m.col[0].data[1], m.col[0].data[1], m.col[0].data[1]);
		vec4 v2 = Vec4(m.col[1].data[2], m.col[0].data[2], m.col[0].data[2], m.col[0].data[2]);
		vec4 v3 = Vec4(m.col[1].data[3], m.col[0].data[3], m.col[0].data[3], m.col[0].data[3]);

		vec4 Inv0 = v1 * f0 - v2 * f1 + v3 * f2;
		vec4 Inv1 = v0 * f0 - v2 * f3 + v3 * f4;
		vec4 Inv2 = v0 * f1 - v1 * f3 + v3 * f5;
		vec4 Inv3 = v0 * f2 - v1 * f4 + v2 * f5;

		vec4 SignA = Vec4(+1, -1, +1, -1);
		vec4 SignB = Vec4(-1, +1, -1, +1);
		mat4 inverse = Mat4(Inv0 * SignA, Inv1 * SignB, Inv2 * SignA, Inv3 * SignB);

		vec4 Row0 = Vec4(inverse.col[0].data[0], inverse.col[1].data[0], inverse.col[2].data[0], inverse.col[3].data[0]);

    vec4 Dot0 = m.col[ 0 ] * Row0;
		float Dot1 = (Dot0.x + Dot0.y) + (Dot0.z + Dot0.w);

		float OneOverDeterminant = 1.0f / Dot1;

		return inverse * OneOverDeterminant;
}

struct rect2
{
  vec2 min;
  vec2 max;
};

rect2 RectMinMax2( vec2 min, vec2 max )
{
  rect2 result;
  result.min = min;
  result.max = max;
  return result;
}

rect2 Rect2( vec2 position, vec2 dimensions )
{
  rect2 result;
  result.min = position;
  result.max = position + dimensions;
  return result;
}

bool ContainsPoint( rect2 rect, vec2 p )
{
  if ( ( p.x >= rect.min.x ) && ( p.x <= rect.max.x ) )
  {
    if ( ( p.y >= rect.min.y ) && ( p.y <= rect.max.y ) )
    {
      return true;
    }
  }
  return false;
}