#pragma once

#include <cstring>
#include <cstdio>
#include <cstdint>

#define internal static
#define global static

#define DEBUG_BREAK (*(int*)0 = 0)

#define ASSERT( COND )                                                         \
  if ( !( COND ) )                                                             \
  {                                                                            \
    printf( "ASSERT_FAILED: %s(%d): %s\n\t%s\n", __FILE__, __LINE__,           \
            __FUNCTION__, #COND );                                             \
    DEBUG_BREAK;                                                               \
  }

#define UNUSED( X ) (void)X
#define BIT( N ) ( 1 << ( N ) )

#define ARRAY_COUNT( ARRAY ) ( sizeof( ARRAY ) / sizeof( ARRAY[0] ) )

#define Swap( A, B, TYPE ) do { TYPE temp = A; A = B; B = temp; } while( 0 )

#define MinVal( A, B ) ( A ) < ( B ) ? ( A ) : ( B )

#define KILOBYTES( N ) ( N * 1024 )
#define MEGABYTES( N ) ( KILOBYTES( N ) * 1024u )
#define GIGABYTES( N ) ( MEGABYTES( N ) * 1024u )

#define ARRAY_COPY( DEST, SRC, COUNT )                                         \
  for ( uint32_t _idx = 0; _idx < COUNT; ++_idx )                              \
  {                                                                            \
    DEST[_idx] = SRC[_idx];                                                    \
  }


inline void ClearToZero( void *memory, uint32_t len )
{
  uint8_t *p = (uint8_t*)memory;
  for ( uint32_t i = 0; i < len; ++i )
  {
    p[i] = 0;
  }
}

#define ZeroStruct( STRUCT ) ClearToZero( &STRUCT, sizeof( STRUCT ) )
#define ZeroPointerToStruct( STRUCT ) ClearToZero( STRUCT, sizeof( *STRUCT ) )

struct RandomNumberGenerator
{
  uint64_t state[2];
};

inline RandomNumberGenerator CreateRandomNumberGenerator( uint64_t s0,
                                                          uint64_t s1 )
{
  RandomNumberGenerator result = {};
  result.state[0] = s0;
  result.state[1] = s1;
  return result;
}

inline uint64_t NextRandomNumber( RandomNumberGenerator *generator )
{
  uint64_t s1 = generator->state[0];
  uint64_t s0 = generator->state[1];
  generator->state[0] = s0;
  s1 ^= s1 << 32;
  generator->state[1] = ( s1 ^ s0 ^ ( s1 >> 17 ) ^ ( s0 >> 26 ) );
  return generator->state[1] + s0;
}

inline uint32_t RandomNumberInRange( RandomNumberGenerator *generator,
                                       uint32_t min, uint32_t max )
{
  ASSERT( max > min );
  uint32_t range = 1 + max - min;
  uint32_t buckets = 0xFFFFFFFF / range;
  uint32_t limit = buckets * range;

  uint32_t r = NextRandomNumber( generator ) % 0xFFFFFFFF;
  while ( r >= limit )
  {
    r = NextRandomNumber( generator ) % 0xFFFFFFFF;
  }

  return min + ( r / buckets );
}

inline float RandomFloat( RandomNumberGenerator *generator )
{
  uint32_t n = RandomNumberInRange( generator, 0, 0xFFFFFFF0 );
  return (float)((double)n/(double)(0xFFFFFFF0));
}

inline float RandomBinomial( RandomNumberGenerator *generator )
{
  return RandomFloat( generator ) - RandomFloat( generator );
}

struct MemoryArena
{
  size_t size, used;
  uint8_t *base;
};

inline void MemoryArenaInitialize( MemoryArena *arena, size_t size,
                                   uint8_t *base )
{
  arena->size = size;
  arena->base = base;
  arena->used = 0;
}

#define AllocateStruct( ARENA, TYPE )                                          \
  ( TYPE * ) MemoryArenaAllocate( ARENA, sizeof( TYPE ) )

#define AllocateArray( ARENA, TYPE, SIZE )                                     \
  ( TYPE * ) MemoryArenaAllocate( ARENA, sizeof( TYPE ) * ( SIZE ) )

inline void* MemoryArenaAllocate( MemoryArena *arena, size_t size )
{
  ASSERT( arena->used + size < arena->size );
  void *result = arena->base + arena->used;
  arena->used += size;
  return result;
}

// NOTE: This also frees all memory that was allocated after the given pointer.
inline void MemoryArenaFree( MemoryArena *arena, void *memory )
{
  ASSERT( memory >= arena->base && memory < arena->base + arena->used );
  size_t size = ( arena->base + arena->used ) - (uint8_t*)memory;
  arena->used -= size;
}

struct MemoryPool
{
  uint8_t *base;
  uint8_t *occupancy; // TODO: Use bitfield
  size_t objectSize;
  size_t capacity;
  size_t size;
  // TODO: Use freelist to speed up allocations;
};

inline MemoryPool CreateMemoryPool( MemoryArena *arena, size_t objectSize,
                                    size_t capacity )
{
  MemoryPool result = {};
  result.objectSize = objectSize;
  result.capacity = capacity;
  result.base = (uint8_t *)MemoryArenaAllocate( arena, objectSize * capacity );
  result.occupancy = (uint8_t *)MemoryArenaAllocate( arena, capacity );
  ClearToZero( result.occupancy, capacity );
  return result;
}

inline MemoryPool CreateMemoryPool( uint8_t *base, size_t len,
                                    size_t objectSize, size_t capacity )
{
  MemoryPool result = {};
  size_t totalSize = objectSize * capacity + capacity;
  ASSERT( totalSize < len );
  result.objectSize = objectSize;
  result.capacity = capacity;
  result.occupancy = base;
  ClearToZero( result.occupancy, capacity );
  result.base = base + capacity;
  return result;
}

#define CreateMemoryPoolArray( OBJECTS, OCCUPANCY )                            \
  CreateMemoryPoolArray_( OBJECTS, ARRAY_COUNT( OBJECTS ),                     \
                          sizeof( OBJECTS[0] ), OCCUPANCY,                     \
                          ARRAY_COUNT( OCCUPANCY ) )

inline MemoryPool CreateMemoryPoolArray_( void *objects, size_t objectsCapacity,
                                          size_t objectSize,
                                          uint8_t *occupancy,
                                          size_t occupancyCapacity )
{
  ASSERT( objectsCapacity == occupancyCapacity );
  MemoryPool result = {};
  result.objectSize = objectSize;
  result.capacity = objectsCapacity;
  result.occupancy = occupancy;
  ClearToZero( result.occupancy, objectsCapacity );
  result.base = (uint8_t *)objects;
  return result;
}

inline void *MemoryPoolAllocate( MemoryPool *pool )
{
  for ( size_t i = 0; i < pool->capacity; ++i )
  {
    if ( !pool->occupancy[i] )
    {
      pool->occupancy[i] = 1;
      pool->size++;
      void *result =  pool->base + ( i * pool->objectSize );
      ClearToZero( result, pool->objectSize );
      return result;
    }
  }
  ASSERT( !"Failed to allocate memory!" );
  return NULL;
}

inline void MemoryPoolFree( MemoryPool *pool, void *ptr )
{
  ASSERT( ptr );
  ASSERT( pool->size > 0 );
  size_t i = ( (uint8_t*)ptr - pool->base ) / pool->objectSize;
  ASSERT( i < pool->capacity );
  pool->occupancy[i] = 0;
  pool->size--;
}

inline void MemoryPoolClear( MemoryPool *pool )
{
  pool->size = 0;
  ClearToZero( pool->occupancy, pool->capacity );
}

struct ContiguousObjectPool
{
  uint32_t size, capacity, objectSize;
  void *start;
};

#define CreateContiguousObjectPoolArray( ARRAY )                               \
  CreateContiguousObjectPool( ARRAY, ARRAY_COUNT( ARRAY ), sizeof( ARRAY[0] ) )

inline ContiguousObjectPool CreateContiguousObjectPool( void *start,
                                                        uint32_t capacity,
                                                        uint32_t objectSize )
{
  ContiguousObjectPool result = {};
  result.start = start;
  result.capacity = capacity;
  result.objectSize = objectSize;
  return result;
}

inline void* AllocateObject( ContiguousObjectPool *pool )
{
  void *result = nullptr;
  if ( pool->size < pool->capacity )
  {
    result = (uint8_t*)pool->start + pool->objectSize * pool->size++;
    ClearToZero( result, pool->objectSize );
  }
  return result;
}

inline void* FreeObject( ContiguousObjectPool *pool, void *object )
{
  void *result = nullptr;
  ASSERT( pool->size > 0 );
  auto last = (uint8_t*)pool->start + pool->objectSize * ( pool->size - 1 );
  if ( object != last )
  {
    memcpy( object, last, pool->objectSize );
    result = object;
  }
  ASSERT( pool->size > 0 );
  pool->size--;
  return result;
}

#define FNV_32_PRIME ((uint32_t)0x01000193)
#define FNV1_32_INIT ((uint32_t)0x811c9dc5)

inline uint32_t HashStringFNV1A32( const char *str )
{
  uint32_t hval = FNV1_32_INIT;
  uint8_t *s = (uint8_t *)str;

  while ( *s )
  {
    hval ^= (uint32_t)*s++;
    hval *= FNV_32_PRIME;
  }
  return hval;
}

struct LinearQueue
{
  uint8_t *start;
  uint32_t capacity;
  uint32_t objectSize;
  uint32_t length;
};

#define LinearQueueInit( QUEUE, ARENA, TYPE, CAPACITY )                        \
  LinearQueueInit_( QUEUE, ARENA, sizeof( TYPE ), CAPACITY )

inline void LinearQueueInit_( LinearQueue *queue, MemoryArena *arena,
                              uint32_t objectSize, uint32_t capacity )
{
  queue->capacity = capacity;
  queue->length = 0;
  queue->objectSize = objectSize;
  queue->start = (uint8_t *)MemoryArenaAllocate( arena, capacity * objectSize );
}

#define LinearQueueInitArray( QUEUE, ARRAY, SIZE )                             \
  LinearQueueInit_( QUEUE, ARRAY, sizeof( ARRAY[0] ) * SIZE,                   \
                    sizeof( ARRAY[0] ) )

inline void LinearQueueInit_( LinearQueue *queue, void *data, uint32_t len,
                              uint32_t objectSize )
{
  queue->capacity = len / objectSize;
  queue->length = 0;
  queue->objectSize = objectSize;
  queue->start = (uint8_t *)data;
}

#define LinearQueuePush( QUEUE, OBJ )                                          \
  LinearQueuePush_( QUEUE, &OBJ, sizeof( OBJ ) )

inline bool LinearQueuePush_( LinearQueue *queue, void *obj,
                              uint32_t objectSize )
{
  ASSERT( objectSize == queue->objectSize );
  if ( queue->length < queue->capacity )
  {
    uint8_t *p = queue->start + ( queue->length * objectSize );
    memcpy( p, obj, objectSize );
    queue->length++;
    return true;
  }
  return false;
}

inline void LinearQueueClear( LinearQueue *queue )
{
  queue->length = 0;
}

#define LinearQueueGet( QUEUE, IDX, TYPE )                                     \
  *( TYPE * ) LinearQueueGet_( QUEUE, IDX, sizeof( TYPE ) )

inline void *LinearQueueGet_( LinearQueue *queue, uint32_t idx,
                              uint32_t objectSize )
{
  ASSERT( objectSize == queue->objectSize );
  ASSERT( idx < queue->length );
  void *p = queue->start + ( idx * objectSize );
  return p;
}

struct CircularBuffer
{
  uint8_t *start;
  uint32_t head;
  uint32_t tail;
  uint32_t length;
  uint32_t capacity;
  uint32_t objectSize;
};

#define CircularBufferInit( BUFFER, ARENA, TYPE, CAPACITY )                    \
  CircularBufferInit_( BUFFER, ARENA, sizeof( TYPE ), CAPACITY )

inline void CircularBufferInit_( CircularBuffer *buffer, MemoryArena *arena,
                                 uint32_t objectSize, uint32_t capacity )
{
  buffer->start =
    (uint8_t *)MemoryArenaAllocate( arena, objectSize * capacity );
  buffer->capacity = capacity;
  buffer->objectSize = objectSize;
  buffer->head = 0;
  buffer->tail = 0;
  buffer->length = 0;
}

#define CircularBufferInitArray( BUFFER, ARRAY )                               \
  CircularBufferInitArray_( BUFFER, ARRAY, sizeof( ARRAY[0] ),                 \
                            ARRAY_COUNT( ARRAY ) )

inline void CircularBufferInitArray_( CircularBuffer *buffer, void *start,
                                      uint32_t objectSize, uint32_t capacity )
{
  buffer->start = (uint8_t*)start;
  buffer->capacity = capacity;
  buffer->objectSize = objectSize;
  buffer->head = 0;
  buffer->tail = 0;
  buffer->length = 0;
}

#define CircularBufferPush( BUFFER, OBJ )                                      \
  CircularBufferPush_( BUFFER, &OBJ, sizeof( OBJ ) )

inline bool CircularBufferPush_( CircularBuffer *buffer, void *obj,
                                 uint32_t objectSize )
{
  ASSERT( buffer->objectSize == objectSize );
  if ( buffer->length < buffer->capacity )
  {
    uint8_t *p = buffer->start + ( buffer->tail * buffer->objectSize );
    memcpy( p, obj, objectSize );
    buffer->length++;
    buffer->tail = ( buffer->tail + 1 ) % buffer->capacity;
    return true;
  }
  return false;
}

// WARNING: This pushes one item at a time so don't expect it to be fast
#define CircularBufferPushArray( BUFFER, ARRAY, LEN ) CircularBufferPushArray_( BUFFER, ARRAY, sizeof( ARRAY[0] ), ARRAY_COUT( ARRAY ) )
inline bool CircularBufferPushArray_( CircularBuffer *buffer, void *array, uint32_t objectSize, uint32_t len )
{
  uint8_t *cursor = ( uint8_t* )array;
  for ( uint32_t i = 0; i < len; ++i )
  {
    if ( !CircularBufferPush_( buffer, cursor, objectSize ) )
    {
      return false;
    }
    cursor += objectSize;
  }
  return true;
}

// TODO: This should be renamed
#define CircularBufferTop( BUFFER, TYPE )                                      \
  *(TYPE *)CircularBufferTop_( BUFFER, sizeof( TYPE ) )

inline void *CircularBufferTop_( CircularBuffer *buffer, uint32_t objectSize )
{
  ASSERT( objectSize == buffer->objectSize );
  ASSERT( buffer->length > 0 );
  return buffer->start + ( buffer->head * objectSize );
}

#define CircularBufferBack( BUFFER, OBJ ) \
  CircularBufferBack_( BUFFER, sizeof( *OBJ ), OBJ )

inline bool CircularBufferBack_( CircularBuffer *buffer, uint32_t objectSize,
                                 void *object )
{
  ASSERT( objectSize == buffer->objectSize );
  if ( buffer->length > 0 )
  {
    int idx = buffer->tail - 1;
    if ( idx < 0 )
    {
      idx = buffer->capacity;
    }
    idx = idx % buffer->capacity;
    memcpy( object, buffer->start + ( idx * objectSize ), objectSize );
    return true;
  }
  return false;
}

inline void CircularBufferPop( CircularBuffer *buffer )
{
  ASSERT( buffer->length > 0 );
  buffer->head = ( buffer->head + 1 ) % buffer->capacity;
  buffer->length--;
}

// WARNING: Really lazy and slow
inline void CircularBufferPopN( CircularBuffer *buffer, uint32_t n )
{
  for ( uint32_t i = 0; i < n; ++i )
  {
    CircularBufferPop( buffer );
  }
}

inline void CircularBufferClear( CircularBuffer *buffer )
{
  buffer->length = 0;
  buffer->head = 0;
  buffer->tail = 0;
}

#define CircularBufferReadArray( BUFFER, TYPE, ARRAY, SIZE )                   \
  CircularBufferReadArray_( BUFFER, sizeof( TYPE ), ARRAY, SIZE )

inline uint32_t CircularBufferReadArray_( CircularBuffer *buffer,
                                         uint32_t objectSize, void *array,
                                         uint32_t size )
{
  ASSERT( objectSize == buffer->objectSize );
  size = MinVal( size, buffer->length );
  if ( size > 0 )
  {
    if ( buffer->head + size < buffer->capacity )
    {
      memcpy( array, buffer->start + ( buffer->head * objectSize ),
              size * objectSize );
    }
    else
    {
      uint32_t partialLength = buffer->capacity - buffer->head;
      memcpy( array, buffer->start + ( buffer->head * objectSize ),
              partialLength * objectSize );
      uint32_t remainder = size - partialLength;
      memcpy( (uint8_t*)array + ( partialLength * objectSize ), buffer->start,
              remainder * objectSize );
    }
  }
  return size;
}

#define CircularBufferGet( BUFFER, TYPE, IDX )                                 \
  ( TYPE * ) CircularBufferGet_( BUFFER, sizeof( TYPE ), IDX )

inline void *CircularBufferGet_( CircularBuffer *buffer, uint32_t objectSize,
                                uint32_t idx )
{
  ASSERT( objectSize == buffer->objectSize );
  ASSERT( idx < buffer->length );
  uint32_t i = ( buffer->head + idx ) % buffer->capacity;
  return (void *)( buffer->start + ( i * objectSize ) );
}

struct ByteQueue
{
  uint8_t *start;
  uint32_t head;
  uint32_t tail;
  uint32_t length;
  uint32_t capacity;
};

#define ByteQueueInitializeArray( QUEUE, ARRAY ) ByteQueueInitialize( QUEUE, ARRAY, sizeof( ARRAY ) )

inline void ByteQueueInitialize( ByteQueue *queue, void *buffer, uint32_t length )
{
  ZeroPointerToStruct( queue );
  queue->start = (uint8_t*)buffer;
  queue->capacity = length;
}

inline bool ByteQueueWrite( ByteQueue *queue, const void *data, uint32_t length )
{
  if ( queue->length + length <= queue->capacity )
  {
    if ( queue->tail + length <= queue->capacity )
    {
      memcpy( queue->start + queue->tail, data, length );
    }
    else
    {
      uint32_t part1 = ( queue->tail + length ) - queue->capacity;
      uint32_t part2 = length - part1;
      memcpy( queue->start + queue->tail, data, part1 );
      memcpy( queue->start, ( uint8_t* )data + part1, part2 );
    }
    queue->tail = ( queue->tail + length ) % queue->capacity;
    queue->length += length;
    return true;
  }
  return false;
}

inline bool ByteQueueRead( ByteQueue *queue, void *data, uint32_t length )
{
  if ( queue->length >= length )
  {
    if ( queue->head + length <= queue->capacity )
    {
      memcpy( data, queue->start + queue->head, length );
    }
    else
    {
      uint32_t part1 = ( queue->head + length ) - queue->capacity;
      uint32_t part2 = length - part1;
      memcpy( data, queue->start + queue->head, part1 );
      memcpy( ( uint8_t* )data + part1, queue->start, part2 );
    }
    queue->head = ( queue->head + length ) % queue->capacity;
    queue->length -= length;
    return true;
  }
  return false;
}

inline void ByteQueueClear( ByteQueue *queue )
{
  queue->length = 0;
  queue->head = 0;
  queue->tail = 0;
}