struct ai_KinematicOutput
{
  vec3 velocity;
  float orientation;
};

struct ai_KinematicState
{
  vec3 position;
  vec3 velocity;
  float orientation;
};

inline float ai_FaceTarget( ai_KinematicState agent,
                                ai_KinematicState target )
{
  float result;
  vec3 d = Normalize( target.position - agent.position );
  result = ATan( d.x, d.z );
  return result;
}

inline float ai_FaceVelocity( ai_KinematicState agent )
{
  float result;
  if ( Length( agent.velocity ) > 0.0f )
  {
    result = ATan( agent.velocity.x, -agent.velocity.z );
    return result;
  }
  return agent.orientation;
}

internal ai_KinematicOutput
  ai_Seek( ai_KinematicState *agent, ai_KinematicState *target, float maxSpeed )
{
  ai_KinematicOutput result = {};

  result.velocity = target->position - agent->position;
  result.velocity = Normalize( result.velocity );
  result.velocity *= maxSpeed;

  agent->velocity = result.velocity;
  agent->orientation = ai_FaceTarget( *agent, *target );

  result.orientation = agent->orientation;

  return result;
}

internal ai_KinematicOutput
  ai_Arrive( ai_KinematicState *agent, ai_KinematicState *target,
             float maxSpeed, float targetRadius, float slowRadius )
{
  ai_KinematicOutput result = {};
  auto dir = target->position - agent->position;
  auto distance = Length( dir );
  dir = Normalize( dir );

  result.velocity = dir;
  agent->velocity = result.velocity;
  agent->orientation = ai_FaceVelocity( *agent );
  if ( distance < targetRadius )
  {
    agent->velocity = Vec3( 0 );
  }
  else if ( distance < slowRadius )
  {
    agent->velocity *= maxSpeed * distance / slowRadius;
  }
  else
  {
    agent->velocity *= maxSpeed;
  }


  return result;
}

internal ai_KinematicOutput
  ai_Flee( ai_KinematicState *agent, ai_KinematicState *target, float maxSpeed )
{
  ai_KinematicOutput result = {};

  result.velocity = agent->position - target->position;
  result.velocity = Normalize( result.velocity );
  result.velocity *= maxSpeed;

  agent->velocity = result.velocity;
  agent->orientation = ai_FaceVelocity( *agent );

  return result;
}

inline vec3 ViewAnglesToVector( vec2 viewAngles )
{
  return Normalize(
    Vec3( Sin( viewAngles.y ), -Tan( viewAngles.x ), -Cos( viewAngles.y ) ) );
}

internal ai_KinematicOutput ai_Wander( ai_KinematicState *agent,
                                       RandomNumberGenerator *generator,
                                       float maxSpeed )
{
  ai_KinematicOutput result = {};

  float wanderRate = 1.0f;
  float wanderOffset = 3.0f;
  float wanderRadius = 2.0f;
  float wanderOrientation = RandomBinomial( generator ) * wanderRate;

  float targetOrientation = wanderOrientation;

  vec3 v = {};
  if ( Length( agent->velocity ) > 0.0f )
  {
    v = Normalize( agent->velocity );
  }
  vec3 target = agent->position + wanderOffset * v;

  target += wanderRadius *
            Vec3( Sin( targetOrientation ), 0.0f, -Cos( targetOrientation ) );
  ai_KinematicState targetState = {};
  targetState.position = target;
  debug_PushPoint( target, Vec4( 0.8, 0.4, 0.1, 1 ), 10.0f );
  agent->velocity = Normalize( target - agent->position ) * maxSpeed;
  result.velocity = agent->velocity;

  return result;
}
