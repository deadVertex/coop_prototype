internal void cmd_Init( CommandSystem *system )
{
  ClearToZero( system, sizeof( *system ) );
  system->cmdPool = CreateContiguousObjectPool(
    system->cmds, ARRAY_COUNT( system->cmds ), sizeof( system->cmds[0] ) );
}

internal void cmd_Register( CommandSystem *system, const char *name,
                            uint32_t id )
{
  auto cmd = (CommandEntry *)AllocateObject( &system->cmdPool );
  ASSERT( cmd );
  ASSERT( strlen( name ) < MAX_CMD_NAME_LENGTH );
  strcpy( cmd->name, name );
  cmd->id = id;
}

internal void cmd_Exec( CommandSystem *system, const char *cmd )
{
  uint32_t len = strlen( cmd );
  auto buffer = system->buffers + system->bufferIdx;
  ASSERT( len < CMD_WORKING_BUFFER_SIZE - buffer->workingBufferSize );

  strcpy( buffer->workingBuffer + buffer->workingBufferSize, cmd );
  const char *name = buffer->workingBuffer + buffer->workingBufferSize;
  char *cursor = buffer->workingBuffer + buffer->workingBufferSize;

  ExecuteCommandEvent event = {};
  ClearToZero( event.args, sizeof( event.args ) );
  while ( *cursor != '\0' )
  {
    if ( *cursor == ' ' )
    {
      *cursor = '\0';
      if ( event.numArgs < CMD_MAX_ARGS )
      {
        event.args[event.numArgs++] = cursor + 1;
      }
      else
      {
        // Error: Too many arguments
        break;
      }
    }
    cursor++;
  }
  for ( uint32_t i = 0; i < system->cmdPool.size; ++i )
  {
    auto cmd = system->cmds + i;
    if ( strcmp( name, cmd->name ) == 0 )
    {
      event.command = cmd->id;
      break;
    }
  }

  if ( event.command != CMD_UNKNOWN_COMMAND )
  {
    buffer->workingBufferSize += len;
    if ( buffer->eventCount < ARRAY_COUNT( buffer->events ) )
    {
      buffer->events[buffer->eventCount++] = event;
    }
  }
}
