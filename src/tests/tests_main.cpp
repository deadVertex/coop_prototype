#include "UnitTest++/UnitTest++.h"

#include "common/utils.h"

#include "common/logging.h"
#include "platform/network_layer.h"
#include "common/byte_buffer.h"
#include "platform/network_layer.cpp"

#include "game/game.h"
#include "game/sv_entity_replication.h"
#include "game/sv_entity_replication.cpp"

#include "game/cl_entity_replication.h"
#include "game/cl_entity_replication.cpp"

#include "game/rpc_system.cpp"

uint32_t log_activeChannels = 0;

struct EntityReplicationSystemFixture
{
  EntityReplicationSystemFixture()
  {
    uint8_t *memory = new uint8_t[KILOBYTES( 16 )];
    MemoryArenaInitialize( &arena, KILOBYTES( 16 ), memory );
  }
  ~EntityReplicationSystemFixture() { delete[] arena.base; }
  MemoryArena arena;
};

SUITE( EntityReplicationSystem )
{
  TEST_FIXTURE( EntityReplicationSystemFixture, AddClient )
  {
    uint32_t maxEntities = 8;
    uint32_t maxClients = 4;
    uint32_t clientId = 4;
    entrep_Server server;
    entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
    CHECK( entrep_AddClient( &server, clientId ) );
    auto client = entrep_GetClient( &server, clientId );
    CHECK_EQUAL( client->entityPool.capacity, maxEntities );

    for ( uint32_t i = 4; i < maxClients + 4 - 1; ++i )
    {
      entrep_AddClient( &server, i );
    }
    CHECK_EQUAL( server.clientPool.size, maxClients );

    CHECK( entrep_AddClient( &server, 1000 ) == false );

    CHECK( entrep_RemoveClient( &server, clientId ) );
    CHECK_EQUAL( server.clientPool.size, maxClients - 1 );
  }

  TEST_FIXTURE( EntityReplicationSystemFixture, AddEntity )
  {
    uint32_t maxEntities = 4;
    uint32_t maxClients = 4;
    net_EntityId netEntityId;
    uint32_t netEntityTypeId = 1;
    entrep_Server server;
    entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
    netEntityId = entrep_AddEntity( &server, netEntityTypeId );
    CHECK( netEntityId != NULL_NET_ENTITY_ID );
    CHECK_EQUAL( server.entityPool.size, 1u );
    for ( uint32_t i = 0; i < maxEntities - 1; ++i )
    {
      entrep_AddEntity( &server, 10 + i, netEntityTypeId );
    }
    CHECK_EQUAL( server.entityPool.size, maxEntities );
    CHECK( entrep_AddEntity( &server, 3, netEntityTypeId ) == false );
    CHECK( entrep_RemoveEntity( &server, netEntityId ) );
    entrep_ServerUpdate( &server );
    CHECK_EQUAL( server.entityPool.size, maxEntities - 1 );
    CHECK( entrep_AddEntity( &server, 3, netEntityTypeId ) );
  }

  TEST_FIXTURE( EntityReplicationSystemFixture, AddEntityToClients )
  {
    uint32_t maxEntities = 4;
    uint32_t maxClients = 4;
    uint32_t netEntityTypeId = 1;
    entrep_Server server;
    entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
    uint32_t clientId = 4;
    entrep_AddClient( &server, clientId );
    auto client = entrep_GetClient( &server, clientId );
    net_EntityId netEntityId = entrep_AddEntity( &server, netEntityTypeId );
    CHECK_EQUAL( client->entityPool.size, 0u );
    entrep_ServerUpdate( &server );
    CHECK_EQUAL( client->entityPool.size, 1u );
    uint32_t clientId2 = 5;
    entrep_AddClient( &server, clientId2 );
    client = entrep_GetClient( &server, clientId2 );
    CHECK_EQUAL( client->entityPool.size, 1u );
    entrep_RemoveEntity( &server, netEntityId );
    CHECK_EQUAL( client->entityPool.size, 1u );
    entrep_ServerUpdate( &server );
    CHECK_EQUAL( client->entityPool.size, 1u );
    auto clientEntity = (entrep_ClientEntity *)client->entityPool.start;
    CHECK( clientEntity->state == CLIENT_ENTITY_STATE_TO_DESTROY );
  }

  TEST_FIXTURE( EntityReplicationSystemFixture, PerClientPriorization )
  {
    uint32_t maxEntities = 4;
    uint32_t maxClients = 4;
    uint32_t netEntityTypeId = 1;
    vec2 origin = {20, 0};
    float basePriority = 0.5f;
    vec2 entityPosition = {10, 0};
    entrep_Server server;
    entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
    uint32_t clientId = 4;
    entrep_AddClient( &server, clientId );
    entrep_SetClientOrigin( &server, clientId, origin );
    auto client = entrep_GetClient( &server, clientId );
    auto netEntityId =
      entrep_AddEntity( &server, netEntityTypeId, basePriority );
    entrep_ServerUpdate( &server );
    auto entity = entrep_ClientGetEntity( client, netEntityId );
    CHECK_EQUAL( entity->basePriority, basePriority );
    CHECK_EQUAL( entity->priority, basePriority );
    CHECK_EQUAL( entity->positionPriority, 0.0f );
    entrep_SetClientOrigin( &server, clientId, origin );
    entrep_ServerUpdateEntityPositions( &server, &netEntityId, &entityPosition,
                                        1 );
    CHECK_EQUAL( entity->positionPriority, 1.0f - ( 10.0f / VIEW_DISTANCE ) );
    entrep_ServerUpdate( &server );
    CHECK_EQUAL( entity->priority,
                 entity->positionPriority + entity->basePriority );
  }

  TEST_FIXTURE( EntityReplicationSystemFixture, EntitiesSortedByPriority )
  {
    const uint32_t maxEntities = 4;
    uint32_t maxClients = 4;
    uint32_t netEntityTypeId = 1;
    entrep_Server server;
    entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
    uint32_t clientId = 4;
    entrep_AddClient( &server, clientId );
    auto lowPriority = entrep_AddEntity( &server, netEntityTypeId, 0.1f );
    auto highPriority = entrep_AddEntity( &server, netEntityTypeId, 0.5f );
    entrep_ServerUpdate( &server );
    entrep_QueueEntry queue[ maxEntities ];
    int ret = entrep_GetEntityQueue( &server, clientId, queue, ARRAY_COUNT(queue) );
    CHECK_EQUAL( ret, 2 );
    CHECK_EQUAL( queue[0].netEntityId, highPriority );
    CHECK_EQUAL( CLIENT_ENTITY_STATE_TO_CREATE, queue[ 0 ].state );
    CHECK_EQUAL( queue[1].netEntityId, lowPriority );
    CHECK_EQUAL( CLIENT_ENTITY_STATE_TO_CREATE, queue[ 1 ].state );

    // TODO: Separate test
    entrep_AdvanceEntityStatesFromQueue( &server, clientId, queue, 1 ); // Packet sent
    ClearToZero( queue, sizeof( queue ) );
    ret = entrep_GetEntityQueue( &server, clientId, queue, ARRAY_COUNT(queue) );
    CHECK_EQUAL( CLIENT_ENTITY_STATE_CREATED, queue[ 0 ].state );
    CHECK_EQUAL( CLIENT_ENTITY_STATE_TO_CREATE, queue[ 1 ].state );
  }

  TEST_FIXTURE( EntityReplicationSystemFixture, IncreaseEntityUpdatePriority )
  {
    const uint32_t maxEntities = 4;
    uint32_t maxClients = 4;
    uint32_t netEntityTypeId = 1;
    entrep_Server server;
    entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
    uint32_t clientId = 4;
    entrep_AddClient( &server, clientId );
    auto netEntityId = entrep_AddEntity( &server, netEntityTypeId, 0.1f );
    entrep_ServerUpdate( &server );
    entrep_ClientAdvanceEntityStates( &server, clientId, &netEntityId,
                                      1 ); // Packet sent
    entrep_QueueEntry queue[maxEntities];
    uint32_t ret = entrep_GetEntityQueue( &server, clientId, queue, ARRAY_COUNT(queue));
    CHECK_EQUAL( ret, 1u );
    CHECK_EQUAL( queue[0].netEntityId, netEntityId );
    auto client = entrep_GetClient( &server, clientId );
    auto entity = entrep_ClientGetEntity( client, netEntityId );
    CHECK_EQUAL( entity->updatePriority, 1.0f );
    entrep_IncreaseEntityUpdatePriority( &server, clientId, &queue[0].netEntityId, 1 );
    entrep_ServerUpdate( &server );
    CHECK_EQUAL( entity->updatePriority, 1.1f ); // TODO: Decide on value
    float expectedPriority = entity->basePriority + entity->positionPriority;
    expectedPriority *= 1.1f;
    CHECK_EQUAL( entity->priority, expectedPriority );
    entrep_ResetEntityUpdatePriority( &server, clientId, &queue[0].netEntityId, 1 );
    CHECK_EQUAL( entity->updatePriority, 1.0f );
  }

  TEST_FIXTURE( EntityReplicationSystemFixture, EntityCreatedTwiceBug )
  {
    uint32_t maxEntities = 4;
    uint32_t maxClients = 4;
    uint32_t netEntityTypeId = 1;
    entrep_Server server;
    entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
    uint32_t clientId = 4;
    entrep_AddEntity( &server, netEntityTypeId, 0.1f );
    entrep_AddClient( &server, clientId );
    entrep_ServerUpdate( &server );
    auto client = entrep_GetClient( &server, clientId );
    CHECK_EQUAL( client->entityPool.size, 1u );
  }

  TEST_FIXTURE( EntityReplicationSystemFixture, EntityDontUpdateFlag )
  {
    const uint32_t maxEntities = 4;
    uint32_t maxClients = 4;
    uint32_t netEntityTypeId = 1;
    entrep_Server server;
    entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
    uint32_t clientId = 4;
    entrep_AddClient( &server, clientId );
    auto netEntityId = entrep_AddEntity( &server, netEntityTypeId, 0.5f,
                                         net_EntityFlagsDontUpdate );
    entrep_ServerUpdate( &server );
    entrep_QueueEntry queue[maxEntities];
    entrep_ClientAdvanceEntityStates( &server, clientId, &netEntityId, 1 );
    int ret = entrep_GetEntityQueue( &server, clientId, queue, ARRAY_COUNT(queue) );
    CHECK_EQUAL( ret, 0 ); // Entity not in queue because it is already created and has don't update flag
  }

  TEST_FIXTURE( EntityReplicationSystemFixture, UnifiedQueue )
  {
    const uint32_t maxEntities = 4;
    uint32_t maxClients = 4;
    uint32_t netEntityTypeId = 1;
    entrep_Server server;
    entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
    uint32_t clientId = 4;
    entrep_AddClient( &server, clientId );
    net_EntityId netEntityIds[ 3 ];
    netEntityIds[0] = entrep_AddEntity( &server, netEntityTypeId, 0.5f );
    netEntityIds[1] = entrep_AddEntity( &server, netEntityTypeId, 0.2f );
    netEntityIds[2] = entrep_AddEntity( &server, netEntityTypeId, 0.1f );
    entrep_ServerUpdate( &server );
    net_EntityId output;

    // Advance first two entities to created state
    entrep_ClientAdvanceEntityStates( &server, clientId, netEntityIds, 2 );

    // Destroy first entity
    entrep_RemoveEntity( &server, netEntityIds[ 0 ] );
    entrep_ServerUpdate( &server );

    entrep_QueueEntry queue[ maxEntities ];
    // Queue order is determined by the entity priority not operation type
    uint32_t queueLength = entrep_GetEntityQueue( &server, clientId, queue, ARRAY_COUNT( queue ) );
    CHECK_EQUAL( queueLength, 3 );
    CHECK_EQUAL( queue[ 0 ].netEntityId, netEntityIds[0] );
    CHECK_EQUAL( queue[ 0 ].state, CLIENT_ENTITY_STATE_TO_DESTROY );
    CHECK_EQUAL( queue[ 1 ].netEntityId, netEntityIds[1] );
    CHECK_EQUAL( queue[ 1 ].state, CLIENT_ENTITY_STATE_CREATED );
    CHECK_EQUAL( queue[ 2 ].netEntityId, netEntityIds[2] );
    CHECK_EQUAL( queue[ 2 ].state, CLIENT_ENTITY_STATE_TO_CREATE );
  }
}

struct NetServerFixture
{
  NetServerFixture()
  {
    uint8_t *memory = new uint8_t[KILOBYTES( 256 )];
    net_ServerInit( &server, memory, KILOBYTES( 256 ) );
  }
  virtual ~NetServerFixture() { delete[] server.arena.base; }
  net_Server server;
};

struct NetServerWithClientFixture : public NetServerFixture
{
  NetServerWithClientFixture() : clientId( 4 )
  {
    net_ServerAddClient( &server, clientId );
    // Get rid of client connected event
    net_ServerClearEventQueue( &server );
  }

  uint32_t clientId;
};

static double testCurrentTime = 0.0;
double net_GetCurrentTime()
{
  return testCurrentTime;
}

SUITE( NetServer )
{
  TEST_FIXTURE( NetServerFixture, AddClientAllocatesFromPool )
  {
    uint32_t clientId = 4;
    CHECK( net_ServerAddClient( &server, clientId ) );
    CHECK_EQUAL( server.clientsPool.size, 1u );
  }

  TEST_FIXTURE( NetServerFixture, AddClientGeneratesEvent )
  {
    uint32_t clientId = 4;
    net_ServerAddClient( &server, clientId );
    net_Event event = {};
    net_ServerPollEvent( &server, &event );
    CHECK_EQUAL( NET_CLIENT_CONNECTED_EVENT, event.type );
  }

  TEST_FIXTURE( NetServerFixture, EventsAreReceivedInOrder )
  {
    uint32_t clientIds[] = { 1, 2 };
    net_ServerAddClient( &server, clientIds[0] );
    net_ServerAddClient( &server, clientIds[1] );
    net_Event event = {};
    net_ServerPollEvent( &server, &event );
    CHECK_EQUAL( clientIds[0], event.clientConnected.clientId );
    net_ServerPollEvent( &server, &event );
    CHECK_EQUAL( clientIds[1], event.clientConnected.clientId );
  }

  TEST_FIXTURE( NetServerFixture, GetClient )
  {
    uint32_t clientId = 4;
    net_ServerAddClient( &server, clientId );
    CHECK( net_ServerGetClient( &server, clientId ) != NULL );
  }

  TEST_FIXTURE( NetServerWithClientFixture, IncomingPacketIsAddedToQueue )
  {
    Packet packet = {};
    net_ServerQueueIncomingPacket( &server, clientId, packet );
    auto client = net_ServerGetClient( &server, clientId );
    CHECK_EQUAL( client->receiveQueueLength, 1u );
  }

  TEST_FIXTURE( NetServerWithClientFixture, GenerateOutgoingPacket )
  {
    auto packet = net_ServerGenerateOutgoingPacket( &server, clientId );
    CHECK_EQUAL( clientId, packet.clientId );
  }

  TEST_FIXTURE( NetServerWithClientFixture,
                OutgoingPacketHasUniqueSequenceNumber )
  {
    auto packet = net_ServerGenerateOutgoingPacket( &server, clientId );
    CHECK_EQUAL( 0, packet.sequenceNumber );
    auto packet2 = net_ServerGenerateOutgoingPacket( &server, clientId );
    CHECK_EQUAL( 1, packet2.sequenceNumber );
  }

  TEST_FIXTURE( NetServerWithClientFixture, ValidIncomingPacketIsNotDropped )
  {
    Packet packet = {};
    CHECK( net_ServerQueueIncomingPacket( &server, clientId, packet ) );
  }

  TEST_FIXTURE( NetServerWithClientFixture,
                IncomingPacketWithInvalidSequenceNumberIsDropped )
  {
    Packet packet = {};
    CHECK( net_ServerQueueIncomingPacket( &server, clientId, packet ) );

    // Duplicate sequence number should cause packet to be dropped
    CHECK( !net_ServerQueueIncomingPacket( &server, clientId, packet ) );
  }

  TEST_FIXTURE( NetServerWithClientFixture, HandlesSequenceNumberWrap )
  {
    Packet packet = {};
    packet.sequenceNumber = 0xFFFF;

    auto client = net_ServerGetClient( &server, clientId );
    client->expectedSequenceNumber = 0xFFFF;

    CHECK( net_ServerQueueIncomingPacket( &server, clientId, packet ) );

    packet.sequenceNumber++;
    CHECK( net_ServerQueueIncomingPacket( &server, clientId, packet ) );

    packet.sequenceNumber = 0xFFFE;
    CHECK( !net_ServerQueueIncomingPacket( &server, clientId, packet ) );
  }

  TEST_FIXTURE( NetServerWithClientFixture, GeneratedPacketContainsAcks )
  {
    Packet incomingPacket = {};
    incomingPacket.sequenceNumber = 0;
    net_ServerQueueIncomingPacket( &server, clientId, incomingPacket );

    incomingPacket.sequenceNumber = 1;
    net_ServerQueueIncomingPacket( &server, clientId, incomingPacket );

    Packet generatedPacket =
      net_ServerGenerateOutgoingPacket( &server, clientId );

    CHECK_EQUAL( 1, generatedPacket.acks[0] );
    CHECK_EQUAL( 0, generatedPacket.acks[1] );
  }

  TEST_FIXTURE( NetServerWithClientFixture, GeneratedPacketContainsLastNAcks)
  {
    Packet incomingPacket = {};
    Packet receivedPacket;
    for ( uint32_t i = 0; i < NET_MAX_ACKS_PER_PACKET + 1; i++ )
    {
      incomingPacket.sequenceNumber = i;
      net_ServerQueueIncomingPacket( &server, clientId, incomingPacket );
      net_ServerReceivePackets( &server, clientId, &receivedPacket, 1 );
      net_ServerFreeReceivedPackets( &server, &receivedPacket, 1 );
    }
    Packet generatedPacket =
      net_ServerGenerateOutgoingPacket( &server, clientId );

    CHECK_EQUAL( NET_MAX_ACKS_PER_PACKET, generatedPacket.acks[0] );
    CHECK_EQUAL( 1, generatedPacket.acks[NET_MAX_ACKS_PER_PACKET - 1] );
  }

  TEST_FIXTURE( NetServerWithClientFixture, KeepsTrackOfInFlightPackets )
  {
    auto client = net_ServerGetClient( &server, clientId );
    client->nextSequenceNumber = 15;
    auto packet = net_ServerGenerateOutgoingPacket( &server, clientId );
    CHECK_EQUAL( packet.sequenceNumber,
                 client->inFlightPacketsArray[0].sequenceNumber );
  }

  TEST_FIXTURE( NetServerWithClientFixture, StoresInFlightPacketsTimestamp )
  {
    testCurrentTime = 0.050; // 50 milliseconds
    net_ServerGenerateOutgoingPacket( &server, clientId );
    auto client = net_ServerGetClient( &server, clientId );
    CHECK_EQUAL( 50u, client->inFlightPacketsArray[0].timestamp);
  }

  TEST_FIXTURE( NetServerWithClientFixture,
                DropsInFlightPacketsThatHaveNotReceivedAckAfterThreshold )
  {
    testCurrentTime = 0.0;
    net_ServerGenerateOutgoingPacket( &server, clientId );
    testCurrentTime = 1.01;

    // Drop packets that have been in flight for more than 1000 milliseconds
    CHECK_EQUAL( 1u, net_ServerDropUnacknowledgedPackets( &server, 1000 ) );
  }

  TEST_FIXTURE( NetServerWithClientFixture,
                DoesNotDropPacketsWhichHaveBeenAcked )
  {
    testCurrentTime = 0.0;
    auto client = net_ServerGetClient( &server, clientId );
    client->nextSequenceNumber = 60;
    auto outgoingPacket = net_ServerGenerateOutgoingPacket( &server, clientId );

    // Acknowledge outgoing packet
    Packet incomingPacket = {};
    incomingPacket.acks[0] = outgoingPacket.sequenceNumber;
    net_ServerQueueIncomingPacket( &server, clientId, incomingPacket );

    // Outgoing packet shouldn't be dropped since it was acked
    testCurrentTime = 1.01;
    CHECK_EQUAL( 0u, net_ServerDropUnacknowledgedPackets( &server, 1000 ) );
  }

  TEST_FIXTURE( NetServerWithClientFixture, DroppedPacketsAreAddedToQueue )
  {
    testCurrentTime = 0.0;
    auto client = net_ServerGetClient( &server, clientId );
    client->nextSequenceNumber = 60;
    auto packet = net_ServerGenerateOutgoingPacket( &server, clientId );
    testCurrentTime = 1.01;

    // Drop packets that have been in flight for more than 1000 milliseconds
    net_ServerDropUnacknowledgedPackets( &server, 1000 );

    net_Event event;
    CHECK( net_ServerPollEvent( &server, &event ) );
    CHECK_EQUAL( NET_PACKET_DROPPED_EVENT, event.type );
    CHECK_EQUAL( clientId, event.packetDropped.clientId );
    CHECK_EQUAL( packet.sequenceNumber, event.packetDropped.sequenceNumber );
  }

  TEST_FIXTURE( NetServerWithClientFixture, AckedPacketsAreAddedToQueue )
  {
    auto client = net_ServerGetClient( &server, clientId );
    client->nextSequenceNumber = 60;
    client->expectedSequenceNumber = 50;
    auto outgoingPacket = net_ServerGenerateOutgoingPacket( &server, clientId );

    Packet incomingPacket = {};
    incomingPacket.sequenceNumber = client->expectedSequenceNumber;
    incomingPacket.acks[0] = outgoingPacket.sequenceNumber;

    net_ServerQueueIncomingPacket( &server, clientId, incomingPacket );

    net_Event event;
    CHECK( net_ServerPollEvent( &server, &event ) );
    CHECK_EQUAL( NET_PACKET_ACKNOWLEDGED_EVENT, event.type );
    CHECK_EQUAL( clientId, event.packetDropped.clientId );
    CHECK_EQUAL( outgoingPacket.sequenceNumber,
                 event.packetAcknowledged.sequenceNumber );
  }

  TEST_FIXTURE( NetServerWithClientFixture, PacketCannotBeAckedTwice )
  {
    auto client = net_ServerGetClient( &server, clientId );
    client->nextSequenceNumber = 60;
    client->expectedSequenceNumber = 50;
    auto outgoingPacket = net_ServerGenerateOutgoingPacket( &server, clientId );

    Packet incomingPacket = {};
    incomingPacket.sequenceNumber = client->expectedSequenceNumber;
    incomingPacket.acks[0] = outgoingPacket.sequenceNumber;

    net_ServerQueueIncomingPacket( &server, clientId, incomingPacket );

    // Next packet arrives with correct sequence number and same ack as previous
    // packet
    incomingPacket.sequenceNumber = client->expectedSequenceNumber;
    net_ServerQueueIncomingPacket( &server, clientId, incomingPacket );

    net_Event event;
    // Only one packet acknowedged event produced for the same sequence number
    CHECK( net_ServerPollEvent( &server, &event ) );
    CHECK( !net_ServerPollEvent( &server, &event ) );
  }

  TEST_FIXTURE( NetServerWithClientFixture, PacketCannotBeDroppedTwice )
  {
    testCurrentTime = 0.0;
    auto client = net_ServerGetClient( &server, clientId );
    client->nextSequenceNumber = 60;
    auto packet = net_ServerGenerateOutgoingPacket( &server, clientId );
    testCurrentTime = 1.01;

    // Drop packets that have been in flight for more than 1000 milliseconds
    net_ServerDropUnacknowledgedPackets( &server, 1000 );

    net_Event event;
    CHECK( net_ServerPollEvent( &server, &event ) );
    CHECK_EQUAL( NET_PACKET_DROPPED_EVENT, event.type );
    CHECK_EQUAL( clientId, event.packetDropped.clientId );
    CHECK_EQUAL( packet.sequenceNumber, event.packetDropped.sequenceNumber );

    testCurrentTime = 2.0;
    // Drop packets that have been in flight for more than 1000 milliseconds
    net_ServerDropUnacknowledgedPackets( &server, 1000 );

    // Should not generate another dropped packet event for the same packet.
    CHECK( !net_ServerPollEvent( &server, &event ) );
  }
}

struct NetClientFixture
{
  NetClientFixture()
  {
    ZeroStruct( client );
    uint8_t *memory = new uint8_t[KILOBYTES( 256 )];
    net_ClientInit( &client, memory, KILOBYTES( 256 ) );
  }
  virtual ~NetClientFixture() { delete[] client.arena.base; }
  net_Client client;
};

SUITE( NetClient )
{
  // TODO: Test when no packets have been received yet
  TEST_FIXTURE( NetClientFixture, GetLastRoundTripTime )
  {
    Packet incomingPacket = {};
    testCurrentTime = 50.0;
    incomingPacket.returnTimestamp = net_GetCurrentTimestamp();
    testCurrentTime = 50.05;
    Packet receivedPacket = {};
    net_ClientQueueIncomingPacket( &client, incomingPacket );
    CHECK_EQUAL( 1u, net_ClientReceivePacket( &client, &receivedPacket, 1 ) );
    CHECK_EQUAL( 50u, net_ClientGetLastRoundTripTime( &client ) );
    testCurrentTime = 0.0;
  }

  TEST_FIXTURE( NetClientFixture, GetRoundTripTimes )
  {
    Packet incomingPacket = {};
    testCurrentTime = 50.0;
    incomingPacket.returnTimestamp = net_GetCurrentTimestamp();
    net_ClientQueueIncomingPacket( &client, incomingPacket );

    incomingPacket.sequenceNumber++;
    testCurrentTime = 50.05;
    incomingPacket.returnTimestamp = net_GetCurrentTimestamp();
    net_ClientQueueIncomingPacket( &client, incomingPacket );

    // Advance time since returnTimestamp for second packet by 50 ms
    testCurrentTime = 50.08;

    Packet receivedPackets[2] = {};
    CHECK_EQUAL( 2u, net_ClientReceivePacket( &client, receivedPackets, 2 ) );
    CHECK_EQUAL( 0, receivedPackets[0].sequenceNumber );
    CHECK_EQUAL( 1, receivedPackets[1].sequenceNumber );

    uint32_t roundTripTimes[2];
    CHECK_EQUAL( 2u,
                 net_ClientGetRoundTripTimes( &client, roundTripTimes, 2 ) );
    CHECK_EQUAL( 80u, roundTripTimes[0] );
    CHECK_EQUAL( 30u, roundTripTimes[1] );
    CHECK_EQUAL( 30u, net_ClientGetLastRoundTripTime( &client ) );
    testCurrentTime = 0.0;
  }

  TEST_FIXTURE( NetClientFixture,
                IncomingPacketWithInvalidSequenceNumberIsDropped )
  {
    Packet incomingPacket = {};
    net_ClientQueueIncomingPacket( &client, incomingPacket );
    CHECK_EQUAL( 1u, client.receiveQueueLength );

    // Duplicate sequence number should cause packet to be dropped
    net_ClientQueueIncomingPacket( &client, incomingPacket );
    CHECK_EQUAL( 1u, client.receiveQueueLength );
  }

  TEST_FIXTURE( NetClientFixture, HandlesSequenceNumberWrap )
  {
    Packet packet = {};
    packet.sequenceNumber = 0xFFFF;

    client.expectedSequenceNumber = 0xFFFF;

    CHECK( net_ClientQueueIncomingPacket( &client, packet ) );

    packet.sequenceNumber++;
    CHECK( net_ClientQueueIncomingPacket( &client, packet ) );

    packet.sequenceNumber = 0xFFFE;
    CHECK( !net_ClientQueueIncomingPacket( &client, packet ) );
  }

#if 0
  TEST_FIXTURE( NetClientFixture,
                DropsInFlightPacketsThatHaveNotReceivedAckAfterThreshold )
  {
    testCurrentTime = 0.0;
    net_ClientGeneratePacket( &client );
    testCurrentTime = 1.01;

    // Drop packets that have been in flight for more than 1000 milliseconds
    CHECK_EQUAL( 1u, net_ClientDropUnacknowledgedPackets( &client, 1000 ) );
  }
#endif
}

struct ClientServerFixture
{
  ClientServerFixture()
  {
    serverMemory = (uint8_t *)malloc(KILOBYTES( 256 ));
    net_ServerInit( &server, serverMemory, KILOBYTES( 256 ) );

    clientMemory = (uint8_t *)malloc(KILOBYTES( 256 ));
    net_ClientInit( &client, clientMemory, KILOBYTES( 256 ) );
  }

  ~ClientServerFixture()
  {
    free( serverMemory );
    free( clientMemory );
  }

  net_Server server;
  net_Client client;
  uint8_t *serverMemory;
  uint8_t *clientMemory;
};

// TODO: Probably should make 0 and invalid sequence number
SUITE( NetworkIntegrationTests )
{
  TEST_FIXTURE( ClientServerFixture, ClientConnectsToServer )
  {
    Packet clientConnectionPacket = net_ClientGeneratePacket( &client );
    clientConnectionPacket.len = 1;

    // Skipped net_ClientQueueOutgoingPacket as that is only used for network
    // delay/loss simulation
    uint8_t buffer[NET_SERVER_PACKET_SIZE + 10];
    uint32_t len =
      net_SerializePacket( buffer, sizeof( buffer ), clientConnectionPacket );
    MemoryPoolFree( &client.outgoingPacketPool, clientConnectionPacket.data );

    // Transfer buffer to server

    // Recognize packet is from new address, allocate new client
    uint32_t clientId = 1;
    net_ServerAddClient( &server, clientId );

    auto packetData =
      net_DeserializePacket( buffer, len, clientId );

    CHECK( net_ServerQueueIncomingPacket( &server, clientId, packetData ) );

    Packet incomingConnectionPacket = {};
    CHECK_EQUAL( 1u, net_ServerReceivePackets( &server, clientId,
                                               &incomingConnectionPacket, 1 ) );

    net_ServerFreeReceivedPackets( &server, &incomingConnectionPacket, 1 );
  }
}

SUITE( ByteQueueTests )
{
  TEST( ByteQueueWrite )
  {
    ByteQueue queue;
    uint8_t buffer[ 80 ];
    ByteQueueInitialize( &queue, buffer, sizeof( buffer ) );

    uint64_t data = 4;
    CHECK( ByteQueueWrite( &queue, &data, sizeof( data ) ) );
    uint64_t out = 0;
    CHECK( ByteQueueRead( &queue, &out, sizeof( out ) ) );
    CHECK_EQUAL( data, out );

    ByteQueueClear( &queue );
  }

  TEST( ByteQueueWriteWrapped )
  {
    ByteQueue queue;
    uint8_t buffer[ 8 ];
    ByteQueueInitialize( &queue, buffer, sizeof( buffer ) );

    uint32_t data4 = 33;
    CHECK( ByteQueueWrite( &queue, &data4, sizeof( data4 ) ) );
    uint32_t out4 = 0;
    CHECK( ByteQueueRead( &queue, &out4, sizeof( out4 ) ) );
    CHECK_EQUAL( data4, out4 );

    uint64_t data8 = 41;
    CHECK( ByteQueueWrite( &queue, &data8, sizeof( data8 ) ) );
    uint64_t out8 = 0;
    CHECK( ByteQueueRead( &queue, &out8, sizeof( out8 ) ) );
    CHECK_EQUAL( data8, out8 );
  }
}

int main( int argc, char **argv )
{
  int result = UnitTest::RunAllTests();
#ifdef _MSC_VER
  getchar();
#endif
  return result;
}
