#pragma once

#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>

#include "common/net.h"

/* VULNERABILTIES
 * - Sending many connection requests from many IPs will cause clientId to wrap
 */
// TODO: Hide implementation details

struct sock_ServerEntry
{
  struct sockaddr_storage address;
  uint32_t clientId;
};

struct sock_Server
{
  sock_ServerEntry clients[NET_MAX_CLIENTS];
  ContiguousObjectPool clientsPool;
  uint32_t nextClientId; // TODO: Must do a freelist.
  int socket;
};

struct sock_Client
{
  int socket;
  struct addrinfo *serverAddress;
  bool isInitialized;
};

internal bool sock_InitializeServer( sock_Server *server );

internal bool sock_InitializeClient( sock_Client *client );

internal void sock_DeinitializeServer( sock_Server *server );

internal void sock_DeinitializeClient( sock_Client *client );

struct sock_ReceivePacketResult
{
  uint32_t packetLength;
  uint32_t clientId;
  bool dropPacket;
};

internal bool sock_ServerReceivePacket( sock_Server *server, uint8_t *data,
                                        uint32_t len,
                                        sock_ReceivePacketResult *result );

internal void sock_ServerSendPacket( sock_Server *server, uint32_t clientId,
                                     uint8_t *data, uint32_t len );

internal void sock_ClientSendPacket( sock_Client *client, uint8_t *data,
                                     uint32_t len );

internal bool sock_ClientReceivePacket( sock_Client *client, uint8_t *data,
                                        uint32_t len,
                                        sock_ReceivePacketResult *result );
