#pragma once

#include "common/utils.h"
#include "common/net.h"

#define NET_SERVER_INCOMING_PACKET_QUEUE_LENGTH (NET_MAX_CLIENTS*8)
#define NET_SERVER_INCOMING_PACKET_POOL_SIZE (NET_MAX_CLIENTS*8)
#define NET_SERVER_OUTGOING_PACKET_POOL_SIZE (NET_MAX_CLIENTS*4)

#define NET_CLIENT_INCOMING_PACKET_POOL_SIZE 24
#define NET_CLIENT_OUTGOING_PACKET_POOL_SIZE 24
#define NET_CLIENT_INCOMING_PACKET_QUEUE_LENGTH 24
#define NET_CLIENT_OUTGOING_PACKET_QUEUE_LENGTH 24
#define NET_CLIENT_RTT_SAMPLES_COUNT 32

#define NET_SERVER_RECEIVE_QUEUE_LENGTH 8

// Roughly one second worth at 60 ticks per second
#define NET_SERVER_MAX_IN_FLIGHT_PACKETS 64

#define NET_SERVER_EVENT_QUEUE_LENGTH 256

#define NET_CLIENT_EVENT_QUEUE_LENGTH 64

enum
{
  NET_PACKET_STATE_UNKNOWN = 0,
  NET_PACKET_STATE_ACKED,
  NET_PACKET_STATE_DROPPED,
};

struct net_InFlightPacket
{
  uint32_t timestamp; // Unit if milliseconds
  uint16_t sequenceNumber;
  uint8_t state;
  uint8_t padding;
};

enum
{
  NET_PACKET_DROPPED_EVENT = 0,
  NET_PACKET_ACKNOWLEDGED_EVENT,
  NET_CLIENT_CONNECTED_EVENT,
  NET_CONNECTION_DROPPED_EVENT,
};

struct net_PacketDroppedEvent
{
  uint32_t type;
  uint32_t clientId;
  uint16_t sequenceNumber;
};

struct net_PacketAcknowledgedEvent
{
  uint32_t type;
  uint32_t clientId;
  uint16_t sequenceNumber;
};

struct net_ClientConnectedEvent
{
  uint32_t type;
  uint32_t clientId;
};

struct net_ConnectionDroppedEvent
{
  uint32_t type;
  uint32_t clientId;
};

union net_Event
{
  uint32_t type;
  net_PacketDroppedEvent packetDropped;
  net_PacketAcknowledgedEvent packetAcknowledged;
  net_ClientConnectedEvent clientConnected;
};

struct net_ServerClientData
{
  Packet receiveQueue[NET_SERVER_RECEIVE_QUEUE_LENGTH];
  int receiveQueueTail;
  int receiveQueueHead;
  uint16_t acksToSend[NET_MAX_ACKS_PER_PACKET];
  net_InFlightPacket inFlightPacketsArray[NET_SERVER_MAX_IN_FLIGHT_PACKETS];
  CircularBuffer inFlightPackets;
  uint32_t id;
  uint32_t receiveQueueLength;
  uint32_t returnTimestamp;
  uint32_t receiveTimestamp;
  uint16_t nextSequenceNumber;
  uint16_t expectedSequenceNumber;
};

struct net_Server
{
  MemoryArena arena;
  MemoryPool incomingPacketPool;
  MemoryPool outgoingPacketPool;;
  net_ServerClientData clients[NET_MAX_CLIENTS];
  ContiguousObjectPool clientsPool;
  CircularBuffer eventQueue;
  net_Event eventQueueArray[NET_SERVER_EVENT_QUEUE_LENGTH];
};

struct net_ReceiveQueueEntry
{
  double deliveryTime;
  Packet packet;
};

struct net_Client
{
  MemoryArena arena;
  MemoryPool incomingPacketPool;
  MemoryPool outgoingPacketPool;
  net_ReceiveQueueEntry receiveQueue[NET_CLIENT_INCOMING_PACKET_QUEUE_LENGTH];
  uint32_t receiveQueueLength;
  net_ReceiveQueueEntry outgoingQueue[NET_CLIENT_OUTGOING_PACKET_QUEUE_LENGTH];
  uint32_t outgoingQueueLength;
  float fakeLatency; // NOTE: Latency is symmetric
  float fakeLoss; // NOTE: 0.0 - 1.0 as a percentage
  CircularBuffer roundTripTimes;
  uint32_t returnTimestamp;
  uint16_t nextSequenceNumber;
  uint16_t expectedSequenceNumber;
  CircularBuffer eventQueue;
  net_Event eventQueueArray[NET_SERVER_EVENT_QUEUE_LENGTH];
  double lastPacketReceiveTime;
};

internal double net_GetCurrentTime();
