#pragma once

#include <cstdint>

#include "common/utils.h"

#define PHYS_MAX_LINE_VERTICES 0xFFFF
#define PHYS_INVALID_HANDLE 0xFFFFFFFF

struct PhysicsSystem;

extern PhysicsSystem *CreatePhysicsSystem();

extern void DestroyPhysicsSystem( PhysicsSystem *physicsSystem );

#define PHYS_UPDATE( NAME ) void NAME( PhysicsSystem *physicsSystem, float dt )

typedef PHYS_UPDATE( PhysUpdateFunction );

enum
{
  PHYS_HANDLE_UNKNOWN = 0,
  PHYS_HANDLE_CHARACTER,
  PHYS_HANDLE_RIGID_BODY,
  PHYS_HANDLE_TRIGGER,
};

struct PhysicsHandle
{
  uint32_t type, index, uniqueId;
};

inline bool ComparePhysicsHandles( PhysicsHandle a, PhysicsHandle b )
{
  return ( a.type == b.type ) && ( a.index == b.index ) &&
         ( a.uniqueId == b.uniqueId );
}

inline bool PhysIsHandleValid( PhysicsHandle handle )
{
  return ( handle.type != PHYS_HANDLE_UNKNOWN );
}

#define PHYS_CAN_CHARACTER_MOVE_TO( NAME )                                     \
  bool NAME( PhysicsSystem *physicsSystem, PhysicsHandle physicsHandle,        \
             vec3 position, float *minSlopeDot, vec3 *normal,        \
             float *closestHitFraction )
typedef PHYS_CAN_CHARACTER_MOVE_TO( PhysCanCharacterMoveToFunction );

#define PHYS_PERFORM_CHARACTER_PUSH_BACK( NAME )                               \
  bool NAME( PhysicsSystem *physicsSystem, PhysicsHandle physicsHandle,        \
             vec3 *result )

typedef PHYS_PERFORM_CHARACTER_PUSH_BACK(
  PhysPerformCharacterPushBackFunction );

#define PHYS_GET_RIGID_BODY_TRANSFORM( NAME )                                  \
  bool NAME( PhysicsSystem *physicsSystem, PhysicsHandle physicsHandle,        \
             vec3 *position)
  typedef PHYS_GET_RIGID_BODY_TRANSFORM( PhysGetRigidBodyTransformFunction );

#define PHYS_CREATE_CHARACTER( NAME )                                          \
  PhysicsHandle NAME( PhysicsSystem *physicsSystem, vec3 position )
typedef PHYS_CREATE_CHARACTER( PhysCreateCharacterFunction );

#define PHYS_SET_CHARACTER_POSITION( NAME )                                    \
  void NAME( PhysicsSystem *physicsSystem, PhysicsHandle physicsHandle,        \
             vec3 position )
typedef PHYS_SET_CHARACTER_POSITION( PhysSetCharacterPositionFunction );

enum
{
  PHYS_SHAPE_CAST_FLAGS_NONE = 0,
  PHYS_SHAPE_CAST_FLAGS_TEST_SLOPE = 1,
};
struct ShapeCastParams
{
  uint32_t shape;
  vec3 start;
  vec3 end;
  float slope;
  int flags;
  PhysicsHandle *ignored;
  uint32_t numIgnored;
};
struct ShapeCastResult
{
  vec3 hitNormal, hitPoint;
  float hitFraction;
  PhysicsHandle physicsHandle;
  bool hasHit;
};
#define PHYS_SHAPE_CAST( NAME )                                                \
  ShapeCastResult NAME( PhysicsSystem *physicsSystem, ShapeCastParams params )

typedef PHYS_SHAPE_CAST( PhysShapeCastFunction );

struct ShapeCastMultiResult
{
  vec3 hitNormal, hitPoint;
  float hitFraction;
  PhysicsHandle physicsHandle;
};
#define PHYS_SHAPE_CAST_MULTI( NAME )                                          \
  int NAME( PhysicsSystem *physicsSystem, uint32_t shapeHandle,                \
            vec3 start, vec3 end, ShapeCastMultiResult *results,     \
            uint32_t maxResults )
typedef PHYS_SHAPE_CAST_MULTI( PhysShapeCastMultiFunction );

#define PHYS_RAY_CAST( NAME )                                                  \
  ShapeCastResult NAME( PhysicsSystem *physicsSystem, vec3 start,         \
                        vec3 end, PhysicsHandle *ignored, uint32_t size,  \
                        uint8_t filterGroup, uint8_t filterMask )

typedef PHYS_RAY_CAST( PhysRayCastFunction );

#define PHYS_RAY_CAST_MULTI( NAME )                                            \
  int NAME( PhysicsSystem *physicsSystem, vec3 start, vec3 end,      \
      ShapeCastMultiResult *results, uint32_t maxResults )
typedef PHYS_RAY_CAST_MULTI( PhysRayCastMultiFunction );

#define PHYS_CREATE_SPHERE_SHAPE( NAME )                                       \
  uint32_t NAME( PhysicsSystem *physicsSystem, float radius )
  typedef PHYS_CREATE_SPHERE_SHAPE( PhysCreateSphereShapeFunction );

#define PHYS_DESTROY_OBJECT( NAME )                                            \
  void NAME( PhysicsSystem *physicsSystem, PhysicsHandle physicsHandle )
typedef PHYS_DESTROY_OBJECT( PhysDestroyObjectFunction );

#define PHYS_CREATE_TRIANGLE_MESH_SHAPE( NAME )                                \
  uint32_t NAME( PhysicsSystem *physicsSystem, vec3 *vertices,            \
                 uint32_t numVertices )
typedef PHYS_CREATE_TRIANGLE_MESH_SHAPE( PhysCreateTriangleMeshShapeFunction );

enum
{
  RIGID_BODY_DYNAMIC,
  RIGID_BODY_STATIC,
  RIGID_BODY_KINEMATIC,
};

enum
{
  COLLISION_TYPE_DEFAULT = BIT( 0 ),
  COLLISION_TYPE_CHARACTER = BIT( 1 ),
  COLLISION_TYPE_KINEMATIC = BIT( 2 ),
  COLLISION_TYPE_STATIC = BIT( 3 ),
  COLLISION_TYPE_ALL = -1
};

struct PhysRigidBodyParameters
{
  vec3 position, localOrigin;
  float mass;
  uint32_t shapeHandle;
  uint8_t type;
  uint8_t collidesWith;
};
#define PHYS_CREATE_RIGID_BODY( NAME )                                         \
  PhysicsHandle NAME( PhysicsSystem *physicsSystem,                            \
                      PhysRigidBodyParameters params )
typedef PHYS_CREATE_RIGID_BODY( PhysCreateRigidBodyFunction );

#define PHYS_CREATE_BOX_SHAPE( NAME )                                          \
  uint32_t NAME( PhysicsSystem *physicsSystem, vec3 dimensions )
typedef PHYS_CREATE_BOX_SHAPE( PhysCreateBoxShapeFunction );

#define PHYS_UPDATE_KINEMATIC_RIGID_BODY( NAME )                               \
  void NAME( PhysicsSystem *physicsSystem, PhysicsHandle physicsHandle,        \
             vec3 position)

typedef PHYS_UPDATE_KINEMATIC_RIGID_BODY(
  PhysUpdateKinematicRigidBodyFunction );

#define PHYS_APPLY_FORCE_TO_DYNAMIC_RIGID_BODY( NAME )                         \
void NAME( PhysicsSystem *physicsSystem, PhysicsHandle physicsHandle,          \
           vec3 force )

typedef PHYS_APPLY_FORCE_TO_DYNAMIC_RIGID_BODY(
    PhysApplyForceToDynamicRigidBodyFunction );

#define PHYS_APPLY_IMPULSE_TO_DYNAMIC_RIGID_BODY( NAME )                       \
  void NAME( PhysicsSystem *physicsSystem, PhysicsHandle physicsHandle,        \
             vec3 impulse, vec3 position )

typedef PHYS_APPLY_IMPULSE_TO_DYNAMIC_RIGID_BODY(
  PhysApplyImpulseToDynamicRigidBodyFunction );

struct TriggerParams
{
  vec3 position;
  uint32_t shapeHandle;
};

#define PHYS_CREATE_TRIGGER( NAME )                                            \
  PhysicsHandle NAME( PhysicsSystem *physicsSystem,                            \
                      const TriggerParams &params )

typedef PHYS_CREATE_TRIGGER( PhysCreateTriggerFunction );

#define PHYS_GET_OVERLAPPING_OBJECTS( NAME )                                   \
  uint32_t NAME( PhysicsSystem *physicsSystem, PhysicsHandle *handles,         \
                 uint32_t size, PhysicsHandle triggerHandle )

typedef PHYS_GET_OVERLAPPING_OBJECTS( PhysGetOverlappingObjectsFunction );

#define PHYS_DESTROY_ALL_OBJECTS( NAME )                                       \
  void NAME( PhysicsSystem *physicsSystem )

typedef PHYS_DESTROY_ALL_OBJECTS( PhysDestroyAllObjectsFunction );

enum
{
  OVERLAP_EVENT_ENTER,
  OVERLAP_EVENT_EXIT,
};

struct OverlapEvent
{
  uint32_t type;
  PhysicsHandle object;
};

#define PHYS_GET_TRIGGER_EVENTS( NAME )                                        \
  uint32_t NAME( PhysicsSystem *physicsSystem, OverlapEvent *events,           \
                 uint32_t size, PhysicsHandle triggerHandle )

typedef PHYS_GET_TRIGGER_EVENTS( PhysGetTriggerEventsFunction );

extern PHYS_PERFORM_CHARACTER_PUSH_BACK( PhysPerformCharacterPushBack );
extern PHYS_CAN_CHARACTER_MOVE_TO( PhysCanCharacterMoveTo );
extern PHYS_GET_RIGID_BODY_TRANSFORM( PhysGetRigidBodyTransform );
extern PHYS_CREATE_CHARACTER( PhysCreateCharacter );
extern PHYS_SET_CHARACTER_POSITION( PhysSetCharacterPosition );
extern PHYS_SHAPE_CAST( PhysShapeCast );
extern PHYS_CREATE_SPHERE_SHAPE( PhysCreateSphereShape );
extern PHYS_DESTROY_OBJECT( PhysDestroyObject );
extern PHYS_SHAPE_CAST_MULTI( PhysShapeCastMulti );
extern PHYS_CREATE_TRIANGLE_MESH_SHAPE( PhysCreateTriangleMeshShape );
extern PHYS_CREATE_RIGID_BODY( PhysCreateRigidBody );
extern PHYS_CREATE_BOX_SHAPE( PhysCreateBoxShape );
extern PHYS_RAY_CAST( PhysRayCast );
extern PHYS_RAY_CAST_MULTI( PhysRayCastMulti );
extern PHYS_UPDATE_KINEMATIC_RIGID_BODY( PhysUpdateKinematicRigidBody );
extern PHYS_APPLY_FORCE_TO_DYNAMIC_RIGID_BODY(
  PhysApplyForceToDynamicRigidBody );
extern PHYS_APPLY_IMPULSE_TO_DYNAMIC_RIGID_BODY(
  PhysApplyImpulseToDynamicRigidBody );
extern PHYS_CREATE_TRIGGER( PhysCreateTrigger );
extern PHYS_GET_OVERLAPPING_OBJECTS( PhysGetOverlappingObjects );
extern PHYS_GET_TRIGGER_EVENTS( PhysGetTriggerEvents );
extern PHYS_DESTROY_ALL_OBJECTS( PhysDestroyAllObjects );
extern PHYS_UPDATE( PhysUpdate );

struct phys_LineVertex
{
  vec3 position;
  vec4 colour;
};
#define PHYS_GET_VERTEX_DATA( NAME )                                           \
  uint32_t NAME( PhysicsSystem *physicsSystem, phys_LineVertex *vertices,      \
                 uint32_t numVertices )

typedef PHYS_GET_VERTEX_DATA( PhysGetVertexDataFunction );

extern uint32_t GetVertexData( PhysicsSystem *physicsSystem,
                               phys_LineVertex *vertices, uint32_t numVertices );

struct GamePhysics
{
  PhysicsSystem *physicsSystem;
  PhysCreateCharacterFunction *createCharacter;
  PhysGetVertexDataFunction *getVertexData;
  PhysSetCharacterPositionFunction *setCharacterPosition;
  PhysCanCharacterMoveToFunction *canCharacterMoveTo;
  PhysPerformCharacterPushBackFunction *performCharacterPushBack;
  PhysGetRigidBodyTransformFunction *getRigidBodyTransform;
  PhysShapeCastFunction *shapeCast;
  PhysCreateSphereShapeFunction *createSphereShape;
  PhysDestroyObjectFunction *destroyObject;
  PhysShapeCastMultiFunction *shapeCastMulti;
  PhysCreateTriangleMeshShapeFunction *createTriangleMeshShape;
  PhysCreateRigidBodyFunction *createRigidBody;
  PhysCreateBoxShapeFunction *createBoxShape;
  PhysRayCastMultiFunction *rayCastMulti;
  PhysRayCastFunction *rayCast;
  PhysUpdateKinematicRigidBodyFunction *updateKinematicRigidBody;
  PhysApplyForceToDynamicRigidBodyFunction *applyForce;
  PhysApplyImpulseToDynamicRigidBodyFunction *applyImpulse;
  PhysCreateTriggerFunction *createTrigger;
  PhysGetOverlappingObjectsFunction *getOverlappingObjects;
  PhysGetTriggerEventsFunction *getTriggerEvents;
  PhysDestroyAllObjectsFunction *destroyAllObjects;
  PhysUpdateFunction *update;
};
