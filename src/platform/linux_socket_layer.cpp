#include "linux_socket_layer.h"

internal bool sock_InitializeServer( sock_Server *server )
{
  bool result = false;
  struct sockaddr_in address;
  uint16_t portNumber = 18000;

  ZeroStruct( address );
  ZeroPointerToStruct( server );

  server->nextClientId = 1;
  server->clientsPool = CreateContiguousObjectPoolArray( server->clients );

  server->socket = socket( AF_INET, SOCK_DGRAM, 0 );
  if ( server->socket > 0 )
  {
    int optionValue = 1;
    //if ( setsockopt( server->socket, SOL_SOCKET, SO_REUSEADDR, &optionValue,
                     //sizeof( optionValue ) ) == 0 )
    {
      //if ( fcntl( server->socket, F_SETFL, O_NONBLOCK ) == 0 )
      {
        address.sin_family = AF_INET;
        address.sin_addr.s_addr = htonl( INADDR_ANY );
        address.sin_port = htons( portNumber );
        if ( bind( server->socket, (struct sockaddr *)&address,
                   sizeof( address ) ) == 0 )
        {
          result = true;
        }
        else
        {
          LOG_ERROR( "Failed to bind server socket" );
        }
      }
      //else
      //{
        //LOG_ERROR( "Failed to set server socket to non-blocking" );
      //}
    }
    //else
    //{
      //LOG_ERROR( "setsockopt failed" );
    //}
  }
  else
  {
    LOG_ERROR( "Failed to open server socket" );
  }

  return result;
}

internal bool sock_InitializeClient( sock_Client *client, const char *address,
                                     const char *port )
{
  bool result = false;
  struct addrinfo hints;
  ZeroStruct( hints );
  ZeroPointerToStruct( client );

  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = 0;

  ZeroStruct( client->serverAddress );
  if ( getaddrinfo( address, port, &hints, &client->serverAddress ) == 0 )
  {
    ASSERT( client->serverAddress->ai_next == NULL );
    client->socket = socket( client->serverAddress->ai_family,
                             client->serverAddress->ai_socktype,
                             client->serverAddress->ai_protocol );
    if ( client->socket > 0 )
    {
      if ( fcntl( client->socket, F_SETFL, O_NONBLOCK ) == 0 )
      {
        result = true;
        // if ( connect( client->socket, client->serverAddress->ai_addr,
        // client->serverAddress->ai_addrlen ) == 0 )
        //{
        // result = true;
        //}
        // else
        //{
        // LOG_ERROR( "Client failed to connect to server" );
        //}
      }
      else
      {
        LOG_ERROR( "Failed to set client socket to non-blocking" );
      }
    }
    else
    {
      LOG_ERROR( "Failed to open client socket" );
    }
  }
  else
  {
    LOG_ERROR( "getaddrinfo failed" );
  }
  client->isInitialized = result;

  return result;
}

internal void sock_DeinitializeServer( sock_Server *server )
{
  close( server->socket );
}

internal void sock_DeinitializeClient( sock_Client *client )
{
  close( client->socket );
  freeaddrinfo( client->serverAddress );
  client->isInitialized = false;
}

internal bool CompareAddresses( struct sockaddr_storage *a,
                                struct sockaddr_storage *b )
{
  if ( ( a->ss_family == b->ss_family ) )
  {
    if ( a->ss_family == AF_INET )
    {
      auto a4 = (sockaddr_in *)a;
      auto b4 = (sockaddr_in *)b;
      return ( ( a4->sin_addr.s_addr == b4->sin_addr.s_addr ) &&
               ( a4->sin_port == b4->sin_port ) );
    }
    else if ( a->ss_family == AF_INET6 )
    {
      auto a6 = (sockaddr_in6 *)a;
      auto b6 = (sockaddr_in6 *)b;
      auto addr1 = (uint32_t *)a6->sin6_addr.s6_addr;
      auto addr2 = (uint32_t *)b6->sin6_addr.s6_addr;
      return ( ( addr1[0] == addr2[0] ) && ( addr1[1] == addr2[1] ) &&
               ( addr1[2] == addr2[2] ) && ( addr1[3] == addr2[3] ) &&
               ( a6->sin6_port == b6->sin6_port ) );
    }
    else
    {
      LOG_WARN( "Unknown address family" );
    }
  }
  return false;
}

internal bool sock_ServerReceivePacket( sock_Server *server, uint8_t *data,
                                        uint32_t len,
                                        sock_ReceivePacketResult *result )
{
  struct sockaddr_storage receivedAddress;
  memset( &receivedAddress, 0, sizeof( receivedAddress ) );

  receivedAddress.ss_family = AF_INET;

  socklen_t receivedAddressLength = sizeof( receivedAddress );

  fd_set fds;
  struct timeval tv;
  FD_ZERO(&fds);
  FD_SET(server->socket, &fds);

  tv.tv_sec = 0;
  tv.tv_usec = 50;

  int ret = select(server->socket+1, &fds, NULL, NULL, &tv);
  if ( ret == -1)
  {
    LOG_ERROR( "select error: %s", strerror(errno));
    return false;
  }
  else if ( ret == 0 )
  {
    return false; // no data available
  }
  else
  {
    uint32_t bytesAvailable = 0;
    ioctl( server->socket, FIONREAD, &bytesAvailable );
    int ret = recvfrom( server->socket, data, len, 0,
                        (sockaddr *)&receivedAddress, &receivedAddressLength );
    if ( ret > 0 )
    {
      result->packetLength = ret;
      result->dropPacket = false;

      bool found = false;
      for ( uint32_t i = 0; i < server->clientsPool.size; ++i )
      {
        auto entry = server->clients + i;
        if ( CompareAddresses( &receivedAddress, &entry->address ) )
        {
          result->clientId = entry->clientId;
          found = true;
          break;
        }
      }

      if ( !found )
      {
        auto entry = (sock_ServerEntry *)AllocateObject( &server->clientsPool );
        if ( entry )
        {
          entry->clientId = server->nextClientId++;
          entry->address = receivedAddress;
          result->clientId = entry->clientId;
        }
        else
        {
          LOG_WARN( "Connection refused, cannot support more clients" );
          result->dropPacket = true;
        }
      }
      return true;
    }
    else
    {
      if ( errno != EAGAIN )
      {
        LOG_WARN( "Error occurred while recieving packet: %s",
                  strerror( errno ) );
      }
      return false;
    }
  }
}

internal void sock_ServerSendPacket( sock_Server *server, uint32_t clientId,
                                     uint8_t *data, uint32_t len )
{
  for ( uint32_t i = 0; i < server->clientsPool.size; ++i )
  {
    auto entry = server->clients + i;
    if ( entry->clientId == clientId )
    {
      if ( sendto( server->socket, data, len, MSG_DONTWAIT,
                   (struct sockaddr *)&entry->address,
                   sizeof( entry->address ) ) != len )
      {
        LOG_WARN( "Server failed to send packets to client" );
      }
      break;
    }
  }
}

internal void sock_ClientSendPacket( sock_Client *client, uint8_t *data,
                                     uint32_t len )
{
  if ( !client->isInitialized )
  {
    return;
  }

  int ret =
    sendto( client->socket, data, len, 0, client->serverAddress->ai_addr,
            client->serverAddress->ai_addrlen );
  if ( ret != (int)len )
  {
    LOG_WARN( "Client failed to send packets to server: %s",
              strerror( errno ) );
  }
}

internal bool sock_ClientReceivePacket( sock_Client *client, uint8_t *data,
                                        uint32_t len,
                                        sock_ReceivePacketResult *result )
{
  int ret = recv( client->socket, data, len, MSG_DONTWAIT );
  if ( ret > 0 )
  {
    result->packetLength = ret;
    result->clientId = NET_SERVER_CLIENT_ID;
    result->dropPacket = false;
    return true;
  }
  else
  {
    return false;
  }
}

internal void sock_ServerProcessIncomingPackets( sock_Server *sockServer,
                                                 net_Server *netServer )
{
  sock_ReceivePacketResult result;
  uint8_t buffer[NET_CLIENT_PACKET_SIZE+10];
  uint32_t i = 0;
  while (
    sock_ServerReceivePacket( sockServer, buffer, sizeof( buffer ), &result ) )
  {
    if ( net_ServerGetClient( netServer, result.clientId ) == NULL )
    {
      if ( !net_ServerAddClient( netServer, result.clientId ) )
      {
        LOG_WARNING( "Server is full, dropping packet" );
        result.dropPacket = true;
      }
    }

    if ( result.dropPacket )
    {
      continue;
    }
    if ( i > 1000 )
    {
      break; // TODO: Do something smarter here so we don't overflow the queue
    }


    auto packet =
      net_DeserializePacket( buffer, result.packetLength, result.clientId );

    if ( packet.data )
    {
      net_ServerQueueIncomingPacket( netServer, result.clientId, packet );
    }
  }
  auto client = net_ServerGetClient( netServer, result.clientId );
  if ( client )
  {
    client->receiveTimestamp = net_GetCurrentTimestamp();
  }
}

internal void sock_ServerProcessOutgoingPacket( sock_Server *sockServer,
                                                net_Server *netServer,
                                                Packet *packet )
{
  if ( packet->len > 0 )
  {
    ASSERT( packet->clientId < NET_MAX_CLIENTS );

    uint8_t buffer[NET_SERVER_PACKET_SIZE + 10];

    packet->timestamp = net_GetCurrentTimestamp();
    auto client = net_ServerGetClient( netServer, packet->clientId );
    ASSERT( client );
    uint32_t frameTime = packet->timestamp - client->receiveTimestamp;
    packet->returnTimestamp -= frameTime;
    uint32_t len = net_SerializePacket( buffer, sizeof( buffer ), *packet );

    sock_ServerSendPacket( sockServer, packet->clientId, buffer, len );
  }
  MemoryPoolFree( &netServer->outgoingPacketPool, packet->data );
}

internal void sock_ClientProcessOutgoingPackets( sock_Client *sockClient,
                                                 net_Client *netClient )
{
  qsort( netClient->outgoingQueue, netClient->outgoingQueueLength,
         sizeof( net_ReceiveQueueEntry ), net_CompareReceiveQueueEntries );

  uint32_t count = 0;
  for ( int i = netClient->outgoingQueueLength - 1; i >= 0; --i )
  {
    auto entry = netClient->outgoingQueue + i;
    if ( entry->deliveryTime <= net_GetCurrentTime() )
    {
      count++;
      auto packet = &entry->packet;
      uint8_t buffer[NET_SERVER_PACKET_SIZE + 10];

      packet->timestamp = net_GetCurrentTimestamp();
      uint32_t len = net_SerializePacket( buffer, sizeof( buffer ), *packet );
      sock_ClientSendPacket( sockClient, buffer, len );
      MemoryPoolFree( &netClient->outgoingPacketPool, packet->data );
    }
  }
  netClient->outgoingQueueLength -= count;
}

internal void sock_ClientProcessIncomingPackets( sock_Client *sockClient,
                                                 net_Client *netClient )
{
  sock_ReceivePacketResult result;
  uint8_t buffer[NET_SERVER_PACKET_SIZE+10];
  uint32_t i = 0;
  if ( !sockClient->isInitialized )
  {
    return;
  }
  while (
    sock_ClientReceivePacket( sockClient, buffer, sizeof( buffer ), &result ) )
  {
    if ( result.dropPacket )
    {
      continue;
    }
    if ( i > 1000 )
    {
      break; // TODO: Do something smarter here so we don't overflow the queue
    }
    Packet packet;
    ByteBuffer byteBuffer = {};
    byteBuffer.data = buffer;
    byteBuffer.size = result.packetLength;
    if ( ReadLong( &packet.timestamp, &byteBuffer ) )
    {
      if ( ReadLong( &packet.returnTimestamp, &byteBuffer ) )
      {
        if ( ReadShort( &packet.sequenceNumber, &byteBuffer ) )
        {
          packet.len = result.packetLength - byteBuffer.readCursor;
          packet.data = buffer + byteBuffer.readCursor;
          packet.clientId = result.clientId;
          net_ClientQueueIncomingPacket( netClient, packet );
        }
        else
        {
          LOG_WARNING( "Failed to deserialize packet sequence number" );
        }
      }
      else
      {
        LOG_WARNING( "Failed to deserialize packet returnTimestamp" );
      }
    }
    else
    {
      LOG_WARNING( "Failed to deserialize packet timestamp" );
    }
    i++;
  }
}
