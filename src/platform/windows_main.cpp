#include <cstdio>
#include <cstdlib>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>

#include "game/game.h"

#include "windows_socket_layer.h"

#include "common/cmd.cpp"
#include "platform/network_layer.cpp"
#include "platform/windows_socket_layer.cpp"

#include "platform/physics.cpp"

uint32_t log_activeChannels = LOG_CHANNEL_DEBUG | LOG_CHANNEL_WARNING |
                              LOG_CHANNEL_ERROR | LOG_CHANNEL_INFO;

#define CLIENT_BASE_ADDRESS GIGABYTES(32)
#define SERVER_BASE_ADDRESS GIGABYTES(64)
#define GAME_LIB "game_lib.dll"

struct ServerState
{
  sock_Server socketLayer;
  net_Server networkLayer;;
  SimpleEventQueue eventQueue;
  uint8_t eventQueueBuffer[KILOBYTES(4)];
  GameMemory memory;
};

struct ClientState
{
  sock_Client socketLayer;
  net_Client networkLayer;
};

struct EventPlaybackState
{
  HANDLE recordingFile;
  HANDLE playbackFile;
  uint32_t recordingIndex;
  uint32_t playingIndex;
  GameMemory memory;
};

global SimpleEventQueue eventQueue;
global uint8_t eventQueueBuffer[KILOBYTES(4)];
global bool run = true;
global bool hasInputFocus = true;
global bool isListenServer = false;
global bool isClientActive = false;
global ServerState serverState;
global ClientState clientState;
global EventPlaybackState clientEventPlaybackState;
global GLFWwindow *window;

internal void StartEventRecording(EventPlaybackState *state, uint32_t recordingIndex)
{
  state->recordingIndex = recordingIndex;
  state->recordingFile = CreateFileA( "test.foo", GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0 );
  DWORD bytesWritten;
  WriteFile( state->recordingFile, state->memory.persistentStorageBase, state->memory.persistentStorageSize, &bytesWritten, 0 );
}

internal void StopEventRecording( EventPlaybackState *state )
{
  CloseHandle( state->recordingFile );
  state->recordingIndex = 0;
}

internal void RecordEvents( EventPlaybackState *state, SimpleEventQueue *eventQueue )
{
  DWORD bytesWritten;
  WriteFile( state->recordingFile, &eventQueue->size, sizeof( eventQueue->size ), &bytesWritten, 0 );
  WriteFile( state->recordingFile, eventQueue->buffer, eventQueue->size, &bytesWritten, 0 );
}

internal void StartEventPlayback( EventPlaybackState *state, uint32_t playingIndex )
{
  state->playingIndex = playingIndex;
  state->playbackFile = CreateFileA( "test.foo", GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0 );
  DWORD bytesRead;
  ReadFile( state->playbackFile, state->memory.persistentStorageBase, state->memory.persistentStorageSize, &bytesRead, 0 );
}

internal void StopEventPlayback( EventPlaybackState *state )
{
  CloseHandle( state->playbackFile );
  state->playingIndex = 0;
}

internal void PlaybackEvents( EventPlaybackState *state, SimpleEventQueue *eventQueue )
{
  DWORD bytesRead;
  ReadFile( state->playbackFile, &eventQueue->size, sizeof( eventQueue->size ), &bytesRead, 0 );
  if (bytesRead == sizeof( eventQueue->size ) )
  {
    ASSERT( eventQueue->size < eventQueue->capacity );
    ReadFile( state->playbackFile, eventQueue->buffer, eventQueue->size, &bytesRead, 0 );
  }
  else
  {
    uint32_t playingIndex = state->playingIndex;
    StopEventPlayback( state );
    StartEventPlayback( state, playingIndex );
  }
}

internal double net_GetCurrentTime()
{
  return glfwGetTime();
}

internal FILETIME GetFileLastWriteTime( const char *path )
{
  FILETIME lastWriteTime = {};
  WIN32_FILE_ATTRIBUTE_DATA data;
  if ( GetFileAttributesEx( path, GetFileExInfoStandard, &data ) )
  {
    lastWriteTime = data.ftLastWriteTime;
  }
  return lastWriteTime;
}

inline uint32_t SafeTruncateUint64ToUint32( uint64_t value )
{
  ASSERT( value <= 0xFFFFFFFF );
  uint32_t result = (uint32_t)value;
  return result;
}

internal void *AllocateMemory( size_t numBytes, size_t baseAddress = 0 )
{
  void *result = VirtualAlloc( (void*)baseAddress, numBytes, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE );
  ASSERT( result );
  return result;
}

internal void FreeMemory( void *p )
{
  VirtualFree( p, 0, MEM_RELEASE );
}

internal DEBUG_FREE_FILE_MEMORY( DebugFreeFileMemory )
{
  FreeMemory( memory );
}

internal DEBUG_READ_ENTIRE_FILE( DebugReadEntireFile )
{
  ReadFileResult result = {};
  HANDLE file = CreateFileA( path, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0 );
  if ( file != INVALID_HANDLE_VALUE )
  {
    LARGE_INTEGER tempSize;
    if ( GetFileSizeEx( file, &tempSize ) )
    {
      result.size = SafeTruncateUint64ToUint32( tempSize.QuadPart );
      result.memory = AllocateMemory( result.size );
      if ( result.memory )
      {
        DWORD bytesRead;
        if ( !ReadFile( file, result.memory, result.size, &bytesRead, 0 ) || ( result.size != bytesRead ) )
        {
          LOG_ERROR( "Failed to read file %s", path );
          DebugFreeFileMemory( result.memory );
          result.memory = nullptr;
          result.size = 0;
        }
      }
      else
      {
        LOG_ERROR( "Failed to allocate %d bytes for file %s", result.size, path );
      }
    }
    else
    {
      LOG_ERROR( "Failed to read file size for file %s", path );
    }
    CloseHandle( file );
  }
  else
  {
    LOG_ERROR( "Failed to open file %s", path );
  }
  return result;
}

internal DEBUG_SHOW_CURSOR( DebugShowCursor )
{
  if ( isCursorVisible )
  {
    glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
  }
  else
  {
    glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );
  }
}

#define KEY_HELPER( NAME ) case GLFW_KEY_##NAME: return K_##NAME;
internal uint8_t ConvertKey( int key )
{
  if ( key >= GLFW_KEY_SPACE && key <= GLFW_KEY_GRAVE_ACCENT )
  {
    return key;
  }
  switch ( key )
  {
    KEY_HELPER( BACKSPACE );
    KEY_HELPER( TAB );
    KEY_HELPER( INSERT );
    KEY_HELPER( HOME );
    KEY_HELPER( PAGE_UP );
    // Can't use KEY_HELPER( DELETE ) as windows has a #define for DELETE
    case GLFW_KEY_DELETE: return K_DELETE;
    KEY_HELPER( END );
    KEY_HELPER( PAGE_DOWN );
    KEY_HELPER( ENTER );

    KEY_HELPER( LEFT_SHIFT );
    case GLFW_KEY_LEFT_CONTROL: return K_LEFT_CTRL;
    KEY_HELPER( LEFT_ALT );
    KEY_HELPER( RIGHT_SHIFT );
    case GLFW_KEY_RIGHT_CONTROL: return K_RIGHT_CTRL;
    KEY_HELPER( RIGHT_ALT );

    KEY_HELPER( LEFT );
    KEY_HELPER( RIGHT );
    KEY_HELPER( UP );
    KEY_HELPER( DOWN );

    KEY_HELPER( ESCAPE );

    KEY_HELPER( F1 );
    KEY_HELPER( F2 );
    KEY_HELPER( F3 );
    KEY_HELPER( F4 );
    KEY_HELPER( F5 );
    KEY_HELPER( F6 );
    KEY_HELPER( F7 );
    KEY_HELPER( F8 );
    KEY_HELPER( F9 );
    KEY_HELPER( F10 );
    KEY_HELPER( F11 );
    KEY_HELPER( F12 );
    case GLFW_KEY_KP_0: return K_NUM0;
    case GLFW_KEY_KP_1: return K_NUM1;
    case GLFW_KEY_KP_2: return K_NUM2;
    case GLFW_KEY_KP_3: return K_NUM3;
    case GLFW_KEY_KP_4: return K_NUM4;
    case GLFW_KEY_KP_5: return K_NUM5;
    case GLFW_KEY_KP_6: return K_NUM6;
    case GLFW_KEY_KP_7: return K_NUM7;
    case GLFW_KEY_KP_8: return K_NUM8;
    case GLFW_KEY_KP_9: return K_NUM9;
    case GLFW_KEY_KP_DECIMAL: return K_NUM_DECIMAL;
    case GLFW_KEY_KP_DIVIDE: return K_NUM_DIVIDE;
    case GLFW_KEY_KP_MULTIPLY: return K_NUM_MULTIPLY;
    case GLFW_KEY_KP_SUBTRACT: return K_NUM_MINUS;
    case GLFW_KEY_KP_ADD: return K_NUM_PLUS;
    case GLFW_KEY_KP_ENTER: return K_NUM_ENTER;
  }
  return K_UNKNOWN;
}

internal void KeyCallback( GLFWwindow *window, int key, int scancode,
                           int action, int mods )
{
  if ( action == GLFW_PRESS )
  {
    KeyPressEvent event = {};
    event.key = ConvertKey( key );
    evt_Push( &eventQueue, &event, KeyPressEvent );
    if ( key == GLFW_KEY_F2 )
    {
      if ( clientEventPlaybackState.playingIndex == 0 )
      {
        if ( clientEventPlaybackState.recordingIndex == 0 )
        {
          StartEventRecording( &clientEventPlaybackState, 1 );
        }
        else
        {
          StopEventRecording( &clientEventPlaybackState );
          StartEventPlayback( &clientEventPlaybackState, 1 );
        }
      }
      else
      {
        StopEventPlayback( &clientEventPlaybackState );
      }
    }
  }
  else if ( action == GLFW_RELEASE )
  {
    KeyReleaseEvent event = {};
    event.key = ConvertKey( key );
    evt_Push( &eventQueue, &event, KeyReleaseEvent );
  }

  UNUSED(window);
  UNUSED(scancode);
  UNUSED(mods);
}

internal uint8_t ConvertMouseButton( int button )
{
  switch ( button )
  {
    case GLFW_MOUSE_BUTTON_LEFT: return K_MOUSE_BUTTON_LEFT;
    case GLFW_MOUSE_BUTTON_MIDDLE: return K_MOUSE_BUTTON_MIDDLE;
    case GLFW_MOUSE_BUTTON_RIGHT: return K_MOUSE_BUTTON_RIGHT;
  }
  return K_UNKNOWN;
}
internal void MouseButtonCallback( GLFWwindow *window, int button, int action,
                                   int mods )
{
  if ( action == GLFW_PRESS )
  {
    KeyPressEvent event = {};
    event.key = ConvertMouseButton( button );
    evt_Push( &eventQueue, &event, KeyPressEvent );
  }
  else if ( action == GLFW_RELEASE )
  {
    KeyReleaseEvent event = {};
    event.key = ConvertMouseButton( button );
    evt_Push( &eventQueue, &event, KeyReleaseEvent );
  }

  UNUSED(window);
  UNUSED(mods);
}


struct GameCode
{
  HMODULE handle;

  GameRenderFunction *render;
  cl_GameUpdateFunction *clientUpdate;
  sv_GameUpdateFunction *serverUpdate;
  GameStartUpdateFunction *updateStart;

  bool isValid;
  FILETIME lastWriteTime;
};

internal GameCode LoadGameCode( const char *libraryName )
{
  GameCode result = {};
  result.lastWriteTime = GetFileLastWriteTime( libraryName );
  result.handle = LoadLibraryA( libraryName );
  if ( result.handle )
  {
    result.clientUpdate =
      (cl_GameUpdateFunction *)GetProcAddress( result.handle, "cl_GameUpdate" );
    result.render = (GameRenderFunction*)GetProcAddress( result.handle, "GameRender" );
    result.serverUpdate = ( sv_GameUpdateFunction* )GetProcAddress( result.handle, "sv_GameUpdate" );
    result.updateStart = ( GameStartUpdateFunction* )GetProcAddress( result.handle, "GameStartUpdate" );

    result.isValid = result.clientUpdate && result.render && result.serverUpdate && result.updateStart;
  }
  else
  {
    LOG_ERROR( "Failed to load game code from %s", libraryName );
  }

  return result;
}

internal void UnloadGameCode( GameCode *gameCode )
{
  if ( gameCode->handle )
  {
    FreeLibrary( gameCode->handle );
    gameCode->handle = 0;
  }
  gameCode->isValid = false;
  gameCode->clientUpdate = nullptr;
  gameCode->render = nullptr;
  gameCode->serverUpdate = nullptr;
}

internal GameMemory
  AllocateGameMemory( size_t baseAddress, size_t totalSize, uint32_t persistentStorageSize )
{
  GameMemory gameMemory = {};
  gameMemory.debugReadEntireFile = DebugReadEntireFile;
  gameMemory.debugFreeFileMemory = DebugFreeFileMemory;
  gameMemory.debugGetCurrentTime = net_GetCurrentTime;
  gameMemory.debugShowCursor = DebugShowCursor;

  gameMemory.persistentStorageSize = persistentStorageSize;
  gameMemory.persistentStorageBase = AllocateMemory( totalSize, baseAddress );
  if ( gameMemory.persistentStorageBase == NULL )
  {
    ZeroStruct( gameMemory );
    return gameMemory;
  }
  gameMemory.transientStorageSize =
    totalSize - gameMemory.persistentStorageSize;
  gameMemory.transientStorageBase =
    (uint8_t *)gameMemory.persistentStorageBase +
    gameMemory.persistentStorageSize;

  return gameMemory;
}

internal bool StartServer(ServerState *server)
{
  if ( isListenServer == false )
  {
    // Initialize socket layer
    if ( !sock_InitializeServer( &server->socketLayer ) )
    {
      LOG_ERROR( "Failed to initialize server socket layer" );
      return false;
    }

    // Initialize network layer
    void *networkLayerMemory = AllocateMemory( KILOBYTES( 256 ) );
    if ( networkLayerMemory == NULL )
    {
      sock_DeinitializeServer( &server->socketLayer );

      LOG_ERROR( "Failed to allocate memory for net_server" );
      return false;
    }
    net_ServerInit( &server->networkLayer, (uint8_t *)networkLayerMemory,
                    KILOBYTES( 256 ) );

    // Setup game memory
    server->memory = AllocateGameMemory( SERVER_BASE_ADDRESS, MEGABYTES(200), MEGABYTES(100) );
    if ( server->memory.persistentStorageBase == NULL )
    {
      FreeMemory( networkLayerMemory );
      sock_DeinitializeServer( &server->socketLayer );

      LOG_ERROR( "Failed to allocate server memory." );
      return false;
    }

    // Setup event queue
    server->eventQueue = evt_CreateQueue(
      server->eventQueueBuffer, ARRAY_COUNT( server->eventQueueBuffer ) );
    isListenServer = true;
  }
  return true;
}

internal void StopServer(ServerState *server)
{
  if (isListenServer)
  {
    FreeMemory( server->memory.persistentStorageBase );
    FreeMemory( server->networkLayer.arena.base );
    sock_DeinitializeServer( &server->socketLayer );
    isListenServer = false;
  }
}

internal void StopClient( ClientState *client )
{
  if (isClientActive)
  {
    FreeMemory( client->networkLayer.arena.base );
    sock_DeinitializeClient( &client->socketLayer );
    isClientActive = false;
  }
}

internal bool ConnectToServer( ClientState *client, const char *address,
                              const char *port )
{
  if (isClientActive)
  {
    StopClient( client );
  }

  // Initialize socket layer
  if ( !sock_InitializeClient( &client->socketLayer, address, port ) )
  {
    LOG_ERROR( "Failed to initialize client socket layer" );
    return false;
  }
  // Initialize network layer
  void *networkLayerMemory = AllocateMemory( KILOBYTES( 16 ) );
  if ( networkLayerMemory == NULL )
  {
    sock_DeinitializeClient( &client->socketLayer );

    LOG_ERROR( "Failed to allocate memory for net_server" );
    return false;
  }
  net_ClientInit( &client->networkLayer, (uint8_t *)networkLayerMemory,
      KILOBYTES( 16 ) );
  isClientActive = true;

  return true;
}

int main( int argc, char **argv )
{
  if ( !glfwInit() )
  {
    LOG_ERROR( "Failed to initialize GLFW." );
    return -1;
  }
  //glfwWindowHint( GLFW_DOUBLEBUFFER, 1 );
  // NOTE: Only enable this in debug builds.
  glfwWindowHint( GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE );
  glfwWindowHint( GLFW_RESIZABLE, false );
  glfwWindowHint( GLFW_DECORATED, true );

  int monitorCount;
  auto monitors = glfwGetMonitors( &monitorCount );
  GLFWmonitor *monitor;
  if ( monitorCount == 1 )
  {
    monitor = monitors[ 0 ];
  }
  else
  {
    monitor = monitors[ 1 ];
  }
  auto videoMode = glfwGetVideoMode( monitor );
  glfwWindowHint(GLFW_RED_BITS, videoMode->redBits);
  glfwWindowHint(GLFW_GREEN_BITS, videoMode->greenBits);
  glfwWindowHint(GLFW_BLUE_BITS, videoMode->blueBits);
  glfwWindowHint(GLFW_REFRESH_RATE, videoMode->refreshRate);

  window = glfwCreateWindow( videoMode->width, videoMode->height,
                                         "coop_prototype", NULL, NULL );
  if ( !window )
  {
    glfwTerminate();
    LOG_ERROR( "Failed to create GLFW window." );
    return -1;
  }
  glfwMakeContextCurrent( window );
  glfwSetKeyCallback( window, KeyCallback );
  glfwSetMouseButtonCallback( window, MouseButtonCallback );
  glfwSwapInterval( 1 );

  GameCode game = LoadGameCode( GAME_LIB );
  if ( !game.isValid )
  {
    LOG_ERROR( "Failed to load game code." );
    glfwTerminate();
    return -1;
  }

  WSADATA wsaData;
  int wsaResult = WSAStartup( MAKEWORD( 2, 2 ), &wsaData );
  if ( wsaResult != 0 )
  {
    LOG_ERROR( "WSAStartup failed: %d", wsaResult );
    glfwTerminate();
    return -1;
  }

  CommandSystem commandSystem = {};
  cmd_Init( &commandSystem );
  cmd_Register( &commandSystem, "exit", ExitCommand );
  cmd_Register( &commandSystem, "host", HostCommand );
  cmd_Register( &commandSystem, "killserver", KillServerCommand );
  cmd_Register( &commandSystem, "connect", ConnectCommand );
  cmd_Register( &commandSystem, "killclient", KillClientCommand );
  cmd_Register( &commandSystem, "setping", NetLatencyCommand );

  clientEventPlaybackState.memory = AllocateGameMemory( CLIENT_BASE_ADDRESS, GIGABYTES(1), MEGABYTES(200) );
  GameMemory *clientMemory = &clientEventPlaybackState.memory;
  if ( clientMemory->persistentStorageBase == NULL )
  {
    LOG_ERROR( "Failed to allocate game memory." );
    glfwTerminate();
    WSACleanup();
    return -1;
  }

  double t = 0.0;
  float logicHz = 60.0f;
  float logicTimestep = 1.0f / logicHz;
  double maxFrameTime = 0.25; // Used to prevent the simulation from being
                              // affected by break points.

  double currentTime = glfwGetTime();
  double accumulator = 0.0;
  double updateTime = 0.0;
  double updateStart = 0.0;

  GamePhysics clientPhysics = InitializePhysicsSystem();
  GamePhysics serverPhysics = InitializePhysicsSystem();

  EventPeakResult peakResult;
  eventQueue =
    evt_CreateQueue( eventQueueBuffer, ARRAY_COUNT( eventQueueBuffer ) );

  while ( run )
  {
    double newTime = glfwGetTime();
    double frameTime = newTime - currentTime;
    if ( frameTime > maxFrameTime )
      frameTime = maxFrameTime;
    currentTime = newTime;

    accumulator += frameTime;

    while ( accumulator >= logicTimestep )
    {
      updateStart = glfwGetTime();
      FILETIME newWriteTime = GetFileLastWriteTime( GAME_LIB );
      if (CompareFileTime(&newWriteTime, &game.lastWriteTime) != 0)
      {
        UnloadGameCode( &game );
        auto newGameCode = LoadGameCode( GAME_LIB );
        if ( newGameCode.isValid )
        {
          game = newGameCode;
          clientMemory->wasCodeReloaded = true;
        }
      }
      glfwPollEvents();
      if ( glfwWindowShouldClose( window ) )
      {
        run = false;
      }
      if ( glfwGetWindowAttrib( window, GLFW_FOCUSED ) )
      {
        if ( hasInputFocus )
        {
          //glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );
          double x, y;
          glfwGetCursorPos( window, &x, &y );
          MouseMotionEvent event = {};
          event.x = (int)x;
          event.y = (int)y;
          evt_Push( &eventQueue, &event, MouseMotionEvent );
        }
        else
        {
          glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
        }
      }
      else
      {
        glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
      }
      game.updateStart(logicTimestep);

      PlatformStatsEvent platformStatsEvent = {};
      platformStatsEvent.frameTime = (float)frameTime;
      platformStatsEvent.isListenServer = isListenServer;
      platformStatsEvent.updateTime = (float)updateTime;
      evt_Push( &eventQueue, &platformStatsEvent, PlatformStatsEvent );

      auto commandSystemBuffer =
        commandSystem.buffers + commandSystem.bufferIdx;
      for ( uint32_t i = 0; i < commandSystemBuffer->eventCount; ++i )
      {
        auto event = commandSystemBuffer->events + i;

        evt_Push( &eventQueue, event, ExecuteCommandEvent );
      }
      // Switch buffers and clear the new buffer
      commandSystem.bufferIdx = ( commandSystem.bufferIdx + 1 ) % 2;
      commandSystem.buffers[commandSystem.bufferIdx].eventCount = 0;
      commandSystem.buffers[commandSystem.bufferIdx].workingBufferSize = 0;

      if ( isListenServer )
      {
        Packet incomingPacket;
        sock_ServerProcessIncomingPackets( &serverState.socketLayer,
                                           &serverState.networkLayer );

        net_Event netEvent;
        while ( net_ServerPollEvent( &serverState.networkLayer, &netEvent ) )
        {
          switch ( netEvent.type )
          {
            case NET_CLIENT_CONNECTED_EVENT:
            {
              ClientConnectedEvent clientConnectedEvent;
              clientConnectedEvent.clientId = netEvent.clientConnected.clientId;
              evt_Push( &serverState.eventQueue, &clientConnectedEvent,
                        ClientConnectedEvent );
              break;
            }
            default:
              break;
          }
        }
        net_ServerClearEventQueue( &serverState.networkLayer );

        for ( uint32_t i = 0; i < serverState.networkLayer.clientsPool.size;
              ++i )
        {
          auto client = serverState.networkLayer.clients + i;
          while ( net_ServerReceivePackets( &serverState.networkLayer,
                                            client->id, &incomingPacket, 1 ) )
          {
            ReceivePacketEvent event = {};
            event.packet = incomingPacket;
            evt_Push( &serverState.eventQueue, &event, ReceivePacketEvent );
          }
          // Must generate packets after receiving them otherwise the return
          // timestamp will be one frame behind
          PacketGeneratedEvent event = {};
          event.packet = net_ServerGenerateOutgoingPacket(
            &serverState.networkLayer, client->id );
          evt_Push( &serverState.eventQueue, &event, PacketGeneratedEvent );
        }

        game.serverUpdate( logicTimestep, &serverState.memory,
                           &serverState.eventQueue, &commandSystem, &serverPhysics );

        peakResult = evt_Peak( &serverState.eventQueue, NULL );
        while ( peakResult.header )
        {
          if ( peakResult.header->typeId == ReceivePacketEventTypeId )
          {
            ReceivePacketEvent *event = (ReceivePacketEvent *)peakResult.data;
            net_ServerFreeReceivedPackets( &serverState.networkLayer,
                                           &event->packet, 1 );
          }
          else if ( peakResult.header->typeId == PacketGeneratedEventTypeId )
          {
            PacketGeneratedEvent *event = (PacketGeneratedEvent *)peakResult.data;
            sock_ServerProcessOutgoingPacket( &serverState.socketLayer,
                                              &serverState.networkLayer,
                                              &event->packet );
          }
          // TODO: Handle commands
          peakResult = evt_Peak( &serverState.eventQueue, peakResult.header );
        }
        evt_ClearQueue( &serverState.eventQueue );
      }

      if ( isClientActive )
      {
        Packet incomingPacket;

        // Fetch received packets from socket and add them to our own queue
        sock_ClientProcessIncomingPackets( &clientState.socketLayer,
                                           &clientState.networkLayer );

        // Grab each packet out of our queue if the delivery time has passed and
        // add it to the client event queue to be processed by the game logic.
        while ( net_ClientReceivePacket( &clientState.networkLayer,
                                         &incomingPacket, 1 ) )
        {
          ReceivePacketEvent packetEvent = {};
          packetEvent.packet = incomingPacket;
          evt_Push( &eventQueue, &packetEvent, ReceivePacketEvent );
        }

        // Add network latency measurement to client event queue
        NetworkStatsEvent netStatsEvent = {};
        netStatsEvent.delay =
          net_ClientGetLastRoundTripTime( &clientState.networkLayer );
        netStatsEvent.receiveQueueLength =
          clientState.networkLayer.receiveQueueLength;
        netStatsEvent.outgoingQueueLength =
          clientState.networkLayer.outgoingQueueLength;
        netStatsEvent.noPacketTime = net_GetCurrentTime() - clientState.networkLayer.lastPacketReceiveTime;
        evt_Push( &eventQueue, &netStatsEvent, NetworkStatsEvent );

        // Add generated packet to client event queue
        PacketGeneratedEvent packetGeneratedEvent = {};
        packetGeneratedEvent.packet =
          net_ClientGeneratePacket( &clientState.networkLayer );
        evt_Push( &eventQueue, &packetGeneratedEvent, PacketGeneratedEvent );
      }

      if ( clientEventPlaybackState.recordingIndex )
      {
        RecordEvents( &clientEventPlaybackState, &eventQueue );
      }

      if ( clientEventPlaybackState.playingIndex )
      {
        PlaybackEvents( &clientEventPlaybackState, &eventQueue );
      }

      game.clientUpdate( logicTimestep, clientMemory, &eventQueue, &commandSystem, &clientPhysics, videoMode->width, videoMode->height );
      peakResult = evt_Peak( &eventQueue, NULL );
      while ( peakResult.header )
      {
        if ( peakResult.header->typeId == ReceivePacketEventTypeId )
        {
          if ( isClientActive ) // Client may be deactivated while going through this event queue
          {
            // Free any received packets that were passed to the game logic
            ReceivePacketEvent *event = ( ReceivePacketEvent * )peakResult.data;
            net_ClientFreeReceivedPackets( &clientState.networkLayer,
              &event->packet, 1 );
          }
        }
        else if ( peakResult.header->typeId == PacketGeneratedEventTypeId )
        {
          if ( isClientActive ) // Client may be deactivated while going through this event queue
          {
            // Queue any outgoing packets
            PacketGeneratedEvent *event = ( PacketGeneratedEvent * )peakResult.data;
            net_ClientQueueOutgoingPacket( &clientState.networkLayer,
              event->packet );
          }
        }
        if ( peakResult.header->typeId == ExecuteCommandEventTypeId )
        {
          ExecuteCommandEvent *event = (ExecuteCommandEvent *)peakResult.data;
          switch ( event->command )
          {
            case ExitCommand:
            {
              run = false;
              break;
            }
            case HostCommand:
            {
              if ( StartServer( &serverState ) )
              {
                LOG_INFO( "Started listen server" );
              }
              break;
            }
            case KillServerCommand:
            {
              StopServer( &serverState );
              LOG_INFO( "Shutdown listen server" );
              break;
            }
            case ConnectCommand:
              if ( event->numArgs == 1 )
              {
                if ( !ConnectToServer( &clientState, event->args[ 0 ], "18000" ) )
                {
                  LOG_ERROR( "Failed to connect to server" );
                }
              }
              else
              {
                LOG_ERROR( "Expected address" );
              }
              break;
            case KillClientCommand:
              StopClient( &clientState );
              break;
            case NetLatencyCommand:
              if ( event->numArgs == 1 )
              {
                int ping = atoi(event->args[0]);
                if ( ping >= 0 )
                {
                  ping = MinVal(ping, 1000);
                  clientState.networkLayer.fakeLatency = ((float)ping ) / 1000.0f;
                }
                else
                {
                  LOG_ERROR( "Ping value cannot be negative" );
                }
              }
              else
              {
                LOG_ERROR( "Expected value" );
              }
              break;
            default:
              break;
          }
        }
        peakResult = evt_Peak( &eventQueue, peakResult.header );
      }
      evt_ClearQueue( &eventQueue );
      if ( isListenServer )
      {
        evt_ClearQueue( &serverState.eventQueue );
      }

      if ( isClientActive )
      {
        sock_ClientProcessOutgoingPackets( &clientState.socketLayer,
                                           &clientState.networkLayer );
      }

      t += logicTimestep;
      accumulator -= logicTimestep;

      updateTime = glfwGetTime() - updateStart;
    }

    game.render( frameTime, clientMemory, &clientPhysics, &serverPhysics, videoMode->width, videoMode->height );
    glfwSwapBuffers( window );
  }
  DestroyPhysicsSystem( clientPhysics.physicsSystem );
  DestroyPhysicsSystem( serverPhysics.physicsSystem );

  if (isListenServer)
  {
    StopServer( &serverState );
  }
  if (isClientActive)
  {
    StopClient( &clientState );
  }

  glfwTerminate();
  WSACleanup();
  return 0;

  UNUSED( argc );
  UNUSED( argv );
}
