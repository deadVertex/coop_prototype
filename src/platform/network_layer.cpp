#include "network_layer.h"

#include "common/byte_buffer.h"

internal uint32_t net_GetCurrentTimestamp()
{
  uint32_t result = (uint32_t)(net_GetCurrentTime() * 1000.0);
  return result;
}

internal void net_ServerFreeReceivedPackets( net_Server *server,
                                             Packet *packets, uint32_t count )
{
  for ( uint32_t i = 0; i < count; ++i )
  {
    MemoryPoolFree( &server->incomingPacketPool, packets[i].data );
  }
}

internal void net_ClientFreeReceivedPackets( net_Client *client, Packet *packets,
                                             uint32_t count )
{
  for ( uint32_t i = 0; i < count; ++i )
  {
    MemoryPoolFree( &client->incomingPacketPool, packets[i].data );
  }
}

internal void net_ClientInit( net_Client *client, uint8_t *memory, uint32_t len )
{
  ZeroStruct( *client );
  MemoryArenaInitialize( &client->arena, len, memory );
  client->incomingPacketPool =
    CreateMemoryPool( &client->arena, NET_SERVER_PACKET_SIZE,
                      NET_CLIENT_INCOMING_PACKET_POOL_SIZE );
  client->outgoingPacketPool =
    CreateMemoryPool( &client->arena, NET_CLIENT_PACKET_SIZE,
                      NET_CLIENT_OUTGOING_PACKET_POOL_SIZE );
  CircularBufferInit( &client->roundTripTimes, &client->arena, uint32_t,
                      NET_CLIENT_RTT_SAMPLES_COUNT );
  CircularBufferInitArray( &client->eventQueue, &client->eventQueueArray );
  client->lastPacketReceiveTime = net_GetCurrentTime();
}

internal bool net_ClientQueueIncomingPacket( net_Client *client,
                                             Packet inPacket )
{
  int32_t tolerance = 640; // 10 seconds for 64 tick server
  if ( ( inPacket.sequenceNumber >= client->expectedSequenceNumber ) &&
      ( inPacket.sequenceNumber - client->expectedSequenceNumber <
        tolerance ) )
  {
    if ( client->receiveQueueLength < NET_CLIENT_INCOMING_PACKET_QUEUE_LENGTH )
    {
      if ( inPacket.len <= client->incomingPacketPool.objectSize )
      {
        Packet packet = inPacket;
        packet.data = MemoryPoolAllocate( &client->incomingPacketPool );
        memcpy( packet.data, inPacket.data, packet.len );

        net_ReceiveQueueEntry *entry =
          client->receiveQueue + client->receiveQueueLength++;
        entry->packet = packet;
        entry->deliveryTime =
          net_GetCurrentTime() + ( client->fakeLatency * 0.5f );

        client->expectedSequenceNumber = inPacket.sequenceNumber + 1;
        return true;
      }
      else
      {
        LOG_WARNING( "Client received packet that it too big, dropping packet." );
      }
    }
    else
    {
      LOG_WARNING( "Client receive queue is full, dropping packet" );
    }
  }
  else
  {
    LOG_WARNING( "Out-of-order packet received by client, dropping packet." );
  }
  return false;
}

// NOTE: This sorts in descending order
internal int net_CompareReceiveQueueEntries( const void *p0, const void *p1 )
{
  net_ReceiveQueueEntry *a = (net_ReceiveQueueEntry*)p0;
  net_ReceiveQueueEntry *b = (net_ReceiveQueueEntry*)p1;

  if ( a->deliveryTime > b->deliveryTime )
  {
    return -1;
  }
  else if ( a->deliveryTime < b->deliveryTime )
  {
    return 1;
  }
  return 0;
}

internal uint32_t net_ClientReceivePacket( net_Client *client, Packet *packets,
                                       uint32_t size )
{
  uint32_t count = 0;
  qsort( client->receiveQueue, client->receiveQueueLength,
         sizeof( net_ReceiveQueueEntry ), net_CompareReceiveQueueEntries );

  for ( int i = client->receiveQueueLength - 1; i >= 0; --i )
  {
    auto entry = client->receiveQueue + i;
    LOG_DEBUG( "deliveryTime = %g currentTime = %g", entry->deliveryTime, net_GetCurrentTime() );
    if ( entry->deliveryTime <= net_GetCurrentTime() )
    {
      if ( count < size )
      {
        packets[count++] = entry->packet;
        if ( entry->packet.timestamp > client->returnTimestamp )
        {
          client->returnTimestamp = entry->packet.timestamp;
        }

        uint32_t roundTripTime =
          net_GetCurrentTimestamp() - entry->packet.returnTimestamp;
        if ( client->roundTripTimes.length == client->roundTripTimes.capacity )
        {
          CircularBufferPop( &client->roundTripTimes );
        }
        CircularBufferPush( &client->roundTripTimes, roundTripTime );
        client->lastPacketReceiveTime = net_GetCurrentTime();
      }
    }
  }
  client->receiveQueueLength -= count;
  return count;
}

internal Packet net_ClientGeneratePacket( net_Client *client )
{
  Packet result = {};
  result.data = MemoryPoolAllocate( &client->outgoingPacketPool );
  result.len = 0;
  result.capacity = client->outgoingPacketPool.objectSize;
  result.clientId = NET_SERVER_CLIENT_ID;
  result.timestamp = 0;
  result.returnTimestamp = client->returnTimestamp;
  result.sequenceNumber = client->nextSequenceNumber++;
  return result;
}


internal void net_ClientQueueOutgoingPacket( net_Client *client, Packet packet )
{
  if ( packet.len > 0 )
  {
    if ( client->outgoingQueueLength < NET_CLIENT_OUTGOING_PACKET_QUEUE_LENGTH )
    {
      auto entry = client->outgoingQueue + client->outgoingQueueLength++;
      LOG_DEBUG( "fakeLatency = %d", client->fakeLatency );
      entry->deliveryTime =
        net_GetCurrentTime() + ( client->fakeLatency * 0.5 );
      entry->packet = packet;
    }
    else
    {
      LOG_WARNING( "Client outgoing queue if full, discarding packet" );
      MemoryPoolFree( &client->outgoingPacketPool, packet.data );
    }
  }
  else
  {
    MemoryPoolFree( &client->outgoingPacketPool, packet.data );
  }
}

internal uint32_t net_ClientGetLastRoundTripTime( net_Client *client )
{
  uint32_t result = 0;
  CircularBufferBack( &client->roundTripTimes, &result );
  return result;
}

internal uint32_t net_ClientGetRoundTripTimes( net_Client *client,
                                               uint32_t *roundTripTimes,
                                               uint32_t size )
{
  return CircularBufferReadArray( &client->roundTripTimes, uint32_t,
                                  roundTripTimes, size );
}

internal bool net_ServerAddClient( net_Server *server, uint32_t clientId )
{
  auto client = (net_ServerClientData *)AllocateObject( &server->clientsPool );
  if ( client )
  {
    client->id = clientId;
    CircularBufferInitArray( &client->inFlightPackets,
                             client->inFlightPacketsArray );

    net_Event event = {};
    event.type = NET_CLIENT_CONNECTED_EVENT;
    event.clientConnected.clientId = client->id;
    if ( !CircularBufferPush( &server->eventQueue, event ) )
    {
      ASSERT( !"Event queue is full" );
    }
    return true;
  }
  return false;
}

internal net_ServerClientData *net_ServerGetClient( net_Server *server,
                                                    uint32_t clientId )
{
  for ( uint32_t i = 0; i < server->clientsPool.size; ++i )
  {
    auto client = server->clients + i;
    if ( client->id == clientId )
    {
      return client;
    }
  }
  return NULL;
}

// NOTE: Packet is only valid while memory given to this function is
internal Packet
  net_DeserializePacket( uint8_t *data, uint32_t len, uint32_t clientId )
{
  Packet packet = {};
  ByteBuffer byteBuffer = {};
  InitializeForReading( &byteBuffer, data, len );
  if ( ReadLong( &packet.timestamp, &byteBuffer ) )
  {
    if ( ReadLong( &packet.returnTimestamp, &byteBuffer ) )
    {
      if ( ReadShort( &packet.sequenceNumber, &byteBuffer ) )
      {
        packet.len = len - byteBuffer.readCursor;
        packet.data = data + byteBuffer.readCursor;
        packet.clientId = clientId;
        return packet;
      }
      else
      {
        LOG_WARNING( "Failed to deserialize packet sequence number" );
      }
    }
    else
    {
      LOG_WARNING( "Failed to deserialize packet returnTimestamp" );
    }
  }
  else
  {
    LOG_WARNING( "Failed to deserialize packet timestamp" );
  }
  return packet;
}

internal uint32_t
  net_SerializePacket( uint8_t *buffer, uint32_t len, Packet packet )
{
  ByteBuffer byteBuffer;
  InitializeByteBuffer( &byteBuffer, buffer, len );
  auto ret = WriteLong( packet.timestamp, &byteBuffer );
  ASSERT( ret );
  ret = WriteLong( packet.returnTimestamp, &byteBuffer );
  ASSERT( ret );
  ret = WriteShort( packet.sequenceNumber, &byteBuffer );
  ASSERT( ret );
  ret = WriteData( packet.data, packet.len, &byteBuffer );
  ASSERT( ret );
  return byteBuffer.size;
}

internal void net_ServerInit( net_Server *server, uint8_t *memory, uint32_t len )
{
  ZeroStruct( *server );
  MemoryArenaInitialize( &server->arena, len, memory );
  server->incomingPacketPool =
    CreateMemoryPool( &server->arena, NET_CLIENT_PACKET_SIZE,
                      NET_SERVER_INCOMING_PACKET_POOL_SIZE );
  server->outgoingPacketPool =
    CreateMemoryPool( &server->arena, NET_SERVER_PACKET_SIZE,
                      NET_SERVER_OUTGOING_PACKET_POOL_SIZE );
  server->clientsPool = CreateContiguousObjectPoolArray( server->clients );
  CircularBufferInitArray( &server->eventQueue, server->eventQueueArray );
}

inline void net_ServerClientAckPacket( net_Server *server,
                                       net_ServerClientData *client,
                                       uint16_t sequenceNumber )
{
  for ( uint32_t j = 0; j < client->inFlightPackets.length; ++j )
  {
    auto inFlightPacket =
      CircularBufferGet( &client->inFlightPackets, net_InFlightPacket, j );
    if ( inFlightPacket->sequenceNumber == sequenceNumber )
    {
      if ( inFlightPacket->state == NET_PACKET_STATE_UNKNOWN )
      {
        net_Event event = {};
        event.type = NET_PACKET_ACKNOWLEDGED_EVENT;
        event.packetAcknowledged.clientId = client->id;
        event.packetAcknowledged.sequenceNumber = sequenceNumber;
        if ( !CircularBufferPush( &server->eventQueue, event ) )
        {
          ASSERT( !"Event queue is full" );
        }
      }
      inFlightPacket->state = NET_PACKET_STATE_ACKED;
      break;
    }
  }
}

// Returns false if packet was dropped
internal bool net_ServerQueueIncomingPacket( net_Server *server,
                                             uint32_t clientId,
                                             Packet inPacket )
{
  int32_t tolerance = 640; // 10 seconds for 64 tick server
  auto client = net_ServerGetClient( server, clientId );
  if ( client )
  {
    if ( ( inPacket.sequenceNumber >= client->expectedSequenceNumber ) &&
         ( inPacket.sequenceNumber - client->expectedSequenceNumber <
           tolerance ) )
    {
      // Effective length of receive queue is actually
      // NET_SERVER_RECEIVE_QUEUE_LENGTH - 1
      if ( ( ( client->receiveQueueTail + 1 ) %
             NET_SERVER_RECEIVE_QUEUE_LENGTH ) != client->receiveQueueHead )
      {
        if ( inPacket.len <= server->incomingPacketPool.objectSize )
        {
          Packet packet = inPacket;
          packet.data = MemoryPoolAllocate( &server->incomingPacketPool );
          memcpy( packet.data, inPacket.data, packet.len );
          client->receiveQueue[client->receiveQueueTail] = packet;
          client->receiveQueueTail = ( client->receiveQueueTail + 1 ) %
                                     NET_SERVER_RECEIVE_QUEUE_LENGTH;
          client->expectedSequenceNumber = inPacket.sequenceNumber + 1;
          client->receiveQueueLength++;
          for ( uint32_t i = NET_MAX_ACKS_PER_PACKET - 1; i != 0; --i )
          {
            client->acksToSend[i] = client->acksToSend[i-1];
          }
          client->acksToSend[0] = packet.sequenceNumber;
          for ( uint32_t i = 0; i < NET_MAX_ACKS_PER_PACKET; ++i )
          {
            net_ServerClientAckPacket( server, client, packet.acks[i] );
          }
          return true;
        }
        else
        {
          LOG_WARNING( "Server received packet from client %d that is too big, "
                       "discarding packet",
                       clientId );
        }
      }
      else
      {
        LOG_WARNING( "Receive queue for client %d is full, discarding packet",
                     clientId );
      }
    }
    else
    {
      LOG_WARNING( "Received %d expected %d", inPacket.sequenceNumber,
                   client->expectedSequenceNumber );
      LOG_WARNING( "Out-of-order packet received by server from client %d, "
                   "dropping packet.",
                   clientId );
      // TODO: Record number of diagnostics
    }
  }
  else
  {
    // incoming packet is dropped
    LOG_WARNING( "Unknown client id %d", clientId );
  }
  return false;
}

// NOTE: Needs to be called after receive to contain latest return timestamp
internal Packet
  net_ServerGenerateOutgoingPacket( net_Server *server, uint32_t clientId )
{
  Packet result = {};
  auto client = net_ServerGetClient( server, clientId );
  if ( client )
  {
    result.data = MemoryPoolAllocate( &server->outgoingPacketPool );
    result.len = 0;
    result.capacity = server->outgoingPacketPool.objectSize;
    result.clientId = clientId;
    result.timestamp = 0;
    result.returnTimestamp = client->returnTimestamp;
    result.sequenceNumber = client->nextSequenceNumber++;
    for ( int i = 0; i < NET_MAX_ACKS_PER_PACKET; ++i )
    {
      result.acks[i] = client->acksToSend[i];
    }
    net_InFlightPacket inFlightPacket = {};
    inFlightPacket.sequenceNumber = result.sequenceNumber;
    inFlightPacket.timestamp = net_GetCurrentTimestamp();
    CircularBufferPush(&client->inFlightPackets, inFlightPacket );
  }
  else
  {
    LOG_ERROR( "Unknown client id %d", clientId );
  }
  return result;
}

internal uint32_t net_ServerReceivePackets( net_Server *server,
                                            uint32_t clientId, Packet *packets,
                                            uint32_t size )
{
  uint32_t count = 0;
  auto client = net_ServerGetClient( server, clientId );
  if ( client )
  {
    for ( uint32_t i = 0; i < size; ++i )
    {
      if ( client->receiveQueueHead != client->receiveQueueTail )
      {
        packets[i] = client->receiveQueue[client->receiveQueueHead];
        client->receiveQueueHead = ( client->receiveQueueHead + 1 ) %
                                   NET_SERVER_RECEIVE_QUEUE_LENGTH;
        client->receiveQueueLength--;
        count++;
        if ( packets[i].timestamp > client->returnTimestamp )
        {
          client->returnTimestamp = packets[i].timestamp;
        }
      }
      else
      {
        break;
      }
    }
  }
  return count;
}

internal uint32_t
  net_ServerDropUnacknowledgedPackets( net_Server *server, uint32_t threshold )
{
  uint32_t count = 0;
  uint32_t currentTimestamp = net_GetCurrentTimestamp();
  for ( uint32_t clientIdx = 0; clientIdx < server->clientsPool.size;
        ++clientIdx )
  {
    auto client = server->clients + clientIdx;
    for ( uint32_t packetIdx = 0; packetIdx < client->inFlightPackets.length;
          ++packetIdx )
    {
      auto inFlightPacket = CircularBufferGet( &client->inFlightPackets,
                                               net_InFlightPacket, packetIdx );

      if ( inFlightPacket->state == NET_PACKET_STATE_UNKNOWN )
      {
        uint32_t t = currentTimestamp - inFlightPacket->timestamp;
        if ( t > threshold )
        {
          inFlightPacket->state = NET_PACKET_STATE_DROPPED;
          // Add dropped packet event to event queue
          net_Event event = {};
          event.type = NET_PACKET_DROPPED_EVENT;
          event.packetDropped.clientId = client->id;
          event.packetDropped.sequenceNumber = inFlightPacket->sequenceNumber;
          if ( !CircularBufferPush( &server->eventQueue, event ) )
          {
            ASSERT( !"Dropped packets queue is full!" );
          }
          count++;
        }
      }
    }
  }
  return count;
}

internal bool net_ServerPollEvent( net_Server *server, net_Event *event )
{
  ASSERT( event );
  if ( server->eventQueue.length > 0 )
  {
    *event = CircularBufferTop( &server->eventQueue, net_Event );
    CircularBufferPop( &server->eventQueue );
    return true;
  }
  return false;
}

internal void net_ServerClearEventQueue( net_Server *server )
{
  CircularBufferClear( &server->eventQueue );
}

#if 0
internal uint32_t
  net_ClientDropUnacknowledgedPackets( net_Client *client, uint32_t threshold )
{
  uint32_t count = 0;
  uint32_t currentTimestamp = net_GetCurrentTimestamp();
    for ( uint32_t packetIdx = 0; packetIdx < client->inFlightPackets.length;
          ++packetIdx )
    {
      auto inFlightPacket = CircularBufferGet( &client->inFlightPackets,
                                               net_InFlightPacket, packetIdx );

      if ( !inFlightPacket->isAcked )
      {
        uint32_t t = currentTimestamp - inFlightPacket->timestamp;
        if ( t > threshold )
        {
          // Add dropped packet event to event queue
          net_Event event = {};
          event.type = NET_PACKET_DROPPED_EVENT;
          event.packetDropped.clientId = NET_SERVER_CLIENT_ID;
          event.packetDropped.sequenceNumber = inFlightPacket->sequenceNumber;
          if ( !CircularBufferPush( &server->eventQueue, event ) )
          {
            ASSERT( !"Dropped packets queue is full!" );
          }
          count++;
        }
      }
    }
  return count;
}
#endif

internal bool net_ClientPollEvent( net_Client *client, net_Event *event )
{
  ASSERT( event );
  if ( client->eventQueue.length > 0 )
  {
    *event = CircularBufferTop( &client->eventQueue, net_Event );
    CircularBufferPop( &client->eventQueue );
    return true;
  }
  return false;
}

internal void net_ClientClearEventQueue( net_Client *client)
{
  CircularBufferClear( &client->eventQueue );
}
